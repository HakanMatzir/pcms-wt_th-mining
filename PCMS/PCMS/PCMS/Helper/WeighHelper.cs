﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class WeighHelper
    {
        private ONLINE.Applikation scale = new ONLINE.Applikation();

        public static int selectedScale = 1;
        public static Areas.Weigh.Models.ScaleRead getWeight(int scaleNo)
        {
            ONLINE.Applikation scale = new ONLINE.Applikation();
            string value = "";
            Areas.Weigh.Models.ScaleRead scaleRead = null;
            if (scale != null)
            {
                int error = 0;
                try {
                    error = scale.SendCommand(scaleNo, "RM", out value);
                }
                catch (Exception e)
                {
                    error = -1;
                }
                if(error == 0)
                {
                    if(value == "12")
                    {
                        scaleRead = new Areas.Weigh.Models.ScaleRead();
                        scaleRead.errorCode = 12;
                    }
                    else{
                        scaleRead = new Areas.Weigh.Models.ScaleRead(value);
                    }                   
                }
                else
                {
                    scaleRead = new Areas.Weigh.Models.ScaleRead();
                    scaleRead.errorCode = error;
                }                
            }
            return scaleRead;
        }
        public static Areas.Weigh.Models.ScaleRead getWeightN(int scaleNo)
        {
            ONLINE.Applikation scale = new ONLINE.Applikation();
            string value = "";
            int error = scale.SendCommand(scaleNo, "RN", out value);
            Areas.Weigh.Models.ScaleRead scaleRead = new Areas.Weigh.Models.ScaleRead();
            //int error = scale.Send("RM", out value);
            if (error == 0)
            {               
                if (value == "Fehler:12")
                {
                    scaleRead = new Areas.Weigh.Models.ScaleRead();
                    scaleRead.errorCode = 12;
                }
                else {
                    scaleRead = new Areas.Weigh.Models.ScaleRead(value);
                }
            }
            else
            {
                scaleRead.errorCode = error;
            }            
            return scaleRead;
        }
    }
}