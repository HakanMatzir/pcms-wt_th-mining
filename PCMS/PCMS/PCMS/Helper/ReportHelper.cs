﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PCMS.Helper
{
    static class ReportHelper
    {

        public static string GetDisplayName<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            return ModelMetadata.FromLambdaExpression<TModel, TProperty>(
                expression,
                new ViewDataDictionary<TModel>(model)
                ).DisplayName;
        }

        public static bool GetIsRequired<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            return ModelMetadata.FromLambdaExpression<TModel, TProperty>(
                expression,
                new ViewDataDictionary<TModel>(model)
                ).IsRequired;
        }

        public static bool GetIsReport<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            try {
                object Attrib = ModelMetadata.FromLambdaExpression<TModel, TProperty>(
                    expression,
                    new ViewDataDictionary<TModel>(model)
                    ).AdditionalValues["report"];

                return Convert.ToBoolean(Attrib);
            }
            catch
            { 
            }
            return false;
        }

        public static List<decimal> eliminateNull(List<decimal?> decNull)
        {
            List<decimal> dec = new List<decimal>();

            decimal lastVal = 0;
            foreach (decimal? a in decNull)
            {
                if (a != null)
                {
                    dec.Add((decimal)a);
                    lastVal = (decimal)a;
                }
                else
                {
                    dec.Add(lastVal);
                }
            }

            return dec;
        }


    }
}
