﻿using PCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class MenuHelper
    {
        public static string getAllFacilities(string facilityId)
        {
            if (String.IsNullOrEmpty(facilityId))
            {
                return "<option value=\"-1\" >Bitte Anmelden</option>";
            }

            MenuContext dbMenu = new MenuContext();

            //var user = dbMenu.AspNetUsers.Where(u => u.UserName == userId).First();
            long facilityIdLong = long.Parse(facilityId);
            string options = "";
            if (facilityId != null)
            {
                options += "<option value=\"-1\" >" + dbMenu.Facilities.Where(r => r.Id == facilityIdLong).First().Description + "</option>";
                options += "<option disabled >──────────</option>";
            }
            else
            {
                options += "<option value=\"-1\" >Bitte wählen</option>";
                options += "<option disabled >──────────</option>";
            }
            
            //foreach(facilities fa in user.Facilities)
            //{
            //    options += "<option value=\""+ fa.Id +"\">" + fa.Description + "</option>";
            //}

            
            
            return options;
        }
        public static bool isAuthenticated(string userId,string[] roles)
        {

            Areas.Admin.Models.AdminContext db = new Areas.Admin.Models.AdminContext();
            var model = db.AspNetUsers.Find(userId);
            if (userId == null)
            {
                return false;
            }
            foreach(var i in model.UsersRoles)
            {
                if(roles.Contains(i.AspNetRoles.Name.ToLower()))
                {
                    return true;
                }
            }
                
            return false;
        }
    }
   
}