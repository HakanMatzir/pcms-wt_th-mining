﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.LIMS.Models;
using PagedList;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.LIMS.Controllers
{
    public class lims_morat_TestController : Controller
    {
        private LimsContext db = new LimsContext();
        private Order.Models.OrderContext db2 = new Order.Models.OrderContext();

        // GET: LIMS/lims_morat_Test
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.lims_morat_Test.Where(a=>a.IsDeleted != true).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "FreshProbDate":
                            if (!String.IsNullOrEmpty(top.from) && !String.IsNullOrEmpty(top.to))
                            {
                                model = model.Where(m => m.FreshProbDate.Date >= new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date && m.FreshProbDate.Date <= new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).Date).ToList();
                                ViewBag.FreshProbDateFrom = top.from;
                                ViewBag.FreshProbDateTo = top.to;
                            }
                            break;
                    }
                }


                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "MaterilaNumber":
                                model = model.Where(m => m.MaterilaNumber != null && m.MaterilaNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MaterilaNumber = pgFF.colVal;
                                break;
                            case "StrengthClass":
                                model = model.Where(m => m.StrengthClass != null && m.StrengthClass.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.StrengthClass = pgFF.colVal;
                                break;
                            case "ProbPlace":
                                model = model.Where(m => m.ProbPlace != null && m.ProbPlace.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ProbPlace = pgFF.colVal;
                                break;
                            case "Customer":
                                model = model.Where(m => m.Customer != null && m.Customer.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Customer = pgFF.colVal;
                                break;
                            case "ConstruktionSite":
                                model = model.Where(m => m.ConstruktionSite != null && m.ConstruktionSite.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ConstruktionSite = pgFF.colVal;
                                break;
                            case "OrderNumber":
                                model = model.Where(m => m.OrderNumber != null && m.OrderNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.OrderNumber = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Identity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Identity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Identity).ToList();
                    break;
                case "MaterilaNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterilaNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterilaNumber).ToList();
                    break;
                case "StrengthClass":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StrengthClass).ToList();
                    else
                        model = model.OrderByDescending(m => m.StrengthClass).ToList();
                    break;
                case "ProbPlace":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ProbPlace).ToList();
                    else
                        model = model.OrderByDescending(m => m.ProbPlace).ToList();
                    break;
                case "Customer":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Customer).ToList();
                    else
                        model = model.OrderByDescending(m => m.Customer).ToList();
                    break;
                case "ConstruktionSite":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConstruktionSite).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConstruktionSite).ToList();
                    break;
                case "DeliveryDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DeliveryDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.DeliveryDate).ToList();
                    break;
                case "OrderNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderNumber).ToList();
                    break;
                case "Sieve0_125":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve0_125).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve0_125).ToList();
                    break;
                case "Sieve0_25":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve0_25).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve0_25).ToList();
                    break;
                case "Sieve0_5":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve0_5).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve0_5).ToList();
                    break;
                case "Sieve0_71":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve0_71).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve0_71).ToList();
                    break;
                case "Sieve1_0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve1_0).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve1_0).ToList();
                    break;
                case "Sieve1_25":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve1_25).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve1_25).ToList();
                    break;
                case "Sieve1_6":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve1_6).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve1_6).ToList();
                    break;
                case "Sieve2_0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve2_0).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve2_0).ToList();
                    break;
                case "Sieve4_0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve4_0).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve4_0).ToList();
                    break;
                case "Sieve8_0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Sieve8_0).ToList();
                    else
                        model = model.OrderByDescending(m => m.Sieve8_0).ToList();
                    break;
                case "FreshProbDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshProbDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshProbDate).ToList();
                    break;
                case "FreshWatherContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshWatherContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshWatherContent).ToList();
                    break;
                case "FreshSlump":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshSlump).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshSlump).ToList();
                    break;
                case "FreshSlumpFlow":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshSlumpFlow).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshSlumpFlow).ToList();
                    break;
                case "FreshMoratDensity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshMoratDensity).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshMoratDensity).ToList();
                    break;
                case "FreshAirContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshAirContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshAirContent).ToList();
                    break;
                case "FreshCup250":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshCup250).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshCup250).ToList();
                    break;
                case "FreshComment":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FreshComment).ToList();
                    else
                        model = model.OrderByDescending(m => m.FreshComment).ToList();
                    break;
                case "FixedPropDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedPropDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedPropDate).ToList();
                    break;
                case "FixedMoratDensity0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedMoratDensity0).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedMoratDensity0).ToList();
                    break;
                case "FixedMoratDensity1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedMoratDensity1).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedMoratDensity1).ToList();
                    break;
                case "FixedMoratDensity2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedMoratDensity2).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedMoratDensity2).ToList();
                    break;
                case "FixedMoratDensityM":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedMoratDensityM).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedMoratDensityM).ToList();
                    break;
                case "FixedMoratDensity24h105C":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedMoratDensity24h105C).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedMoratDensity24h105C).ToList();
                    break;
                case "FixedFlexuralStrength0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedFlexuralStrength0).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedFlexuralStrength0).ToList();
                    break;
                case "FixedFlexuralStrength1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedFlexuralStrength1).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedFlexuralStrength1).ToList();
                    break;
                case "FixedFlexuralStrength2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedFlexuralStrength2).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedFlexuralStrength2).ToList();
                    break;
                case "FixedFlexuralStrengthM":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedFlexuralStrengthM).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedFlexuralStrengthM).ToList();
                    break;
                case "FixedCompressiveStrength0":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedCompressiveStrength0).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedCompressiveStrength0).ToList();
                    break;
                case "FixedCompressiveStrength1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedCompressiveStrength1).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedCompressiveStrength1).ToList();
                    break;
                case "FixedCompressiveStrength2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedCompressiveStrength2).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedCompressiveStrength2).ToList();
                    break;
                case "FixedCompressiveStrengthM":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FixedCompressiveStrengthM).ToList();
                    else
                        model = model.OrderByDescending(m => m.FixedCompressiveStrengthM).ToList();
                    break;
                case "TesterName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TesterName).ToList();
                    else
                        model = model.OrderByDescending(m => m.TesterName).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: LIMS/lims_morat_Test/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_morat_Test lims_morat_Test = db.lims_morat_Test.Find(id);
            if (lims_morat_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_morat_Test);
        }

        // GET: LIMS/lims_morat_Test/Create
        public ActionResult Create()
        {
            ViewBag.ProbPlace = new SelectList(db.Lims_morat_SelectMenu.Where(a=>a.MenuId == 1 && a.IsDeleted == false), "Description", "Description");
            ViewBag.TesterName = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description");
            ViewBag.TesterName2 = ViewBag.TesterName;
            ViewBag.TesterName3 = ViewBag.TesterName;
            return View();
        }

        // POST: LIMS/lims_morat_Test/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Identity,MaterilaNumber,StrengthClass,ProbPlace,Customer,ConstruktionSite,DeliveryDate,OrderNumber,Sieve0_125,Sieve0_25,Sieve0_5,Sieve0_71,Sieve1_0,Sieve1_25,Sieve1_6,Sieve2_0,Sieve4_0,Sieve8_0,FreshProbDate,FreshWatherContent,FreshSlump,FreshSlumpFlow,FreshMoratDensity,FreshAirContent,FreshCup250,FreshComment,FixedPropDate,FixedMoratDensity0,FixedMoratDensity1,FixedMoratDensity2,FixedMoratDensityM,FixedMoratDensity24h105C,FixedFlexuralStrength0,FixedFlexuralStrength1,FixedFlexuralStrength2,FixedFlexuralStrengthM,FixedCompressiveStrength0,FixedCompressiveStrength1,FixedCompressiveStrength2,FixedCompressiveStrengthM,TesterName,TesterName2,TesterName3")] lims_morat_Test lims_morat_Test)
        {
            if (ModelState.IsValid)
            {
                lims_morat_Test.IsActive = true;
                lims_morat_Test.IsDeleted = false;
                db.lims_morat_Test.Add(lims_morat_Test);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            ViewBag.ProbPlace = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 1   && a.IsDeleted == false), "Description", "Description",lims_morat_Test.ProbPlace);
            ViewBag.TesterName = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2  && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName);
            ViewBag.TesterName2 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName2);
            ViewBag.TesterName3 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName3);
            return View(lims_morat_Test);
        }

        // GET: LIMS/lims_morat_Test/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_morat_Test lims_morat_Test = db.lims_morat_Test.Find(id);
            if (lims_morat_Test == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProbPlace = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 1   && a.IsDeleted == false), "Description", "Description", lims_morat_Test.ProbPlace);
            ViewBag.TesterName = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2  && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName);
            ViewBag.TesterName2 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName2);
            ViewBag.TesterName3 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName3);
            return View(lims_morat_Test);
        }

        // POST: LIMS/lims_morat_Test/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Identity,MaterilaNumber,StrengthClass,ProbPlace,Customer,ConstruktionSite,DeliveryDate,OrderNumber,Sieve0_125,Sieve0_25,Sieve0_5,Sieve0_71,Sieve1_0,Sieve1_25,Sieve1_6,Sieve2_0,Sieve4_0,Sieve8_0,FreshProbDate,FreshWatherContent,FreshSlump,FreshSlumpFlow,FreshMoratDensity,FreshAirContent,FreshCup250,FreshComment,FixedPropDate,FixedMoratDensity0,FixedMoratDensity1,FixedMoratDensity2,FixedMoratDensityM,FixedMoratDensity24h105C,FixedFlexuralStrength0,FixedFlexuralStrength1,FixedFlexuralStrength2,FixedFlexuralStrengthM,FixedCompressiveStrength0,FixedCompressiveStrength1,FixedCompressiveStrength2,FixedCompressiveStrengthM,TesterName,TesterName2,TesterName3")] lims_morat_Test lims_morat_Test)
        {
            if (ModelState.IsValid)
            {
                lims_morat_Test.IsActive = true;
                lims_morat_Test.IsDeleted = false;
                db.Entry(lims_morat_Test).State = EntityState.Modified;
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            ViewBag.ProbPlace = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 1   && a.IsDeleted == false ), "Description", "Description", lims_morat_Test.ProbPlace);
            ViewBag.TesterName = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2  && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName);
            ViewBag.TesterName2 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName2);
            ViewBag.TesterName3 = new SelectList(db.Lims_morat_SelectMenu.Where(a => a.MenuId == 2 && a.IsDeleted == false), "Description", "Description", lims_morat_Test.TesterName3);
            return View(lims_morat_Test);
        }

        // GET: LIMS/lims_morat_Test/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_morat_Test lims_morat_Test = db.lims_morat_Test.Find(id);
            if (lims_morat_Test == null)
            {
                return HttpNotFound();
            }
            return View(lims_morat_Test);
        }

        // POST: LIMS/lims_morat_Test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {

            lims_morat_Test lims_morat_Test = db.lims_morat_Test.Find(id);
            lims_morat_Test.IsActive = true;
            lims_morat_Test.IsDeleted = true;
            db.Entry(lims_morat_Test).State = EntityState.Modified;
            //db.SaveChanges();
            db.SaveChanges(User.Identity.Name);
            //db.lims_morat_Test.Remove(lims_morat_Test);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult createTag(string tagName, int menuId)
        {
            lims_morat_SelectMenu menus = db.Lims_morat_SelectMenu.Where(a => a.Description == tagName && a.MenuId == menuId).FirstOrDefault();
            if (menus != null)
            {
                menus.IsDeleted = false;
                db.Entry(menus).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return Json(new { success = true, value = menus.Description });
            }
            lims_morat_SelectMenu menu = new lims_morat_SelectMenu()
            {
                MenuId = menuId,
                Description = tagName,
                IsDeleted = false,
                IsActive = true
            };
            db.Lims_morat_SelectMenu.Add(menu);
            db.SaveChanges(User.Identity.Name);
            return Json(new { success = true, value = menu.Description });

        }
        
            public ActionResult DeleteTag(string tagName, int menuId)
        {
            lims_morat_SelectMenu menus = db.Lims_morat_SelectMenu.Where(a => a.Description == tagName && a.MenuId == menuId && a.IsDeleted == false).FirstOrDefault();
            if (menus != null)
            {
                menus.IsDeleted = true;                
                db.Entry(menus).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return Json(new { success = true, value = menus.Description });
            }

            return Json(new { success = false, value = tagName });

        }
        public ActionResult ReportDetail(long? Id)
        {
            try
            {
                var model = db.lims_morat_Test.Find(Id);
                //List<string> Sieves = Definitions.allSieves.ToList();
                //
                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();
                //
                //Dictionary<string, decimal> durchgang = model.getKumulativList();
                //var fields = durchgang.Values.ToList();
                //
                if(model.Sieve0_125!=null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("0,125", model.Sieve0_125 ?? 0, 0, 0, 0);
                if (model.Sieve0_25 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("0,25", model.Sieve0_25 ?? 0, 0, 0, 0);
                if (model.Sieve0_5 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("0,5", model.Sieve0_5 ?? 0, 0, 0, 0);
                if (model.Sieve0_71 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("0,71", model.Sieve0_71 ?? 0, 0, 0, 0);
                if (model.Sieve1_0 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("1,0,", model.Sieve1_0 ?? 0, 0, 0, 0);
                if (model.Sieve1_25 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("1,25", model.Sieve1_25 ?? 0, 0, 0, 0);
                if (model.Sieve1_6 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("1,6", model.Sieve1_6 ?? 0, 0, 0, 0);
                if (model.Sieve2_0 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("2,0", model.Sieve2_0 ?? 0, 0, 0, 0);
                if (model.Sieve4_0 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("4,0", model.Sieve4_0 ?? 0, 0, 0, 0);
                if (model.Sieve8_0 != null)
                    ds.RuleGradingCurve.AddRuleGradingCurveRow("8,0", model.Sieve8_0 ?? 0, 0, 0, 0);


                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.RuleGradingCurve.ToList());

                List<ReportParameter> repParams = new List<ReportParameter>();

                repParams.Add(new ReportParameter("MaterialNumber", (model.MaterilaNumber ?? "--").ToString()));
                repParams.Add(new ReportParameter("StrengthClass", (model.StrengthClass ?? "--").ToString()));
                repParams.Add(new ReportParameter("ProbPlace", (model.ProbPlace ?? "--").ToString()));
                repParams.Add(new ReportParameter("Customer", (model.Customer ?? "--").ToString()));
                repParams.Add(new ReportParameter("ConstruktionSite", (model.ConstruktionSite ?? "--").ToString()));
                repParams.Add(new ReportParameter("DeliveryDate", (model.DeliveryDate).ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("OrderNumber", (model.OrderNumber ?? "--").ToString()));

                repParams.Add(new ReportParameter("FreshProbDate", (model.FreshProbDate).ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("FreshWatherContent", (model.FreshWatherContent ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshSlump", (model.FreshSlump ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshSlumpFlow", (model.FreshSlumpFlow ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshMoratDensity", (model.FreshMoratDensity ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshAirContent", (model.FreshAirContent ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshCup250", (model.FreshCup250 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FreshComment", (model.FreshComment ?? "--").ToString()));

                repParams.Add(new ReportParameter("FixedPropDate", model.FixedPropDate.ToString("dd.MM.yyyy")));
                repParams.Add(new ReportParameter("FixedMoratDensity0", (model.FixedMoratDensity0 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedMoratDensity1", (model.FixedMoratDensity1 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedMoratDensity2", (model.FixedMoratDensity2 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedMoratDensityM", (model.FixedMoratDensityM ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedFlexuralStrength0", (model.FixedFlexuralStrength0 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedFlexuralStrength1", (model.FixedFlexuralStrength1 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedFlexuralStrength2", (model.FixedFlexuralStrength2 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedFlexuralStrengthM", (model.FixedFlexuralStrengthM ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedCompressiveStrength0", (model.FixedCompressiveStrength0 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedCompressiveStrength1", (model.FixedCompressiveStrength1 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedCompressiveStrength2", (model.FixedCompressiveStrength2 ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedCompressiveStrengthM", (model.FixedCompressiveStrengthM ?? 0).ToString()));
                repParams.Add(new ReportParameter("FixedMoratDensity24h105C", (model.FixedMoratDensity24h105C ?? 0).ToString()));
                repParams.Add(new ReportParameter("TesterName", (model.TesterName ?? String.Empty ).ToString()));
                repParams.Add(new ReportParameter("TesterName2", (model.TesterName2 ?? String.Empty).ToString()));
                repParams.Add(new ReportParameter("TesterName3", (model.TesterName3 ?? String.Empty).ToString()));

                repParams.Add(new ReportParameter("S0_125", model.Sieve0_125.ToString()));
                repParams.Add(new ReportParameter("S0_25", model.Sieve0_25.ToString()));
                repParams.Add(new ReportParameter("S0_5", model.Sieve0_5.ToString()));
                repParams.Add(new ReportParameter("S0_71", model.Sieve0_71.ToString()));
                repParams.Add(new ReportParameter("S1_0", model.Sieve1_0.ToString()));
                repParams.Add(new ReportParameter("S1_25", model.Sieve1_25.ToString()));
                repParams.Add(new ReportParameter("S1_6", model.Sieve1_6.ToString()));
                repParams.Add(new ReportParameter("S2_0", model.Sieve2_0.ToString()));
                repParams.Add(new ReportParameter("S4_0", model.Sieve4_0.ToString()));
                repParams.Add(new ReportParameter("S8_0", model.Sieve8_0.ToString()));


                repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
      

                string SievesList = "";
                //for (int i = 0; i < Sieves.Count; i++)
                //{
                //    SievesList += Sieves[i].ToString() + ";";
                //}
                //for (int i = 0; i < (17 - Sieves.Count); i++)
                //{
                //    SievesList += "-;";
                //}

                //repParams.Add(new ReportParameter("SievesList", SievesList));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/LIMS/MoratTestDetail.rdlc";
                //reportViewer.ShowPrintButton = false;
                //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
                //reportViewer.LocalReport.DataSources.Add(DSReportA);
                //reportViewer.LocalReport.DataSources.Add(DSReportB);
                //reportViewer.LocalReport.DataSources.Add(DSReportC);
                //reportViewer.LocalReport.DataSources.Add(DSReportU);

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
        public ActionResult choosLoadingOrder(int? selected, string dateFrom, string dateTo)
        {


            //selected = selected ?? 4;
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            //var modelLoading = db2.Md_order_LoadingOrder.Where(a => a.RegistrationDate.Value.Day == day && a.RegistrationDate.Value.Month == month && a.RegistrationDate.Value.Year == year && a.State == selected).OrderBy(a => a.Sort).ToList();

            var modelLoading = db2.Md_order_LoadingOrder.OrderBy(a => a.RegistrationDate).ToList();
            if (selected != null && selected != 0)
                modelLoading = modelLoading.Where(a => a.State == selected).ToList();
            if (String.IsNullOrEmpty(dateFrom) && String.IsNullOrEmpty(dateTo))
            {
                dateFrom = DateTime.Now.ToString("yyyy-MM-dd");
                dateTo = DateTime.Now.ToString("yyyy-MM-dd");
            }

                if (!String.IsNullOrEmpty(dateFrom) && !String.IsNullOrEmpty(dateTo))
            {
                modelLoading = modelLoading.Where(m => m.RegistrationDate != null && m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(dateFrom.Split('-')[0]), Convert.ToInt32(dateFrom.Split('-')[1]), Convert.ToInt32(dateFrom.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(dateTo.Split('-')[0]), Convert.ToInt32(dateTo.Split('-')[1]), Convert.ToInt32(dateTo.Split('-')[2])).Date).ToList();

            }


            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            List<SelectListItem> listItems = new List<SelectListItem>();
            listItems.Add(new SelectListItem { Text = "Alle", Value = "0" });
            listItems.Add(new SelectListItem { Text = "Gesperrt", Value = "1" });
            listItems.Add(new SelectListItem { Text = "Freigegeben", Value = "2", Selected = true });
            listItems.Add(new SelectListItem { Text = "in Bearbeitung", Value = "3" });
            listItems.Add(new SelectListItem { Text = "beendet", Value = "4" });
            listItems.Add(new SelectListItem { Text = "storniert", Value = "5" });

            ViewBag.SelectState = new SelectList(listItems, "Value", "Text", selected);
            ViewBag.DateFrom = dateFrom;
            ViewBag.DateTo = dateTo;
            return View((modelLoading));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
