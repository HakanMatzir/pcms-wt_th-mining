﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace PCMS.Areas.Admin.Models
{

    public class AdminContext : DbContext
    {
        public AdminContext()
            : base("DefaultConnection")
        {
        }

       
        public virtual DbSet<facilities> Facilities { get; set; }
        public virtual DbSet<aspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<aspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<LIMS.Models.AuditLog> Auditlog { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<aspNetUsers>()
                .HasMany(up => up.Facilities)
                .WithMany(course => course.AspNetUsers)
                .Map(mc =>
                {
                    mc.ToTable("UserFacilities");
                    mc.MapLeftKey("UserId");
                    mc.MapRightKey("FacilityId");
                }
            );
            //modelBuilder.Entity<aspNetUsers>()
            //    .HasMany(up => up.Facilities)
            //    .WithMany(course => course.AspNetUsers)
            //    .Map(mc =>
            //    {
            //        mc.ToTable("AspNetUserRoles");
            //        mc.MapLeftKey("UserId");
            //        mc.MapRightKey("RoleId");
            //    }
            //);

            base.OnModelCreating(modelBuilder);
        }
        public int SaveChanges(string userId)
        {
            try
            {
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them
                    foreach (LIMS.Models.AuditLog x in Helper.AuditLogH.GetAuditRecordsForChange(ent, userId))
                    {
                        this.Auditlog.Add(x);
                    }
                }
                int u = base.SaveChanges();
                return u;
            }
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            catch (Exception e)
            {

                //int u = base.SaveChanges();
                Helper.ExceptionHelper.LogException(e, userId);
                return 0;
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records

        }

    }
    [Table("Facilities")]
    public partial class facilities
    {
        public facilities()
        {
            AspNetUsers = new List<aspNetUsers>();
        }

        public long Id { get; set; }
        [DisplayName("Name")]
        public string Description { get; set; }
        [DisplayName("Aktiv")]
        public bool Active { get; set; }
        [DisplayName("Mischlinienvorhanden")]
        public bool UseMixingLines { get; set; }
        [DisplayName("Mischlinien")]
        public int MixingLines { get; set; }
        [DisplayName("Beton")]
        public bool Concrete { get; set; }
        [DisplayName("Mörtel")]
        public bool Mortar { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ICollection<aspNetUsers> AspNetUsers { get; set; }

    }
    [Table("AspNetUsers")]
    public partial class aspNetUsers
    {
        public aspNetUsers()
        {
            Facilities = new List<facilities>();
            UsersRoles = new List<aspNetUserRoles>();
        }

        public string Id { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Beuntzername")]
        public string UserName { get; set; }
        [DisplayName("Werksauswahl")]
        public long? Selectedfacility { get; set; }
        public byte[] Sign { get; set; }

        [ForeignKey("Selectedfacility")]
        public virtual facilities Facility { get; set; }

        public virtual ICollection<facilities> Facilities { get; set; }
        [ScriptIgnore]
        public virtual ICollection<aspNetUserRoles> UsersRoles { get; set; }
    }
    [Table("AspNetRoles")]
    public partial class aspNetRoles
    {
        public string Id { get; set; }
        public string Name { get; set; }
        [ScriptIgnore]
        public virtual ICollection<aspNetUserRoles> UsersRoles { get; set; }
    }
    [Table("AspNetUserRoles")]
    public partial class aspNetUserRoles
    {
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        [Key, Column(Order = 1)]
        public string RoleId { get; set; }
        [ForeignKey("UserId")]
        public virtual aspNetUsers AspNetUsers { get; set; }
        [ForeignKey("RoleId")]
        public virtual aspNetRoles AspNetRoles { get; set; }
    }
    public class AdminModel
    {
    }
}