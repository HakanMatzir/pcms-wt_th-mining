﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace PCMS.Areas.Order.Models
{
    public class OrderContext : DbContext
    {
        public OrderContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<OrderContext>(new CreateDatabaseIfNotExists<OrderContext>());
        }

        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Address> Md_order_Address { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Contact> Md_order_Contact { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Material> Md_order_Material { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Customer> Md_order_Customer { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_ConstructionSite> Md_order_ConstructionSite { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_recipe_Recipe> Md_order_Recipe { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_VehicleType> Md_order_VehicleType { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Driver> Md_order_Driver { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_material_RecipeMaterial> Md_order_RecipeMaterial { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_recipe_RecipeMaterial> Md_orderRecipe_RecipeMaterial { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_recipe_RecipeMaterialM> Md_recipe_RecipeMaterialM { get; set; }
        public virtual DbSet<PCMS.Areas.RecipeM.Models.md_equipment> Md_Equipment { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Trader> Md_Trader { get; set; }
        //public virtual DbSet<PCMS.Areas.RecipeM.Models.md_recipe_RecipeM> Md_RecipeRecipeM { get; set; }

        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Vehicle> md_order_Vehicle { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_VehicleGroup> md_order_VehicleGroup { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_material_BinderDetails> Md_order_Binder { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_PriceMarkUp> Md_order_PriceMarkUp { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_material_SortDetails> Md_order_Sort { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_masterData_Standard> Md_order_Norm { get; set; }
        public virtual DbSet<PCMS.Areas.MasterData.Models.md_material_ConsistencyGroup> Md_order_Consistency { get; set; }

        public virtual DbSet<md_order_LoadingOrder> Md_order_LoadingOrder { get; set; }
        public virtual DbSet<md_order_ContingentOrder> Md_order_ContingentOrder { get; set; }
        public virtual DbSet<md_order_DeliveryNoteType> Md_order_DeliveryNoteType { get; set; }

        public virtual DbSet<md_masterData_RetardationTime> Md_order_RetardationTime { get; set; }

        public virtual DbSet<md_Config> Md_Config { get; set; }
        public virtual DbSet<md_order_OrderComponents> Md_Components { get; set; }
        public virtual DbSet<PCMS.Areas.Material.Models.md_material_Location> Md_Location { get; set; }
        public virtual DbSet<PCMS.Areas.Material.Models.md_material_MaterialMoving> Md_MaterialMoving { get; set; }

        public virtual DbSet<PCMS.Areas.MasterData.Models.md_material_ConcreteFamilyGroup> ConcreteFamily { get; set; }

        public virtual DbSet<md_order_InvoiceHeader> InvoiceHeader { get; set; }
        public virtual DbSet<md_order_InvoicePositions> InvoicePositions { get; set; }

        public virtual DbSet<md_order_InvoiceHeaderTemp> InvoiceHeaderTemp { get; set; }
        public virtual DbSet<md_order_InvoicePositionsTemp> InvoicePositionsTemp { get; set; }
    }

    //[Table("md_material_ConcreteFamilyGroup")]
    //public partial class md_material_ConcreteFamilyGroup
    //{
    //    public long Id { get; set; }
    //    [DisplayName("Betonfamilie")]
    //    public string Description { get; set; }
    //    public int InitialProduction_0_Count { get; set; }
    //    public int InitialProduction_0_Val { get; set; }
    //    public int InitialProduction_1_Count { get; set; }
    //    public int InitialProduction_1_Val { get; set; }
    //    public int SteadyProduction_0_Count { get; set; }
    //    public int SteadyProduction_0_val { get; set; }
    //    public int SteadyProduction_1_Count { get; set; }
    //    public int SteadyProduction_1_Val { get; set; }
    //    [DisplayName("2 Tagen")]
    //    public decimal StrengthAfter2 { get; set; }
    //    [DisplayName("7 Tagen")]
    //    public decimal StrengthAfter7 { get; set; }
    //    [DisplayName("28 Tagen")]
    //    public decimal StrengthAfter28 { get; set; }
    //    [DisplayName("56 Tagen")]
    //    public decimal StrengthAfter56 { get; set; }
    //    [DisplayName("90 Tagen")]
    //    public decimal StrengthAfter90 { get; set; }
    //    public bool IsActive { get; set; }
    //    public bool IsDeleted { get; set; }
    //    public bool TimeTestRequired { get; set; }
    //    public bool AmountTestRequired { get; set; }
    //}

    [Table("md_order_OrderComponents")]
    public partial class md_order_OrderComponents
    {
        public long Id { get; set; }
        public long? MaterialId { get; set; }
        public long? RecipeId { get; set; }
        public decimal Weight { get; set; }
        public decimal Value { get; set; }
        public long? LoadingOrderId { get; set; }
        public long? ContingentOrderId { get; set; }
        public int? FunctionType { get; set; }
        public decimal? LotSetPoint { get; set; }
        public decimal? OrderSetPoint { get; set; }
        public string LocationNumber { get; set; }
        public long Sort { get; set; }
        public int Type { get; set; }
        [ForeignKey("MaterialId")]
        public virtual PCMS.Areas.MasterData.Models.md_masterData_Material Md_material_Material { get; set; }
        [ForeignKey("RecipeId")]
        public virtual PCMS.Areas.MasterData.Models.md_recipe_Recipe Md_recipe_Recipe { get; set; }
        [ForeignKey("LoadingOrderId")]
        public virtual md_order_LoadingOrder Md_order_LoadingOrder { get; set; }
        [ForeignKey("ContingentOrderId")]
        public virtual md_order_ContingentOrder Md_order_ContingentOrder { get; set; }
    }


    [Table("md_Config")]
    public partial class md_Config
    {
        public long Id { get; set; }
        public string LoadingOrderNumber { get; set; }
        public string ContingentOrderNumber { get; set; }
        public decimal BasicCalculation { get; set; }
        public decimal? MixerSize { get; set; }
        public string CustomerNumber { get; set; }
        public int Season { get; set; }
        public long InvoiceNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? InvoiceSelectDate { get; set; }
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        //public DateTime? InvoiceDateTill { get; set; }
    }

    [Table("md_material_RetardationTime")]
    public partial class md_masterData_RetardationTime
    {
        public long Id { get; set; }
        [DisplayName("Verzögerungszeit")]
        public decimal? RetardTime { get; set; }
        [DisplayName("Prozentwert")]
        public decimal? Value { get; set; }
        public long? MaterialId { get; set; }
        public int Season { get; set; }

        [ForeignKey("MaterialId")]
        public virtual PCMS.Areas.MasterData.Models.md_masterData_Material Material { get; set; }
    }

    [Table("md_order_DeliveryNoteType")]
    public partial class md_order_DeliveryNoteType
    {
        public long Id { get; set; }
        [DisplayName("Lieferscheintyp")]
        public string Description { get; set; }
    }

    [Table("md_order_ContingentOrder")]
    public partial class md_order_ContingentOrder
    {
        public long Id { get; set; }
        public long? MaterialId { get; set; }
        public long? RecipeId { get; set; }
        public long? DeliveryNoteTypeId { get; set; }
        [DisplayName("Kontingent Nummer")]
        public string ContingentNumber { get; set; }
        [DisplayName("Bestellte Menge")]
        public decimal? OrderedQuantity { get; set; }
        [DisplayName("Gelieferte Menge")]
        public decimal? DeliveredQuantity { get; set; }
        [DisplayName("Rückmenge")]
        public decimal? BackAmount { get; set; }
        [DisplayName("Rückmenge Lieferschein")]
        public string BackAmountDN { get; set; }
        [DisplayName("Kundennummer")]
        public string CustomerNumber { get; set; }
        [DisplayName("Kunde")]
        public string CustomerName { get; set; }
        [DisplayName("Baustellennummer")]
        public string ConstructionSiteNumber { get; set; }
        [DisplayName("Baustelle")]
        public string ConstructionSiteDescription { get; set; }
        [DisplayName("Anzahl Fahrzeuge")]
        public string VehicleAmount { get; set; }
        [DisplayName("Artikelnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Artikel")]
        public string MaterialDescription { get; set; }
        [DisplayName("Rezeptnummer")]
        public string RecipeNumber { get; set; }
        [DisplayName("Rezept")]
        public string RecipeDescription { get; set; }
        [DisplayName("Ladezeit")]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan? LoadingTime { get; set; }
        [DisplayName("Datum")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationDate { get; set; }
        public long DistanceTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Entfernungszeit")]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm\:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan? DistanceTime
        {
            get { return TimeSpan.FromTicks(DistanceTimeTick); }
            set
            {
                if (value.HasValue)
                    DistanceTimeTick = value.Value.Ticks;
                else
                    DistanceTimeTick = 0;
            }
        }

        public long DeliveryCycleTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Lieferzyklus Zeit")]
        public TimeSpan? DeliveryCycleTime
        {
            get { return TimeSpan.FromTicks(DeliveryCycleTimeTick); }
            set
            {
                if (value.HasValue)
                    DeliveryCycleTimeTick = value.Value.Ticks;
                else
                    DeliveryCycleTimeTick = 0;
            }
        }

        [DisplayName("Baustellen Zeit")]
        public TimeSpan? ConstructionSiteTime { get; set; }
        [DisplayName("Lieferzyklus Menge")]
        public decimal? DeliveryCycleAmount { get; set; }
        [DisplayName("Status")]
        public int? State { get; set; }
        public string CementNumber1 { get; set; }
        public string CementDescription1 { get; set; }
        public decimal? CementAdditionValue1 { get; set; }
        public string CementNumber2 { get; set; }
        public string CementDescription2 { get; set; }
        public decimal? CementAdditionValue2 { get; set; }
        public string CementNumber3 { get; set; }
        public string CementDescription3 { get; set; }
        public decimal? CementAdditionValue3 { get; set; }
        public string AdmixturesNumber1 { get; set; }
        public string AdmixturesDescription1 { get; set; }
        public decimal? AdmixturesAdditionValue1 { get; set; }
        public string AdmixturesNumber2 { get; set; }
        public string AdmixturesDescription2 { get; set; }
        public decimal? AdmixturesAdditionValue2 { get; set; }
        public string AdmixturesNumber3 { get; set; }
        public string AdmixturesDescription3 { get; set; }
        public decimal? AdmixturesAdditionValue3 { get; set; }
        public string AdmixturesNumber4 { get; set; }
        public string AdmixturesDescription4 { get; set; }
        public decimal? AdmixturesAdditionValue4 { get; set; }
        public string AdmixturesNumber5 { get; set; }
        public string AdmixturesDescription5 { get; set; }
        public decimal? AdmixturesAdditionValue5 { get; set; }
        public string WaterNumber1 { get; set; }
        public string WaterDescription1 { get; set; }
        public decimal? WaterAdditionValue1 { get; set; }
        public string WaterNumber2 { get; set; }
        public string WaterDescription2 { get; set; }
        public decimal? WaterAdditionValue2 { get; set; }
        public string WaterLCNumber1 { get; set; }
        public string WaterLCDescription1 { get; set; }
        [DisplayName("Wasser letzte Charge")]
        public decimal? WaterLCValue1 { get; set; }
        public string WaterLCNumber2 { get; set; }
        public string WaterLCDescription2 { get; set; }
        public decimal? WaterLCValue2 { get; set; }
        [DisplayName("Sonderleistung 1")]
        public string SpecialDescription1 { get; set; }
        [DisplayName("Wert 1")]
        public string SpecialValue1 { get; set; }
        [DisplayName("EDV Code 1")]
        public string SpecialNumber1 { get; set; }
        [DisplayName("Sonderleistung 2")]
        public string SpecialDescription2 { get; set; }
        [DisplayName("Wert 2")]
        public string SpecialValue2 { get; set; }
        [DisplayName("EDV Code 2")]
        public string SpecialNumber2 { get; set; }
        [DisplayName("Sonderleistung 3")]
        public string SpecialDescription3 { get; set; }
        [DisplayName("Wert 3")]
        public string SpecialValue3 { get; set; }
        [DisplayName("EDV Code 3")]
        public string SpecialNumber3 { get; set; }
        [DisplayName("Sonderleistung 4")]
        public string SpecialDescription4 { get; set; }
        [DisplayName("Wert 4")]
        public string SpecialValue4 { get; set; }
        [DisplayName("EDV Code 4")]
        public string SpecialNumber4 { get; set; }
        [DisplayName("Sonderleistung 5")]
        public string SpecialDescription5 { get; set; }
        [DisplayName("Wert 5")]
        public string SpecialValue5 { get; set; }
        [DisplayName("EDV Code 5")]
        public string SpecialNumber5 { get; set; }
        [DisplayName("LS-Bermerkung")]
        public string DNComment { get; set; }
        [DisplayName("Bemerkung 1")]
        public string Comment1 { get; set; }
        [DisplayName("Bemerkung 2")]
        public string Comment2 { get; set; }
        [DisplayName("Bemerkung 3")]
        public string Comment3 { get; set; }
        [DisplayName("Chargengröße")]
        public decimal? BatchSize { get; set; }
        [DisplayName("Mischzeit")]
        public int? MixTime { get; set; }
        [DisplayName("VA-Zeit")]
        public decimal? Workability { get; set; }
        [DisplayName("DIN")]
        public string NormDescription { get; set; }
        [DisplayName("Konsistenz")]
        public string ConsistencyDescription { get; set; }
        [DisplayName("Konsistenzwert")]
        public decimal? ConsistencyValue { get; set; }
        public long? LocationId { get; set; }
        public string LocationDescription { get; set; }
        //[DisplayName("Datum")]
        //public DateTime? RegistrationDate { get; set; }
        [DisplayName("Min Charge")]
        public decimal? MinLot { get; set; }
        [DisplayName("Max Charge")]
        public decimal? MaxLot { get; set; }
        public long? TraderId { get; set; }
        [DisplayName("Händler")]
        public string TraderName { get; set; }
        [DisplayName("Händlernummer")]
        public string TraderNumber { get; set; }

        [ForeignKey("DeliveryNoteTypeId")]
        public virtual md_order_DeliveryNoteType DeliveryNoteType { get; set; }
    }

    [Table("md_order_LoadingOrder")]
    public partial class md_order_LoadingOrder
    {
        public long Id { get; set; }
        public long? DeliveryNoteTypeId { get; set; }
        public long? MaterialId { get; set; }
        public long? RecipeId { get; set; }
        [DisplayName("Entleerprogramm")]
        public int? EmptyingProgram { get; set; }
        [DisplayName("Abrufnummer")]
        public int? Sort { get; set; }
        [DisplayName("Kontingent Nummer")]
        public string ContingentOrderNumber { get; set; }
        [DisplayName("Auftragsnummer")]
        public string OrderNumber { get; set; }
        [DisplayName("Bestellte Menge")]
        public decimal? OrderedQuantity { get; set; }
        [DisplayName("Gelieferte Menge")]
        public decimal? DeliveredQuantity { get; set; }
        [DisplayName("Rückmenge")]
        public decimal? BackAmount { get; set; }
        [DisplayName("Rückmenge Lieferschein")]
        public string BackAmountDN { get; set; }
        [DisplayName("Kundennummer")]
        public string CustomerNumber { get; set; }
        [DisplayName("Kunde")]
        public string CustomerName { get; set; }
        [DisplayName("Baustellennummer")]
        public string ConstructionSiteNumber { get; set; }
        [DisplayName("Baustelle")]
        public string ConstructionSiteDescription { get; set; }
        [DisplayName("Fahrzeugnummer")]
        public string VehicleNumber { get; set; }
        [DisplayName("Kennzeichen")]
        public string VehiclePlateNo { get; set; }
        [DisplayName("Fahrzeugart")]
        public string VehicleType { get; set; }
        [DisplayName("Fahrernummer")]
        public string DriverNumber { get; set; }
        [DisplayName("Fahrer")]
        public string DriverName { get; set; }
        [DisplayName("Artikelnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Artikel")]
        public string MaterialDescription { get; set; }
        [DisplayName("Rezeptnummer")]
        [Required(ErrorMessage = "Rezept muss ausgeählt werden")]
        public string RecipeNumber { get; set; }
        [DisplayName("Rezept")]
        public string RecipeDescription { get; set; }
        [DisplayName("Rezeptbemerkung")]
        public string RecipeComment { get; set; }
        [DisplayName("Warm Wasser")]
        public bool? HotWater { get; set; }
        [DisplayName("FM-Zugabe")]
        public string FlowSubstanceAdmit { get; set; }
        [DisplayName("Ladezeit")]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan? LoadingTime { get; set; }

        public long DistanceTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Entfernungszeit")]
        [DisplayFormat(DataFormatString = @"{0:hh\:mm\:ss}", ApplyFormatInEditMode = true)]
        public TimeSpan? DistanceTime
        {
            get { return TimeSpan.FromTicks(DistanceTimeTick); }
            set
            {
                if (value.HasValue)
                    DistanceTimeTick = value.Value.Ticks;
                else
                    DistanceTimeTick = 0;
            }
        }

        public long DeliveryCycleTimeTick { get; set; }
        [NotMapped]
        [DisplayName("Lieferzyklus Zeit")]
        public TimeSpan? DeliveryCycleTime
        {
            get { return TimeSpan.FromTicks(DeliveryCycleTimeTick); }
            set
            {
                if (value.HasValue)
                    DeliveryCycleTimeTick = value.Value.Ticks;
                else
                    DeliveryCycleTimeTick = 0;
            }
        }
        [DisplayName("Baustellen Zeit")]
        public TimeSpan? ConstructionSiteTime { get; set; }
        [DisplayName("Lieferzyklus Menge")]
        public decimal? DeliveryCycleAmount { get; set; }
        [DisplayName("Status")]
        public int? State { get; set; }
        public string CementNumber1 { get; set; }
        public string CementDescription1 { get; set; }
        public decimal? CementAdditionValue1 { get; set; }
        public string CementNumber2 { get; set; }
        public string CementDescription2 { get; set; }
        public decimal? CementAdditionValue2 { get; set; }
        public string CementNumber3 { get; set; }
        public string CementDescription3 { get; set; }
        public decimal? CementAdditionValue3 { get; set; }
        public string AdmixturesNumber1 { get; set; }
        public string AdmixturesDescription1 { get; set; }
        public decimal? AdmixturesAdditionValue1 { get; set; }
        public string AdmixturesNumber2 { get; set; }
        public string AdmixturesDescription2 { get; set; }
        public decimal? AdmixturesAdditionValue2 { get; set; }
        public string AdmixturesNumber3 { get; set; }
        public string AdmixturesDescription3 { get; set; }
        public decimal? AdmixturesAdditionValue3 { get; set; }
        public string AdmixturesNumber4 { get; set; }
        public string AdmixturesDescription4 { get; set; }
        public decimal? AdmixturesAdditionValue4 { get; set; }
        public string AdmixturesNumber5 { get; set; }
        public string AdmixturesDescription5 { get; set; }
        public decimal? AdmixturesAdditionValue5 { get; set; }
        public string WaterNumber1 { get; set; }
        public string WaterDescription1 { get; set; }
        public decimal? WaterAdditionValue1 { get; set; }
        public string WaterNumber2 { get; set; }
        public string WaterDescription2 { get; set; }
        public decimal? WaterAdditionValue2 { get; set; }
        public string WaterLCNumber1 { get; set; }
        public string WaterLCDescription1 { get; set; }
        [DisplayName("Wasser letzte Charge")]
        public decimal? WaterLCValue1 { get; set; }
        public string WaterLCNumber2 { get; set; }
        public string WaterLCDescription2 { get; set; }
        public decimal? WaterLCValue2 { get; set; }
        [DisplayName("Sonderleistung 1")]
        public string SpecialDescription1 { get; set; }
        [DisplayName("Wert 1")]
        public string SpecialValue1 { get; set; }
        [DisplayName("EDV Code 1")]
        public string SpecialNumber1 { get; set; }
        [DisplayName("Sonderleistung 2")]
        public string SpecialDescription2 { get; set; }
        [DisplayName("Wert 2")]
        public string SpecialValue2 { get; set; }
        [DisplayName("EDV Code 2")]
        public string SpecialNumber2 { get; set; }
        [DisplayName("Sonderleistung 3")]
        public string SpecialDescription3 { get; set; }
        [DisplayName("Wert 3")]
        public string SpecialValue3 { get; set; }
        [DisplayName("EDV Code 3")]
        public string SpecialNumber3 { get; set; }
        [DisplayName("Sonderleistung 4")]
        public string SpecialDescription4 { get; set; }
        [DisplayName("Wert 4")]
        public string SpecialValue4 { get; set; }
        [DisplayName("EDV Code 4")]
        public string SpecialNumber4 { get; set; }
        [DisplayName("Sonderleistung 5")]
        public string SpecialDescription5 { get; set; }
        [DisplayName("Wert 5")]
        public string SpecialValue5 { get; set; }
        [DisplayName("EDV Code 5")]
        public string SpecialNumber5 { get; set; }
        [DisplayName("LS-Bermerkung")]
        public string DNComment { get; set; }
        [DisplayName("Bemerkung 1")]
        public string Comment1 { get; set; }
        [DisplayName("Bemerkung 2")]
        public string Comment2 { get; set; }
        [DisplayName("Bemerkung 3")]
        public string Comment3 { get; set; }
        [DisplayName("Chargengröße")]
        public decimal? BatchSize { get; set; }
        [DisplayName("Mischzeit")]
        public int? MixTime { get; set; }
        [DisplayName("VA-Zeit")]
        public decimal? Workability { get; set; }
        public long? LocationId { get; set; }
        [DisplayName("Datum")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? RegistrationDate { get; set; }
        [DisplayName("Min Charge [Kg]")]
        public decimal? MinLot { get; set; }
        [DisplayName("Max Charge [Kg]")]
        public decimal? MaxLot { get; set; }
        [DisplayName("Anzahl Chargen")]
        public decimal? LotCount { get; set; }
        [DisplayName("Chargengröße [Kg]")]
        public decimal? LotWeight { get; set; }
        [DisplayName("Chargengröße [%]")]
        public decimal? LotProcent { get; set; }
        [DisplayName("Absolute Produktionsmenge")]
        public decimal? NewOrderedQuantity { get; set; }
        [DisplayName("Ziel Lagerort")]
        public string LocationDescription { get; set; }
        [DisplayName("Konsistenz Sollwert")]
        public decimal? ConsistencySetPoint { get; set; }
        [DisplayName("Konsistenz Min")]
        public decimal? ConsistencyMin { get; set; }
        [DisplayName("Konsistenz Max")]
        public decimal? ConsistencyMax { get; set; }
        public decimal? RemainingLot { get; set; }
        public decimal? LotSizeInProcent { get; set; }
        public int? RemainingLotAmount { get; set; }
        public int? LotAmount { get; set; }
        public long? TraderId { get; set; }
        [DisplayName("Händler")]
        public string TraderName { get; set; }
        [DisplayName("Händlernummer")]
        public string TraderNumber { get; set; }

        //änderung Manu 2016_05_13 Benutzername für den ausdruck
        public string UserName { get; set; }
        //änderung Manu 2016_05_20 Benutzername für den ausdruck
        public bool? Printed { get; set; }

        [ForeignKey("DeliveryNoteTypeId")]
        public virtual md_order_DeliveryNoteType DeliveryNoteType { get; set; }
    }

    [Table("md_order_InvoiceHeader")]
    public partial class md_order_InvoiceHeader
    {
        public long Id { get; set; }
        [DisplayName("Rechnungsnr.:")]
        [Required]
        public long InvoiceNumber { get; set; }
        [DisplayName("Rechnungsdatum")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime InvoiceDate { get; set; }
        [DisplayName("Leistungsdatum")]
        public DateTime? ServiceDate { get; set; }
        [DisplayName("Kunde")]
        [Required]
        public string CustomerDescription { get; set; }
        [DisplayName("Kundennummer")]
        [Required]
        public string CustomerNumber { get; set; }
        [DisplayName("Kundennummer EDV")]
        public string CustomerNumberEDV { get; set; }
        [DisplayName("Straße")]
        [Required]
        public string CustomerStreet { get; set; }
        [DisplayName("PLZ")]
        [Required]
        public long CustomerZipCode { get; set; }
        [DisplayName("Stadt")]
        [Required]
        public string CustomerCity { get; set; }
        [DisplayName("Bundesland")]
        public string CustomerState { get; set; }
        [DisplayName("Land")]
        public string CustomerCountry { get; set; }
        [DisplayName("Titel")]
        public string Title { get; set; }
        [DisplayName("Bemerkung")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DisplayName("Zahlungsbedingungen")]
        public string PaymentTerms { get; set; }
        [DisplayName("Nachbemerkung")]
        [DataType(DataType.MultilineText)]
        public string FinalDescription { get; set; }
        [DisplayName("bezahlt")]
        public bool Settled { get; set; }
        [DisplayName("storniert")]
        public bool Storno { get; set; }
        [DisplayName("Gesamtrabatt")]
        public int? TotalDiscount { get; set; }
        public bool IsPrinted { get; set; }
    }

    [Table("md_order_InvoicePositions")]
    public partial class md_order_InvoicePositions
    {
        public long Id { get; set; }
        public long FidHeader { get; set; }
        [DisplayName("Artikelnummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Materialnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Artikelbezeichnung")]
        [Required]
        public string ArticleDescription { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Preis")]
        [Required]
        public decimal Price { get; set; }
        [DisplayName("Menge")]
        [Required]
        public decimal Quantity { get; set; }
        [DisplayName("Steuer")]
        [Required]
        public decimal Tax { get; set; }
        [DisplayName("Rabatt")]
        public decimal Discount { get; set; }
        [DisplayName("Auftrags/-Lieferscheinnummer")]
        public string OrderNumber { get; set; }
        [DisplayName("Lieferscheindatum")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DeliveryDate { get; set; }
        public decimal Freight { get; set; }

        //[ForeignKey("FidHeader")]
        //public virtual md_order_InvoiceHeader Header { get; set; }
    }

    [Table("md_order_InvoiceHeaderTemp")]
    public partial class md_order_InvoiceHeaderTemp
    {
        public long Id { get; set; }
        [DisplayName("Rechnungsnr.:")]
        [Required]
        public long InvoiceNumber { get; set; }
        [DisplayName("Rechnungsdatum")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime InvoiceDate { get; set; }
        [DisplayName("Leistungsdatum")]
        public DateTime? ServiceDate { get; set; }
        [DisplayName("Kunde")]
        [Required]
        public string CustomerDescription { get; set; }
        [DisplayName("Kundennummer")]
        [Required]
        public string CustomerNumber { get; set; }
        [DisplayName("Kundennummer EDV")]
        public string CustomerNumberEDV { get; set; }
        [DisplayName("Straße")]
        [Required]
        public string CustomerStreet { get; set; }
        [DisplayName("PLZ")]
        [Required]
        public long CustomerZipCode { get; set; }
        [DisplayName("Stadt")]
        [Required]
        public string CustomerCity { get; set; }
        [DisplayName("Bundesland")]
        public string CustomerState { get; set; }
        [DisplayName("Land")]
        public string CustomerCountry { get; set; }
        [DisplayName("Titel")]
        public string Title { get; set; }
        [DisplayName("Bemerkung")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DisplayName("Zahlungsbedingungen")]
        public string PaymentTerms { get; set; }
        [DisplayName("Nachbemerkung")]
        [DataType(DataType.MultilineText)]
        public string FinalDescription { get; set; }
        [DisplayName("bezahlt")]
        public bool Settled { get; set; }
        [DisplayName("storniert")]
        public bool Storno { get; set; }
        [DisplayName("Gesamtrabatt")]
        public int? TotalDiscount { get; set; }
        public bool IsPrinted { get; set; }
    }

    [Table("md_order_InvoicePositionsTemp")]
    public partial class md_order_InvoicePositionsTemp
    {
        public long Id { get; set; }
        public long FidHeader { get; set; }
        [DisplayName("Artikelnummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Materialnummer")]
        public string MaterialNumber { get; set; }
        [DisplayName("Artikelbezeichnung")]
        [Required]
        public string ArticleDescription { get; set; }
        [DisplayName("Einheit")]
        public string Unit { get; set; }
        [DisplayName("Preis")]
        [Required]
        public decimal Price { get; set; }
        [DisplayName("Menge")]
        [Required]
        public decimal Quantity { get; set; }
        [DisplayName("Steuer")]
        [Required]
        public decimal Tax { get; set; }
        [DisplayName("Rabatt")]
        public decimal Discount { get; set; }
        [DisplayName("Auftrags/-Lieferscheinnummer")]
        public string OrderNumber { get; set; }
        [DisplayName("Lieferscheindatum")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime? DeliveryDate { get; set; }
        public decimal Freight { get; set; }

        [ForeignKey("FidHeader")]
        public virtual md_order_InvoiceHeaderTemp Header { get; set; }
    }

}
