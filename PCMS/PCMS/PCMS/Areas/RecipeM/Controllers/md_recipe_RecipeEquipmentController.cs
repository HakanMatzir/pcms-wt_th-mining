﻿using PCMS.Areas.LIMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using PagedList;
using PCMS.Areas.RecipeM.Models;
using PCMS.Models;
using PCMS.Helper;
using System.Net;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.RecipeM.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_recipe_RecipeEquipmentController : Controller
    {
        private Models.RecipeMContext db = new Models.RecipeMContext();
        //private Sieve.Models.SieveContext db2 = new Sieve.Models.SieveContext();
        private LIMS.Models.LimsContext db3 = new LIMS.Models.LimsContext();
        // GET: Recipe/md_recipe_SieveLine

        public ActionResult Index(PaginationModel pg)
        {

            var model = db.Md_recipe_Recipe.Where(m => m.IsDeleted == false && m.RecipeTypeId == 2).Include(m => m.Lims_gradingCurv_GradingCurv).OrderBy(a => a.RecipeTypeId).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    pgFF.colVal = pgFF.colVal.ToLower();
                    switch (pgFF.colName)
                    {
                        case "Description":
                            model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Description = pgFF.colVal;
                            break;
                        case "Number":
                            model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Number = pgFF.colVal;
                            break;
                        case "GradingCurvId":
                            model = model.Where(m => m.GradingCurvId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.GradingCurvId = pgFF.colVal;
                            break;
                        case "RecipeTypeId":
                            model = model.Where(m => m.RecipeTypeId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.RecipeTypeId = pgFF.colVal;
                            break;
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "NumberEDV":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.NumberEDV).ToList();
                    else
                        model = model.OrderByDescending(m => m.NumberEDV).ToList();
                    break;
                case "GradingCurvId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GradingCurvId).ToList();
                    else
                        model = model.OrderByDescending(m => m.GradingCurvId).ToList();
                    break;
                case "MaterialId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialId).ToList();
                    break;
                case "RecipeTypeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeTypeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeTypeId).ToList();
                    break;
                case "AirContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AirContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.AirContent).ToList();
                    break;
                case "ConsistencySetPoint":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConsistencySetPoint).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConsistencySetPoint).ToList();
                    break;
                case "MaxWCValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaxWCValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaxWCValue).ToList();
                    break;
                case "HotWater":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.HotWater).ToList();
                    else
                        model = model.OrderByDescending(m => m.HotWater).ToList();
                    break;
                case "ActualValueTolerance":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ActualValueTolerance).ToList();
                    else
                        model = model.OrderByDescending(m => m.ActualValueTolerance).ToList();
                    break;
                case "SetPointDeviation":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SetPointDeviation).ToList();
                    else
                        model = model.OrderByDescending(m => m.SetPointDeviation).ToList();
                    break;
                case "MixerFilling":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MixerFilling).ToList();
                    else
                        model = model.OrderByDescending(m => m.MixerFilling).ToList();
                    break;
                case "WasteWaterFactor":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WasteWaterFactor).ToList();
                    else
                        model = model.OrderByDescending(m => m.WasteWaterFactor).ToList();
                    break;
                case "BatchProgram":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.BatchProgram).ToList();
                    else
                        model = model.OrderByDescending(m => m.BatchProgram).ToList();
                    break;
                case "FillMonitoringCement":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FillMonitoringCement).ToList();
                    else
                        model = model.OrderByDescending(m => m.FillMonitoringCement).ToList();
                    break;
                case "FillRating":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FillRating).ToList();
                    else
                        model = model.OrderByDescending(m => m.FillRating).ToList();
                    break;
                case "CementEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CementEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.CementEmptyingDelay).ToList();
                    break;
                case "WaterEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterEmptyingDelay).ToList();
                    break;
                case "AdditiveEmptyingDelay":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AdditiveEmptyingDelay).ToList();
                    else
                        model = model.OrderByDescending(m => m.AdditiveEmptyingDelay).ToList();
                    break;
                case "FoamAdditionTime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FoamAdditionTime).ToList();
                    else
                        model = model.OrderByDescending(m => m.FoamAdditionTime).ToList();
                    break;
                case "MixingTime":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MixingTime).ToList();
                    else
                        model = model.OrderByDescending(m => m.MixingTime).ToList();
                    break;
                case "EmptyingPartlyOpen1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingPartlyOpen1).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingPartlyOpen1).ToList();
                    break;
                case "EmptyingPartlyOpen2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingPartlyOpen2).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingPartlyOpen2).ToList();
                    break;
                case "EmptyingOpen":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.EmptyingOpen).ToList();
                    else
                        model = model.OrderByDescending(m => m.EmptyingOpen).ToList();
                    break;
                case "SuitabilityGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SuitabilityGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SuitabilityGroupId).ToList();
                    break;
                case "ConcreteFamilyId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConcreteFamilyId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ConcreteFamilyId).ToList();
                    break;
                case "ExposureGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ExposureGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ExposureGroupId).ToList();
                    break;
                case "MaterialGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialGroupId).ToList();
                    break;
                case "MasterUnit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MasterUnit).ToList();
                    else
                        model = model.OrderByDescending(m => m.MasterUnit).ToList();
                    break;
                case "AirVoidContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AirVoidContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.AirVoidContent).ToList();
                    break;
                case "WaterContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WaterContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.WaterContent).ToList();
                    break;
                case "ContentOfCement":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ContentOfCement).ToList();
                    else
                        model = model.OrderByDescending(m => m.ContentOfCement).ToList();
                    break;
                case "Additive":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Additive).ToList();
                    else
                        model = model.OrderByDescending(m => m.Additive).ToList();
                    break;
                case "SolidMaterialrest":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SolidMaterialrest).ToList();
                    else
                        model = model.OrderByDescending(m => m.SolidMaterialrest).ToList();
                    break;
                case "Aggregate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Aggregate).ToList();
                    else
                        model = model.OrderByDescending(m => m.Aggregate).ToList();
                    break;
                case "AggregateFixed":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AggregateFixed).ToList();
                    else
                        model = model.OrderByDescending(m => m.AggregateFixed).ToList();
                    break;
                case "SurfaceDamp":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SurfaceDamp).ToList();
                    else
                        model = model.OrderByDescending(m => m.SurfaceDamp).ToList();
                    break;
                case "AdditionWater":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.AdditionWater).ToList();
                    else
                        model = model.OrderByDescending(m => m.AdditionWater).ToList();
                    break;
                case "WetConcreteDensity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WetConcreteDensity).ToList();
                    else
                        model = model.OrderByDescending(m => m.WetConcreteDensity).ToList();
                    break;
                case "MealGrain":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MealGrain).ToList();
                    else
                        model = model.OrderByDescending(m => m.MealGrain).ToList();
                    break;
                case "MealFineGrain":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MealFineGrain).ToList();
                    else
                        model = model.OrderByDescending(m => m.MealFineGrain).ToList();
                    break;
                case "MortarContent":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MortarContent).ToList();
                    else
                        model = model.OrderByDescending(m => m.MortarContent).ToList();
                    break;
                case "ClementPaste":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ClementPaste).ToList();
                    else
                        model = model.OrderByDescending(m => m.ClementPaste).ToList();
                    break;
                case "WZValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WZValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.WZValue).ToList();
                    break;
                case "WZValueEq":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WZValueEq).ToList();
                    else
                        model = model.OrderByDescending(m => m.WZValueEq).ToList();
                    break;
                case "Price":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Price).ToList();
                    else
                        model = model.OrderByDescending(m => m.Price).ToList();
                    break;
            }


            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        public ActionResult Create()
        {
            ViewBag.GradingCurvId = new SelectList(db3.lims_gradingCurv_GradingCurv.Where(a => a.Id == 0), "Id", "Description");
            ViewBag.MaterialId = new SelectList(db.Md_masterData_Material.Where(a => a.MaterialGroupId == 5), "Id", "Name");
            ViewBag.GradingCurvSieveRangeId = new SelectList(db3.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.FacilityId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            //ViewBag.FunctionType = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Description, Number, RecipeTypeId, MinCharge, MaxCharge, MixingTime, Comment, IsActive")] md_recipe_RecipeM md_recipe_RecipeM, List<string> materialId, List<string> sort)
        {
            if (ModelState.IsValid)
            {
                int i = 0;
                long HashCode = 0;
                foreach (string s in materialId)
                {
                    md_recipe_RecipeM.RecipeMaterials.Add(new md_recipe_RecipeMaterial() { RecipeId = md_recipe_RecipeM.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Weight = Convert.ToDecimal(s.Split('|')[1]), Value = Convert.ToDecimal(s.Split('|')[2]), Unit = 0, Sort = i, Type = Convert.ToInt32(s.Split('|')[3]), FunctionType = Convert.ToInt32(s.Split('|')[4]) });
                    var a = db.Md_equipment.Find(Convert.ToInt64(s.Split('|')[0]));
                    HashCode += a.Number;
                    i++;
                }
                if (sort != null)
                {
                    foreach (string s in sort)
                    {
                        md_recipe_RecipeM.RecipeConcrete.Add(new md_material_RecipeMaterial() { RecipeId = md_recipe_RecipeM.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Valid = Convert.ToInt32(s.Split('|')[1]), Active = Convert.ToBoolean(s.Split('|')[2]) });
                    }
                }
                //md_recipe_RecipeM.IsActive = true;
                md_recipe_RecipeM.HashCode = HashCode;
                md_recipe_RecipeM.RecipeTypeId = 2;
                md_recipe_RecipeM.IsDeleted = false;
                db.Md_recipe_Recipe.Add(md_recipe_RecipeM);
                //db.lims_gradingCurv_GradingCurv.Add(lims_gradingCurv_GradingCurv);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);

                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.FacilityId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(md_recipe_RecipeM);
        }

        public ActionResult Edit(int id)
        {
            var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == id);
            if (model.RecipeMaterials != null)
                model.RecipeMaterials = model.RecipeMaterials.OrderBy(s => s.Sort).ToList();
            List<RecipeModelMMaterialDetails> de = new List<RecipeModelMMaterialDetails>();
            foreach (var item in model.RecipeMaterials)
            {
                if (item.Type == 1)
                {
                    var m = db.Md_masterData_Material.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.ArticleNumber, Description = m.Name, Deliverer = (m.Md_masterData_Deliverer != null) ? m.Md_masterData_Deliverer.Name : "---", density = m.Density.ToString(), IsDeleted = m.IsDeleted ?? false });
                }
                else if (item.Type == 2)
                {
                    var m = db.Md_equipment.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.Number.ToString(), Description = m.Description, Deliverer = "--", density = "--", IsDeleted = m.IsDeleted ?? false });
                }
                else if (item.Type == 3)
                {
                    var m = db.Md_recipe_Recipe.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.Number, Description = m.Description, Deliverer = "--", density = "--", IsDeleted = m.IsDeleted ?? false });
                }
            }
            ViewBag.Details = de;
            ViewBag.FacilityId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description, Number, RecipeTypeId, MinCharge, MaxCharge, MixingTime, Comment, IsActive")] md_recipe_RecipeM md_recipe_RecipeM, List<string> materialId, List<string> sort)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_recipe_RecipeM).State = EntityState.Modified;
                db.Entry(md_recipe_RecipeM).Collection(st => st.RecipeMaterials).Load();
                db.Entry(md_recipe_RecipeM).Collection(st => st.RecipeConcrete).Load();
                md_recipe_RecipeM.RecipeMaterials.Clear();
                md_recipe_RecipeM.RecipeConcrete.Clear();
                int i = 0;
                long HashCode = 0;
                foreach (string s in materialId)
                {

                    md_recipe_RecipeM.RecipeMaterials.Add(new md_recipe_RecipeMaterial() { RecipeId = md_recipe_RecipeM.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Weight = Convert.ToDecimal(s.Split('|')[1]), Value = Convert.ToDecimal(s.Split('|')[2]), Unit = 0, Sort = i, Type = Convert.ToInt32(s.Split('|')[3]), FunctionType = Convert.ToInt32(s.Split('|')[4]) });
                    //da nur eqipment
                    var a = db.Md_equipment.Find(Convert.ToInt64(s.Split('|')[0]));
                    HashCode += a.Number;
                    i++;
                }
                if (sort != null)
                {
                    foreach (string s in sort)
                    {
                        md_recipe_RecipeM.RecipeConcrete.Add(new md_material_RecipeMaterial() { RecipeId = md_recipe_RecipeM.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Valid = Convert.ToInt32(s.Split('|')[1]), Active = Convert.ToBoolean(s.Split('|')[2]) });
                    }
                }
                //md_recipe_RecipeM.IsActive = true;
                md_recipe_RecipeM.HashCode = HashCode;
                md_recipe_RecipeM.RecipeTypeId = 2;
                md_recipe_RecipeM.IsDeleted = false;
                //db.Md_recipe_Recipe.Add(md_recipe_Recipe);
                //db.lims_gradingCurv_GradingCurv.Add(lims_gradingCurv_GradingCurv);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);


                return RedirectToAction("Index");
            }

            List<RecipeModelMMaterialDetails> de = new List<RecipeModelMMaterialDetails>();
            foreach (var item in md_recipe_RecipeM.RecipeMaterials)
            {
                if (item.Type == 1)
                {
                    var m = db.Md_masterData_Material.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.ArticleNumber, Description = m.Name, Deliverer = (m.Md_masterData_Deliverer != null) ? m.Md_masterData_Deliverer.Name : "---", density = m.Density.ToString(), IsDeleted = m.IsDeleted ?? false });
                }
                else if (item.Type == 2)
                {
                    var m = db.Md_equipment.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.Number.ToString(), Description = m.Description, Deliverer = "--", density = "--", IsDeleted = m.IsDeleted ?? false });
                }
                else if (item.Type == 3)
                {
                    var m = db.Md_recipe_Recipe.Find(item.MaterialId);
                    de.Add(new RecipeModelMMaterialDetails { Number = m.Number, Description = m.Description, Deliverer = "--", density = "--", IsDeleted = m.IsDeleted ?? false });
                }
            }
            ViewBag.Details = de;
            ViewBag.FacilityId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
           
            return View(md_recipe_RecipeM);
        }
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == id);
            if (model == null)
            {
                return HttpNotFound();
            }
            List<md_recipe_RecipeM> rM = db.Md_recipe_Recipe.Where(a => a.IsDeleted==false && a.RecipeMaterials.Any(s => s.Type == 3 && s.MaterialId == id)).ToList();
            ViewBag.md_recipe_RecipeM = rM;
            return View(model);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            var model = db.Md_recipe_Recipe.Find(id);

            //model.IsActive = true;
            model.IsDeleted = true;
            db.Entry(model).State = EntityState.Modified;
            //db.Md_recipe_Recipe.Remove(model);
            //db.SaveChanges();
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }
        public ActionResult getGradingCurv(int id)
        {
            //db3.Configuration.ProxyCreationEnabled = false;
            List<lims_aggregate_Test> tests = new List<lims_aggregate_Test>();
            var model = db.Lims_gradingCurv_GradingCurv.FirstOrDefault(a => a.Id == id);
            {
                foreach (var r in model.Lims_gradingCurvMaterial)
                {
                    tests.Add(db3.Lims_aggregate_Test.Where(t => t.MaterialId == r.MaterialId).FirstOrDefault());
                }
            }



            return Json(new { data1 = model, data2 = tests });
        }
        public ActionResult choosMaterial(PCMS.Models.PaginationModel pg)
        {

            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId != 5 && m.MaterialGroupId != 3 && m.MaterialGroupId != 4 && m.MaterialGroupId != 8).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            ViewBag.MaterialGroup = new SelectList(db.Md_material_MaterialGroup.Where(a => a.Id != 5 && a.Id != 3 && a.Id != 4 && a.Id != 8), "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult choosConcrete(PCMS.Models.PaginationModel pg)
        {

            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 8).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            ViewBag.MaterialGroup = new SelectList(db.Md_material_MaterialGroup.Where(a => a.Id != 1 && a.Id != 8), "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult choosEquipment(PaginationModel pg)
        {
            var model = db.Md_equipment.OrderBy(a => a.Number).Where(a => a.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Number":
                                model = model.Where(m => m.Number.ToString().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult choosRecipe(PaginationModel pg)
        {

            var model = db.Md_recipe_Recipe.Include(m => m.Lims_gradingCurv_GradingCurv).OrderBy(a => a.RecipeTypeId).Where(a => a.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    pgFF.colVal = pgFF.colVal.ToLower();
                    switch (pgFF.colName)
                    {
                        case "Description":
                            model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Description = pgFF.colVal;
                            break;
                        case "Number":
                            model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                            ViewBag.Number = pgFF.colVal;
                            break;
                        case "GradingCurvId":
                            model = model.Where(m => m.GradingCurvId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.GradingCurvId = pgFF.colVal;
                            break;
                        case "RecipeTypeId":
                            model = model.Where(m => m.RecipeTypeId == Convert.ToInt64(pgFF.colVal)).ToList();
                            ViewBag.RecipeTypeId = pgFF.colVal;
                            break;
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult getMaterialById(int id)
        {
            return Json(db.Md_masterData_Material.Find(id));
        }
        public ActionResult gradingCurvsJson(string q)
        {
            var model = db.Lims_gradingCurv_GradingCurv.Select(a => new { a.Description, a.SieveLineNumber, a.Id }).OrderBy(a => a.SieveLineNumber).ToList();
            if (!String.IsNullOrEmpty(q))
                model = model.Where(a => a.SieveLineNumber.Contains(q)).ToList();
            return Json(new { res = model });
        }
        public ActionResult ReportDetail(long? Id)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_recipe_Recipe.Include(m => m.RecipeMaterials).Include(m => m.RecipeConcrete).FirstOrDefault(m => m.Id == Id);

                PCMSDataSet ds = new PCMSDataSet();
                ds.List.Clear();

                int i = 0;
                decimal allVal = 0;
                foreach (var dataset in model.RecipeMaterials)
                {
                    if (dataset.Type == 1)
                    {
                        var m = db.Md_masterData_Material.Find(dataset.MaterialId);
                        if (dataset.Type == 1)
                        {
                            allVal += dataset.Value;
                        }

                        string functType = "";
                        if (dataset.FunctionType == 1)
                        {
                            functType = "dosieren";
                        }
                        if (dataset.FunctionType == 2)
                        {
                            functType = "dosieren ent.";
                        }
                        ds.List.AddListRow(i.ToString(), m.MaterialNumber, m.Name, functType, dataset.Value.ToString(), m.Density.ToString() ?? "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 2)
                    {
                        var m = db.Md_equipment.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(i.ToString(), m.Number.ToString(), m.Description, functType, dataset.Value.ToString(), "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }
                    else if (dataset.Type == 3)
                    {
                        var m = db.Md_recipe_Recipe.Find(dataset.MaterialId);


                        string functType = "";

                        ds.List.AddListRow(i.ToString(), m.Number, m.Description, functType, "--", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    }


                    i++;
                }
                ReportDataSource DSReport = new ReportDataSource("Materialien", ds.List.ToList());

                //ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

                List<ReportParameter> repParams = new List<ReportParameter>();

                repParams.Add(new ReportParameter("RezeptName", model.Description));
                repParams.Add(new ReportParameter("RezeptNummer", model.Number));
                repParams.Add(new ReportParameter("MinCharge", model.MinCharge != null ? model.MinCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MaxCharge", model.MaxCharge != null ? model.MaxCharge.ToString() : "-"));
                repParams.Add(new ReportParameter("MixingTime", model.MixingTime != null ? model.MixingTime.ToString() : "-"));
                repParams.Add(new ReportParameter("FabrikatNummer", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.MaterialNumber : "-"));
                repParams.Add(new ReportParameter("FabrikatName", model.RecipeConcrete.FirstOrDefault() != null ? model.RecipeConcrete.FirstOrDefault().Md_material_Material.Name : "-"));
                repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
                repParams.Add(new ReportParameter("Sum", allVal.ToString()));
                //repParams.Add(new ReportParameter("materialDescription", model.Md_material_Material.Name));
                //repParams.Add(new ReportParameter("materialCategory", model.Md_material_Material.MaterialTypeId.ToString()));
                //repParams.Add(new ReportParameter("producerName", "fehlt"));
                //repParams.Add(new ReportParameter("producerStreet", "fehlt"));
                //repParams.Add(new ReportParameter("producerZIP", "fehlt"));
                //repParams.Add(new ReportParameter("producerCity", "fehlt"));
                //repParams.Add(new ReportParameter("deliveryNoteNo", model.DeliveryNoteNo));
                //repParams.Add(new ReportParameter("delivererName", "fehlt"));
                //repParams.Add(new ReportParameter("delivererStreet", "fehlt"));
                //repParams.Add(new ReportParameter("delivererZIP", "fehlt"));
                //repParams.Add(new ReportParameter("delivererCity", "fehlt"));
                //repParams.Add(new ReportParameter("probeTakingDate", model.ProbTakingDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceId.ToString()));
                //repParams.Add(new ReportParameter("probePlace", model.ProbPlaceDetail.ToString()));
                //repParams.Add(new ReportParameter("probeTaker", model.UserId.ToString()));
                //repParams.Add(new ReportParameter("probeType", model.ProbTypeId.ToString()));
                //repParams.Add(new ReportParameter("probeQuantity", model.Quantity.ToString()));
                //repParams.Add(new ReportParameter("probeEnteringDate", model.ProbEnteringDatetime.Value.ToShortDateString()));
                //repParams.Add(new ReportParameter("sieveSet", model.SievSetId.ToString()));
                //repParams.Add(new ReportParameter("kValue", model.KValue.ToString()));
                //repParams.Add(new ReportParameter("dValue", model.DValue.ToString()));


                string SievesList = "";
                //for (int i = 0; i < Sieves.Count; i++)
                //{
                //    SievesList += Sieves[i].ToString() + ";";
                //}
                //for (int i = 0; i < (17 - Sieves.Count); i++)
                //{
                //    SievesList += "-;";
                //}

                //repParams.Add(new ReportParameter("SievesList", SievesList));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/RecipeM/RecipeMDetail.rdlc";
                //reportViewer.ShowPrintButton = false;
                //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
                //reportViewer.LocalReport.DataSources.Add(DSReportA);
                //reportViewer.LocalReport.DataSources.Add(DSReportB);
                //reportViewer.LocalReport.DataSources.Add(DSReportC);
                //reportViewer.LocalReport.DataSources.Add(DSReportU);

                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                ViewBag.ReportViewer = reportViewer;

                return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
    }
}