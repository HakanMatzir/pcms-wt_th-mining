﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_AggregatesController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.ArticleNumber).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
           // ViewBag.RuleGradingCurve = new SelectList(db.Md_RuleGrading.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 1 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.Alkali = new SelectList(db.Md_material_AlkaliGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
           // ViewBag.Suitability = new SelectList(db.Md_masterData_Suitability.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.Geo = new SelectList(db.Md_material_GeologicalSource.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.Petro = new SelectList(db.Md_material_PetrographicType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.GType = new SelectList(db.Md_material_GravelType.Where(a=>a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 1 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName");
           // ViewBag.GGroup = new SelectList(db.Md_material_GravelGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.Aggregate = new SelectList(db.Md_material_Aggregate.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
           // ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 9;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                //db.Md_material_AggregateDetails.Add(model.aggregateDetails);
                //db.SaveChanges();
                //model.material.AggregateId = model.aggregateDetails.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.RuleGradingCurve = new SelectList(db.Md_RuleGrading.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 1 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Alkali = new SelectList(db.Md_material_AlkaliGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class");
            ViewBag.Suitability = new SelectList(db.Md_masterData_Suitability.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Geo = new SelectList(db.Md_material_GeologicalSource.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Petro = new SelectList(db.Md_material_PetrographicType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.GType = new SelectList(db.Md_material_GravelType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 1 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName");
            ViewBag.GGroup = new SelectList(db.Md_material_GravelGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.Aggregate = new SelectList(db.Md_material_Aggregate.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View(model);
        }

        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
               // model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);
               //
               // model.aggregateDetails = db.Md_material_AggregateDetails.Find(model.material.AggregateId);
               // model.aggregate = db.Md_material_Aggregate.Find(model.aggregateDetails.AggregateId);
               // model.gravelType = db.Md_material_GravelType.Find(model.aggregateDetails.GravelTypeId);
               // model.gravelGroup = db.Md_material_GravelGroup.Find(model.aggregateDetails.GravelGroupId);
               // model.rock = db.Md_material_RockType.Find(model.aggregateDetails.RockTypeId);
               // model.alkali = db.Md_material_AlkaliGroup.Find(model.aggregateDetails.AlkaliGroupId);
               // model.petrograph = db.Md_material_PetrographicType.Find(model.aggregateDetails.PetrographicTypeId);
               // model.geo = db.Md_material_GeologicalSource.Find(model.aggregateDetails.GeologicalSourceId);
               // model.standard = db.Md_masterData_Material_Standard.Find(model.aggregateDetails.NormId);
               // model.suitability = db.Md_masterData_Suitability.Find(model.aggregateDetails.SuitabilityId);
               // model.Rule = db.Md_RuleGrading.Find(model.aggregateDetails.RuleGradingCurveId);
            }
            //ViewBag.RuleGradingCurve = new SelectList(db.Md_RuleGrading.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
            //ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            //ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 1 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.material.MaterialTypeId);
            //ViewBag.Alkali = new SelectList(db.Md_material_AlkaliGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class", model.aggregateDetails.AlkaliGroupId);
            //ViewBag.Suitability = new SelectList(db.Md_masterData_Suitability.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.SuitabilityId);
            //ViewBag.Geo = new SelectList(db.Md_material_GeologicalSource.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GeologicalSourceId);
            //ViewBag.Petro = new SelectList(db.Md_material_PetrographicType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.PetrographicTypeId);
            //ViewBag.GType = new SelectList(db.Md_material_GravelType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GravelTypeId);
            //ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 1 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName", model.aggregateDetails.NormId);
            //ViewBag.GGroup = new SelectList(db.Md_material_GravelGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GravelGroupId);
            //ViewBag.Aggregate = new SelectList(db.Md_material_Aggregate.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.AggregateId);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
               // db.Entry(model.aggregateDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
          //  ViewBag.RuleGradingCurve = new SelectList(db.Md_RuleGrading.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description");
          //  ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
          //  ViewBag.MaterialType = new SelectList(db.Md_material_MaterialType.Where(a => a.MaterialGroupId == 1 && a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.material.MaterialTypeId);
          //  ViewBag.Alkali = new SelectList(db.Md_material_AlkaliGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Class", model.aggregateDetails.AlkaliGroupId);
          //  ViewBag.Suitability = new SelectList(db.Md_masterData_Suitability.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.SuitabilityId);
          //  ViewBag.Geo = new SelectList(db.Md_material_GeologicalSource.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GeologicalSourceId);
          //  ViewBag.Petro = new SelectList(db.Md_material_PetrographicType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.PetrographicTypeId);
          //  ViewBag.GType = new SelectList(db.Md_material_GravelType.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GravelTypeId);
          //  ViewBag.Norm = new SelectList(db.Md_masterData_Material_Standard.Where(a => a.MaterialTypeId == 1 && a.IsDeleted == false && a.IsActive == true).ToList(), "Id", "ShortName", model.aggregateDetails.NormId);
          //  ViewBag.GGroup = new SelectList(db.Md_material_GravelGroup.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.GravelGroupId);
          //  ViewBag.Aggregate = new SelectList(db.Md_material_Aggregate.Where(a => a.IsActive == true && a.IsDeleted == false).ToList(), "Id", "Description", model.aggregateDetails.AggregateId);

            return View(model);
        }
         
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.RecipeCheck = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a=>a.Md_recipe_Recipe.Number).ToList().Count();
            ViewBag.RecipeNumber = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a => a.Md_recipe_Recipe.Number).ToList();
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        //####################################################################################################################################
        //########################################### Material Typ hinzufügen und löschen Start ##############################################
        //####################################################################################################################################

        public ActionResult CreateMaterialType()
        {
            return View();
        }

        public ActionResult SaveMaterialType(string description)
        {
            md_material_MaterialType model = new md_material_MaterialType();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            model.MaterialGroupId = 1;
            db.Md_material_MaterialType.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id, type = model.Description });
        }

        public ActionResult DeleteMaterialType(md_material_MaterialType model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_MaterialType.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveMaterialType(md_material_MaterialType model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.MaterialGroupId = 1;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Alkali hinzufügen und löschen Start ################################################
        //####################################################################################################################################

        public ActionResult CreateAlkali()
        {
            return View();
        }

        public ActionResult SaveAlkali(string classDescription, string ContentValue, string WO, string WA, string WF)
        {
            md_material_AlkaliGroup model = new md_material_AlkaliGroup();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Class = classDescription;
            model.ContentValue = ContentValue;
            model.WA = WA;
            model.WF = WF;
            model.WO = WO;
            db.Md_material_AlkaliGroup.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteAlkali(md_material_AlkaliGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_AlkaliGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveAlkali(md_material_AlkaliGroup model, long? id, string classDescription, string ContentValue, string WO, string WA, string WF)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Class = classDescription;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Eignung hinzufügen und löschen Start ###############################################
        //####################################################################################################################################

        public ActionResult CreateSuitability()
        {
            return View();
        }

        public ActionResult SaveSuitability(string description)
        {
            md_masterData_Suitability model = new md_masterData_Suitability();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            db.Md_masterData_Suitability.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteSuitability(md_masterData_Suitability model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_masterData_Suitability.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveSuitability(md_masterData_Suitability model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Geolog. hinzufügen und löschen Start ###############################################
        //####################################################################################################################################

        public ActionResult CreateGeological()
        {
            return View();
        }

        public ActionResult SaveGeological(string description)
        {
            md_material_GeologicalSource model = new md_material_GeologicalSource();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            db.Md_material_GeologicalSource.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteGeological(md_material_GeologicalSource model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_GeologicalSource.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveGeological(md_material_GeologicalSource model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Petrogr hinzufügen und löschen Start ###############################################
        //####################################################################################################################################

        public ActionResult CreatePetrograph()
        {
            return View();
        }

        public ActionResult SavePetrograph(string description)
        {
            md_material_PetrographicType model = new md_material_PetrographicType();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            db.Md_material_PetrographicType.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeletePetrograph(md_material_PetrographicType model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_PetrographicType.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemovePetrograph(md_material_PetrographicType model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Kornart hinzufügen und löschen Start ###############################################
        //####################################################################################################################################

        public ActionResult CreateGravelType()
        {
            return View();
        }

        public ActionResult SaveGravelType(string description)
        {
            md_material_GravelType model = new md_material_GravelType();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            db.Md_material_GravelType.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteGravelType(md_material_GravelType model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_GravelType.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveGravelType(md_material_GravelType model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Körnung hinzufügen und löschen Start ###############################################
        //####################################################################################################################################

        public ActionResult CreateAggregate()
        {
            return View();
        }

        public ActionResult SaveAggregate(string description)
        {
            md_material_Aggregate model = new md_material_Aggregate();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Description = description;
            db.Md_material_Aggregate.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteAggregate(md_material_Aggregate model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_Aggregate.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveAggregate(md_material_Aggregate model, long? id, string description)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //####################################################################################################################################
        //############################################### Korngruppe hinzufügen und löschen Start ############################################
        //####################################################################################################################################

        public ActionResult CreateGradinGroup()
        {
            return View();
        }

        public ActionResult SaveGradinGroup(string description)
        {
            md_material_GravelGroup model = new md_material_GravelGroup();

            model.IsActive = true;
            model.IsDeleted = false;
            model.Sort = 9;
            model.Description = description;
            db.Md_material_GravelGroup.Add(model);
            db.SaveChanges();

            return Json(new { id = model.Id });
        }

        public ActionResult DeleteGradinGroup(md_material_GravelGroup model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_material_GravelGroup.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveGradinGroup(md_material_GravelGroup model, long? id, string description, int sort)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Description = description;
            if(sort == null)
            {
                sort = 9;
            }
            model.Sort = sort;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        //#################################################################################################################################
        //############################################### Norm hinzufügen und löschen Start ###############################################
        //#################################################################################################################################

        public ActionResult CreateStandard()
        {
            return View();
        }

        public ActionResult SaveStandard(string name, string shortname, string description, string comment)
        {
            md_masterData_Standard model = new md_masterData_Standard();
            if (ModelState.IsValid)
            {
                model.MaterialTypeId = 1;
                model.IsActive = true;
                model.IsDeleted = false;
                model.Name = name;
                model.ShortName = shortname;
                model.Description = description;
                model.Comment = comment;
                db.Md_masterData_Material_Standard.Add(model);
                db.SaveChanges();

                return Json(new { id = model.Id });
            }

            return Json(false);
        }

        public ActionResult DeleteStandard(md_masterData_Standard model, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_masterData_Material_Standard.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        public ActionResult RemoveStandard(md_masterData_Standard model, long? id, string name, string shortname, string description, string comment, long? materialtypeid)
        {
            model.IsActive = true;
            model.IsDeleted = true;
            model.Name = name;
            model.ShortName = shortname;
            model.Description = description;
            model.Comment = comment;
            model.MaterialTypeId = materialtypeid;
            model.Id = id;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            return Json(true);
        }

        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 1).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            } 
            
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.MaterialNumber, dataset.ArticleNumber.ToString(), dataset.Name.ToString(), dataset.ShortName.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "true"));
            repParams.Add(new ReportParameter("Header_01", "Materialnummer"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Artikelnummer"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", "Aktiv"));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Material"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Zuschlagstoffe Übersicht"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/MasterData/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();


        }
    }
}