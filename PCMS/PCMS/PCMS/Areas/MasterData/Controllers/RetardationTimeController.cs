﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class RetardationTimeController : Controller
    {
        MasterDataContext db = new MasterDataContext();
        public ActionResult Index()
        {
            var season = db.Md_material_Config.Where(a=>a.Id == 1).Select(a=>a.Season).FirstOrDefault();
            var model = db.Md_RetardationTime.Where(a=>a.Season == season).ToList();
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(md_material_RetardationTime model, long Id)
        {
            model = db.Md_RetardationTime.Find(Id);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_material_RetardationTime model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult SelectSeason(int season)
        {
            var model = db.Md_RetardationTime.Where(a => a.Season == season).ToList();
            return PartialView(model);
        }

        public ActionResult ChooseMaterial(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(a => a.MaterialGroupId == 3 && a.IsActive == true && a.IsDeleted == false).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.MaterialGroupId == 3 && m.IsActive == true && m.IsDeleted == false && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.MaterialGroupId == 3 && m.IsActive == true && m.IsDeleted == false && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
            }


            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChangeSeason(int season)
        {
            string query = "UPDATE md_config SET Season = " + season + " WHERE id = 1";
            db.Database.ExecuteSqlCommand(query);
            return Json(true);
        }
    }
}