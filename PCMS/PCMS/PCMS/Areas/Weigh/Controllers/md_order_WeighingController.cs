﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Weigh.Models;
using PagedList;
using Microsoft.Reporting.WebForms;
using System.Runtime.InteropServices;
using PCMS.Helper;
using System.IO;
using System.Drawing;

namespace PCMS.Areas.Weigh.Controllers
{
    [Authorize]
    public class md_order_WeighingController : Controller
    {
        private WeighContext db = new WeighContext();
        private Material.Models.MaterialContext MaterialContext = new Areas.Material.Models.MaterialContext();
        private MasterData.Models.MasterDataContext MasterDataContext = new Areas.MasterData.Models.MasterDataContext();
        private System.IO.Ports.SerialPort port;
        private byte[] UserSign = null;
        private long? UserSignId = null;
        // GET: Weigh/md_order_Weighing
        [LogActionFilter]
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            DateTime regDate = DateTime.Now;
            // Auf dem Server:
            // var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day).ToList();

            // Waage 1:
            var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 1 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 1 || m.Weight2.ScaleNo == 0)).ToList();

            // Waage 2:
            //var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 2 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 2 || m.Weight2.ScaleNo == 0)).ToList();

            // Waage 3:
            //var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 3 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 3 || m.Weight2.ScaleNo == 0)).ToList();

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? PCMS.Helper.Definitions.pageSize));
        }
        [LogActionFilter]
        public ActionResult SelectDate(DateTime regDate, int state)
        {
            //var modelLoading = db.Md_order_LoadingOrder.Where(m => m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day).OrderBy(a => a.Sort).ToList();
            if(state == 3)
            {
                // Server:
                //var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day).ToList();

                // Waage 1:
                var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 1 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 1 || m.Weight2.ScaleNo == 0)).ToList();

                // Waage 2:
                //var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 2 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 2 || m.Weight2.ScaleNo == 0)).ToList();

                // Waage 3:
                //var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && (m.Weight1.ScaleNo == 3 || m.Weight1.ScaleNo == 0) && (m.Weight2.ScaleNo == 3 || m.Weight2.ScaleNo == 0)).ToList();

                return View("IndexContent", model.ToPagedList(1, PCMS.Helper.Definitions.pageSize));
            }
            else if (state == -1)
            {
                var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && m.Status < 2).ToList();
                return View("IndexContent", model.ToPagedList(1, PCMS.Helper.Definitions.pageSize));
            }
            else
            {
                var model = db.Md_order_Weighing.Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.RegistrationDate).Where(m => m.RegistrationDate.Year == regDate.Year && m.RegistrationDate.Month == regDate.Month && m.RegistrationDate.Day == regDate.Day && m.Status == state).ToList();
                return View("IndexContent", model.ToPagedList(1, PCMS.Helper.Definitions.pageSize));
            }
        }
        // GET: Weigh/md_order_Weighing/Details/5
        [LogActionFilter]
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Weighing md_order_Weighing = db.Md_order_Weighing.Find(id);
            if (md_order_Weighing == null)
            {
                return HttpNotFound();
            }
            if (md_order_Weighing.WeightId1 != null)
            {
                md_order_Weighing.Weight1 = db.md_Weigh.Find(md_order_Weighing.WeightId1);
            }
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2, se3 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text", md_order_Weighing.Direction);
            return View(md_order_Weighing);
        }

        // GET: Weigh/md_order_Weighing/Create
        [LogActionFilter]
        public ActionResult Create()
        {
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2, se3 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text");
            return View();
        }

        // POST: Weigh/md_order_Weighing/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [LogActionFilter]
        public ActionResult Create([Bind(Include = "Id,LicensePlate,RegistrationDate,WeightId1,WeightId2,CustomerDescription,CustomerId,ConstructionSiteDescription,ConstructionSiteId,ArticleDescription,OrderNumber,ArticleId,Direction,UserName1,UserName2,VehicleTaraWeight,VehicleType,CustomerStreet,CustomerCity,CustomerZIPCode,ConstructionSiteStreet,ConstructionSiteCity,ConstructionSiteZIPCode,ArticleNumber,ArticlePrice,ContingentNumber,Comment,Status,SealNumber,FuelTank,ContainerNo,AVVDescription,AVVNumber,ProducerId,ProducerDescription,ProducerStreet,ProducerCity,ProducerZIPCode,ProducerDateTime,ProducerNumber,CarrierId,CarrierDescription,CarrierStreet,CarrierCity,CarrierZIPCode,CarrierDateTime,CarrierNumber,DisposalId,DisposalDescription,DisposalStreet,DisposalCity,DisposalZIPCode,DisposalDateTime,DisposalNumber,DisposalProofNumber,VehicleNumber,EvenDelivery,EvenPickingUp,PickUpConatiner,UpContainer,Signature1Id,Signature2Id,Signature3Id, CustomerNumber, ConstructionSiteNumber")] md_order_Weighing md_order_Weighing, bool? print)
        {

            if (ModelState.IsValid)
            {
                md_order_Weighing.InvoiceCreated = false;
                md_order_Weighing.LicensePlate = md_order_Weighing.LicensePlate.ToUpper();
                md_order_Weighing.RegistrationDate = DateTime.Now;
                md_order_Weighing.Status = setStatus(md_order_Weighing);
                md_order_Weighing.UserName1 = User.Identity.Name;
                
                //md_order_Weighing.Signature1Id = lastSignature;

                if (md_order_Weighing.WeightId1 != null)
                {
                    md_order_Weighing.Status = 1;//1.Wägung
                   
                }
                if (md_order_Weighing.WeightId1 != null && md_order_Weighing.WeightId2 != null)
                {
                    md_order_Weighing.Status = 2;//2.Wägung
                   
                }

                // Manuelle Tara Eintrag Erstwiegung wird direkt als PT (-1) eingetragen
                if(md_order_Weighing.WeightId1 == null && md_order_Weighing.WeightId2 != null && md_order_Weighing.VehicleTaraWeight != null)
                {
                    md_Weigh weigh = new md_Weigh();
                    weigh.AlibiNo = -1;
                    weigh.Unit = "kg";
                    weigh.Weight = Convert.ToDecimal(md_order_Weighing.VehicleTaraWeight);
                    weigh.WightDateTime = DateTime.Now;

                    db.md_Weigh.Add(weigh);
                    db.SaveChanges();

                    md_order_Weighing.WeightId1 = weigh.Id;
                }

                md_order_Weighing.OrderNumber = (Helper.Definitions.getLoadinOrderNumber() +1 ).ToString();
                Helper.Definitions.setLoadinOrderNumber(long.Parse(md_order_Weighing.OrderNumber));
                db.Md_order_Weighing.Add(md_order_Weighing);
                db.SaveChanges();
                var mov = MaterialContext.Md_material_MaterialMoving.Where(a => a.OrderId == md_order_Weighing.Id).FirstOrDefault();
                //Material Moving Eintrag
                if (md_order_Weighing.WeightId2 != null && (md_order_Weighing.WeightId1 != null || md_order_Weighing.VehicleTaraWeight != null) && mov == null)
                {
                    addMaterialMovin(md_order_Weighing);
                }
                if (print != null && print == true)
                {
                    ReportDetail(md_order_Weighing.Id, true);
                }

                return RedirectToAction("Index");
            }
            if (md_order_Weighing.WeightId1 != null)
            {
                md_order_Weighing.Weight1 = db.md_Weigh.Find(md_order_Weighing.WeightId1);
            }
            if (md_order_Weighing.WeightId2 != null)
            {
                md_order_Weighing.Weight2 = db.md_Weigh.Find(md_order_Weighing.WeightId2);
            }
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2, se3 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text", md_order_Weighing.Direction);
            return View(md_order_Weighing);
        }
        // GET: Weigh/md_order_Weighing/Create
        [LogActionFilter]
        public ActionResult CreateManuell()
        {
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            //SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text");
            ViewBag.OrderNumber = (Helper.Definitions.getLoadinOrderNumber() + 1).ToString();
            return View();
        }

        // POST: Weigh/md_order_Weighing/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [LogActionFilter]
        public ActionResult CreateManuell([Bind(Include = "Id,LicensePlate,RegistrationDate,WeightId1,WeightId2,CustomerDescription,CustomerId,ConstructionSiteDescription,ConstructionSiteId,ArticleDescription,OrderNumber,ArticleId,Direction,UserName1,UserName2,VehicleTaraWeight,VehicleType,CustomerStreet,CustomerCity,CustomerZIPCode,ConstructionSiteStreet,ConstructionSiteCity,ConstructionSiteZIPCode,ArticleNumber,ArticlePrice,ContingentNumber,Comment,Status,SealNumber,FuelTank,ContainerNo,AVVDescription,AVVNumber,ProducerId,ProducerDescription,ProducerStreet,ProducerCity,ProducerZIPCode,ProducerDateTime,ProducerNumber,CarrierId,CarrierDescription,CarrierStreet,CarrierCity,CarrierZIPCode,CarrierDateTime,CarrierNumber,DisposalId,DisposalDescription,DisposalStreet,DisposalCity,DisposalZIPCode,DisposalDateTime,DisposalNumber,DisposalProofNumber,VehicleNumber,EvenDelivery,EvenPickingUp,PickUpConatiner,UpContainer,Signature1Id,Signature2Id,Signature3Id, CustomerNumber, ConstructionSiteNumber")] md_order_Weighing md_order_Weighing, bool? print,decimal? Weight1Manuell,decimal? Weight2Manuell)
        {

            if (ModelState.IsValid)
            {
                if(Weight1Manuell == null || Weight2Manuell == null)
                {
                    SelectListItem seE = new SelectListItem() { Text = "Eingang", Value = "0" };
                    SelectListItem seE2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
                    //SelectListItem se3E = new SelectListItem() { Text = "Barverkauf", Value = "2" };
                    List<SelectListItem> seEE = new List<SelectListItem>() { seE, seE2 };
                    ViewBag.Direction = new SelectList(seEE, "Value", "Text", md_order_Weighing.Direction);
                    ViewBag.ErrorMessage = "Erstwägung und Zweitwägung müssen ausgefüllt sein!";
                    return View(md_order_Weighing);
                    
                }
                md_order_Weighing.InvoiceCreated = false;
                md_order_Weighing.LicensePlate = md_order_Weighing.LicensePlate.ToUpper();
                md_order_Weighing.RegistrationDate = DateTime.Now;
                md_order_Weighing.Status = setStatus(md_order_Weighing);
                md_order_Weighing.UserName1 = User.Identity.Name;
                md_order_Weighing.Status = 2;

                md_Weigh weigh = new md_Weigh();
                weigh.WightDateTime = DateTime.Now;
                weigh.ScaleNo = 0;
                weigh.AlibiNo = -1;
                weigh.Weight = Weight1Manuell??0;
                weigh.Unit = "kg";
                weigh.UserName = User.Identity.Name;
                db.md_Weigh.Add(weigh);
                md_Weigh weigh2 = new md_Weigh();
                weigh2.WightDateTime = DateTime.Now;
                weigh2.ScaleNo = 0;
                weigh2.AlibiNo = -1;
                weigh2.Weight = Weight2Manuell??0;
                weigh2.Unit = "kg";
                weigh2.UserName = User.Identity.Name;
                db.md_Weigh.Add(weigh2);
                db.SaveChanges();
                md_order_Weighing.WeightId1 = weigh.Id ;
                md_order_Weighing.WeightId2 = weigh2.Id ;


               
                db.Md_order_Weighing.Add(md_order_Weighing);
                db.SaveChanges();
                //Material Moving Eintrag
                var mov = MaterialContext.Md_material_MaterialMoving.Where(a => a.OrderId == md_order_Weighing.Id).FirstOrDefault();
                if (md_order_Weighing.WeightId2 != null && (md_order_Weighing.WeightId1 != null || md_order_Weighing.VehicleTaraWeight != null) && mov == null)
                {
                    addMaterialMovin(md_order_Weighing);
                }
                if (print != null && print == true)
                {
                    ReportDetail(md_order_Weighing.Id, true);
                }
                Helper.Definitions.setLoadinOrderNumber(long.Parse(md_order_Weighing.OrderNumber));
                return RedirectToAction("Index");
            }
            if (md_order_Weighing.WeightId1 != null)
            {
                md_order_Weighing.Weight1 = db.md_Weigh.Find(md_order_Weighing.WeightId1);
            }
            if (md_order_Weighing.WeightId2 != null)
            {
                md_order_Weighing.Weight2 = db.md_Weigh.Find(md_order_Weighing.WeightId2);
            }
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            //SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text", md_order_Weighing.Direction);

            //Helper.Definitions.setLoadinOrderNumber(long.Parse(md_order_Weighing.OrderNumber));

            return View(md_order_Weighing);
        }
        // GET: Weigh/md_order_Weighing/Edit/5
        [LogActionFilter]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Weighing md_order_Weighing = db.Md_order_Weighing.Find(id);
            if (md_order_Weighing == null)
            {
                return HttpNotFound();
            }
            if (md_order_Weighing.WeightId1 != null)
            {
                md_order_Weighing.Weight1 = db.md_Weigh.Find(md_order_Weighing.WeightId1);
            }
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2, se3 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text", md_order_Weighing.Direction);
            return View(md_order_Weighing);
        }

        // POST: Weigh/md_order_Weighing/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [LogActionFilter]
        public ActionResult Edit([Bind(Include = "Id,LicensePlate,RegistrationDate,WeightId1,WeightId2,CustomerDescription,CustomerId,ConstructionSiteDescription,ConstructionSiteId,ArticleDescription,OrderNumber,ArticleId,Direction,UserName1,UserName2,VehicleTaraWeight,VehicleType,CustomerStreet,CustomerCity,CustomerZIPCode,ConstructionSiteStreet,ConstructionSiteCity,ConstructionSiteZIPCode,ArticleNumber,ArticlePrice,ContingentNumber,Comment,Status,SealNumber,FuelTank,ContainerNo,AVVDescription,AVVNumber,ProducerId,ProducerDescription,ProducerStreet,ProducerCity,ProducerZIPCode,ProducerDateTime,ProducerNumber,CarrierId,CarrierDescription,CarrierStreet,CarrierCity,CarrierZIPCode,CarrierDateTime,CarrierNumber,DisposalId,DisposalDescription,DisposalStreet,DisposalCity,DisposalZIPCode,DisposalDateTime,DisposalNumber,DisposalProofNumber,VehicleNumber,EvenDelivery,EvenPickingUp,PickUpConatiner,UpContainer,Signature1Id,Signature2Id,Signature3Id, CustomerNumber, ConstructionSiteNumber")] md_order_Weighing md_order_Weighing,bool? print)
        {
            if (ModelState.IsValid)
            {
                md_order_Weighing.InvoiceCreated = false;
                md_order_Weighing.LicensePlate = md_order_Weighing.LicensePlate.ToUpper();
                md_order_Weighing.Status = setStatus(md_order_Weighing);
                md_order_Weighing.UserName2 = User.Identity.Name;
                db.Entry(md_order_Weighing).State = EntityState.Modified;
                db.SaveChanges();
                var mov = MaterialContext.Md_material_MaterialMoving.Where(a => a.OrderId == md_order_Weighing.Id).FirstOrDefault();
                //Material Moving Eintrag
                if (md_order_Weighing.WeightId2 != null && (md_order_Weighing.WeightId1 != null  || md_order_Weighing.VehicleTaraWeight != null) &&  mov == null)
                {
                    addMaterialMovin(md_order_Weighing);
                    
                }
                if(print!= null && print == true)
                {
                    ReportDetail(md_order_Weighing.Id, true);
                }
                return RedirectToAction("Index");
            }
            SelectListItem se = new SelectListItem() { Text = "Eingang", Value = "0" };
            SelectListItem se2 = new SelectListItem() { Text = "Ausgang", Value = "1" };
            SelectListItem se3 = new SelectListItem() { Text = "Barverkauf", Value = "2" };
            List<SelectListItem> seL = new List<SelectListItem>() { se, se2, se3 };
            ViewBag.Direction = new SelectList(seL, "Value", "Text", md_order_Weighing.Direction);
            return View(md_order_Weighing);
        }
        public ActionResult ChooseVehicle(PCMS.Models.PaginationModel pg)
        {
            var model = MasterDataContext.Md_masterData_Vehicle.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_Customer).Include(m => m.md_Facilities).Include(m => m.md_masterData_Driver).Include(m => m.Md_masterData_VehicleType).OrderBy(a => a.VehicleNumber).AsQueryable();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "VehicleNumber":
                                model = model.Where(m => m.VehicleNumber != null && m.VehicleNumber.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.VehicleNumber = pgFF.colVal;
                                break;
                            case "PlateNumber":
                                model = model.Where(m => m.PlateNumber != null && m.PlateNumber.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.NumberPlate = pgFF.colVal;
                                break;
                            case "Capacity":
                                model = model.Where(m => m.Capacity != null && m.Capacity.ToString().ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Capacity = pgFF.colVal;
                                break;
                            case "Customer":
                                model = model.Where(m => m.Md_Customer.Name != null && m.Md_Customer.Name.ToString().ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Capacity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "VehicleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.VehicleNumber).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.VehicleNumber).AsQueryable();
                    break;
                case "PlateNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.PlateNumber).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.PlateNumber).AsQueryable();
                    break;
                case "Capacity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Capacity).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Capacity).AsQueryable();
                    break;
                case "Customer":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Customer.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_Customer.Name).AsQueryable();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // GET: Weigh/md_order_Weighing/Delete/5
        [LogActionFilter]
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_Weighing md_order_Weighing = db.Md_order_Weighing.Find(id);
            if (md_order_Weighing == null)
            {
                return HttpNotFound();
            }
            return View(md_order_Weighing);
        }

        // POST: Weigh/md_order_Weighing/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [LogActionFilter]
        public ActionResult DeleteConfirmed(long id)
        {
            md_order_Weighing md_order_Weighing = db.Md_order_Weighing.Find(id);
            //db.Md_order_Weighing.Remove(md_order_Weighing);
            md_order_Weighing.Status = 4;
            db.Entry(md_order_Weighing).State = EntityState.Modified;
            db.SaveChanges();
            var matmov = MaterialContext.Md_material_MaterialMoving.Where(a => a.OrderId == md_order_Weighing.Id).FirstOrDefault();
            if(matmov != null)
            {
                MaterialContext.Md_material_MaterialMoving.Remove(matmov);
            }
            MaterialContext.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            //port.Close();
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [LogActionFilter]
        public ActionResult getWeight(int no, int? scaleNo, long? WeightId)
        {

            //string sERG = port.ReadLine();
            //string[] stringPart = sERG.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            //string sWeight = stringPart[stringPart.Length - 1].Replace("g", "").Trim().Replace(".", ",");
            ////button1.Text = sWeight + "g";



            var w = Helper.WeighHelper.getWeightN(scaleNo ?? 1);
            if(w.errorCode != 0)
            {
                return Json(new { weigh = w });
            }
            else
            {
                
                md_Weigh weigh = new md_Weigh();
                weigh.WightDateTime = w.date;
                // Bemerkung Hakan Matzir: Eigentlich ist das auskommentierte richtig. Im Antwort von der Waage erhält man an welcher waage man wiegt!
                //weigh.ScaleNo = w.scaleNumber;
                weigh.ScaleNo = scaleNo ?? 1;
                weigh.AlibiNo = w.identNumber;
                weigh.Weight = w.net;
                weigh.Unit = w.unit;
                weigh.UserName = User.Identity.Name;
                db.md_Weigh.Add(weigh);
                db.SaveChanges();

                return Json(new {weigh= w,  weighDB= weigh });
            }
        }
        
        public ActionResult choosMaterial(PCMS.Models.PaginationModel pg)
        {

            //var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId != 1 && m.MaterialGroupId != 5).OrderBy(a => a.Id).ToList();
            var model = MasterDataContext.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Name).AsQueryable();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).AsQueryable();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Id).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.ShortName).AsQueryable();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).AsQueryable();
                    break;
            }

            ViewBag.MaterialGroup = new SelectList(MasterDataContext.Md_material_MaterialGroup, "Id", "Description", ViewBag.MaterialGroupId);
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        [HttpPost]
        public ActionResult getCustomer(long id)
        {
            var model = MasterDataContext.Md_masterData_Customer.Include(a=>a.Md_masterData_Contact).Where(a => a.Id == id).FirstOrDefault();
            model.Md_masterData_Contact = null;
            //model.Md_masterData_Contact.Companys = null;
            return Json(model);
        }
        [HttpPost]
        public ActionResult getVehicle(string number)
        {
            var model = MasterDataContext.Md_masterData_Vehicle.Include(a=>a.Md_Customer).Where(a => a.VehicleNumber == number && a.IsDeleted == false).FirstOrDefault();
            if (model == null)
                return null;
            if (model.Md_Customer != null)
            {
                model.Md_Customer.Md_masterData_Contact.Companys = null;
            }
            
            Areas.MasterData.Models.md_masterData_Vehicle veh = new MasterData.Models.md_masterData_Vehicle();
            veh = model;
            if (veh.Md_Customer != null)
            {
                veh.Md_Customer.Md_masterData_Contact = null;
            }
            if(veh.md_masterData_Driver != null && veh.md_masterData_Driver.Md_masterData_Contact != null)
            {
                veh.md_masterData_Driver.Md_masterData_Contact = null;
            }
            return Json(veh);
        }
        [HttpPost]
        public ActionResult getArticel(string number)
        {
            var model = MasterDataContext.Md_masterData_Material.Where(a => a.ArticleNumber == number && a.IsDeleted == false).FirstOrDefault();
            if (model == null)
                return null;
            
            return Json(model);
        }

        public ActionResult ChooseCustomer(PCMS.Models.PaginationModel pg)
        {
            var model = MasterDataContext.Md_masterData_Customer.Include(m => m.Md_masterData_Contact).Include(m => m.Md_masterData_Address).Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Name).AsQueryable();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).AsQueryable();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).AsQueryable();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult ChooseProducer(PCMS.Models.PaginationModel pg)
        {
            var model = MasterDataContext.Md_masterData_Customer.Include(m => m.Md_masterData_Contact).Include(m => m.Md_masterData_Address).Where(m => m.IsProducer == true && m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Name).AsQueryable();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.CustomerId = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Street = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.ZipCode = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.City = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).AsQueryable();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).AsQueryable();
                    break;
            }
            ViewBag.Url = Url.Action("ChooseProducer", "md_order_weighing", new { area = "Weigh" });
            ViewBag.Func = "ChooseProducer(this)";
            return View("ChooseCustomer",model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult ChooseCarrier(PCMS.Models.PaginationModel pg)
        {
            var model = MasterDataContext.Md_masterData_Customer.Include(m => m.Md_masterData_Contact).Include(m => m.Md_masterData_Address).Where(m => m.IsCarrier == true && m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).AsQueryable();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.CustomerId = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Street = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.ZipCode = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.City = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).AsQueryable();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).AsQueryable();
                    break;
            }
            ViewBag.Url = Url.Action("ChooseCarrier", "md_order_weighing", new { area = "Weigh" });
            ViewBag.Func = "ChooseCarrier(this)";
            return View("ChooseCustomer", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult ChooseDisposal(PCMS.Models.PaginationModel pg)
        {
            var model = MasterDataContext.Md_masterData_Customer.Include(m => m.Md_masterData_Contact).Include(m => m.Md_masterData_Address).Where(m => m.IsDisposal == true && m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).AsQueryable();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.CustomerId = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.Street = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.ZipCode = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.City = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).AsQueryable();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).AsQueryable();
                    break;
            }
            ViewBag.Url = Url.Action("ChooseDisposal", "md_order_weighing", new { area = "Weigh" });
            ViewBag.Func = "ChooseDisposal(this)";
            return View("ChooseCustomer", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult CheckConstructionSite(long id)
        {
            if (MasterDataContext.Md_masterData_ConstructionSite.Where(a => a.CustomerId == id).Count() > 0)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }
        public ActionResult ChooseConstructionSite(PCMS.Models.PaginationModel pg, long? id, string number)
        {
            var model = MasterDataContext.Md_masterData_ConstructionSite.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_masterData_Address).OrderBy(a => a.Name).AsQueryable();
            if (id != null)
            {
                var checkModel = MasterDataContext.Md_masterData_ConstructionSite.Where(m => m.IsActive == true && m.IsDeleted == false && m.CustomerId == id).ToList();
                if(checkModel.Count > 0)
                {
                    model = model.Where(a => a.CustomerId == id).AsQueryable();
                }
            }
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ConstructionSiteId":
                                model = model.Where(m => m.ConstructionSiteId != null && m.CustomerId == id && m.ConstructionSiteId.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.CustomerId == id && m.Name.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.CustomerId == id && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.CustomerId == id && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.CustomerId == id && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).AsQueryable();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "ConstructionSiteId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConstructionSiteId).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.ConstructionSiteId).AsQueryable();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Name).AsQueryable();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).AsQueryable();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).AsQueryable();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).AsQueryable();
                    break;
            }

            ViewBag.CustomerId = id;
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));

            //var model = db.Md_order_ConstructionSite.Where(a => a.CustomerId == id).ToList();
            //return View(model);



        }
        public ActionResult SelectConstructionSite(long id, PCMS.Areas.MasterData.Models.md_masterData_ConstructionSite model)
        {
            model = MasterDataContext.Md_masterData_ConstructionSite.Find(MasterDataContext.Md_masterData_ConstructionSite.Where(a => a.CustomerId == id).Select(a => a.Id).FirstOrDefault());

            if (model == null)
            {
                return Json(new { ConId = "", Name = "", Distance = "" });
            }
            else
            {
                model.Md_masterData_Customer.Md_masterData_Contact.Companys = null;
                model.Md_masterData_Customer.Md_masterData_Contact = null;
                return Json(model);
            }
        }
        public long setStatus(md_order_Weighing md_order_Weighing)
        {
            if ((md_order_Weighing.WeightId1 != null || md_order_Weighing.VehicleTaraWeight !=null )  && md_order_Weighing.WeightId2 != null)
            {
                return 2;//Beendet
            }

            else if (md_order_Weighing.WeightId1 != null)
            {
                return 1;//1.Wägung
            }
            else
            {
                return 0;//angelegt
            }

        }
        public void addMaterialMovin(md_order_Weighing md_order_Weighing)
        {
            Areas.Material.Models.md_material_MaterialMoving md_material_Moving = new Material.Models.md_material_MaterialMoving();
            md_material_Moving.UserId = "Waage";
            //Direction 0 = Eingang, 1 = Ausgang
            md_material_Moving.MovingType = md_order_Weighing.Direction;
            md_material_Moving.LineNo = 0;
            Areas.Material.Models.md_material_Location md_material_Location = null;
            //Location  active1 = active entnahme, active2 = aktive befüllung;
            if (md_order_Weighing.Direction == 0)
            {
                md_material_Location = MaterialContext.Md_material_Location.Where(a => a.MaterialId == md_order_Weighing.ArticleId && a.Active2 == true).FirstOrDefault();
            }
            else if (md_order_Weighing.Direction == 1)
            {
                md_material_Location = MaterialContext.Md_material_Location.Where(a => a.MaterialId == md_order_Weighing.ArticleId && a.Active1 == true).FirstOrDefault();
            }
            md_material_Moving.LocationNo = md_material_Location != null ? md_material_Location.Number : "";
            md_material_Moving.LocationMin = md_material_Location != null ? md_material_Location.Min : 0;
            md_material_Moving.LocationMax = md_material_Location != null ? md_material_Location.Max : 0;
            md_material_Moving.LocationDescription = md_material_Location != null ? md_material_Location.Description : "";
            md_material_Moving.LocationId = md_material_Location != null ? md_material_Location.Id : 0; // noch zu ändern;
            md_material_Moving.LocationType = md_material_Location != null && md_material_Location.Md_material_LocationGroup != null ? md_material_Location.Md_material_LocationGroup.Id : 0;

            Areas.MasterData.Models.md_masterData_Material md_masterData_Material = MasterDataContext.Md_masterData_Material.Where(a => a.Id == md_order_Weighing.ArticleId).FirstOrDefault();
            md_material_Moving.ArticleId = md_masterData_Material != null ? md_masterData_Material.Id : 0;
            md_material_Moving.ArticleDescription = md_masterData_Material != null ? md_masterData_Material.Name : "";
            md_material_Moving.ArticleNo = md_masterData_Material != null ? md_masterData_Material.ArticleNumber : "";
            md_material_Moving.RecipeDescription = null;
            md_material_Moving.RecipeId = null;
            md_material_Moving.RecipeVariant = null;
            md_material_Moving.ScaleNo = null;
            md_material_Moving.QuantitySetpoint = null;
            if (md_order_Weighing.WeightId1 != null)
            {
                md_order_Weighing.Weight1 = db.md_Weigh.Find(md_order_Weighing.WeightId1);
            }
            if (md_order_Weighing.WeightId2 != null)
            {
                md_order_Weighing.Weight2 = db.md_Weigh.Find(md_order_Weighing.WeightId2);
            }
            if (md_order_Weighing.WeightId1 == null)
                md_material_Moving.QuantityIs = (md_order_Weighing.VehicleTaraWeight != null ? md_order_Weighing.VehicleTaraWeight : 0) - (md_order_Weighing.Weight2 != null ? md_order_Weighing.Weight2.Weight : 0);
            else
                md_material_Moving.QuantityIs = (md_order_Weighing.Weight1 != null ? md_order_Weighing.Weight1.Weight : 0) - (md_order_Weighing.Weight2 != null ? md_order_Weighing.Weight2.Weight : 0) ;
            md_material_Moving.Unit = "kg";
            md_material_Moving.OrderId = md_order_Weighing.Id;
            md_material_Moving.OrderNo = md_order_Weighing.OrderNumber;
            md_material_Moving.OrderLineNo = null;
            md_material_Moving.LotNo = null;
            md_material_Moving.LotNoExtern = null;
            md_material_Moving.WarrantyDate = null;
            md_material_Moving.ExpirationDate = null;
            md_material_Moving.RegistrationDate = DateTime.Now;
            md_material_Moving.ApprovedTolPlus = null;
            md_material_Moving.ApprovedTolMinus = null;
            md_material_Moving.Trailing = null;
            md_material_Moving.ShiftFine = null;
            md_material_Moving.StartDate = null;
            if (md_order_Weighing.Weight1 != null)
                md_material_Moving.StartDate = md_order_Weighing.Weight1.WightDateTime;
            md_material_Moving.StopDate = null;
            if (md_order_Weighing.Weight2 != null)
                md_material_Moving.StopDate = md_order_Weighing.Weight2.WightDateTime;
            md_material_Moving.FacilitieId = 0;
            md_material_Moving.FacilitieId = null;
            md_material_Moving.Exported = null;
            md_material_Moving.Wet = null;
            md_material_Moving.BatchLine = null;
            //var del = MaterialContext.Md_material_MaterialMoving.Where(a => a.OrderId == md_order_Weighing.Id).FirstOrDefault();
            //MaterialContext.Md_material_MaterialMoving.Remove(del);
            MaterialContext.Md_material_MaterialMoving.Add(md_material_Moving);
            MaterialContext.SaveChanges();
        }
        [LogActionFilter]
        public ActionResult ReportDetailPrint(long? Id)
        {
            ReportDetail(Id, true);
            return Json( new { status = true });
            //return RedirectToAction("Index");
        }
        [LogActionFilter]
        public ActionResult ReportDetail(long? Id, bool? printr)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_order_Weighing.Find(Id);

                model.Weight1 = db.md_Weigh.Find(model.WeightId1);
                model.Weight2 = db.md_Weigh.Find(model.WeightId2);

                List<ReportParameter> repParams = new List<ReportParameter>();
                if(model.Direction == 0)
                {
                    repParams.Add(new ReportParameter("Title", "Wareneingang"));
                }
                if(model.Direction == 1)
                {
                    repParams.Add(new ReportParameter("Title", "Warenausgang"));
                }
                if(model.Direction == 2) // Barverkauf
                {
                    repParams.Add(new ReportParameter("Title", "Barverkauf"));

                }

                

                //if(model.Direction==0)
                //    repParams.Add(new ReportParameter("Title", "Wareneingang"));
                //else
                //    repParams.Add(new ReportParameter("Title", "Warenausgang"));
                repParams.Add(new ReportParameter("DeliveryNoteNo", model.OrderNumber));
                repParams.Add(new ReportParameter("CustomerNo", model.CustomerNumber + "\n" + model.CustomerDescription + "\n" + model.CustomerStreet + "\n" +  model.CustomerZIPCode + " " + model.CustomerCity));
                repParams.Add(new ReportParameter("ConstructionSiteName", model.ConstructionSiteNumber + "\n" + model.ConstructionSiteDescription + "\n" + model.ConstructionSiteStreet + "\n" + model.ConstructionSiteZIPCode + " " + model.ConstructionSiteCity));
                repParams.Add(new ReportParameter("ProducerName", model.ProducerDescription + "\n" + model.ProducerStreet + "\n" + model.ProducerZIPCode + " " + model.ProducerCity));

                

                repParams.Add(new ReportParameter("VehiclePlateNo", model.LicensePlate));
                string articelID = "";
                if(model.ArticleId != null)
                {
                    articelID = Convert.ToString( MasterDataContext.Md_masterData_Material.Where(a => a.Id == model.ArticleId).Select(a => a.MaterialNumber).FirstOrDefault());
                }

                repParams.Add(new ReportParameter("MaterialNo", model.ArticleNumber + ", " +model.ArticleDescription + "\n" + articelID));
                repParams.Add(new ReportParameter("ContainerNo", model.ContainerNo));
                if (model.WeightId1 == null && model.WeightId2 != null && model.VehicleTaraWeight != null)
                {
                    //PT oben
                    //repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.VehicleTaraWeight??0,0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("Weight1Alibi", "PT"));
                    //repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight,0 ).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo).ToString()));
                    //repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));

                    //PT unten
                    //repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.VehicleTaraWeight ?? 0, 0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("Weight2Alibi", "PT"));
                    //repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight2.Weight, 0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("Weight1Alibi", (model.Weight2.AlibiNo).ToString()));
                    //repParams.Add(new ReportParameter("Weight1Date", (model.Weight2.WightDateTime).ToString()));
                    //repParams.Add(new ReportParameter("Weight1No", (model.Weight2.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.VehicleTaraWeight ?? 0, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", "PT"));
                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo).ToString()));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight2No", (model.Weight2.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Netto", Math.Round(Math.Abs((model.VehicleTaraWeight -  model.Weight2.Weight) ?? 0),0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.VehicleTaraWeight).ToString()));

                    if (model.Direction == 2) // Barverkauf
                    {
                        

                        decimal? netWeight;

                        if (model.VehicleTaraWeight > model.Weight1.Weight)
                        {
                            netWeight = model.VehicleTaraWeight / 1000 - model.Weight1.Weight / 1000;
                        }
                        else
                        {
                            netWeight = model.Weight2.Weight / 1000 - model.VehicleTaraWeight / 1000;
                        }

                        decimal? Prize = model.ArticlePrice;
                        decimal? netEUR = netWeight * Prize;
                        decimal? mwst = netEUR * Convert.ToDecimal(0.19);
                        decimal? netSum = mwst + netEUR;

                        netSum = Math.Round(Convert.ToDecimal(netSum), 2);
                        netEUR = Math.Round(Convert.ToDecimal(netEUR), 2);
                        mwst = Math.Round(Convert.ToDecimal(mwst), 2);

                        repParams.Add(new ReportParameter("Prize", "Preis / t: " + Prize.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("Tax", "19% MwSt: " + mwst.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("Net", "Netto: " + netEUR.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("NetSum", "Gesamt: " + netSum.ToString() + " EUR"));
                    }

                }
                else if (model.WeightId1 != null && model.WeightId2 != null )
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight1.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", model.Weight1.AlibiNo  == -1 ? "PT" : model.Weight1.AlibiNo.ToString() ));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo == -1 ? "PT" : model.Weight2.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight2No", (model.Weight2.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Netto", Math.Round(Math.Abs((model.Weight1.Weight - model.Weight2.Weight)),0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                    if (model.Direction == 2) // Barverkauf
                    {
                        decimal? netWeight;

                        if (model.Weight1.Weight > model.Weight2.Weight)
                        {
                            netWeight = model.Weight1.Weight / 1000 - model.Weight2.Weight / 1000;
                        }
                        else
                        {
                            netWeight = model.Weight2.Weight / 1000 - model.Weight1.Weight / 1000;
                        }

                        decimal? Prize = model.ArticlePrice;
                        decimal? netEUR = netWeight * Prize;
                        decimal? mwst = netEUR * Convert.ToDecimal(0.19);
                        decimal? netSum = mwst + netEUR;

                        netSum = Math.Round(Convert.ToDecimal(netSum), 2);
                        netEUR = Math.Round(Convert.ToDecimal(netEUR), 2);
                        mwst = Math.Round(Convert.ToDecimal(mwst), 2);

                        repParams.Add(new ReportParameter("Prize", "Preis / t: " + Prize.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("Tax", "19% MwSt: " + mwst.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("Net", "Netto: " + netEUR.ToString() + " EUR"));
                        repParams.Add(new ReportParameter("NetSum", "Gesamt: " + netSum.ToString() + " EUR"));
                    }

                }
                else if (model.WeightId1 != null && model.WeightId2 == null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight1.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", (model.Weight1.AlibiNo == -1 ? "PT" : model.Weight1.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));

                    
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                }
                if (model.VehicleTaraWeight != null)
                {
                    repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                    if(model.Weight2 != null)
                    {
                        repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                        repParams.Add(new ReportParameter("Solas", ((model.Weight2.Weight) - (model.VehicleTaraWeight ?? 0)).ToString() + "kg"));
                    }
                    
                    
                }
                repParams.Add(new ReportParameter("SealNumber", model.SealNumber ?? ""));
                repParams.Add(new ReportParameter("CommentText", model.Comment ?? ""));
                repParams.Add(new ReportParameter("Date", model.RegistrationDate.ToShortDateString() + " " + model.RegistrationDate.ToShortTimeString()));

                if (model.Direction == 2) // Barverkauf
                {

                    decimal? netWeight;
                    
                    if(model.VehicleTaraWeight > model.Weight2.Weight)
                    {
                        netWeight = model.VehicleTaraWeight / 1000 - model.Weight2.Weight / 1000;
                    }
                    else
                    {
                        netWeight = model.Weight2.Weight / 1000 - model.VehicleTaraWeight / 1000;
                    }
                    decimal? Prize = model.ArticlePrice;
                    decimal? netEUR = netWeight * Prize;
                    decimal? mwst = netEUR * Convert.ToDecimal(0.19);
                    decimal? netSum = mwst + netEUR;

                    netSum = Math.Round(Convert.ToDecimal(netSum), 2);
                    netEUR = Math.Round(Convert.ToDecimal(netEUR), 2);
                    mwst = Math.Round(Convert.ToDecimal(mwst), 2);

                    repParams.Add(new ReportParameter("Prize", "Preis / t: " + Prize.ToString() + " EUR"));
                    repParams.Add(new ReportParameter("Tax", "19% MwSt: " + mwst.ToString() + " EUR"));
                    repParams.Add(new ReportParameter("Net", "Netto: " + netEUR.ToString() + " EUR"));
                    repParams.Add(new ReportParameter("NetSum", "Gesamt: " + netSum.ToString() + " EUR"));
                }

                var weightContext = new Areas.Weigh.Models.WeighContext();
                if (model.Signature1Id != null)
                {
                    byte[] img = weightContext.Md_order_weighting_Signature.Where(a => a.Id == model.Signature1Id).Select(a => a.Signature).FirstOrDefault();
                    repParams.Add(new ReportParameter("Siganture1", Convert.ToBase64String(img)));
                    repParams.Add(new ReportParameter("Siganture1Show", "0"));
                }
                else
                {
                    repParams.Add(new ReportParameter("Siganture1Show", "1"));
                }
                if (model.Signature2Id != null)
                {
                    byte[] img = weightContext.Md_order_weighting_Signature.Where(a => a.Id == model.Signature2Id).Select(a => a.Signature).FirstOrDefault();
                    repParams.Add(new ReportParameter("Siganture2", Convert.ToBase64String(img)));
                    repParams.Add(new ReportParameter("Siganture2Show", "0"));
                }
                else
                {
                    repParams.Add(new ReportParameter("Siganture2Show", "1"));
                }
                if (model.Signature3Id != null)
                {
                    byte[] img = weightContext.Md_order_weighting_Signature.Where(a => a.Id == model.Signature3Id).Select(a => a.Signature).FirstOrDefault();
                    repParams.Add(new ReportParameter("Siganture3", Convert.ToBase64String(img)));
                    repParams.Add(new ReportParameter("Siganture3Show", "0"));
                }
                else
                {                    
                    repParams.Add(new ReportParameter("Siganture3Show", "1"));
                }
                



                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Weigh/Weigh.rdlc";



                PCMSDataSet ds = new PCMSDataSet();
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());
                reportViewer.LocalReport.DataSources.Add(DSReport);

                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                

                if(printr != null && printr == true)
                {
                    Helper.DeliveryNotePrint.Export(reportViewer.LocalReport);
                    Helper.DeliveryNotePrint.Print();
                    return null;
                }
                //
                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
        public ActionResult ReportTakeover(long? Id, bool? printr)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_order_Weighing.Find(Id);


                List<ReportParameter> repParams = new List<ReportParameter>();
                
                repParams.Add(new ReportParameter("DeliveryNoteNo", model.OrderNumber));
                repParams.Add(new ReportParameter("AVVDescription", model.AVVDescription));
                repParams.Add(new ReportParameter("AVVNumber", model.AVVNumber));
                repParams.Add(new ReportParameter("DisposalProofNumber", model.DisposalProofNumber));
                
                repParams.Add(new ReportParameter("ProducerNumber", model.ProducerNumber));
                repParams.Add(new ReportParameter("ProducerDateTime", model.ProducerDateTime != null ? model.ProducerDateTime.Value.ToShortDateString() : "" ));
                repParams.Add(new ReportParameter("ProducerDescription", model.ProducerDescription + "\n" + model.ProducerStreet + "\n" + model.ProducerCity + " " + model.ProducerZIPCode));
                repParams.Add(new ReportParameter("CarrierNumber", model.CarrierNumber));
                repParams.Add(new ReportParameter("CarrierDateTime", model.CarrierDateTime != null ? model.CarrierDateTime.Value.ToShortDateString() : ""));
                repParams.Add(new ReportParameter("CarrierDescription", model.CarrierDescription + "\n" + model.CarrierStreet + "\n" + model.CarrierCity + " " + model.CarrierZIPCode));
                repParams.Add(new ReportParameter("DisposalNumber", model.DisposalNumber));
                repParams.Add(new ReportParameter("DisposalDateTime", model.DisposalDateTime != null ? model.DisposalDateTime.Value.ToShortDateString() : ""));
                repParams.Add(new ReportParameter("DisposalDescription", model.DisposalDescription + "\n" + model.DisposalStreet + "\n" + model.DisposalCity + " " + model.DisposalZIPCode));


                if (model.WeightId1 == null && model.WeightId2 != null && model.VehicleTaraWeight != null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", model.VehicleTaraWeight.ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", "PT"));
                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo).ToString()));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Netto", (model.VehicleTaraWeight - model.Weight2.Weight).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.VehicleTaraWeight).ToString()));

                }
                else if (model.WeightId1 != null && model.WeightId2 != null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight1.Weight, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", model.Weight1.AlibiNo == -1 ? "PT" : model.Weight1.AlibiNo.ToString()));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo == -1 ? "PT" : model.Weight2.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight2No", (model.Weight2.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Netto", (model.Weight1.Weight - model.Weight2.Weight).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                }
                else if (model.WeightId1 != null && model.WeightId2 == null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight1.Weight, 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", (model.Weight1.AlibiNo == -1 ? "PT" : model.Weight1.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));


                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                }
                if (model.VehicleTaraWeight != null)
                {
                    repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                    if (model.Weight2 != null)
                    {
                        repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                    }
                    repParams.Add(new ReportParameter("Solas", ((model.Weight2.Weight) - (model.VehicleTaraWeight ?? 0)).ToString() + "kg"));

                }
                repParams.Add(new ReportParameter("SealNumber", model.SealNumber ?? ""));
                repParams.Add(new ReportParameter("CommentText", model.Comment ?? ""));
                repParams.Add(new ReportParameter("Date", model.RegistrationDate.ToShortDateString() + " " + model.RegistrationDate.ToShortTimeString()));

                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Weigh/WeighTakeover.rdlc";

                PCMSDataSet ds = new PCMSDataSet();
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());
                reportViewer.LocalReport.DataSources.Add(DSReport);

                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                

                if (printr != null && printr == true)
                {
                    Helper.DeliveryNotePrint.Export(reportViewer.LocalReport);
                    Helper.DeliveryNotePrint.Print();
                    return null;
                }
                //
                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        }
        public ActionResult ReportDetailSolas(long? Id, bool? printr)
        {
            try
            {
                //var model = db.Md_recipe_Recipe.Find(Id);
                var model = db.Md_order_Weighing.Find(Id);


                List<ReportParameter> repParams = new List<ReportParameter>();
                if (model.Status == 0)
                    repParams.Add(new ReportParameter("Title", "Wareneingang"));
                else
                    repParams.Add(new ReportParameter("Title", "Warenausgang"));
                repParams.Add(new ReportParameter("DeliveryNoteNo", model.OrderNumber));
                repParams.Add(new ReportParameter("CustomerNo", model.CustomerDescription + "\n" + model.CustomerStreet + "\n" + model.CustomerCity + " " + model.CustomerZIPCode));
                repParams.Add(new ReportParameter("ConstructionSiteName", model.ConstructionSiteDescription + "\n" + model.ConstructionSiteStreet + "\n" + model.ConstructionSiteCity + " " + model.ConstructionSiteZIPCode));

                repParams.Add(new ReportParameter("VehiclePlateNo", model.LicensePlate));
                repParams.Add(new ReportParameter("MaterialNo", model.ArticleDescription));
                repParams.Add(new ReportParameter("ContainerNo", model.ContainerNo));

                if (model.WeightId1 == null && model.WeightId2 != null && model.VehicleTaraWeight != null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", model.VehicleTaraWeight.ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", "PT"));
                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo).ToString()));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Netto", (model.VehicleTaraWeight - model.Weight2.Weight).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.VehicleTaraWeight).ToString()));

                }
                else if (model.WeightId1 != null && model.WeightId2 != null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round(model.Weight1.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", model.Weight1.AlibiNo == -1 ? "PT" : model.Weight1.AlibiNo.ToString()));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Weight2Weight", Math.Round(model.Weight2.Weight,0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight2Alibi", (model.Weight2.AlibiNo == -1 ? "PT" : model.Weight2.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight2Date", (model.Weight2.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight2No", (model.Weight2.ScaleNo).ToString()));

                    repParams.Add(new ReportParameter("Netto", Math.Round((model.Weight1.Weight - model.Weight2.Weight),0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Solas", Math.Round((Math.Abs((model.Weight1.Weight - model.Weight2.Weight)) + (model.FuelTank ?? 0)),0).ToString() + "kg"));
                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                }
                else if (model.WeightId1 != null && model.WeightId2 == null)
                {
                    repParams.Add(new ReportParameter("Weight1Weight", Math.Round((model.Weight1.Weight),0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("Weight1Alibi", (model.Weight1.AlibiNo == -1 ? "PT" : model.Weight1.AlibiNo.ToString())));
                    repParams.Add(new ReportParameter("Weight1Date", (model.Weight1.WightDateTime).ToString()));
                    repParams.Add(new ReportParameter("Weight1No", (model.Weight1.ScaleNo).ToString()));


                    //repParams.Add(new ReportParameter("ConstructionSiteNo", (model.Weight2.Weight - model.Weight1.Weight).ToString()));

                }
                repParams.Add(new ReportParameter("FuelTank", Math.Round((model.FuelTank ?? 0),0).ToString() + "kg"));
                
                /*if (model.VehicleTaraWeight != null)
                {
                    repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                    repParams.Add(new ReportParameter("FuelTank", (model.FuelTank ?? 0).ToString() + "kg"));
                    
                    if (model.Weight2 != null)
                    {
                        repParams.Add(new ReportParameter("VehicleMass", (model.VehicleTaraWeight ?? 0).ToString() + "kg"));
                    }
                    repParams.Add(new ReportParameter("Solas", ((model.Weight2.Weight) - (model.VehicleTaraWeight ?? 0) - (model.FuelTank ?? 0)).ToString() + "kg"));

                }*/
                repParams.Add(new ReportParameter("SealNumber", model.SealNumber ?? ""));
                repParams.Add(new ReportParameter("CommentText", model.Comment ?? ""));
                repParams.Add(new ReportParameter("Date", model.RegistrationDate.ToShortDateString() + " " + model.RegistrationDate.ToShortTimeString()));

                
                


                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = "Reports/Weigh/WeighSolas.rdlc";

                PCMSDataSet ds = new PCMSDataSet();
                PCMSDataSet dsImg = new PCMSDataSet();
                ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());
                ReportDataSource DSReportImg = new ReportDataSource("DataSetImage", ds.List.ToList());
                reportViewer.LocalReport.DataSources.Add(DSReport);
                reportViewer.LocalReport.DataSources.Add(DSReport);

                reportViewer.LocalReport.SetParameters(repParams);

                reportViewer.ShowPrintButton = true;
                reportViewer.ShowParameterPrompts = true;
                reportViewer.ShowBackButton = true;
                //reportViewer.DocumentMapWidth = 1000;
                reportViewer.Width = 800;
                reportViewer.Height = 700;

                if (printr != null && printr == true)
                {
                    Helper.DeliveryNotePrint.Export(reportViewer.LocalReport);
                    Helper.DeliveryNotePrint.Print();
                    return null;
                }
                //
                //------------------
                string mimeType = "";
                string encoding = "";
                string filenameExtension = "";
                string[] streamids = null;
                Warning[] warnings = null;

                byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                return File(file, mimeType);

                //ViewBag.ReportViewer = reportViewer;
                //return View("Report");
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception", e);
            }
        } 
        public ActionResult sign(int? padNo,long? id)
        {
            if (Helper.SignoTecHelper._processState != SignoTecHelper.ProcessState.Start)
                Helper.SignoTecHelper.CancelProcess();

            if (Helper.SignoTecHelper.selectedDevice == -1)
            {
                Helper.SignoTecHelper.init(padNo ?? 1);
            }
            
            if (Helper.SignoTecHelper.selectedDevice != -1  )
            {
                if (id != null)
                {
                    var weith = db.Md_order_Weighing.Find(id);
                    string message = "";
                    message += (weith.WeightId1 != null) ? weith.Weight1.Weight.ToString() : "";
                    message += "\n";
                    message += (weith.WeightId2 != null) ? weith.Weight2.Weight.ToString() : "";
                    message += "\n--------------";
                    message += "\n";
                    message += (weith.WeightId1 != null) && (weith.WeightId2 != null) ? (weith.Weight1.Weight - weith.Weight2.Weight).ToString() : "";

                    Helper.SignoTecHelper.StartCaprute(message);
                }
                else
                {
                     Helper.SignoTecHelper.StartCaprute("");
                }
                return Json(new { status = true });
            }
            return Json(new { status = false, error= Helper.SignoTecHelper.lastError });
        }
        public ActionResult checkState()
        {
              return Json( new { status = Convert.ToInt32(Helper.SignoTecHelper._processState), id = Helper.SignoTecHelper.lastId });
        }
        [HttpGet]
        public ActionResult lastSignature(long id)
        {
            var img = db.Md_order_weighting_Signature.Find(id);

            if(img!=null)
            return base.File(img.Signature, "image/jpeg");

            return Json(false);
        }
        public ActionResult getUserSign()
        {
            Admin.Models.AdminContext dba = new Admin.Models.AdminContext();
            try
            {
                UserSign = dba.AspNetUsers.Where(a => a.UserName == User.Identity.Name).Select(a => a.Sign).FirstOrDefault();

                if (UserSign != null)
                {
                    var weightContext = new Areas.Weigh.Models.WeighContext();
                    SignoTecHelper.lastSignauter = (Bitmap)SignoTecHelper.byteArrayToImage(UserSign);
                    md_order_weighting_Signature sig = new Areas.Weigh.Models.md_order_weighting_Signature();
                    sig.Signature = UserSign;
                    weightContext.Md_order_weighting_Signature.Add(sig);
                    weightContext.SaveChanges();

                    return Json(sig.Id);
                }
                else
                {
                    return Json(false);
                }
            }
            catch
            {
                return Json(false);
            }
        }
    }
}
