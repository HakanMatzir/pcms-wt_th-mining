﻿using PCMS.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Areas.Weigh.Models
{

    [LogActionFilter]
    [Authorize]
    public class WeighContext : DbContext
    {
        public WeighContext() : base("DefaultConnection")
        {
            //Database.SetInitializer<WeighContext>(new CreateDatabaseIfNotExists<WeighContext>());
            // Database.SetInitializer<WeighContext>(new DropCreateDatabaseIfModelChanges<WeighContext>());
            // Database.SetInitializer<WeighContext>(new Cre<WeighContext>());

        }
        public virtual DbSet<md_order_Weighing> Md_order_Weighing { get; set; }
        public virtual DbSet<md_order_weighting_Signature> Md_order_weighting_Signature { get; set; }

        public System.Data.Entity.DbSet<PCMS.Areas.Weigh.Models.md_Weigh> md_Weigh { get; set; }
    }
    [Table("md_order_Weighing")]
    public partial class md_order_Weighing
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("KFZ-Kennzeichen*"), Required]
        public string LicensePlate { get; set; }
        [DisplayName("KFZ-Nummer")]
        public string VehicleNumber { get; set; }        
        [DisplayName("Leergewicht")]
        public decimal? VehicleTaraWeight { get; set; }
        public long? VehicleType { get; set; }
        public DateTime RegistrationDate { get; set; }
        public long? WeightId1 { get; set; }
        public long? WeightId2 { get; set; }
        [DisplayName("Kunde")]
        public string CustomerDescription { get; set; }
        [DisplayName("Straße")]
        public string CustomerStreet { get; set; }
        [DisplayName("Plz")]
        public string CustomerZIPCode { get; set; }
        [DisplayName("Stadt")]
        public string CustomerCity { get; set; }
        public long? CustomerId { get; set; }
        [DisplayName("Baustelle")]
        public string ConstructionSiteDescription { get; set; }
        [DisplayName("Straße")]
        public string ConstructionSiteStreet { get; set; }
        [DisplayName("Plz")]
        public string ConstructionSiteZIPCode { get; set; }
        [DisplayName("Stadt")]
        public string ConstructionSiteCity { get; set; }
        public long? ConstructionSiteId { get; set; }
        [DisplayName("Material")]
        public string ArticleDescription { get; set; }
        [DisplayName("Nummer")]
        public string ArticleNumber { get; set; }
        [DisplayName("Preis")]
        public decimal? ArticlePrice { get; set; }
        [DisplayName("Lieferscheinnummer")]
        public string OrderNumber { get; set; }
        public string ContingentNumber { get; set; }
        public long? ArticleId { get; set; }
        [DisplayName("_")]
        public int Direction { get; set; }
        [DisplayName("Status")]
        public long Status { get; set; }
        [DisplayName("Bemerkung")]
        public string Comment { get; set; }
        [DisplayName("Plombennummer")]
        public string SealNumber { get; set; }
        [DisplayName("Containerleergewicht")]
        public decimal? FuelTank { get; set; }
        [DisplayName("Container-Nr")]
        public string ContainerNo { get; set; }

        [DisplayName("Abfallbezeichnung²")]
        public string AVVDescription { get; set; }
        [DisplayName("Abfallschlüssel²")]
        public string AVVNumber { get; set; }
        [DisplayName("Entsorgungsnachweis-Nummer")]
        public string DisposalProofNumber { get; set; }
        


        public long? ProducerId { get; set; }
        [DisplayName("Spediteur")]
        public string ProducerDescription { get; set; }
        [DisplayName("Straße")]
        public string ProducerStreet { get; set; }
        [DisplayName("Stadt")]
        public string ProducerCity { get; set; }
        [DisplayName("Plz")]
        public string ProducerZIPCode { get; set; }
        [DisplayName("Datum der Übergabe")]
        public DateTime? ProducerDateTime { get; set; }
        [DisplayName("Erzeuger-Nr.")]
        public string ProducerNumber { get; set; }

        public long? CarrierId { get; set; }
        [DisplayName("Name")]
        public string CarrierDescription { get; set; }
        [DisplayName("Straße")]
        public string CarrierStreet { get; set; }
        [DisplayName("Stadt")]
        public string CarrierCity { get; set; }
        [DisplayName("Plz")]
        public string CarrierZIPCode { get; set; }
        [DisplayName("Datum der Übernahme")]
        public DateTime? CarrierDateTime { get; set; }
        [DisplayName("Beförderer-Nr.")]
        public string CarrierNumber { get; set; }


        public long? DisposalId { get; set; }
        [DisplayName("Name")]
        public string DisposalDescription { get; set; }
        [DisplayName("Straße")]
        public string DisposalStreet { get; set; }
        [DisplayName("Stadt")]
        public string DisposalCity { get; set; }
        [DisplayName("Plz")]
        public string DisposalZIPCode { get; set; }
        [DisplayName("Datum der Annahme")]
        public DateTime? DisposalDateTime { get; set; }
        [DisplayName("Entsorger-Nr.")]
        public string DisposalNumber { get; set; }


        [DisplayName("Container-Aufstellen")]
        public string UpContainer { get; set; }
        [DisplayName("Container-Abholen")]
        public string PickUpConatiner { get; set; }
        [DisplayName("Selbstabholung")]
        public bool EvenPickingUp { get; set; }
        [DisplayName("Selbstanlieferung")]
        public bool EvenDelivery { get; set; }


        public string UserName1 { get; set; }
        public string UserName2 { get; set; }

        public long? Signature1Id { get; set; }
        public long? Signature2Id { get; set; }
        public long? Signature3Id { get; set; }

        [DisplayName("Kundennr.:")]
        public string CustomerNumber { get; set; }
        [DisplayName("Baustellennr.:")]
        public string ConstructionSiteNumber { get; set; }

        [DisplayName("Rechnung gestellt")]
        public bool InvoiceCreated { get; set; }

        [ForeignKey("WeightId1")]
        public virtual md_Weigh Weight1 { get; set; }
        [ForeignKey("WeightId2")]
        public virtual md_Weigh Weight2 { get; set; }

    }
    public partial class md_Weigh
    {
        [Key]
        public long Id { get; set; }
        public long AlibiNo { get; set; }
        public DateTime WightDateTime { get; set; }
        public byte[] Picture { get; set; }
        public decimal Weight { get; set; }
        public int ScaleNo { get; set; }
        public string Unit { get; set; }
        public string UserName { get; set; }
    }
    public partial class md_order_weighting_Signature
    {
        [Key]
        public long Id { get; set; }
        public byte[] Signature { get; set; }
    }


    public partial class ScaleRead
    {
        public ScaleRead()
        {

        }
        public ScaleRead(string values)
        {
            try
            {
                errorCode = int.Parse(values.Substring(0, 2));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                //status = bool.Parse(values.Substring(2, 1));
                string a = values.Substring(2, 1);
                status = a == "1" ? true : false;
            }
            catch (Exception e)
            {

                var a = 3;
            }
            try
            {
                //sign = bool.Parse(values.Substring(3, 1));
                
                sign = Convert.ToBoolean( Convert.ToInt32( values.Substring(3, 1)));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                date = Convert.ToDateTime(values.Substring(4, 8) + " " + values.Substring(12, 5));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                identNumber = int.Parse(values.Substring(17, 4));
            }
            catch (Exception e)
            {

            }
            try
            {
                scaleNumber = int.Parse(values.Substring(21, 1));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                gross = decimal.Parse(values.Substring(22, 8));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                tara = decimal.Parse(values.Substring(30, 8));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                net = decimal.Parse(values.Substring(38, 8));
            }
            catch (Exception e)
            {
                var s = e.Message;

            }
            try
            {
                unit = values.Substring(46, 2);
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                taracode = values.Substring(48, 2);
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                var stes = values.Substring(50, 1);
                if(stes!=" ")
                    weightingArea = int.Parse(values.Substring(50, 1));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                terminalNumber = int.Parse(values.Substring(51, 3));
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
            try
            {
                checksum = values.Substring(54, 8);
            }
            catch (Exception e)
            {
                var s = e.Message;
            }
        }

        public int errorCode { get; set; }
        public bool status { get; set; }
        public bool sign { get; set; }
        public DateTime date { get; set; }
        public int identNumber { get; set; }
        public int scaleNumber { get; set; }
        public decimal gross { get; set; }
        public decimal tara { get; set; }
        public decimal net { get; set; }
        public string unit { get; set; }
        public string taracode { get; set; }
        public int weightingArea { get; set; }
        public int terminalNumber { get; set; }
        public string checksum { get; set; }

        public string getErrorString
        {
            get
            {
                return ErrorToString(errorCode);
            }

        }
        public static string ErrorToString(int ErrorI)
        {
            string Error = ErrorI.ToString();
            string sRetVal = "";

            switch (Error)
            {
                case "-99":
                    sRetVal = "Received wrong length!";
                    break;
                case "-2":
                    sRetVal = "CE NAK Transmitted";
                    break;
                case "-1":
                    sRetVal = "CE NAK Received";
                    break;
                case "0":
                case "00":
                    sRetVal = "No Error";
                    break;
                case "1":
                    sRetVal = "Receive Queue Overflow";
                    break;
                case "2":
                    sRetVal = "Receive Overrun Error";
                    break;
                case "4":
                    sRetVal = "Receive Parity Error";
                    break;
                case "8":
                    sRetVal = "Receive Framing Error";
                    break;
                case "11":  //waage
                    sRetVal = "Allgeminer Waagenfehler";
                    break;
                case "12":  //waage
                    sRetVal = "Waage in Überlast";
                    break;
                case "13":  //waage
                    sRetVal = "Waage in Bewegung";
                    break;
                case "14":  //waage
                    sRetVal = "Waage nicht Verfügbar";
                    break;
                case "15":  //waage
                    sRetVal = "Tarierungsfehler";
                    break;
                case "16":  //waage
                    sRetVal = "Break Detected / Gewichts-Drucker nicht bereit";
                    break;
                case "17":  //waage
                    sRetVal = "Druckmuster enthählt ungültiges Kommando";
                    break;
                case "31":  //waage
                    sRetVal = "Übertragungsfehler";
                    break;
                case "32":  //waage
                    sRetVal = "File IO Error / Ungültiger Befehl";
                    break;
                case "33":  //waage
                    sRetVal = "Ungültiger Paramter";
                    break;
                //case "16":
                //    sRetVal = "Break Detected";
                //    break;
                //case "32":
                //    sRetVal = "File IO Error";
                //    break;
                case "256":
                    sRetVal = "TX Queue Is Full";
                    break;
                case "512":
                    sRetVal = "Timeout";
                    break;
                case "1024":
                    sRetVal = "Common Error";
                    break;
                case "1025":
                    sRetVal = "Unknown Connection Number";
                    break;
                case "1026":
                    sRetVal = "Port not open";
                    break;
                case "1027":
                    sRetVal = "Unknown Multidrop-Address";
                    break;
                case "1028":
                    sRetVal = "Device is not connected";
                    break;
                case "-999":
                    sRetVal = "ComPlus is not running";
                    break;
            }


            return sRetVal;
        }

        
    }

}