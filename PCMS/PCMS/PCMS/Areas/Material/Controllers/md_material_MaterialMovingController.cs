﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Material.Models;
using PCMS.Areas.Order.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System.Data.Entity.Core.Objects;
using System.Text;
using System.IO;


namespace PCMS.Areas.Material.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_material_MaterialMovingController : Controller
    {
        public static string csvExport;
        private MaterialContext db = new MaterialContext();
        public static IPagedList<md_material_MaterialMoving> liste;
        public ActionResult Index(PaginationModel pg)
        {
            DateTime Time1 = DateTime.Now;
            var model = pageModel2(pg);
            var ma = model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize);
            liste = ma;
            DateTime Time2 = DateTime.Now;
            TimeSpan ts = Time2 - Time1;
            ViewBag.Milliseconds = ts.TotalSeconds + "s";
            return View("Index", ma);
        }

        public ActionResult Index2(PaginationModel pg)
        {
            DateTime Time1 = DateTime.Now;
            var model = pageModel2(pg);
            var ma = model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize);
            DateTime Time2 = DateTime.Now;
            TimeSpan ts = Time2 - Time1;
            ViewBag.Milliseconds = ts.TotalSeconds + "s";
            return View("Index", ma);
        }
        // CSV-Export
        public FileResult CSVExport()
        {
            // Laden der angezeigten Datensätze in eine Liste
            PaginationModel pg2 = new PaginationModel();
            var ma = pageModel2(pg2);
            var model = liste;

            // Überschrift der CSV
            csvExport = "Bewegungsart;Fahrzeugkennzeichen;Artikelnummer;Artikel;Kundennummer;Kunde;Baustellennummer;Baustelle;Lieferscheinnummer;Lieferscheindatum;Erstgewicht;Datum Erstgewicht;Zweitgewicht;Datum Zweitgewicht;Benutzer;\n";


            // Einträge in die CSV
            for (int i = 0; i < model.Count; i++)
            {
                // Materialbewegung
                string direction;
                if(model[i].Md_order_Weighing.Direction == 0)
                {
                    direction = "Eingang";
                }
                else if(model[i].Md_order_Weighing.Direction == 1)
                {
                    direction = "Ausgang";
                }
                else if(model[i].Md_order_Weighing.Direction == 2)
                {
                    direction = "Barverkauf";
                }
                else
                {
                    direction = "N.A.";
                }

                // Eintrag Bei Erst- und Zweitwiegung
                if (model[i].Md_order_Weighing != null && model[i].Md_order_Weighing.WeightId1 != null && model[i].Md_order_Weighing.WeightId2 != null)
                {
                    csvExport += direction + ";" + model[i].Md_order_Weighing.LicensePlate + ";" + model[i].Md_order_Weighing.ArticleNumber + ";" + model[i].Md_order_Weighing.ArticleDescription + ";" + model[i].Md_order_Weighing.CustomerNumber + ";" + model[i].Md_order_Weighing.CustomerDescription + ";" + model[i].Md_order_Weighing.ConstructionSiteNumber + ";" + model[i].Md_order_Weighing.ConstructionSiteDescription + ";" + model[i].Md_order_Weighing.OrderNumber + ";" + model[i].Md_order_Weighing.RegistrationDate + ";" + model[i].Md_order_Weighing.Weight1.Weight + ";" + model[i].Md_order_Weighing.Weight1.WightDateTime + ";" + model[i].Md_order_Weighing.Weight2.Weight + ";" + model[i].Md_order_Weighing.Weight2.WightDateTime + ";" + model[i].Md_order_Weighing.UserName1 + "\n";
                }
                // Eintrag bei nur Erstwiegung
                else if(model[i].Md_order_Weighing != null && model[i].Md_order_Weighing.WeightId1 != null && model[i].Md_order_Weighing.WeightId2 == null)
                {
                    csvExport += direction + ";" + model[i].Md_order_Weighing.LicensePlate + ";" + model[i].Md_order_Weighing.ArticleNumber + ";" + model[i].Md_order_Weighing.ArticleDescription + ";" + model[i].Md_order_Weighing.CustomerNumber + ";" + model[i].Md_order_Weighing.CustomerDescription + ";" + model[i].Md_order_Weighing.ConstructionSiteNumber + ";" + model[i].Md_order_Weighing.ConstructionSiteDescription + ";" + model[i].Md_order_Weighing.OrderNumber + ";" + model[i].Md_order_Weighing.RegistrationDate + ";" + model[i].Md_order_Weighing.Weight1.Weight + ";" + model[i].Md_order_Weighing.Weight1.WightDateTime + ";" + model[i].Md_order_Weighing.Weight2.Weight + ";" + model[i].Md_order_Weighing.Weight2.WightDateTime + ";" + model[i].Md_order_Weighing.UserName1 + "\n";
                }
                // Eintrag bei direktem Zweitwigung mit manueller Tara
                else if(model[i].Md_order_Weighing != null && model[i].Md_order_Weighing.WeightId1 == null && model[i].Md_order_Weighing.WeightId2 != null)
                {
                    csvExport +=  direction + ";" + model[i].Md_order_Weighing.LicensePlate + ";" + model[i].Md_order_Weighing.ArticleNumber + ";" + model[i].Md_order_Weighing.ArticleDescription + ";" + model[i].Md_order_Weighing.CustomerNumber + ";" + model[i].Md_order_Weighing.CustomerDescription + ";" + model[i].Md_order_Weighing.ConstructionSiteNumber + ";" + model[i].Md_order_Weighing.ConstructionSiteDescription + ";" + model[i].Md_order_Weighing.OrderNumber + ";" + model[i].Md_order_Weighing.RegistrationDate + ";" + model[i].Md_order_Weighing.Weight1.Weight + ";" + model[i].Md_order_Weighing.Weight1.WightDateTime + ";" + model[i].Md_order_Weighing.Weight2.Weight + ";" + model[i].Md_order_Weighing.Weight2.WightDateTime + ";" + model[i].Md_order_Weighing.UserName1 + "\n";
                }
                // sonstige Einträge
                else
                {
                    csvExport += "0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;" + "\n";
                } 
            }
            // Download der CSV-Datei
            return File(new UTF8Encoding().GetBytes(csvExport),"text/csv","CSV-Export.csv");
        }

        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            var weightOrderContext = new Areas.Weigh.Models.WeighContext();


            foreach (var dataset in model)
            {
                var customer = weightOrderContext.Md_order_Weighing.Where(a => a.Id == dataset.OrderId).Select(a => a.CustomerDescription).FirstOrDefault();
                ds.List.AddListRow(dataset.MovingType == 0 ? "Eingang" : "Ausgang", dataset.ArticleNo, dataset.ArticleDescription, customer, dataset.RecipeDescription != null ? dataset.RecipeDescription.ToString() : "", dataset.LotNo, dataset.LotNoExtern, dataset.OrderNo, (dataset.RegistrationDate ?? new DateTime()).ToString("dd.MM.yyyy HH:mm:ss"), dataset.QuantitySetpoint.HasValue ? dataset.QuantitySetpoint.Value.ToString() : "", dataset.QuantityIs.HasValue ? dataset.QuantityIs.Value.ToString() : "", dataset.UserId, "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

            //ReportDataSource test = fillReport(pg);

            string from = "-";
            string to = "-";
            foreach (PCMS.Models.TopFilter top in pg.topFilter)
            {
                switch (top.colName)
                {
                    case "ProbTakingDatetime":
                        if (!String.IsNullOrEmpty(top.from) && !String.IsNullOrEmpty(top.to))
                        {
                            model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).Date).ToList();
                            from = new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).ToString("dd.MM.yyyy");
                            to = new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).ToString("dd.MM.yyyy");
                        }
                        break;
                }
            }


            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
            repParams.Add(new ReportParameter("DateFrom", from));
            repParams.Add(new ReportParameter("DateTo", to));


            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialMovingList.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, PageCountMode.Estimate, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);

            //ViewBag.ReportViewer = reportViewer;
            //return View();
        }
        public ActionResult ReportLot(string OrderNumber)
        {
            var dbMaterial = new MasterData.Models.MasterDataContext();
            var model = new List<md_material_MaterialMoving>();
            if (Helper.Definitions.getFactoryGroup() == 1)
                model = db.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && !(a.LocationId == 42 || a.LocationId == 40)).ToList().OrderBy(a => int.Parse(a.LotNo)).ThenBy(a => a.ArticleNo).ToList();
            if (Helper.Definitions.getFactoryGroup() == 0)
                model = db.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber && a.LotNo != "0").ToList().OrderBy(a => int.Parse(a.LotNo)).ThenBy(a => a.ArticleNo).ToList();
            if (model == null)
            {
                return HttpNotFound();
            }
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            string recipeDescription = "";
            decimal waterSum = 0;
            decimal aggreSum = 0;
            decimal binderSum = 0;
            decimal additive = 0;
            foreach (var dataset in model)
            {
                long mType = dbMaterial.Md_masterData_Material.Where(a => a.Id == dataset.ArticleId).FirstOrDefault().MaterialGroupId ?? 0;
                int WaterConten = 0;
                string abw = "--";
                if (dataset.QuantityIs != null && dataset.QuantitySetpoint != null)
                {
                    abw = (dataset.QuantitySetpoint - dataset.QuantityIs).ToString();
                    if ((dataset.QuantitySetpoint - dataset.QuantityIs) < (dataset.ApprovedTolMinus * -1) || ((dataset.QuantitySetpoint - dataset.QuantityIs) > (dataset.ApprovedTolPlus)))
                        abw = abw + "F";
                }
                recipeDescription = dataset.RecipeDescription;

                if (mType == 3)
                {
                    waterSum += Math.Abs(dataset.QuantityIs ?? 0);
                    WaterConten = Math.Abs(Convert.ToInt32(dataset.QuantityIs ?? 0));
                }
                else if (mType == 4)
                {
                    waterSum += Math.Abs(dataset.QuantityIs ?? 0);
                    WaterConten = Math.Abs(Convert.ToInt32(dataset.QuantityIs ?? 0));
                }
                else if (mType == 1 && dataset.Wet != null && dataset.Wet > 0)
                {
                    waterSum += Math.Abs((dataset.QuantityIs ?? 0) * ((dataset.Wet ?? 0) / 100));
                    WaterConten = Math.Abs(Convert.ToInt32((dataset.QuantityIs ?? 0) * ((dataset.Wet ?? 0) / 100)));
                    aggreSum += Math.Abs(dataset.QuantityIs ?? 0) - Math.Abs(Convert.ToInt32((dataset.QuantityIs ?? 0) * ((dataset.Wet ?? 0) / 100))); ;
                }
                else if (mType == 2)
                {

                    binderSum += Math.Abs(dataset.QuantityIs ?? 0);
                }
                else if (mType == 6)
                {
                    var mat = dbMaterial.Md_masterData_Material.Where(a => a.Id == dataset.ArticleId).FirstOrDefault();
                    var additi = dbMaterial.Md_material_AdditivesDetail.Where(a => a.Id == mat.AdditivesId).FirstOrDefault();
                    if (additi != null && additi.KValue != null)
                    {
                        additive += Math.Abs(dataset.QuantityIs ?? 0) * (additi.KValue ?? 0);
                    }

                }


                ds.List.AddListRow(dataset.LotNo, dataset.ArticleNo, dataset.ArticleDescription, dataset.QuantitySetpoint.HasValue ? (dataset.QuantitySetpoint.Value < 0 ? dataset.QuantitySetpoint.Value * -1 : dataset.QuantitySetpoint.Value).ToString() : "", dataset.QuantityIs.HasValue ? (dataset.QuantityIs.Value < 0 ? dataset.QuantityIs.Value * -1 : dataset.QuantityIs.Value).ToString() : "", abw, dataset.ApprovedTolPlus.HasValue ? dataset.ApprovedTolPlus.Value.ToString() : "", dataset.ApprovedTolMinus.HasValue ? dataset.ApprovedTolMinus.Value.ToString() : "", dataset.Trailing.HasValue ? dataset.Trailing.Value.ToString() : "", dataset.ShiftFine.HasValue ? dataset.ShiftFine.Value.ToString() : "", dataset.LineNo.ToString(), dataset.Wet != null ? dataset.Wet + "%" : "", WaterConten > 0 ? WaterConten.ToString() : "", dataset.Wet != null ? Convert.ToInt32(Math.Abs(dataset.QuantityIs ?? 0) - Math.Abs(WaterConten)).ToString() : "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.List.ToList());

            Order.Models.OrderContext orderContext = new OrderContext();
            MasterData.Models.MasterDataContext mdContext = new MasterData.Models.MasterDataContext();
            var loadingOrder = orderContext.Md_order_LoadingOrder.Where(a => a.OrderNumber == OrderNumber).FirstOrDefault();



            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
            repParams.Add(new ReportParameter("DateFrom", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
            repParams.Add(new ReportParameter("DateTo", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss")));
            repParams.Add(new ReportParameter("OrderNo", OrderNumber));
            repParams.Add(new ReportParameter("RecipeDescription", loadingOrder.RecipeDescription));
            repParams.Add(new ReportParameter("IsValue", (model.Sum(a => a.QuantityIs) < 0 ? model.Sum(a => a.QuantityIs) * -1 : model.Sum(a => a.QuantityIs)).ToString()));
            repParams.Add(new ReportParameter("SetValue", (model.Sum(a => a.QuantitySetpoint) < 0 ? model.Sum(a => a.QuantitySetpoint) * -1 : model.Sum(a => a.QuantitySetpoint)).ToString()));
            repParams.Add(new ReportParameter("Comment", loadingOrder.DNComment));
            repParams.Add(new ReportParameter("RecipeNumber", loadingOrder.RecipeNumber));
            repParams.Add(new ReportParameter("LotAmount", loadingOrder.OrderedQuantity != null ? loadingOrder.OrderedQuantity.ToString() : "-"));
            repParams.Add(new ReportParameter("WaterContent", Convert.ToString(waterSum)));
            repParams.Add(new ReportParameter("BackAmount", Convert.ToString(loadingOrder.BackAmount ?? 0)));
            repParams.Add(new ReportParameter("Customer", loadingOrder.CustomerNumber + ", " + loadingOrder.CustomerName));
            repParams.Add(new ReportParameter("ConstructionSite", loadingOrder.ConstructionSiteNumber + ", " + loadingOrder.ConstructionSiteDescription));
            repParams.Add(new ReportParameter("aggregateSum", Convert.ToString(aggreSum)));
            if (waterSum != 0 && binderSum != 0)
                repParams.Add(new ReportParameter("wz", Math.Round((waterSum / binderSum), 2).ToString()));
            if (waterSum != 0 && binderSum != 0 && additive != 0)
                repParams.Add(new ReportParameter("wzEq", Math.Round((waterSum / (binderSum + additive)), 2).ToString()));

            repParams.Add(new ReportParameter("MixTime", (loadingOrder.MixTime ?? 0).ToString()));
            var sort = mdContext.Md_masterData_Material.Where(a => a.Id == loadingOrder.MaterialId).FirstOrDefault();
            string consisty = "";
            if (sort != null)
            {
                var sortDet = mdContext.Md_material_SortDetails.Where(a => a.Id == sort.SortId).FirstOrDefault();
                if (sortDet != null)
                {
                    consisty = mdContext.Md_material_ConsistencyGroup.Where(a => a.Id == sortDet.ConsistencyId).FirstOrDefault().Class;
                }
            }

            repParams.Add(new ReportParameter("Consitency", consisty));


            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Material/MaterialLot.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);

            //ViewBag.ReportViewer = reportViewer;
            //return View();
        }
        public ActionResult checkOrderNumber(string OrderNumber)
        {
            var model = db.Md_material_MaterialMoving.Where(a => a.OrderNo == OrderNumber).FirstOrDefault();
            if (model != null)
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(false, JsonRequestBehavior.AllowGet);
        }
        private List<md_material_MaterialMoving> pageModel(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_material_MaterialMoving.OrderByDescending(a => a.RegistrationDate).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "ProbTakingDatetime":
                            if (!String.IsNullOrEmpty(top.from) && !String.IsNullOrEmpty(top.to))
                            {
                                model = model.Where(m => m.RegistrationDate.Value.Date >= new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date && m.RegistrationDate.Value.Date <= new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).Date).ToList();
                                ViewBag.ProbTakingDatetimeFrom = top.from;
                                ViewBag.ProbTakingDatetimeTo = top.to;
                            }
                            break;
                    }
                }

                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "UserId":
                                model = model.Where(m => m.UserId != null && m.UserId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.UserId = pgFF.colVal;
                                break;
                            case "MovingType":

                                if (pgFF.colVal.ToLower() == "eingang")
                                {
                                    pgFF.colVal = "0";
                                }
                                if (pgFF.colVal.ToLower() == "ausgang")
                                {
                                    pgFF.colVal = "1";
                                }
                                model = model.Where(m => m.MovingType != null && m.MovingType.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MovingType = pgFF.colVal;
                                break;
                            case "LineNo":
                                model = model.Where(m => m.LineNo != null && m.LineNo.Equals(pgFF.colVal)).ToList();
                                ViewBag.LineNo = pgFF.colVal;
                                break;
                            case "LocationNo":
                                model = model.Where(m => m.LocationNo != null && m.LocationNo.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LocationNo = pgFF.colVal;
                                break;
                            case "LocationMin":
                                model = model.Where(m => m.LocationMin != null && m.LocationMin.Equals(pgFF.colVal)).ToList();
                                ViewBag.LocationMin = pgFF.colVal;
                                break;
                            case "LocationMax":
                                model = model.Where(m => m.LocationMax != null && m.LocationMax.Equals(pgFF.colVal)).ToList();
                                ViewBag.LocationMax = pgFF.colVal;
                                break;
                            case "LocationDescription":
                                model = model.Where(m => m.LocationDescription != null && m.LocationDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LocationDescription = pgFF.colVal;
                                break;
                            case "CustomerDescription":
                                model = model.Where(m => m.Md_order_Weighing.CustomerDescription != null && m.Md_order_Weighing.CustomerDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.CustomerDescription = pgFF.colVal;
                                break;
                            case "LocationId":
                                model = model.Where(m => m.LocationId != null && m.LocationId.Equals(pgFF.colVal)).ToList();
                                ViewBag.LocationId = pgFF.colVal;
                                break;
                            case "LocationType":
                                model = model.Where(m => m.LocationType != null && m.LocationType.Equals(pgFF.colVal)).ToList();
                                ViewBag.LocationType = pgFF.colVal;
                                break;
                            case "ArticleId":
                                model = model.Where(m => m.ArticleId != null && m.ArticleId.Equals(pgFF.colVal)).ToList();
                                ViewBag.ArticleId = pgFF.colVal;
                                break;
                            case "ArticleDescription":
                                model = model.Where(m => m.ArticleDescription != null && m.ArticleDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleDescription = pgFF.colVal;
                                break;
                            case "ArticleNo":
                                model = model.Where(m => m.ArticleNo != null && m.ArticleNo.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNo = pgFF.colVal;
                                break;
                            case "RecipeDescription":
                                model = model.Where(m => m.RecipeDescription != null && m.RecipeDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.RecipeDescription = pgFF.colVal;
                                break;
                            case "RecipeId":
                                model = model.Where(m => m.RecipeId != null && m.RecipeId.Equals(pgFF.colVal)).ToList();
                                ViewBag.RecipeId = pgFF.colVal;
                                break;
                            case "RecipeVariant":
                                model = model.Where(m => m.RecipeVariant != null && m.RecipeVariant.Equals(pgFF.colVal)).ToList();
                                ViewBag.RecipeVariant = pgFF.colVal;
                                break;
                            case "ScaleNo":
                                model = model.Where(m => m.ScaleNo != null && m.ScaleNo.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ScaleNo = pgFF.colVal;
                                break;
                            case "QuantitySetpoint":
                                model = model.Where(m => m.QuantitySetpoint != null && m.QuantitySetpoint.Equals(pgFF.colVal)).ToList();
                                ViewBag.QuantitySetpoint = pgFF.colVal;
                                break;
                            case "QuantityIs":
                                model = model.Where(m => m.QuantityIs != null && m.QuantityIs.Equals(pgFF.colVal)).ToList();
                                ViewBag.QuantityIs = pgFF.colVal;
                                break;
                            case "Unit":
                                model = model.Where(m => m.Unit != null && m.Unit.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Unit = pgFF.colVal;
                                break;
                            case "OrderId":
                                model = model.Where(m => m.OrderId != null && m.OrderId.Equals(pgFF.colVal)).ToList();
                                ViewBag.OrderId = pgFF.colVal;
                                break;
                            case "OrderNo":
                                model = model.Where(m => m.OrderNo != null && m.OrderNo.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.OrderNo = pgFF.colVal;
                                break;
                            case "OrderLineNo":
                                model = model.Where(m => m.OrderLineNo != null && m.OrderLineNo.Equals(pgFF.colVal)).ToList();
                                ViewBag.OrderLineNo = pgFF.colVal;
                                break;
                            case "LotNo":
                                model = model.Where(m => m.LotNo != null && m.LotNo.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LotNo = pgFF.colVal;
                                break;
                            case "LotNoExtern":
                                model = model.Where(m => m.LotNoExtern != null && m.LotNoExtern.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LotNoExtern = pgFF.colVal;
                                break;
                            case "WarrantyDate":
                                model = model.Where(m => m.WarrantyDate != null && m.WarrantyDate.Equals(pgFF.colVal)).ToList();
                                ViewBag.WarrantyDate = pgFF.colVal;
                                break;
                            case "ExpirationDate":
                                model = model.Where(m => m.ExpirationDate != null && m.ExpirationDate.Equals(pgFF.colVal)).ToList();
                                ViewBag.ExpirationDate = pgFF.colVal;
                                break;
                            case "RegistrationDate":
                                model = model.Where(m => m.RegistrationDate != null && m.RegistrationDate.Equals(pgFF.colVal)).ToList();
                                ViewBag.RegistrationDate = pgFF.colVal;
                                break;
                            case "ApprovedTol+":
                                model = model.Where(m => m.ApprovedTolPlus != null && m.ApprovedTolPlus.Equals(pgFF.colVal)).ToList();
                                ViewBag.ApprovedTolPlus = pgFF.colVal;
                                break;
                            case "ApprovedTol-":
                                model = model.Where(m => m.ApprovedTolMinus != null && m.ApprovedTolMinus.Equals(pgFF.colVal)).ToList();
                                ViewBag.ApprovedTolMinus = pgFF.colVal;
                                break;
                            case "Trailing":
                                model = model.Where(m => m.Trailing != null && m.Trailing.Equals(pgFF.colVal)).ToList();
                                ViewBag.Trailing = pgFF.colVal;
                                break;
                            case "ShiftFine":
                                model = model.Where(m => m.ShiftFine != null && m.ShiftFine.Equals(pgFF.colVal)).ToList();
                                ViewBag.ShiftFine = pgFF.colVal;
                                break;
                            case "StartDate":
                                model = model.Where(m => m.StartDate != null && m.StartDate.Equals(pgFF.colVal)).ToList();
                                ViewBag.StartDate = pgFF.colVal;
                                break;
                            case "StopDate":
                                model = model.Where(m => m.StopDate != null && m.StopDate.Equals(pgFF.colVal)).ToList();
                                ViewBag.StopDate = pgFF.colVal;
                                break;
                            case "FacilitieId":
                                model = model.Where(m => m.FacilitieId != null && m.FacilitieId.Equals(pgFF.colVal)).ToList();
                                ViewBag.FacilitieId = pgFF.colVal;
                                break;
                            case "FacilitieDescription":
                                model = model.Where(m => m.FacilitieDescription != null && m.FacilitieDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FacilitieDescription = pgFF.colVal;
                                break;
                            case "Exported":
                                model = model.Where(m => m.Exported != null && m.Exported.Equals(pgFF.colVal)).ToList();
                                ViewBag.Exported = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {

                case "CustomerDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_order_Weighing.CustomerDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_order_Weighing.CustomerDescription).ToList();
                    break;
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "UserId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserId).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserId).ToList();
                    break;
                case "MovingType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MovingType).ToList();
                    else
                        model = model.OrderByDescending(m => m.MovingType).ToList();
                    break;
                case "LineNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LineNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.LineNo).ToList();
                    break;
                case "LocationNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationNo).ToList();
                    break;
                case "LocationMin":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationMin).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationMin).ToList();
                    break;
                case "LocationMax":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationMax).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationMax).ToList();
                    break;
                case "LocationDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationDescription).ToList();
                    break;
                case "LocationId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationId).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationId).ToList();
                    break;
                case "LocationType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationType).ToList();
                    else
                        model = model.OrderByDescending(m => m.LocationType).ToList();
                    break;
                case "ArticleId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleId).ToList();
                    break;
                case "ArticleDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleDescription).ToList();
                    break;
                case "ArticleNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNo).ToList();
                    break;
                case "RecipeDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeDescription).ToList();
                    break;
                case "RecipeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeId).ToList();
                    break;
                case "RecipeVariant":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeVariant).ToList();
                    else
                        model = model.OrderByDescending(m => m.RecipeVariant).ToList();
                    break;
                case "ScaleNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ScaleNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.ScaleNo).ToList();
                    break;
                case "QuantitySetpoint":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.QuantitySetpoint).ToList();
                    else
                        model = model.OrderByDescending(m => m.QuantitySetpoint).ToList();
                    break;
                case "QuantityIs":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.QuantityIs).ToList();
                    else
                        model = model.OrderByDescending(m => m.QuantityIs).ToList();
                    break;
                case "Unit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Unit).ToList();
                    else
                        model = model.OrderByDescending(m => m.Unit).ToList();
                    break;
                case "OrderId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderId).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderId).ToList();
                    break;
                case "OrderNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderNo).ToList();
                    break;
                case "OrderLineNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderLineNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.OrderLineNo).ToList();
                    break;
                case "LotNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LotNo).ToList();
                    else
                        model = model.OrderByDescending(m => m.LotNo).ToList();
                    break;
                case "LotNoExtern":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LotNoExtern).ToList();
                    else
                        model = model.OrderByDescending(m => m.LotNoExtern).ToList();
                    break;
                case "WarrantyDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WarrantyDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.WarrantyDate).ToList();
                    break;
                case "ExpirationDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ExpirationDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.ExpirationDate).ToList();
                    break;
                case "RegistrationDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RegistrationDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.RegistrationDate).ToList();
                    break;
                case "ApprovedTol+":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ApprovedTolPlus).ToList();
                    else
                        model = model.OrderByDescending(m => m.ApprovedTolPlus).ToList();
                    break;
                case "ApprovedTol-":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ApprovedTolMinus).ToList();
                    else
                        model = model.OrderByDescending(m => m.ApprovedTolMinus).ToList();
                    break;
                case "Trailing":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Trailing).ToList();
                    else
                        model = model.OrderByDescending(m => m.Trailing).ToList();
                    break;
                case "ShiftFine":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShiftFine).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShiftFine).ToList();
                    break;
                case "StartDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StartDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.StartDate).ToList();
                    break;
                case "StopDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StopDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.StopDate).ToList();
                    break;
                case "FacilitieId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilitieId).ToList();
                    else
                        model = model.OrderByDescending(m => m.FacilitieId).ToList();
                    break;
                case "FacilitieDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilitieDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.FacilitieDescription).ToList();
                    break;
                case "Exported":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Exported).ToList();
                    else
                        model = model.OrderByDescending(m => m.Exported).ToList();
                    break;
            }
            var mT = new List<SelectListItem>();
            mT.Add(new SelectListItem { Text = "Barverkauf", Value = "2" });
            mT.Add(new SelectListItem { Text = "Ausgang", Value = "1" });
            mT.Add(new SelectListItem { Text = "Eingang", Value = "0" });

            ViewBag.MovingType = new SelectList(mT, "Value", "Text", ViewBag.MovingType);
            return model.ToList();
        }
        private IQueryable<md_material_MaterialMoving> pageModel2(PCMS.Models.PaginationModel pg)
        {
            IQueryable<md_material_MaterialMoving> model = db.Md_material_MaterialMoving.OrderByDescending(a => a.RegistrationDate);
            if (pg != null)
            {
                foreach (PCMS.Models.TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "ProbTakingDatetime":
                            if (!String.IsNullOrEmpty(top.from) && !String.IsNullOrEmpty(top.to))
                            {
                                DateTime from = new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date;
                                DateTime to = new DateTime(Convert.ToInt32(top.to.Split('-')[0]), Convert.ToInt32(top.to.Split('-')[1]), Convert.ToInt32(top.to.Split('-')[2])).Date;

                                model = model.Where(m => EntityFunctions.TruncateTime(m.RegistrationDate.Value) >= from && EntityFunctions.TruncateTime(m.RegistrationDate.Value) <= to);
                                ViewBag.ProbTakingDatetimeFrom = top.from;
                                ViewBag.ProbTakingDatetimeTo = top.to;
                            }
                            break;
                    }
                }

                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "UserId":
                                model = model.Where(m => m.UserId != null && m.UserId.ToLower().Contains(pgFF.colVal));
                                ViewBag.UserId = pgFF.colVal;
                                break;
                            case "MovingType":

                                if (pgFF.colVal.ToLower() == "eingang")
                                {
                                    pgFF.colVal = "0";
                                }
                                if (pgFF.colVal.ToLower() == "ausgang")
                                {
                                    pgFF.colVal = "1";
                                }
                                if (pgFF.colVal.ToLower() == "barverkauf")
                                {
                                    pgFF.colVal = "2";
                                }
                                model = model.Where(m => m.MovingType != null && m.MovingType.ToString().ToLower().Contains(pgFF.colVal));
                                ViewBag.MovingType = pgFF.colVal;
                                break;
                            case "LineNo":
                                model = model.Where(m => m.LineNo != null && m.LineNo.Equals(pgFF.colVal));
                                ViewBag.LineNo = pgFF.colVal;
                                break;
                            case "LocationNo":
                                model = model.Where(m => m.LocationNo != null && m.LocationNo.ToLower().Contains(pgFF.colVal));
                                ViewBag.LocationNo = pgFF.colVal;
                                break;
                            case "LocationMin":
                                model = model.Where(m => m.LocationMin != null && m.LocationMin.Equals(pgFF.colVal));
                                ViewBag.LocationMin = pgFF.colVal;
                                break;
                            case "LocationMax":
                                model = model.Where(m => m.LocationMax != null && m.LocationMax.Equals(pgFF.colVal));
                                ViewBag.LocationMax = pgFF.colVal;
                                break;
                            case "LocationDescription":
                                model = model.Where(m => m.LocationDescription != null && m.LocationDescription.ToLower().Contains(pgFF.colVal));
                                ViewBag.LocationDescription = pgFF.colVal;
                                break;
                            case "CustomerDescription":
                                model = model.Where(m => m.Md_order_Weighing.CustomerDescription != null && m.Md_order_Weighing.CustomerDescription.ToLower().Contains(pgFF.colVal));
                                ViewBag.CustomerDescription = pgFF.colVal;
                                break;
                            case "LocationId":
                                model = model.Where(m => m.LocationId != null && m.LocationId.Equals(pgFF.colVal));
                                ViewBag.LocationId = pgFF.colVal;
                                break;
                            case "LocationType":
                                model = model.Where(m => m.LocationType != null && m.LocationType.Equals(pgFF.colVal));
                                ViewBag.LocationType = pgFF.colVal;
                                break;
                            case "ArticleId":
                                model = model.Where(m => m.ArticleId != null && m.ArticleId.Equals(pgFF.colVal));
                                ViewBag.ArticleId = pgFF.colVal;
                                break;
                            case "ArticleDescription":
                                model = model.Where(m => m.ArticleDescription != null && m.ArticleDescription.ToLower().Contains(pgFF.colVal));
                                ViewBag.ArticleDescription = pgFF.colVal;
                                break;
                            case "ArticleNo":
                                model = model.Where(m => m.ArticleNo != null && m.ArticleNo.ToLower().Contains(pgFF.colVal));
                                ViewBag.ArticleNo = pgFF.colVal;
                                break;
                            case "RecipeDescription":
                                model = model.Where(m => m.RecipeDescription != null && m.RecipeDescription.ToLower().Contains(pgFF.colVal));
                                ViewBag.RecipeDescription = pgFF.colVal;
                                break;
                            case "RecipeId":
                                model = model.Where(m => m.RecipeId != null && m.RecipeId.Equals(pgFF.colVal));
                                ViewBag.RecipeId = pgFF.colVal;
                                break;
                            case "RecipeVariant":
                                model = model.Where(m => m.RecipeVariant != null && m.RecipeVariant.Equals(pgFF.colVal));
                                ViewBag.RecipeVariant = pgFF.colVal;
                                break;
                            case "ScaleNo":
                                model = model.Where(m => m.ScaleNo != null && m.ScaleNo.ToLower().Contains(pgFF.colVal));
                                ViewBag.ScaleNo = pgFF.colVal;
                                break;
                            case "QuantitySetpoint":
                                model = model.Where(m => m.QuantitySetpoint != null && m.QuantitySetpoint.Equals(pgFF.colVal));
                                ViewBag.QuantitySetpoint = pgFF.colVal;
                                break;
                            case "QuantityIs":
                                model = model.Where(m => m.QuantityIs != null && m.QuantityIs.Equals(pgFF.colVal));
                                ViewBag.QuantityIs = pgFF.colVal;
                                break;
                            case "Unit":
                                model = model.Where(m => m.Unit != null && m.Unit.ToLower().Contains(pgFF.colVal));
                                ViewBag.Unit = pgFF.colVal;
                                break;
                            case "OrderId":
                                model = model.Where(m => m.OrderId != null && m.OrderId.Equals(pgFF.colVal));
                                ViewBag.OrderId = pgFF.colVal;
                                break;
                            case "OrderNo":
                                model = model.Where(m => m.OrderNo != null && m.OrderNo.ToLower().Contains(pgFF.colVal));
                                ViewBag.OrderNo = pgFF.colVal;
                                break;
                            case "OrderLineNo":
                                model = model.Where(m => m.OrderLineNo != null && m.OrderLineNo.Equals(pgFF.colVal));
                                ViewBag.OrderLineNo = pgFF.colVal;
                                break;
                            case "LotNo":
                                model = model.Where(m => m.LotNo != null && m.LotNo.ToLower().Contains(pgFF.colVal));
                                ViewBag.LotNo = pgFF.colVal;
                                break;
                            case "LotNoExtern":
                                model = model.Where(m => m.LotNoExtern != null && m.LotNoExtern.ToLower().Contains(pgFF.colVal));
                                ViewBag.LotNoExtern = pgFF.colVal;
                                break;
                            case "WarrantyDate":
                                model = model.Where(m => m.WarrantyDate != null && m.WarrantyDate.Equals(pgFF.colVal));
                                ViewBag.WarrantyDate = pgFF.colVal;
                                break;
                            case "ExpirationDate":
                                model = model.Where(m => m.ExpirationDate != null && m.ExpirationDate.Equals(pgFF.colVal));
                                ViewBag.ExpirationDate = pgFF.colVal;
                                break;
                            case "RegistrationDate":
                                model = model.Where(m => m.RegistrationDate != null && m.RegistrationDate.Equals(pgFF.colVal));
                                ViewBag.RegistrationDate = pgFF.colVal;
                                break;
                            case "ApprovedTol+":
                                model = model.Where(m => m.ApprovedTolPlus != null && m.ApprovedTolPlus.Equals(pgFF.colVal));
                                ViewBag.ApprovedTolPlus = pgFF.colVal;
                                break;
                            case "ApprovedTol-":
                                model = model.Where(m => m.ApprovedTolMinus != null && m.ApprovedTolMinus.Equals(pgFF.colVal));
                                ViewBag.ApprovedTolMinus = pgFF.colVal;
                                break;
                            case "Trailing":
                                model = model.Where(m => m.Trailing != null && m.Trailing.Equals(pgFF.colVal));
                                ViewBag.Trailing = pgFF.colVal;
                                break;
                            case "ShiftFine":
                                model = model.Where(m => m.ShiftFine != null && m.ShiftFine.Equals(pgFF.colVal));
                                ViewBag.ShiftFine = pgFF.colVal;
                                break;
                            case "StartDate":
                                model = model.Where(m => m.StartDate != null && m.StartDate.Equals(pgFF.colVal));
                                ViewBag.StartDate = pgFF.colVal;
                                break;
                            case "StopDate":
                                model = model.Where(m => m.StopDate != null && m.StopDate.Equals(pgFF.colVal));
                                ViewBag.StopDate = pgFF.colVal;
                                break;
                            case "FacilitieId":
                                model = model.Where(m => m.FacilitieId != null && m.FacilitieId.Equals(pgFF.colVal));
                                ViewBag.FacilitieId = pgFF.colVal;
                                break;
                            case "FacilitieDescription":
                                model = model.Where(m => m.FacilitieDescription != null && m.FacilitieDescription.ToLower().Contains(pgFF.colVal));
                                ViewBag.FacilitieDescription = pgFF.colVal;
                                break;
                            case "Exported":
                                model = model.Where(m => m.Exported != null && m.Exported.Equals(pgFF.colVal));
                                ViewBag.Exported = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "CustomerDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_order_Weighing.CustomerDescription);
                    else
                        model = model.OrderByDescending(m => m.Md_order_Weighing.CustomerDescription);
                    break;
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id);
                    else
                        model = model.OrderByDescending(m => m.Id);
                    break;
                case "UserId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserId);
                    else
                        model = model.OrderByDescending(m => m.UserId);
                    break;
                case "MovingType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MovingType);
                    else
                        model = model.OrderByDescending(m => m.MovingType);
                    break;
                case "LineNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LineNo);
                    else
                        model = model.OrderByDescending(m => m.LineNo);
                    break;
                case "LocationNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationNo);
                    else
                        model = model.OrderByDescending(m => m.LocationNo);
                    break;
                case "LocationMin":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationMin);
                    else
                        model = model.OrderByDescending(m => m.LocationMin);
                    break;
                case "LocationMax":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationMax);
                    else
                        model = model.OrderByDescending(m => m.LocationMax);
                    break;
                case "LocationDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationDescription);
                    else
                        model = model.OrderByDescending(m => m.LocationDescription);
                    break;
                case "LocationId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationId);
                    else
                        model = model.OrderByDescending(m => m.LocationId);
                    break;
                case "LocationType":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LocationType);
                    else
                        model = model.OrderByDescending(m => m.LocationType);
                    break;
                case "ArticleId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleId);
                    else
                        model = model.OrderByDescending(m => m.ArticleId);
                    break;
                case "ArticleDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleDescription);
                    else
                        model = model.OrderByDescending(m => m.ArticleDescription);
                    break;
                case "ArticleNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNo);
                    else
                        model = model.OrderByDescending(m => m.ArticleNo);
                    break;
                case "RecipeDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeDescription);
                    else
                        model = model.OrderByDescending(m => m.RecipeDescription);
                    break;
                case "RecipeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeId);
                    else
                        model = model.OrderByDescending(m => m.RecipeId);
                    break;
                case "RecipeVariant":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RecipeVariant);
                    else
                        model = model.OrderByDescending(m => m.RecipeVariant);
                    break;
                case "ScaleNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ScaleNo);
                    else
                        model = model.OrderByDescending(m => m.ScaleNo);
                    break;
                case "QuantitySetpoint":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.QuantitySetpoint);
                    else
                        model = model.OrderByDescending(m => m.QuantitySetpoint);
                    break;
                case "QuantityIs":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.QuantityIs);
                    else
                        model = model.OrderByDescending(m => m.QuantityIs);
                    break;
                case "Unit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Unit);
                    else
                        model = model.OrderByDescending(m => m.Unit);
                    break;
                case "OrderId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderId);
                    else
                        model = model.OrderByDescending(m => m.OrderId);
                    break;
                case "OrderNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNo);
                    else
                        model = model.OrderByDescending(m => m.OrderNo);
                    break;
                case "OrderLineNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderLineNo);
                    else
                        model = model.OrderByDescending(m => m.OrderLineNo);
                    break;
                case "LotNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LotNo);
                    else
                        model = model.OrderByDescending(m => m.LotNo);
                    break;
                case "LotNoExtern":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LotNoExtern);
                    else
                        model = model.OrderByDescending(m => m.LotNoExtern);
                    break;
                case "WarrantyDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.WarrantyDate);
                    else
                        model = model.OrderByDescending(m => m.WarrantyDate);
                    break;
                case "ExpirationDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ExpirationDate);
                    else
                        model = model.OrderByDescending(m => m.ExpirationDate);
                    break;
                case "RegistrationDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.RegistrationDate);
                    else
                        model = model.OrderByDescending(m => m.RegistrationDate);
                    break;
                case "ApprovedTol+":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ApprovedTolPlus);
                    else
                        model = model.OrderByDescending(m => m.ApprovedTolPlus);
                    break;
                case "ApprovedTol-":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ApprovedTolMinus);
                    else
                        model = model.OrderByDescending(m => m.ApprovedTolMinus);
                    break;
                case "Trailing":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Trailing);
                    else
                        model = model.OrderByDescending(m => m.Trailing);
                    break;
                case "ShiftFine":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShiftFine);
                    else
                        model = model.OrderByDescending(m => m.ShiftFine);
                    break;
                case "StartDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StartDate);
                    else
                        model = model.OrderByDescending(m => m.StartDate);
                    break;
                case "StopDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.StopDate);
                    else
                        model = model.OrderByDescending(m => m.StopDate);
                    break;
                case "FacilitieId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilitieId);
                    else
                        model = model.OrderByDescending(m => m.FacilitieId);
                    break;
                case "FacilitieDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilitieDescription);
                    else
                        model = model.OrderByDescending(m => m.FacilitieDescription);
                    break;
                case "Exported":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Exported);
                    else
                        model = model.OrderByDescending(m => m.Exported);
                    break;
            }
            var mT = new List<SelectListItem>();
            mT.Add(new SelectListItem { Text = "Barverkauf", Value = "2" });
            mT.Add(new SelectListItem { Text = "Ausgang", Value = "1" });
            mT.Add(new SelectListItem { Text = "Eingang", Value = "0" });

            ViewBag.MovingType = new SelectList(mT, "Value", "Text", ViewBag.MovingType);
            return model;
        }
    }
}