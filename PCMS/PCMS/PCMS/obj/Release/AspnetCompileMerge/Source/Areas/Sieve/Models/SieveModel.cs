﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PCMS.Areas.Sieve.Models
{
    public class SieveContext : DbContext
    {
        public SieveContext()
            : base("DefaultConnection")
        {
            //this.Configuration.LazyLoadingEnabled = false;
        }
        
        public virtual DbSet<md_sieve_Sieves> Md_sieve_Sieves { get; set; }
        public virtual DbSet<md_sieve_SieveSet> Md_sieve_SieveSet { get; set; }
        public virtual DbSet<md_sieve_RuleGradingCurve> Md_sieve_RuleGradingCurve { get; set; }
        public virtual DbSet<md_material_GradingGroup> md_material_GradingGroup { get; set; }

        // public virtual DbSet<md_order_Company_Contact> Md_order_Company_Contact { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<md_sieve_SieveSet>()
                .HasMany(up => up.Sieves)
                .WithMany(course => course.SieveSet)
                .Map(mc =>
                {
                    mc.ToTable("md_sieve_SieveSet_Sieves");
                    mc.MapLeftKey("SieveSetId");
                    mc.MapRightKey("SievesId");
                }
                
            );
      
            base.OnModelCreating(modelBuilder);
        }

       
    }




    [Table("md_sieve_Sieves")]
    public partial class md_sieve_Sieves
    { 

        public md_sieve_Sieves()
        {
            SieveSet = new List<md_sieve_SieveSet>();
        }


        //[DisplayName("ID")]
        [AdditionalMetadata("report", true)]
        public long Id{ get; set; }
    	[DisplayName("Größe")]
        [DisplayFormat(DataFormatString = "{0:0.0000}", ApplyFormatInEditMode = true)]
        [Localizable(true)]
        public decimal Size{ get; set; }
    	[DisplayName("Grenzwert")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Localizable(true)]
        public decimal? PassLimit{ get; set; }

        public virtual ICollection<md_sieve_SieveSet> SieveSet { get; set; }


       
    }
    [Table("md_sieve_SieveSet")]
    public partial class md_sieve_SieveSet
    {
        public md_sieve_SieveSet()
        {
            Sieves = new List<md_sieve_Sieves>();
        }

    	public long Id{ get; set; }
    	[DisplayName("Name")]
    	public string Description{ get; set; }
    	[DisplayName("Nummer")]
    	public string Number{ get; set; }
        [ScriptIgnore]
        public virtual ICollection<md_sieve_Sieves> Sieves { get; set; }
        [DisplayName("Siebe")]
        public string AllSives
        {
            get
            {
                string ret = "[";
                Sieves = Sieves.OrderBy(s => s.Size).ToList();
                foreach(var s in Sieves)
                {
                    ret += s.Size.ToString("0.####") +"|";
                }
                if (ret.Length > 1)
                    ret = ret.Substring(0, ret.Length - 1);
                ret += "]";
                return ret; 
            }
        }

    }
    [Table("md_sieve_RuleGradingCurve")]
    public partial class md_sieve_RuleGradingCurve
    {
        public long Id { get; set; }
        [DisplayName("Nummer")]
        public string Number { get; set; }
        [DisplayName("Name")]
        public string Description { get; set; }
        [DisplayName("Siebsatz")]
        public long SieveSetId { get; set; }
        [DisplayName("Korngruppe")]
        public long GradingId { get; set; }
        //public long SiveId0 { get; set; }
        [DisplayName("Siebsatz")]
        [ForeignKey("SieveSetId")]
        public virtual md_sieve_SieveSet md_sieve_SieveSet { get; set; }
        [DisplayName("Korngruppe")]
        [ForeignKey("GradingId")]
        public virtual md_material_GradingGroup md_material_GradinGroup { get; set; }

        public decimal? A0 { get; set; }
        public decimal? A1 { get; set; }
        public decimal? A2 { get; set; }
        public decimal? A3 { get; set; }
        public decimal? A4 { get; set; }
        public decimal? A5 { get; set; }
        public decimal? A6 { get; set; }
        public decimal? A7 { get; set; }
        public decimal? A8 { get; set; }
        public decimal? A9 { get; set; }
        public decimal? A10 { get; set; }
        public decimal? A11 { get; set; }
        public decimal? A12 { get; set; }
        public decimal? A13 { get; set; }
        public decimal? A14 { get; set; }
        public decimal? A15 { get; set; }
        public decimal? A16 { get; set; }
        public decimal? B0 { get; set; }
        public decimal? B1 { get; set; }
        public decimal? B2 { get; set; }
        public decimal? B3 { get; set; }
        public decimal? B4 { get; set; }
        public decimal? B5 { get; set; }
        public decimal? B6 { get; set; }
        public decimal? B7 { get; set; }
        public decimal? B8 { get; set; }
        public decimal? B9 { get; set; }
        public decimal? B10 { get; set; }
        public decimal? B11 { get; set; }
        public decimal? B12 { get; set; }
        public decimal? B13 { get; set; }
        public decimal? B14 { get; set; }
        public decimal? B15 { get; set; }
        public decimal? B16 { get; set; }
        public decimal? C0 { get; set; }
        public decimal? C1 { get; set; }
        public decimal? C2 { get; set; }
        public decimal? C3 { get; set; }
        public decimal? C4 { get; set; }
        public decimal? C5 { get; set; }
        public decimal? C6 { get; set; }
        public decimal? C7 { get; set; }
        public decimal? C8 { get; set; }
        public decimal? C9 { get; set; }
        public decimal? C10 { get; set; }
        public decimal? C11 { get; set; }
        public decimal? C12 { get; set; }
        public decimal? C13 { get; set; }
        public decimal? C14 { get; set; }
        public decimal? C15 { get; set; }
        public decimal? C16 { get; set; }
        public decimal? U0 { get; set; }
        public decimal? U1 { get; set; }
        public decimal? U2 { get; set; }
        public decimal? U3 { get; set; }
        public decimal? U4 { get; set; }
        public decimal? U5 { get; set; }
        public decimal? U6 { get; set; }
        public decimal? U7 { get; set; }
        public decimal? U8 { get; set; }
        public decimal? U9 { get; set; }
        public decimal? U10 { get; set; }
        public decimal? U11 { get; set; }
        public decimal? U12 { get; set; }
        public decimal? U13 { get; set; }
        public decimal? U14 { get; set; }
        public decimal? U15 { get; set; }
        public decimal? U16 { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }


        public List<decimal?> getA
        {
            get
            {





                

                List<decimal?> ret = new List<decimal?>();
                ret.Add(A0);        
                ret.Add( A1 );
                ret.Add( A2 );
                ret.Add( A3 );
                ret.Add( A4 );
                ret.Add( A5 );
                ret.Add( A6 );
                ret.Add( A7 );
                ret.Add( A8 );
                ret.Add( A9 );
                ret.Add( A10);
                ret.Add( A11);
                ret.Add( A12);
                ret.Add( A13);
                ret.Add( A14);
                ret.Add( A15);
                ret.Add(A16);
                List<decimal?> ret2 = new List<decimal?>();
                
        List<string> siveSetList = this.md_sieve_SieveSet.AllSives.Trim(new Char[] { '[', ']' }).Split('|').ToList();
                int u = 0;
                for (int i = 0; i <  Helper.Definitions.allSieves.Length; i++)
                {
                    if (siveSetList.Contains(Helper.Definitions.allSieves[i]))
                    {
                        ret2.Add(ret[u]);
                        u++;
                    }
                    else
                        ret2.Add(null);
                }

                return ret2;
            }
        }
        public List<decimal?> getB
        {
            get
            {
                List<decimal?> ret = new List<decimal?>();
                ret.Add(B0);
                ret.Add(B1);
                ret.Add(B2);
                ret.Add(B3);
                ret.Add(B4);
                ret.Add(B5);
                ret.Add(B6);
                ret.Add(B7);
                ret.Add(B8);
                ret.Add(B9);
                ret.Add(B10);
                ret.Add(B11);
                ret.Add(B12);
                ret.Add(B13);
                ret.Add(B14);
                ret.Add(B15);
                ret.Add(B16);
                List<decimal?> ret2 = new List<decimal?>();
                List<string> siveSetList = this.md_sieve_SieveSet.AllSives.Trim(new Char[] { '[', ']' }).Split('|').ToList();
                int u = 0;
                for (int i = 0; i < Helper.Definitions.allSieves.Length; i++)
                {
                    if (siveSetList.Contains(Helper.Definitions.allSieves[i]))
                    {
                        ret2.Add(ret[u]);
                        u++;
                    }
                    else
                        ret2.Add(null);
                }

                return ret2;
            }
        }
        public List<decimal?> getC
        {
            get
            {
                List<decimal?> ret = new List<decimal?>();
                ret.Add(C0);
                ret.Add(C1);
                ret.Add(C2);
                ret.Add(C3);
                ret.Add(C4);
                ret.Add(C5);
                ret.Add(C6);
                ret.Add(C7);
                ret.Add(C8);
                ret.Add(C9);
                ret.Add(C10);
                ret.Add(C11);
                ret.Add(C12);
                ret.Add(C13);
                ret.Add(C14);
                ret.Add(C15);
                ret.Add(C16);
                List<decimal?> ret2 = new List<decimal?>();
                List<string> siveSetList = this.md_sieve_SieveSet.AllSives.Trim(new Char[] { '[', ']' }).Split('|').ToList();
                int u = 0;
                for (int i = 0; i < Helper.Definitions.allSieves.Length; i++)
                {
                    if (siveSetList.Contains(Helper.Definitions.allSieves[i]))
                    {
                        ret2.Add(ret[u]);
                        u++;
                    }
                    else
                        ret2.Add(null);
                }

                return ret2;
            }
        }
        public List<decimal?> getU
        {
            get
            {
                List<decimal?> ret = new List<decimal?>();
                ret.Add(U0);
                ret.Add(U1);
                ret.Add(U2);
                ret.Add(U3);
                ret.Add(U4);
                ret.Add(U5);
                ret.Add(U6);
                ret.Add(U7);
                ret.Add(U8);
                ret.Add(U9);
                ret.Add(U10);
                ret.Add(U11);
                ret.Add(U12);
                ret.Add(U13);
                ret.Add(U14);
                ret.Add(U15);
                ret.Add(U16);
                List<decimal?> ret2 = new List<decimal?>();
                List<string> siveSetList = this.md_sieve_SieveSet.AllSives.Trim(new Char[] { '[', ']' }).Split('|').ToList();
                int u = 0;
                for (int i = 0; i < Helper.Definitions.allSieves.Length; i++)
                {
                    if (siveSetList.Contains(Helper.Definitions.allSieves[i]))
                    {
                        ret2.Add(ret[u]);
                        u++;
                    }
                    else
                        ret2.Add(null);
                }
                return ret2;
            }
        }
    }
    [Table("md_material_GradinGroup")]
    public partial class md_material_GradingGroup
    {
        public long Id { get; set; }
        [DisplayName("Korngruppe")]
        public string Description { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        public int sort { get; set; }
    }

    public class SieveModel
    {
    }
}