﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace PCMS.Areas.MasterData.Models
{
    public class MasterDataContactView : DbContext
    {
        public md_masterData_Contact contact { get; set; }
        public md_masterData_Address address { get; set; }
        public md_masterData_Company company { get; set; }
        public md_masterData_Vehicle vehicle { get; set; }
        public md_masterData_ConstructionSite constructionSite { get; set; }
        public md_masterData_Driver driver { get; set; }
        public md_masterData_CustomerRating custRating { get; set; }
        public md_masterData_Trader trader { get; set; }
        public md_masterData_Customer customer { get; set; }
        public md_masterData_Deliverer deliverer { get; set; }
        public md_masterData_VehicleType vehicleType { get; set; }
        public Facilities facili { get; set; }
        public md_masterData_Material material { get; set; }
        public md_material_MaterialGroup materialGroup { get; set; }
        public md_masterData_Standard standard { get; set; }
        public md_masterData_SieveSet sievset { get; set; }
        public md_material_ChlorideGroup chlorid { get; set; }
        public md_material_CompressionStrenghtGroup compression { get; set; }
        public md_material_ConsistencyGroup consistency { get; set; }
        public md_material_ExposureKombi exposure { get; set; }
        public md_material_MaxAggregateSizeGroup maxAggregateSize { get; set; }
        public md_material_ConcreteFamilyGroup concrete { get; set; }
        public md_masterData_Suitability suitability { get; set; }
        public md_material_GravelGroup gravelGroup { get; set; }
        public md_material_GravelType gravelType { get; set; }
        public md_material_AlkaliGroup alkali { get; set; }
        public md_material_GeologicalSource geo { get; set; }
        public md_material_RockType rock { get; set; }
        public md_material_PetrographicType petrograph { get; set; }
        public md_material_Client client { get; set; }
        public md_material_Aggregate aggregate { get; set; }
        public md_material_MaterialType materialType { get; set; }
        public md_material_CompressionGroupCement compCement { get; set; }
        public md_material_Progress progress { get; set; }
        public md_recipe_Recipe recipe { get; set; }
        public md_masterData_ConcentratesDetails concentrates { get; set; }
        public md_material_Color color { get; set; }
        public md_expoGroup expoGroup { get; set; }
        public md_material_RetardationTime retardation { get; set; }

        public md_material_AdditivesDetail additivesDetails { get; set; }
        public md_material_AdmixturesDetails admixturesDetails { get; set; }
        public md_material_AggregateDetails aggregateDetails { get; set; }
        public md_material_BinderDetails binderDetails { get; set; }
        public md_material_SortDetails sortDetails { get; set; }
        public md_material_BrandDetails brand { get; set; }

        public md_order_DeliveryNoteType dnType { get; set; }

        public md_ZipCodes ZipCodes { get; set; }
        public md_sieve_RuleGradingCurve Rule { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<md_masterData_Vehicle>().Property(e => e.LastDateTareWeighing).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_BinderDetails>().Property(e => e.DateOfIssue).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_BinderDetails>().Property(e => e.TerminationDate).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AdmixturesDetails>().Property(e => e.DateOfIssue).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AdmixturesDetails>().Property(e => e.TerminationDate).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AdditivesDetail>().Property(e => e.DateOfIssue).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AdditivesDetail>().Property(e => e.TerminationDate).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AggregateDetails>().Property(e => e.DateOfIssue).HasColumnType("datetime2");
            modelBuilder.Entity<md_material_AggregateDetails>().Property(e => e.TerminationDate).HasColumnType("datetime2");
            modelBuilder.Entity<md_masterData_ConcentratesDetails>().Property(e => e.DateOfIssue).HasColumnType("datetime2");
            modelBuilder.Entity<md_masterData_ConcentratesDetails>().Property(e => e.TerminationDate).HasColumnType("datetime2");

            base.OnModelCreating(modelBuilder);
        }

    }
}