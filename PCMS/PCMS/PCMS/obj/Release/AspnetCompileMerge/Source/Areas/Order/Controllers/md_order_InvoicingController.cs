﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PCMS.Models;
using PCMS.Areas.Material.Models;
using PCMS.Areas.Order.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System.Data.Entity.Core.Objects;
using System.Data;
using PCMS.Areas.Weigh.Models;
using System.Data.Entity;
using PCMS.Areas.MasterData.Models;
using System.Drawing;
using System.Net;


namespace PCMS.Areas.Order.Controllers
{
    public class md_order_InvoicingController : Controller
    {
        private OrderContext db = new OrderContext();
        private WeighContext dbw = new WeighContext();
        public string CustNoTmp;
        
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.InvoiceHeader.Where(a=>a.Storno == false).OrderByDescending(m => m.InvoiceDate).OrderByDescending(m=>m.InvoiceNumber).ToList();
            if(pg != null)
            {
                foreach (TopFilter top in pg.topFilter)
                {
                    switch (top.colName)
                    {
                        case "InvoiceDate":
                            if (!String.IsNullOrEmpty(top.from))
                            {
                                DateTime from = new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date;

                                //model = model.Where(m => EntityFunctions.TruncateTime(m.InvoiceDate) >= from).ToList();
                                model = model.Where(m => m.InvoiceDate == from).ToList();
                                ViewBag.ProbTakingDatetimeFrom = top.from;
                                ViewBag.ProbTakingDatetimeTo = top.to;
                            }
                            break;
                    }
                }

                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if(!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "InvoiceNo":
                                model = model.Where(m => m.InvoiceNumber.Equals(pgFF.colVal)).ToList();
                                ViewBag.InvoiceNo = pgFF.colVal;
                                break;
                            case "Customer":
                                model = model.Where(m => m.CustomerDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.InvoiceNo = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch(pg.orderCol)
            {
                case "InvoiceNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.InvoiceNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.InvoiceNumber).ToList();
                    break;
                case "Customer":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerDescription).ToList();
                    break;
                case "InvoiceDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.InvoiceDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.InvoiceDate).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? PCMS.Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            string day = DateTime.Now.Day.ToString();
            string month = DateTime.Now.Month.ToString();
            string year = DateTime.Now.Year.ToString();

            if(day.Length == 1)
            {
                day = "0" + day;
            }
            if(month.Length == 1)
            {
                month = "0" + month;
            }

            string date = day + "." + month + "." + DateTime.Now.Year.ToString();
            ViewBag.Date = date;
            ViewBag.InvoiceNo = db.Md_Config.Where(a => a.Id == 1).Select(a => a.InvoiceNumber).FirstOrDefault();
            ViewBag.PaymentTerms = "Zahlbar innerhalb 10 Tagen ohne Abzug nach Rechnungsdatum";
            ViewBag.Error = "";
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(md_order_InvoiceHeader model)
        {
            bool IsError = false;
            long InvoiceNo = db.Md_Config.Where(a => a.Id == 1).Select(a => a.InvoiceNumber).FirstOrDefault();

            int PosCount = 0;
            md_order_InvoicePositions modelPos = new md_order_InvoicePositions();

            if(ModelState.IsValid)
            {
                try
                {
                    if (Request["PositionsCount"] != null || Request["PositionsCount"] != "")
                    {
                        PosCount = Convert.ToInt16(Request["PositionsCount"]);
                    }

                    var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                    var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                    var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                    var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                    var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                    md_Config configModel = new md_Config();
                    long f = Int64.Parse(valueLoading);
                    configModel.ContingentOrderNumber = valueContingent;
                    configModel.Id = 1;
                    configModel.LoadingOrderNumber = valueLoading;
                    configModel.BasicCalculation = valueBasicCalc;
                    configModel.MixerSize = valueMixerSize;
                    configModel.CustomerNumber = valueCustomerNumber;
                    configModel.InvoiceNumber = (InvoiceNo + 1);
                    
                    model.IsPrinted = false;
                    model.Settled = false;
                    model.Storno = false;
                    if(model.InvoiceDate == null)
                    {
                        model.InvoiceDate = DateTime.Now;
                    }
                    db.InvoiceHeader.Add(model);
                    db.Entry(configModel).State = EntityState.Modified;
                    db.SaveChanges();
                    long Hid = model.Id;

                    for (int i = 0; i < PosCount; i++)
                    {
                        modelPos.FidHeader = Hid;
                        if (Request["Date" + i] != null || Request["Date" + i] != "")
                        {
                            DateTime date = new DateTime();
                            DateTime.TryParse(Request["Date" + i], out date);
                            modelPos.DeliveryDate = date.Date;
                        }

                        if (Request["OrderNo" + i] != null || Request["OrderNo" + i] != "")
                        {
                            modelPos.OrderNumber = Request["OrderNo" + i];
                        }
                        else
                        {
                            modelPos.OrderNumber = null;
                        }
                        if (Request["Discount" + i] != null || Request["Discount" + i] != "")
                        {
                            modelPos.Discount = Convert.ToDecimal(Request["Discount" + i]);
                        }
                        else
                        {
                            modelPos.Discount = 0;
                        }
                        if (Request["Price" + i] != null || Request["Price" + i] != "")
                        {
                            modelPos.Price = Convert.ToDecimal(Request["Price" + i]);
                        }
                        else
                        {
                            IsError = true;
                            ViewBag.Error = "Preis fehlt!";
                        }
                        if (Request["Quantity" + i] != null || Request["Quantity" + i] != "")
                        {
                            modelPos.Quantity = Convert.ToDecimal(Request["Quantity" + i].Replace(".",","));
                        }
                        else
                        {
                            IsError = true;
                            ViewBag.Error = "Menge fehlt!";
                        }
                        if (Request["Tax" + i] != null || Request["Tax" + i] != "")
                        {
                            modelPos.Tax = Convert.ToDecimal(Request["Tax" + i]);
                        }
                        else
                        {
                            modelPos.Tax = 19;
                        }
                        if (Request["ArticleNumber" + i] != null || Request["ArticleNumber" + i] != "")
                        {
                            modelPos.ArticleNumber = Request["ArticleNumber" + i];
                        }
                        else
                        {
                            IsError = true;
                            ViewBag.Error = "Artikelnummer fehlt!";
                        }
                        if (Request["ArticleDescription" + i] != null || Request["ArticleDescription" + i] != "")
                        {
                            modelPos.ArticleDescription = Request["ArticleDescription" + i];
                        }
                        else
                        {
                            IsError = true;
                            ViewBag.Error = "Artikel Bezeichnung fehlt!";
                        }
                        if (Request["Unit" + i] != null || Request["Unit" + i] != "")
                        {
                            modelPos.Unit = Request["Unit" + i];
                        }
                        else
                        {
                            modelPos.Unit = "";
                        }

                        if (!String.IsNullOrEmpty(modelPos.ArticleNumber) && modelPos.Quantity != 0 && modelPos.Price != 0)
                        {
                            db.InvoicePositions.Add(modelPos);
                            db.SaveChanges();
                        }

                        var modelWeight = dbw.Md_order_Weighing.Where(a => a.OrderNumber == modelPos.OrderNumber).FirstOrDefault();
                        if (modelWeight != null)
                        {
                            modelWeight.InvoiceCreated = true;
                            dbw.Entry(modelWeight).State = EntityState.Modified;
                            dbw.SaveChanges();
                        }
                    }

                    IsError = false;
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                    var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                    var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                    var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                    var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                    md_Config configModel = new md_Config();
                    long f = Int64.Parse(valueLoading);
                    configModel.ContingentOrderNumber = valueContingent;
                    configModel.Id = 1;
                    configModel.LoadingOrderNumber = valueLoading;
                    configModel.BasicCalculation = valueBasicCalc;
                    configModel.MixerSize = valueMixerSize;
                    configModel.CustomerNumber = valueCustomerNumber;
                    configModel.InvoiceNumber = (InvoiceNo - 1);

                    db.InvoiceHeader.Remove(model);
                    db.SaveChanges();
                    ViewBag.Error = ex.ToString();
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Edit(md_order_InvoiceHeader model, long? id)
        {
            model = db.InvoiceHeader.Find(id);

            ViewBag.OrderNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.OrderNumber).ToList());
            ViewBag.ArtNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleNumber).ToList());
            ViewBag.MatNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.MaterialNumber).ToList());
            ViewBag.ArtDesc = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleDescription).ToList());
            ViewBag.Unit = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Unit).ToList());
            ViewBag.Tax = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Tax).ToList());
            ViewBag.Quantity = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Quantity).ToList());
            ViewBag.Price = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Price).ToList());
            ViewBag.Discount = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Discount).ToList());
            ViewBag.Date = new List<DateTime?>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.DeliveryDate).ToList());
            ViewBag.rows = db.InvoicePositions.Where(a => a.FidHeader == model.Id).Count();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_order_InvoiceHeader model)
        {
            if (model.Settled)
            {
                ViewBag.OrderNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.OrderNumber).ToList());
                ViewBag.ArtNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleNumber).ToList());
                ViewBag.MatNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.MaterialNumber).ToList());
                ViewBag.ArtDesc = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleDescription).ToList());
                ViewBag.Unit = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Unit).ToList());
                ViewBag.Tax = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Tax).ToList());
                ViewBag.Quantity = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Quantity).ToList());
                ViewBag.Price = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Price).ToList());
                ViewBag.Discount = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Discount).ToList());
                ViewBag.Date = new List<DateTime?>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.DeliveryDate).ToList());
                ViewBag.rows = db.InvoicePositions.Where(a => a.FidHeader == model.Id).Count();
                ViewBag.Error = "Bereits bezahlte Rechnungen können nicht geändert werden!";
                return View(model);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    int PosCount = 0;
                    md_order_InvoicePositions modelPos = new md_order_InvoicePositions();

                    

                    if (Request["PositionsCount"] != null || Request["PositionsCount"] != "")
                    {
                        PosCount = Convert.ToInt16(Request["PositionsCount"]);
                    }

                    model.IsPrinted = false;
                    model.Settled = false;
                    model.Storno = false;
                    model.InvoiceDate = DateTime.Now;
                    db.Entry(model).State = EntityState.Modified;


                    var PosToDel = db.InvoicePositions.Where(a => a.FidHeader == model.Id);

                    foreach (var items in PosToDel)
                    {
                        md_order_InvoicePositions Positions = db.InvoicePositions.Find(items.Id);
                        db.Entry(Positions).State = EntityState.Deleted;
                        
                    }
                    db.SaveChanges();
                    for (int i = 0; i < PosCount; i++)
                    {
                        ViewBag.Date = new List<DateTime?>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.DeliveryDate).ToList());
                        ViewBag.OrderNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.OrderNumber).ToList());
                        ViewBag.ArtNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleNumber).ToList());
                        ViewBag.MatNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.MaterialNumber).ToList());
                        ViewBag.ArtDesc = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleDescription).ToList());
                        ViewBag.Unit = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Unit).ToList());
                        ViewBag.Tax = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Tax).ToList());
                        ViewBag.Quantity = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Quantity).ToList());
                        ViewBag.Price = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Price).ToList());
                        ViewBag.Discount = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Discount).ToList());
                        
                        ViewBag.rows = db.InvoicePositions.Where(a => a.FidHeader == model.Id).Count();

                        modelPos.FidHeader = model.Id;
                        if (Request["Date" + i] != null || Request["Date" + i] != "")
                        {
                            modelPos.DeliveryDate = Convert.ToDateTime(Request["Date" + i]);
                        }
                        if (Request["OrderNo" + i] != null || Request["OrderNo" + i] != "")
                        {
                            modelPos.OrderNumber = Request["OrderNo" + i];
                        }
                        else
                        {
                            modelPos.OrderNumber = null;
                        }
                        if (Request["Discount" + i] != null || Request["Discount" + i] != "")
                        {
                            modelPos.Discount = Convert.ToDecimal(Request["Discount" + i]);
                        }
                        else
                        {
                            modelPos.Discount = 0;
                        }
                        if (Request["Price" + i] != null || Request["Price" + i] != "")
                        {
                            modelPos.Price = Convert.ToDecimal(Request["Price" + i]);
                        }
                        else
                        {
                            ViewBag.Error = "Preis fehlt!";
                            return View(model);
                        }
                        if (Request["Quantity" + i] != null || Request["Quantity" + i] != "")
                        {
                            modelPos.Quantity = Convert.ToDecimal(Request["Quantity" + i]);
                        }
                        else
                        {
                            ViewBag.Error = "Menge fehlt!";
                            return View(model);
                        }
                        if (Request["Tax" + i] != null || Request["Tax" + i] != "")
                        {
                            modelPos.Tax = Convert.ToDecimal(Request["Tax" + i]);
                        }
                        else
                        {
                            modelPos.Tax = 19;
                        }
                        if (Request["ArticleNumber" + i] != null || Request["ArticleNumber" + i] != "")
                        {
                            modelPos.ArticleNumber = Request["ArticleNumber" + i];
                        }
                        else
                        {
                            ViewBag.Error = "Artikelnummer fehlt!";
                            return View(model);
                        }
                        if (Request["ArticleDescription" + i] != null || Request["ArticleDescription" + i] != "")
                        {
                            modelPos.ArticleDescription = Request["ArticleDescription" + i];
                        }
                        else
                        {
                            ViewBag.Error = "Artikel Bezeichnung fehlt!";
                            return View(model);
                        }
                        if (Request["Unit" + i] != null || Request["Unit" + i] != "")
                        {
                            modelPos.Unit = Request["Unit" + i];
                        }
                        else
                        {
                            modelPos.Unit = "";
                        }
                        if (modelPos.ArticleNumber != null && modelPos.Quantity != 0 && modelPos.Price != 0)
                        {
                            db.InvoicePositions.Add(modelPos);
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ArtNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleNumber).ToList());
                    ViewBag.MatNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.MaterialNumber).ToList());
                    ViewBag.ArtDesc = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.ArticleDescription).ToList());
                    ViewBag.Unit = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Unit).ToList());
                    ViewBag.Tax = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Tax).ToList());
                    ViewBag.Quantity = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Quantity).ToList());
                    ViewBag.Price = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Price).ToList());
                    ViewBag.Discount = new List<decimal>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.Discount).ToList());
                    ViewBag.Date = new List<DateTime?>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.DeliveryDate).ToList());
                    ViewBag.OrderNo = new List<string>(db.InvoicePositions.Where(a => a.FidHeader == model.Id).Select(a => a.OrderNumber).ToList());
                    ViewBag.rows = db.InvoicePositions.Where(a => a.FidHeader == model.Id).Count();

                    return View(model);
                }
            }
        }
        
        public ActionResult Delete(md_order_InvoiceHeader model, long? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.InvoiceHeader.Find(id);
            if(model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(md_order_InvoiceHeader model, long? id)
        {
            model = db.InvoiceHeader.Find(id);
            var modelPos = db.InvoicePositions.Where(a => a.FidHeader == id).ToList();

            model.Storno = true;

            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();


            for(int i = 0; i<modelPos.Count; i++)
            {
                var Posi = modelPos[i].OrderNumber;
                var modelWeight = dbw.Md_order_Weighing.Where(a => a.OrderNumber == Posi).FirstOrDefault();
                if (modelWeight != null)
                {
                    modelWeight.InvoiceCreated = false;
                    dbw.Entry(modelWeight).State = EntityState.Modified;
                    dbw.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }

            public ActionResult ChooseArticle(PaginationModel pg, int? field)
        {
            MasterDataContext db2 = new MasterDataContext();
            ViewBag.field = field;
            var model = db2.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ArticleNo":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Article":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Price":
                                model = model.Where(m => m.Price.Equals(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "ArticleNo":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Article":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Price":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Price).ToList();
                    else
                        model = model.OrderByDescending(m => m.Price).ToList();
                    break;
                case "ArticleNumberEDV":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10).ToList());
        }


        public ActionResult ChooseCustomer(PaginationModel pg)
        {
            MasterDataContext db2 = new MasterDataContext();

            var model = new List<md_masterData_Customer>();
            model.Clear();
            
            var weighings = (from x in dbw.Md_order_Weighing group x by x.CustomerId).ToList();
            int i = 0;
            foreach(var items in weighings)
            {
                long id = Convert.ToInt64(weighings[i].Where(a=>a.Status == 2 && a.InvoiceCreated == false).Select(a=>a.CustomerId).FirstOrDefault());
                if (id != 0)
                {
                    var test = db2.Md_masterData_Customer.Find(id);
                    model.Add(test);
                }
                i++;
            }
            
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList(1,1000));
        }

        public ActionResult CreateFromDelivery(PaginationModel pg, string CustNo)
        {
            if(Request["CustomerNumber"] != null)
            {
                CustNoTmp = Request["CustomerNumber"].ToString();
            }
            var model = dbw.Md_order_Weighing.Where(a=>a.Status == 2 && a.InvoiceCreated == false).Include(m => m.Weight1).Include(m => m.Weight2).OrderByDescending(m => m.ArticleDescription).AsQueryable();
            if(!String.IsNullOrEmpty(CustNo))
            {
                model = model.Where(m => m.CustomerNumber == CustNo && m.InvoiceCreated == false && m.Status == 2).Include(m=>m.Weight1).Include(m => m.Weight2).OrderByDescending(m=>m.ArticleDescription).AsQueryable();
            }
            
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "OrderNumber":
                                if(!String.IsNullOrEmpty(CustNoTmp))
                                {
                                    model = model.Where(m => m.OrderNumber != null && m.OrderNumber.Equals(pgFF.colVal) && m.CustomerNumber == CustNoTmp).ToList().AsQueryable();
                                }
                                else
                                {
                                    model = model.Where(m => m.OrderNumber != null && m.OrderNumber.Equals(pgFF.colVal)).ToList().AsQueryable();
                                }
                                ViewBag.OrderNumber = pgFF.colVal;
                                break;
                            case "LicensePlate":
                                if(!String.IsNullOrEmpty(CustNoTmp))
                                {
                                    model = model.Where(m => m.LicensePlate.Contains(pgFF.colVal) && m.CustomerNumber == CustNoTmp).ToList().AsQueryable();
                                }
                                else
                                {
                                    model = model.Where(m => m.LicensePlate.Contains(pgFF.colVal)).ToList().AsQueryable();
                                }
                                ViewBag.LicensePlate = pgFF.colVal;
                                break;
                            case "Construction":
                                if(!String.IsNullOrEmpty(CustNoTmp))
                                {
                                    model = model.Where(m => m.ConstructionSiteDescription.ToLower().Contains(pgFF.colVal) && m.CustomerNumber == CustNoTmp).ToList().AsQueryable();
                                }
                                else
                                {
                                    model = model.Where(m => m.ConstructionSiteDescription.ToLower().Contains(pgFF.colVal)).ToList().AsQueryable();
                                }
                                ViewBag.Construction = pgFF.colVal;
                                break;
                            case "Article":
                                if(!String.IsNullOrEmpty(CustNoTmp))
                                {
                                    model = model.Where(m => m.ArticleDescription.ToLower().Contains(pgFF.colVal) && m.CustomerNumber == CustNoTmp).ToList().AsQueryable();
                                }
                                else
                                {
                                    model = model.Where(m => m.ArticleDescription.ToLower().Contains(pgFF.colVal)).ToList().AsQueryable();
                                }
                                ViewBag.Article = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "OrderNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.OrderNumber).ToList().AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.OrderNumber).ToList().AsQueryable();
                    break;
                case "LicensePlate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LicensePlate).ToList().AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.LicensePlate).ToList().AsQueryable();
                    break;
                case "Construction":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ConstructionSiteDescription).ToList().AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.ConstructionSiteDescription).ToList().AsQueryable();
                    break;
                case "Article":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleDescription).ToList().AsQueryable();
                    else
                        model = model.OrderByDescending(m => m.ArticleDescription).ToList().AsQueryable();
                    break;
            }
            if(!String.IsNullOrEmpty(CustNo))
            {
                ViewBag.CustNo = CustNo;
            }
            
            return View(model.ToPagedList(1,1000));
        }

        public ActionResult GetWeighing(long? Id)
        {
            if(Id != null)
            {
                decimal Net = 0;
                var model = dbw.Md_order_Weighing.Find(Id);
                if(model.Weight1 != null && model.Weight2 != null)
                {
                    if(model.Weight1.Weight > model.Weight2.Weight)
                    {
                        Net = Math.Round(model.Weight1.Weight - model.Weight2.Weight);
                    }
                    else
                    {
                        Net = Math.Round(model.Weight2.Weight - model.Weight1.Weight);
                    }
                }
                return Json(new { ArticleDescription = model.ArticleDescription, ArticleNumber = model.ArticleNumber, ArticlePrice = model.ArticlePrice, NetWeight = Net, RegistrationDate = model.RegistrationDate.Date.ToShortDateString(), OrderNo = model.OrderNumber });
            }
            else
            {
                return Json(false);
            }
        }

        public ActionResult ReportDetailPrint(long? Id)
        {
            ReportDetail(Id, false);
            return Json(new { status = true });
        }
        public ActionResult ReportDetail(long? Id, bool? printr)
        {
            List<ReportParameter> repParams = new List<ReportParameter>();

            // Auslesen der Kopfdaten aus der Datenbank
            var model = db.InvoiceHeader.Find(Id);
            // Auslesen der Positionen aus der Datenbank
            var modelPos = db.InvoicePositions.Where(m => m.FidHeader == model.Id);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();

            // Erstellen der Kopfdaten

            //repParams.Add(new ReportParameter("ServiceDate", model.ServiceDate.ToString()));


            repParams.Add(new ReportParameter("PaymentTerms", model.PaymentTerms));
            repParams.Add(new ReportParameter("FinalDescription", model.FinalDescription));
            repParams.Add(new ReportParameter("TotalDiscount", model.TotalDiscount.ToString() + " %"));
            repParams.Add(new ReportParameter("CustomerNumberEDV", model.CustomerNumberEDV));
            repParams.Add(new ReportParameter("InvoiceNumber", model.InvoiceNumber.ToString()));
            repParams.Add(new ReportParameter("InvoiceDate", model.InvoiceDate.ToShortDateString()));
            repParams.Add(new ReportParameter("Title", model.Title));
            repParams.Add(new ReportParameter("Description", model.Description));
            repParams.Add(new ReportParameter("CustomerDescription", model.CustomerDescription));
            repParams.Add(new ReportParameter("CustomerNumber", model.CustomerNumber));
            repParams.Add(new ReportParameter("CustomerStreet", model.CustomerStreet));
            repParams.Add(new ReportParameter("CustomerZipCode", model.CustomerZipCode.ToString()));
            repParams.Add(new ReportParameter("CustomerCity", model.CustomerCity));
            repParams.Add(new ReportParameter("CustomerState", model.CustomerState));
            repParams.Add(new ReportParameter("CustomerCountry", model.CustomerCountry));

            if (model.IsPrinted == true)
            {
                // Falls eine Rechnung schon einmal ausgedruckt worden ist, muss es, bei weiteren ausdrucken, als Duplikat gekennzeichnet sein.
                repParams.Add(new ReportParameter("Duplikat", "DUPLIKAT"));
            }
            decimal totalNet = 0;
            decimal totalGross = 0;
            decimal totalTax = 0;
            decimal PosSumTemp = 0;
            int pos = 1;

            // Rechnungspositionen auslesen und in eine Liste hinzufügen
            foreach (var dataset in modelPos)
            {


                var ItemCount = (from item in modelPos where item.ArticleDescription == dataset.ArticleDescription group item by item.ArticleDescription into g select new { test = g.Sum(a=>a.Quantity) }).Select(a=>a.test).FirstOrDefault();

                var PosSum = Math.Round(ItemCount * dataset.Price, 2);

                if(PosSum != PosSumTemp)
                {
                    totalNet += PosSum;
                    PosSumTemp = PosSum;
                }

                //totalNet += PosSum;
                totalTax = Math.Round(totalNet / 100 * 19, 2);
                totalGross = totalNet + totalTax;

                var CustNo = model.CustomerNumber;
                var Construction = dbw.Md_order_Weighing.Where(a => a.OrderNumber == dataset.OrderNumber).Select(a => a.ConstructionSiteDescription).FirstOrDefault();

                ds.List.AddListRow(dataset.ArticleNumber, dataset.ArticleDescription, dataset.MaterialNumber, dataset.Quantity.ToString(), dataset.Price.ToString().Replace(".", ",") + " €", "", dataset.Tax.ToString() + " %", dataset.Discount.ToString() + " %", Construction, "", "", pos.ToString(), dataset.OrderNumber, dataset.DeliveryDate.ToString().Substring(0, 10), totalNet.ToString() + " €", totalTax.ToString() + " €", totalGross.ToString() + " €", ItemCount.ToString(), PosSum.ToString() + " €", "");
                
                
                pos = pos + 1;
            }

            repParams.Add(new ReportParameter("TotalNet", Math.Round(totalNet,2).ToString() + " €"));
            repParams.Add(new ReportParameter("TotalGross", Math.Round(totalGross,2).ToString() + " €"));
            repParams.Add(new ReportParameter("TotalTax", Math.Round(totalTax,2).ToString() + " €"));

            // Positionen als Liste übergeben
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            // Definition reportViewer mit Attribute
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;

            reportViewer.LocalReport.Refresh();
            reportViewer.LocalReport.ReportPath = "Reports/LOrder/Invoice.rdlc";
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            //reportViewer.Width = 800;
            //reportViewer.Height = 700;


            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            // rendern einer PDF Datei
            byte[] file = reportViewer.LocalReport.Render("PDF", null, PageCountMode.Actual, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

            // direktes Drucken
            if (printr != null && printr == true)
            {
                Helper.DeliveryNotePrint.Export(reportViewer.LocalReport);
                Helper.DeliveryNotePrint.Print();

                // Änderung der Rechnung im DB dass es schon mal ausgedruckt wurde
                model.IsPrinted = true;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return null;
            }
            return File(file, mimeType);
        }
    }
}