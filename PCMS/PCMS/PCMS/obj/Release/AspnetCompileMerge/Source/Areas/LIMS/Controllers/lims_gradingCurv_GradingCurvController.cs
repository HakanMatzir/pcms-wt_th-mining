﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.LIMS.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.LIMS.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class lims_gradingCurv_GradingCurvController : Controller
    {
        private LimsContext db = new LimsContext();

        // GET: LIMS/lims_gradingCurv_GradingCurv
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.lims_gradingCurv_GradingCurv.Where(m=>m.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "ResciveGroupId":
                                model = model.Where(m => m.ResciveGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.ResciveGroupId = pgFF.colVal;
                                break;
                            case "SieveLineNumber":
                                model = model.Where(m => m.SieveLineNumber != null && m.SieveLineNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.SieveLineNumber = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "SieveRangeId":
                                model = model.Where(m => m.SieveRangeId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.SieveRangeId = pgFF.colVal;
                                break;
                            case "Price":
                                model = model.Where(m => m.Price == Convert.ToDecimal(pgFF.colVal)).ToList();
                                ViewBag.Price = pgFF.colVal;
                                break;
                            case "KValue":
                                model = model.Where(m => m.KValue == Convert.ToDecimal(pgFF.colVal)).ToList();
                                ViewBag.KValue = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "ResciveGroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ResciveGroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.ResciveGroupId).ToList();
                    break;
                case "SieveLineNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SieveLineNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.SieveLineNumber).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "SieveRangeId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SieveRangeId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SieveRangeId).ToList();
                    break;
                case "Price":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Price).ToList();
                    else
                        model = model.OrderByDescending(m => m.Price).ToList();
                    break;
                case "KValue":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.KValue).ToList();
                    else
                        model = model.OrderByDescending(m => m.KValue).ToList();
                    break;
            }

            
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }



        // GET: LIMS/lims_gradingCurv_GradingCurv/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv = db.lims_gradingCurv_GradingCurv.Find(id);
            if (lims_gradingCurv_GradingCurv == null)
            {
                return HttpNotFound();
            }
            return View(lims_gradingCurv_GradingCurv);
        }

        // GET: LIMS/lims_gradingCurv_GradingCurv/Create
        public ActionResult Create()
        {
            ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.ResciveGroupId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View();
        }

        // POST: LIMS/lims_gradingCurv_GradingCurv/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ResciveGroupId,SieveLineNumber,Description,SieveRangeId,Price")] lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv,List<string> materialId)
        {
            if (ModelState.IsValid)
            {
                int i = 0;
                 foreach(string s in materialId)
                {
                    lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.Add(new lims_gradingCurvMaterial() { GradingCurvId = lims_gradingCurv_GradingCurv.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Percent = Convert.ToDecimal(s.Split('|')[1]),Sort = i });
                    //db.Lims_gradingCurvMaterial.Add(new lims_gradingCurvMaterial() { GradingCurvId = lims_gradingCurv_GradingCurv.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Percent = Convert.ToDecimal(s.Split('|')[1]) });
                    i++;
                }
                lims_gradingCurv_GradingCurv.IsDeleted = false;
                lims_gradingCurv_GradingCurv.IsActive = true;
                db.lims_gradingCurv_GradingCurv.Add(lims_gradingCurv_GradingCurv);
                db.SaveChanges();
               
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial = lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.OrderBy(s => s.Sort).ToList();
            ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description");
            ViewBag.ResciveGroupId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(lims_gradingCurv_GradingCurv);
        }

        // GET: LIMS/lims_gradingCurv_GradingCurv/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv = db.lims_gradingCurv_GradingCurv.Find(id);

            

            if (lims_gradingCurv_GradingCurv == null)
            {
                return HttpNotFound();
            }
            lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial = lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.OrderBy(s => s.Sort).ToList();
            ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description", lims_gradingCurv_GradingCurv.Md_sieve_RuleGradingCurve.Id);
            ViewBag.ResciveGroupId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(lims_gradingCurv_GradingCurv);
        }

        // POST: LIMS/lims_gradingCurv_GradingCurv/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ResciveGroupId,SieveLineNumber,Description,SieveRangeId,Price,S0_063,S0_125,S0_25,S0_5,S1,S1_4,S2,S2_8,S4,S5_6,S8,S11_2,S16,S22_4,S31_5,S45,S63")] lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv, List<string> materialId)
        {
            if (ModelState.IsValid)
            {

             
                db.Entry(lims_gradingCurv_GradingCurv).State = EntityState.Modified;
                db.Entry(lims_gradingCurv_GradingCurv).Collection(st => st.Lims_gradingCurvMaterial).Load();
                lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.Clear();
                int i = 0;
                foreach (string s in materialId)
                {
                    lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.Add(new lims_gradingCurvMaterial() { GradingCurvId = lims_gradingCurv_GradingCurv.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Percent = Convert.ToDecimal(s.Split('|')[1].Replace('.',',')),Sort = i });
                    //db.Lims_gradingCurvMaterial.Attach(new lims_gradingCurvMaterial() { GradingCurvId = lims_gradingCurv_GradingCurv.Id, MaterialId = Convert.ToInt64(s.Split('|')[0]), Percent = Convert.ToDecimal(s.Split('|')[1]) });
                    i++;
                }
                lims_gradingCurv_GradingCurv.IsDeleted = false;
                lims_gradingCurv_GradingCurv.IsActive = true;                
                db.SaveChanges();
                calcRecipeMaterial(lims_gradingCurv_GradingCurv);

                return RedirectToAction("Index");
            }
            lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial = lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.OrderBy(s => s.Sort).ToList();
            ViewBag.SieveRangeId = new SelectList(db.Md_sieve_RuleGradingCurve, "Id", "Description", lims_gradingCurv_GradingCurv.Md_sieve_RuleGradingCurve.Id);
            ViewBag.ResciveGroupId = new List<SelectListItem> { new SelectListItem() { Text = "Werk Thannhausen", Value = "1" } };
            return View(lims_gradingCurv_GradingCurv);
        }

        // GET: LIMS/lims_gradingCurv_GradingCurv/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv = db.lims_gradingCurv_GradingCurv.Find(id);
            if (lims_gradingCurv_GradingCurv == null)
            {
                return HttpNotFound();
            }
            return View(lims_gradingCurv_GradingCurv);
        }

        // POST: LIMS/lims_gradingCurv_GradingCurv/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv = db.lims_gradingCurv_GradingCurv.Find(id);
            lims_gradingCurv_GradingCurv.IsActive = true;
            lims_gradingCurv_GradingCurv.IsDeleted = true;
            db.Entry(lims_gradingCurv_GradingCurv).State = EntityState.Modified;
            //db.lims_gradingCurv_GradingCurv.Remove(lims_gradingCurv_GradingCurv);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult choosAggreate(PCMS.Models.PaginationModel pg)
        {
            
            var model = db.md_material_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 1).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                     else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
            }

            return View( model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }
        public ActionResult getMaterialbyId(int id)
        {
            var model = db.md_material_Material.Where(a => a.Id == id).FirstOrDefault();
            var model2 = db.Lims_aggregate_Test.Where(t => t.MaterialId == id).FirstOrDefault();
            //http://stackoverflow.com/questions/5588143/ef-4-1-code-first-json-circular-reference-serialization-error
            //    db.Configuration.ProxyCreationEnabled = false;
            //    public ActionResult GetAll()
            //{
            //    return Json(ppEFContext.Orders
            //                           .Include(o => o.Patient)
            //                           .Include(o => o.Patient.PatientAddress)
            //                           .Include(o => o.CertificationPeriod)
            //                           .Include(o => o.Agency)
            //                           .Include(o => o.Agency.Address)
            //                           .Include(o => o.PrimaryDiagnosis)
            //                           .Include(o => o.ApprovalStatus)
            //                           .Include(o => o.Approver)
            //                           .Include(o => o.Submitter),
            //        JsonRequestBehavior.AllowGet);
            //}

            return Json( new { data1 = model, data2 = model2 });
        }
        public ActionResult loadSieveData(int id)
        {
            var model = db.Md_sieve_RuleGradingCurve.Find(id);
            return Json(new { A = model.getA, B = model.getB, C = model.getC, U = model.getU });
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //**********************************************************************Reporting
        public ActionResult Report(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                //ds.List.AddListRow(dataset.Id.ToString(), dataset.Description, dataset.Materials.ToString(), dataset.SieveLineNumber.ToString(), dataset.KValue.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Description"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Materials"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Sieblinie"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "K Wert"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", ""));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", ""));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Sieblinie Liste"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Sieblinie"));
           // repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count() + " Anzahl Mischlinien: " + model.Sum(c => c.MixingLines).ToString()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;



            ViewBag.ReportViewer = reportViewer;

            return View();
        }


        private List<lims_gradingCurv_GradingCurv> pageModel(PCMS.Models.PaginationModel pg)
        {
            return db.lims_gradingCurv_GradingCurv.ToList();
        }

        private void calcRecipeMaterial(lims_gradingCurv_GradingCurv lims_gradingCurv_GradingCurv)
        {
            Areas.Recipe.Models.RecipeContext dbR = new Recipe.Models.RecipeContext();
            var recipe = dbR.Md_recipe_Recipe.Include(a=>a.RecipeMaterials).Where(a => a.GradingCurvId == lims_gradingCurv_GradingCurv.Id).ToList();
            
            foreach (var re in recipe)
            {

                
                List<Recipe.Models.md_recipe_RecipeMaterial> maRlist = new List<Recipe.Models.md_recipe_RecipeMaterial>();
                decimal sum = 0;
                int sort = 0;
                foreach(var maR in re.RecipeMaterials.Where(a=>a.Md_material_Material.MaterialGroupId == 1 ))
                {
                    sum += maR.Weight / maR.Md_material_Material.Density ?? 1;
                }
                foreach(var maG in lims_gradingCurv_GradingCurv.Lims_gradingCurvMaterial.OrderBy(a => a.Sort)){
                    var weight = sum * (maG.Percent / 100) * dbR.Md_masterData_Material.Where(a => a.Id == maG.MaterialId).FirstOrDefault().Density;
                    maRlist.Add(new Recipe.Models.md_recipe_RecipeMaterial(){RecipeId = re.Id,MaterialId =maG.MaterialId, Weight = weight ?? 0 ,Value = maG.Percent,Unit = 1, Sort= sort,Type = 1 });
                    sort++;
                }
                foreach(var maR in re.RecipeMaterials.Where(a => a.Md_material_Material.MaterialGroupId != 1))
                {
                    maR.Sort = sort;
                    maRlist.Add(maR);
                    sort++;
                }
                dbR.Entry(re).State = EntityState.Modified;
                dbR.Entry(re).Collection(st => st.RecipeMaterials).Load();
                re.RecipeMaterials.Clear();
                re.RecipeMaterials = maRlist;

                dbR.SaveChanges();

            }



        }


        }
}
