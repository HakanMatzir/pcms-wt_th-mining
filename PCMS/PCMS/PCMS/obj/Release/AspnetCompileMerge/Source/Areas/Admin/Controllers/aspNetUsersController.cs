﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Admin.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using signotec.STPadLibNet;

namespace PCMS.Areas.Admin.Controllers
{
    [AuthorizeHelper]
    public class aspNetUsersController : Controller
    {
        // Variablen für Unterschriftenpad Start
        public static ProcessState _processState;
        private static STPadLib _stPad = new STPadLib();
        public static SignPad[] _signPads = null;
        private static List<int> ListOfDevices = new List<int>();
        public static Bitmap lastSignauter = null;
        public static string lastId = "";
        public static string lastError = "";

        public static int selectedDevice = -1;

        private static DisplayTarget _storeIdSigning = DisplayTarget.NewStandardStore;

        private static int _buttonCancelId = -1;
        private static int _buttonRetryId = -1;
        private static int _buttonConfirmId = -1;

        private static FontFamily _fontFamily = new FontFamily(System.Drawing.Text.GenericFontFamilies.Monospace);
        private static int fontSize = 30;
        private static FontStyle fontStyle = FontStyle.Bold;
        // Variablen für Unterschriftenpad Ende

        private AdminContext db = new AdminContext();

        // GET: Admin/aspNetUsers

        public ActionResult Index(PaginationModel pg)
        {
            var model = db.AspNetUsers.ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Email":
                                model = model.Where(m => m.Email != null && m.Email.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Email = pgFF.colVal;
                                break;
                            case "UserName":
                                model = model.Where(m => m.UserName != null && m.UserName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.UserName = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Email":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Email).ToList();
                    else
                        model = model.OrderByDescending(m => m.Email).ToList();
                    break;
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName).ToList();
                    break;
                case "Selectedfacility":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Selectedfacility).ToList();
                    else
                        model = model.OrderByDescending(m => m.Selectedfacility).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }



        // GET: Admin/aspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Create
        public ActionResult Create()
        {
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description");
            return View();
        }

        // POST: Admin/aspNetUsers/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,UserName,Selectedfacility")] Models.aspNetUsers aspNetUsers)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUsers);
                //db.SaveChanges();
                //db.SaveChanges(aspNetUsers.UserName);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility);
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if(aspNetUsers.Sign != null)
            {
                lastSignauter = (System.Drawing.Bitmap)byteArrayToImage(aspNetUsers.Sign);
            }
            else
            {
                lastSignauter = null;
            }
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility).ToList();

            List<string> rolsId = new List<string>();

            foreach (var si in aspNetUsers.UsersRoles)
            {
                rolsId.Add(si.AspNetRoles.Id);
            }
            MultiSelectList rol = new MultiSelectList(db.AspNetRoles.ToList(), "Id", "Name", rolsId);
            ViewBag.roles = rol;

            return View(aspNetUsers);
        }

        // POST: Admin/aspNetUsers/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,UserName,Selectedfacility")] Models.aspNetUsers aspNetUsers, string[] Facilities, string[] userRoles)
        {
            if(lastSignauter == null)
            {
                aspNetUsers.Sign = null;
            }
            else
            {
                aspNetUsers.Sign = imageToByteArray(lastSignauter);
            }
            if (ModelState.IsValid)
            {
                if (Facilities != null)
                {
                    if (!Facilities.Contains(Convert.ToString(aspNetUsers.Selectedfacility)))
                    {
                        aspNetUsers.Selectedfacility = null;
                    }
                }
                var item = db.Entry<Models.aspNetUsers>(aspNetUsers);
                item.State = EntityState.Modified;
                //facility
                item.Collection(i => i.Facilities).Load();
                aspNetUsers.Facilities.Clear();
                //db.Entry(aspNetUsers).State = EntityState.Modified;
                //aspNetUsers.Facilities.Clear();
                if (Facilities != null)
                {
                    foreach (string s in Facilities)
                    {
                        aspNetUsers.Facilities.Add(db.Facilities.Find(Convert.ToInt64(s)));
                    }
                }
                //Roles
                item.Collection(i => i.UsersRoles).Load();
                aspNetUsers.UsersRoles.Clear();
                if (userRoles != null)
                {
                    foreach (string s in userRoles)
                    {
                        aspNetUsers.UsersRoles.Add(new aspNetUserRoles { UserId = aspNetUsers.Id, RoleId = s });
                    }
                }


                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                lastSignauter = null;
                return RedirectToAction("Index");
            }
            ViewBag.Selectedfacility = new SelectList(db.Facilities, "Id", "Description", aspNetUsers.Selectedfacility);
            return View(aspNetUsers);
        }

        // GET: Admin/aspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            if (aspNetUsers == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUsers);
        }

        // POST: Admin/aspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Models.aspNetUsers aspNetUsers = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUsers);
            // db.SaveChanges();
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }
        public ActionResult sign(int? padNo, string id)
        {
            if (_processState != ProcessState.Start)
                CancelProcess();

            if (selectedDevice == -1)
            {
                init(padNo ?? 1);
            }

            if (selectedDevice != -1)
            {
                if (id != null)
                {
                    var user = db.AspNetUsers.Find(id);
                    string message = "";
                    StartCaprute(message);
                }
                else
                {
                    StartCaprute("");
                }
                return Json(new { status = true });
            }
            return Json(new { status = false, error = lastError });
        }
        public ActionResult checkState()
        {
            return Json(new { status = Convert.ToInt32(_processState), id = lastId });
            //return Json(true);
        }
        [HttpGet]
        public ActionResult lastSignature()
        {
            if(lastSignauter != null)
            {
                return base.File(imageToByteArray(lastSignauter), "image/jpeg");
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult unsigned()
        {
            try
            {
                lastSignauter = null;
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // Unterschriftenpad


        public enum ProcessState
        {
            Start = 0,
            Terms,
            Capturing,
            Stopped
        };

        public static int getState()
        {
            return Convert.ToInt32(_processState);
        }
        public static void init(int padNo)
        {
            fontStyle |= FontStyle.Bold;
            _stPad.DeviceDisconnected += new DeviceDisconnectedEventHandler(STPad_DeviceDisconnected);
            _stPad.SensorHotSpotPressed += new SensorHotSpotPressedEventHandler(STPad_SensorHotSpotPressed);
            _stPad.SensorTimeoutOccured += new SensorTimeoutOccuredEventHandler(STPad_SensorTimeoutOccured);
            _stPad.DisplayScrollPosChanged += new DisplayScrollPosChangedEventHandler(STPad_DisplayScrollPosChanged);
            _stPad.SignatureDataReceived += new SignatureDataReceivedEventHandler(STPad_SignatureDataReceived);

            // set app name
            _stPad.ControlAppName = "signoPAD-API Demo";

            // get connected devices and open first one if any
            GetDevices(padNo);
            if (_signPads != null && _signPads.Count() > 0)
            {
                lastError = "";
                OpenDevice();
            }
            else
            {
                lastError = "Kein Gerät gefunden";
            }
        }
        private static void STPad_SignatureDataReceived(object sender, SignatureDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }
        private static void STPad_DisplayScrollPosChanged(object sender, DisplayScrollPosChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private static void STPad_SensorTimeoutOccured(object sender, SensorTimeoutOccuredEventArgs e)
        {
            throw new NotImplementedException();
        }
        private static void STPad_SensorHotSpotPressed(object sender, SensorHotSpotPressedEventArgs e)
        {
            if (e.hotSpotId == _buttonCancelId)
            {
                CancelProcess();
            }

            else if (e.hotSpotId == _buttonRetryId && !(_processState == ProcessState.Terms))
            {
                Retry();
            }

            else if (e.hotSpotId == _buttonConfirmId)
            {
                if (_processState == ProcessState.Terms)
                    // accept disclaimer and start capturing
                    StartDefaultCapturing();
                else
                    ConfirmCapturing();
            }
            //throw new NotImplementedException();
        }
        private static void STPad_DeviceDisconnected(object sender, DeviceDisconnectedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private static void Start()
        {
            GetDevices(1);
            if (_signPads.Count() > 0)
            {
                OpenDevice();
                SetTarget(DisplayTarget.ForegroundBuffer);
                lastError = "";
            }
            else
            {
                lastError = "Kein Gerät gefunden";
            }
        }
        private static void GetDevices(int padNo)
        {
            lastError = "";
            try
            {
                // get number of connected devices
                int deviceCount = _stPad.DeviceGetCount();
                if (deviceCount > 0)
                {
                    selectedDevice = 0;
                    if (padNo == 2 && deviceCount > 1)
                    {
                        selectedDevice = 1;
                    }
                    // build device list
                    _signPads = new SignPad[deviceCount];
                    for (int i = 0; i < deviceCount; i++)
                    {
                        _signPads[i] = new SignPad(_stPad, i);
                    }
                }
                else
                {
                    lastError = "Kein Gerät gefunden";
                    // throw NotImplementedException;
                }
            }
            catch (STPadException exc)
            {
                lastError = exc.Message;
                var s = "";
                _signPads = new SignPad[0];
                //MessageBox.Show(exc.Message);
            }
        }
        public static void CancelProcess()
        {
            try
            {
                if (_stPad.SignatureState)
                    // cancel capturing (this clears the LCD, too)
                    _stPad.SignatureCancel();
                else
                    // erase LCD
                    _stPad.DisplayErase();
                // clear all hot spots
                ClearHotSpots();
            }
            catch (STPadException exc)
            {

            }
            _processState = ProcessState.Start;
        }
        private static void DrawText(int x, int y, string text, TextAlignment align)
        {
            _stPad.DisplaySetText(x, y, align, text);
        }
        private static void SetFont()
        {
            if ((selectedDevice < 0) || !_signPads[selectedDevice].Open)
                return;
            try
            {
                Font font = new Font(_fontFamily, fontSize, fontStyle);
                _stPad.DisplaySetFont(font);
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }
            try
            {
                // set font color
                _stPad.DisplaySetFontColor(Color.Black);
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }
        }
        private static void OpenDevice()
        {
            if (!(_signPads[selectedDevice].Sigma || _signPads[selectedDevice].Omega ||
                  _signPads[selectedDevice].Gamma || _signPads[selectedDevice].Alpha))
                return;
            try
            {
                // open device
                try
                {
                    _signPads[selectedDevice].DeviceOpen();
                }
                catch (STPadException exc)
                {
                    //MessageBox.Show(exc.Message);
                    return;
                }
                // get sample rate
                SampleRate sampleRate = _stPad.SensorGetSampleRateMode();
            }
            catch (STPadException exc)
            {
                CloseDevice();
                //MessageBox.Show(exc.Message);
            }
            finally
            {

            }
        }
        private static void CloseDevice()
        {
            try
            {
                _signPads[selectedDevice].DeviceClose();
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }
            //ResetAfterClose();    
            try
            {
                ClearHotSpots();
            }
            catch { }
        }
        private static void ClearHotSpots()
        {
            try
            {
                _stPad.SensorClearHotSpots();
            }
            catch (STPadException exc)
            {
                if (exc.ErrorCode != -22)
                    throw exc;
            }
        }
        private static void clearDisplay()
        {
            try
            {
                // erase LCD
                _stPad.DisplayErase();
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }
        }
        private static DisplayTarget SetTarget(DisplayTarget target)
        {
            // return if pad is closed

            // set target
            DisplayTarget id = _stPad.DisplaySetTarget(target);
            try
            {

            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }
            return id;
        }

        private static bool ShowDisclaimer(string message)
        {
            // display disclaimer and two buttons "Cancel" and "Confirm"
            SetFont();
            try
            {
                // clear signature window
                _stPad.SensorClearSignRect();

                // erase LCD and background buffer
                _stPad.DisplayErase();

                try
                {
                    _stPad.DisplaySetFontColor(Color.Black);
                }
                catch { }
                if (_signPads[selectedDevice].Sigma)
                {
                    // do all the following drawing operations in the background buffer
                    SetTarget(DisplayTarget.BackgroundBuffer);

                    // load button bitmaps and set hot spots
                    // "Cancel" button
                    Bitmap button = new Bitmap(@"C:\inetpub\wwwroot\PCMS-WT\Content\signotec\DefaultCancel_sigma.png");

                    int x = 20;
                    int y = _stPad.DisplayHeight - button.Height - 7;
                    _stPad.DisplaySetImage(x, y, button);
                    _buttonCancelId = _stPad.SensorAddHotSpot(x, y, button.Width, button.Height);


                    // "Confirm" button
                    button = new Bitmap(@"C:\inetpub\wwwroot\PCMS-WT\Content\signotec\DefaultOK_sigma.png");
                    x = _stPad.DisplayWidth - button.Width - 20;
                    _stPad.DisplaySetImage(x, y, button);

                    _buttonConfirmId = _stPad.SensorAddHotSpot(x, y, button.Width, button.Height);

                    // display disclaimer
                    _stPad.DisplaySetTextInRect(10, 10, _stPad.DisplayWidth - 20, _stPad.DisplayHeight - 60, TextAlignment.Right, message);
                }


                // do all drawing operations on the LCD
                SetTarget(DisplayTarget.ForegroundBuffer);

                // draw buffered image
                _stPad.DisplaySetImageFromStore(DisplayTarget.BackgroundBuffer);
                _processState = ProcessState.Terms;
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
                return false;
            }
            finally
            {

            }
            return true;
        }
        private static void Retry()
        {
            try
            {
                _stPad.SignatureRetry();
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }

        }
        private static void Cancel()
        {
            try
            {
                _stPad.SignatureCancel();
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }

        }
        private static bool StartDefaultCapturing()
        {
            //this.Cursor = Cursors.WaitCursor;

            try
            {
                // erase display
                _stPad.DisplayErase();

                // clear hot spots
                ClearHotSpots();

                if (_signPads[selectedDevice].Sigma ||
                    (_signPads[selectedDevice].Gamma && _signPads[selectedDevice].USB) ||
                    (_signPads[selectedDevice].Alpha && (_signPads[selectedDevice].USB || _signPads[selectedDevice].IP)))
                {   // "fast" pad or connection

                    SetTarget(DisplayTarget.BackgroundBuffer);
                }
                else
                {   // "slow" pad or connection: do all drawing operations in the permanent memory
                    // make sure that always the second permanent memory is used
                }

                // draw the bitmaps
                Bitmap bitmap = null;
                if (_signPads[selectedDevice].Sigma)
                    //bitmap = new Bitmap(@"C:\Program Files (x86)\signotec\signoPAD-API\Samples\C#.NET\Source\STPadLibNet Demo App\STPadLibNet Demo App\Resources\DefaultBitmap_sigma.png"); //STPadLibNet_Demo_App.Properties.Resources.DefaultBitmap_sigma;
                    bitmap = new Bitmap(@"C:\inetpub\wwwroot\PCMS-WT\Content\signotec\DefaultBitmap_sigma.png");

                _stPad.DisplaySetImage(0, 0, bitmap);

                if (_signPads[selectedDevice].Omega ||
                    (_signPads[selectedDevice].Gamma && !_signPads[selectedDevice].USB) ||
                    (_signPads[selectedDevice].Alpha && !(_signPads[selectedDevice].USB || _signPads[selectedDevice].IP)))
                {

                    // do all drawing operations in the background buffer
                    SetTarget(DisplayTarget.BackgroundBuffer);

                    // draw stored image
                    _stPad.DisplaySetImageFromStore(_storeIdSigning);
                }

                if (_signPads[selectedDevice].Alpha)
                    // draw disclaimer
                    _stPad.DisplaySetTextInRect(50, 250, _stPad.DisplayWidth - 100, 300, TextAlignment.Left, "With my signature, I certify that I'm excited about the signotec LCD Signature Pad and the signotec Pad Capture Control. This demo application has blown me away and I can't wait to integrate all these great features in my own application.");

                int x = 0;
                int y = 0;
                int width = 0;
                int height = 0;

                // do all drawing operations on the  LCD directly
                SetTarget(DisplayTarget.ForegroundBuffer);


                _stPad.DisplaySetImageFromStore(DisplayTarget.BackgroundBuffer);

                x = _signPads[selectedDevice].Alpha ? 90 : 0;
                y = _signPads[selectedDevice].Sigma ? 50 : ((_signPads[selectedDevice].Omega || _signPads[selectedDevice].Gamma) ? 100 : 600);
                width = _signPads[selectedDevice].Alpha ? 590 : 0;
                height = _signPads[selectedDevice].Alpha ? 370 : 0;
                _stPad.SensorSetSignRect(x, y, width, height);

                x = _signPads[selectedDevice].Sigma ? 12 : ((_signPads[selectedDevice].Omega || _signPads[selectedDevice].Gamma) ? 24 : 30);
                y = _signPads[selectedDevice].Sigma ? 9 : ((_signPads[selectedDevice].Omega || _signPads[selectedDevice].Gamma) ? 18 : 30);
                width = _signPads[selectedDevice].Sigma ? 85 : ((_signPads[selectedDevice].Omega || _signPads[selectedDevice].Gamma) ? 170 : 80);
                height = _signPads[selectedDevice].Sigma ? 33 : ((_signPads[selectedDevice].Omega || _signPads[selectedDevice].Gamma) ? 66 : 80);
                _buttonCancelId = _stPad.SensorAddHotSpot(x, y, width, height);
                x = _signPads[selectedDevice].Sigma ? 117 : (_signPads[selectedDevice].Omega ? 234 : (_signPads[selectedDevice].Gamma ? 315 : 344));
                _buttonRetryId = _stPad.SensorAddHotSpot(x, y, width, height);
                x = _signPads[selectedDevice].Sigma ? 222 : (_signPads[selectedDevice].Omega ? 444 : (_signPads[selectedDevice].Gamma ? 604 : 658));
                _buttonConfirmId = _stPad.SensorAddHotSpot(x, y, width, height);


                // start capturing
                _stPad.SignatureStart();


                _processState = ProcessState.Capturing;
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
                return false;
            }
            finally
            {
                // this.Cursor = Cursors.Default;
            }
            return true;
        }
        private static void ConfirmCapturing()
        {
            try
            {
                // clear hot spots
                ClearHotSpots();

                // confirm capturing
                int Count = _stPad.SignatureConfirm();

                _processState = ProcessState.Start;
                SaveImg();
            }
            catch (STPadException exc)
            {
                //MessageBox.Show(exc.Message);
            }


        }
        private static void SaveImg()
        {
            SignatureImageFlag options = SignatureImageFlag.None;
            ImageFormat format = ImageFormat.Jpeg;

            int resolution = 300;
            int width = 0;
            int height = 0;
            int penWidth = 0;
            Color penColor = Color.Black;

            try
            {
                var test = new AdminContext();
                lastSignauter = _stPad.SignatureSaveAsStreamEx(resolution, width, height, penWidth, penColor, options);

                _stPad.DeviceClose(selectedDevice);
                selectedDevice = -1;

            }
            catch (STPadException exc)
            {
                var test = "";
                //MessageBox.Show(exc.Message);
            }
        }

        private static void Closed()
        {
            _stPad.Dispose();
        }

        private static void Clear()
        {
            clearDisplay();
        }

        private static void SetText()
        {
            SetFont();
            DrawText(40, 40, "Hallo", TextAlignment.Left);
        }
        public static void StartCaprute(string message)
        {
            clearDisplay();
            if (String.IsNullOrEmpty(message))
                StartDefaultCapturing();
            else
            {
                ShowDisclaimer(message);
            }
        }
        private static void btnSave_Click()
        {
            SaveImg();
        }
        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }


    }
}
