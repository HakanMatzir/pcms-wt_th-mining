﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Order.Models;
using Microsoft.Reporting.WebForms;
using PCMS.Helper;
using PCMS.Models;
using PagedList;
using Newtonsoft.Json;

namespace PCMS.Areas.Order.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_order_OrderController : Controller
    {
        public string Type;
        public bool checkOrder = false;
        public List<long?> BinderId = new List<long?>();
        public List<string> BinderNumber = new List<string>();
        public List<string> BinderDescription = new List<string>();
        public List<string> BinderValue = new List<string>();
        public List<string> BinderAdmit = new List<string>();
        public List<long> BinderSort = new List<long>();

        public List<long?> AdmixId = new List<long?>();
        public List<string> AdmixNumber = new List<string>();
        public List<string> AdmixDescription = new List<string>();
        public List<string> AdmixValue = new List<string>();
        public List<string> AdmixAdmit = new List<string>();
        public List<long> AdmixSort = new List<long>();

        public List<long?> WaterId = new List<long?>();
        public List<string> WaterNumber = new List<string>();
        public List<string> WaterDescription = new List<string>();
        public List<string> WaterValue = new List<string>();
        public List<string> WaterAdmit = new List<string>();
        public List<long> WaterSort = new List<long>();

        public List<long?> AggregateId = new List<long?>();
        public List<string> AggregateNumber = new List<string>();
        public List<string> AggregateDescription = new List<string>();
        public List<string> AggregateValue = new List<string>();
        public List<long> AggregateSort = new List<long>();

        public List<long?> AdditiveId = new List<long?>();
        public List<string> AdditiveNumber = new List<string>();
        public List<string> AdditiveDescription = new List<string>();
        public List<string> AdditiveValue = new List<string>();
        public List<long> AdditiveSort = new List<long>();

        public List<long?> MatId = new List<long?>();
        public List<string> MatNo = new List<string>();
        public List<string> MatDesc = new List<string>();
        public List<string> MatValue = new List<string>();
        public List<int> MatType = new List<int>();
        public List<int> MatGroup = new List<int>();

        public List<string> newSetpoint = new List<string>();
        public List<string> newOrderSetpoint = new List<string>();
        public List<string> LocNo = new List<string>();
        public List<bool> LocUnlock = new List<bool>();


        private OrderContext db = new OrderContext();

        public ActionResult Index()
        {
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            var modelLoading = db.Md_order_LoadingOrder.Where(a => a.RegistrationDate.Value.Day == day && a.RegistrationDate.Value.Month == month && a.RegistrationDate.Value.Year == year && a.State == 2).OrderBy(a => a.Sort).ToList();
            var modelContingent = db.Md_order_ContingentOrder.Where(a => a.RegistrationDate.Value.Day == day && a.RegistrationDate.Value.Month == month && a.RegistrationDate.Value.Year == year).OrderBy(a => a.State).ThenBy(a => a.LoadingTime).ToList();
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

            var OrderId = new List<long?>();
            var IsValue = new List<decimal?>();

            for (int i = 0; i < modelLoading.Count(); i++)
            {
                var tmpArticleNo = modelLoading[i].MaterialNumber;
                OrderId.Add(modelLoading[i].Id);
                var tmpOrderId = OrderId[i];
                IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
            }
            ViewBag.MaterialMoving = new List<decimal?>(IsValue);

            return View(Tuple.Create(modelLoading, modelContingent));
        }

        public ActionResult Create()
        {
            md_order_LoadingOrder model = new md_order_LoadingOrder();
            var value = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();

            model.OrderNumber = value;
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.DeliveryType = db.Md_order_DeliveryNoteType.Where(a => a.Id == 1).Select(a => a.Description).FirstOrDefault();
            ViewBag.DeliveryTypeId = 1;
            ViewBag.Location = Location();
            ViewBag.OrderedDefault = 0;
            ViewBag.DeliveredDefault = 0;
            ViewBag.MaxLotDefault = 0;
            ViewBag.MinLotDefault = 0;
            if (DateTime.Now.DayOfWeek.ToString() == "Saturday" || DateTime.Now.DayOfWeek.ToString() == "Sunday")
            {
                ViewBag.SpNo = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.EDVCode).FirstOrDefault();
                ViewBag.SpDescription = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Description).FirstOrDefault();
                ViewBag.SpValue = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Amount).FirstOrDefault();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(md_order_LoadingOrder model)
        {
            if (ModelState.IsValid)
            {
                // Hier Eintrag in die Tabelle OrderComponents --Start--
                //änderung Manu 2016_05_13 start
                model.UserName = "";
                try
                {
                    Areas.Admin.Models.AdminContext adminContex = new Admin.Models.AdminContext();
                    model.UserName = adminContex.AspNetUsers.Where(a => a.UserName == User.Identity.Name).Select(a => a.Email).FirstOrDefault();
                }
                catch(Exception e)
                {

                }
                //änderung Manu 2016_05_13 end
                if (Helper.Definitions.getFactoryGroup() == 1)
                {
                    if (model.MinLot > (model.OrderedQuantity - model.DeliveredQuantity))
                    {
                        //ErrorMessages("Min Charge darf nicht kleiner sein als Produktionsmenge - Restmenge","Create");
                        ViewBag.ErrorMinCharge = "Min Charge darf nicht kleiner sein als Produktionsmenge - Restmenge";
                        ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                        ViewBag.Location = Location();
                        ViewBag.OrderedDefault = 0;
                        ViewBag.DeliveredDefault = 0;
                        ViewBag.MaxLotDefault = 0;
                        ViewBag.MinLotDefault = 0;
                        return View(model);
                    }
                    else
                    {
                        if (model.Sort == null)
                        {
                            model.Sort = db.Md_order_LoadingOrder.Where(a => a.Sort != null).Count() + 1;
                        }
                        else
                        {
                            var values = db.Md_order_LoadingOrder.Where(a => a.Sort != null).OrderBy(a => a.Sort).ToList();

                            int r = 0;
                            for (int i = 1; i < values.Count; i++)
                            {
                                if (i == model.Sort)
                                    r = 1;
                                values[i].Sort = r + i;
                            }
                        }
                        model.State = 2;
                        model.DeliveryNoteType = null;
                        if (model.RegistrationDate == null)
                        {
                            model.RegistrationDate = DateTime.Now;
                        }
                        db.Md_order_LoadingOrder.Add(model);


                        int counter = 0;

                        long? MaterialId;
                        string MaterialNumber;
                        string MaterialDescription;
                        decimal MaterialValue;
                        int MaterialGroup;
                        int MaterialType;
                        decimal NewSetpoint;
                        decimal OrderSetpoint;
                        string LocationNumber;
                        int twcount;

                        if (Request["MaterialCount"] != null && Request["MaterialCount"] != "")
                        {
                            twcount = Convert.ToInt32(Request["MaterialCount"]);
                        }
                        else
                        {
                            ViewBag.ErrorMatCount = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatCount]!";
                            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                            ViewBag.Location = Location();
                            ViewBag.OrderedDefault = 0;
                            ViewBag.DeliveredDefault = 0;
                            ViewBag.MaxLotDefault = 0;
                            ViewBag.MinLotDefault = 0;
                            return View(model);
                        }

                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        CompModel.Md_material_Material = null;
                        CompModel.Md_order_ContingentOrder = null;
                        CompModel.Md_order_LoadingOrder = null;
                        CompModel.Md_recipe_Recipe = null;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.ContingentOrderId = null;

                        for (int b = 0; b <= twcount; b++)
                        {
                            CompModel.LocationNumber = null;
                            CompModel.LotSetPoint = 0;
                            CompModel.OrderSetPoint = 0;

                            if (Request["MaterialId" + counter] != null && Request["MaterialId" + counter] != "")
                            {
                                MaterialId = Convert.ToInt32(Request["MaterialId" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatId = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatId]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialNumber" + counter] != null && Request["MaterialNumber" + counter] != "")
                            {
                                MaterialNumber = Request["MaterialNumber" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorMatNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatNumber]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialDescription" + counter] != null && Request["MaterialDescription" + counter] != "")
                            {
                                MaterialDescription = Request["MaterialDescription" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorMatDescription = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatDescription]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialValue" + counter] != null && Request["MaterialValue" + counter] != "")
                            {
                                MaterialValue = Convert.ToDecimal(Request["MaterialValue" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorSetPoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatValue]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialType" + counter] != null && Request["MaterialType" + counter] != "")
                            {
                                MaterialType = Convert.ToInt32(Request["MaterialType" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatType = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatType]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialGroup" + counter] != null && Request["MaterialGroup" + counter] != "")
                            {
                                MaterialGroup = Convert.ToInt32(Request["MaterialGroup" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatGroup = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatGroup]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["LocationNumber" + counter] != null && Request["LocationNumber" + counter] != "")
                            {
                                LocationNumber = Request["LocationNumber" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorLocNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[LocationNumber]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["ChargeSetpoint" + counter] != null && Request["ChargeSetpoint" + counter] != "")
                            {
                                NewSetpoint = Convert.ToDecimal(Request["ChargeSetpoint" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorChargeSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["OrderSetpoint" + counter] != null && Request["OrderSetpoint" + counter] != "")
                            {
                                OrderSetpoint = Convert.ToDecimal(Request["OrderSetpoint" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorOrderSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            CompModel.LocationNumber = LocationNumber;
                            CompModel.LotSetPoint = NewSetpoint;
                            CompModel.OrderSetPoint = OrderSetpoint;
                            CompModel.RecipeId = db.Md_order_Recipe.Where(a => a.Number == model.RecipeNumber).Select(a => a.Id).FirstOrDefault();
                            CompModel.MaterialId = MaterialId;
                            CompModel.Weight = MaterialValue;
                            CompModel.FunctionType = MaterialType;
                            CompModel.Type = MaterialGroup;
                            counter++;
                            db.Md_Components.Add(CompModel);
                            db.SaveChanges();
                        }
                        if (model.MaterialId != null)
                        {
                            if (model.RecipeId != null)
                            {
                                if (model.LocationId != null)
                                {
                                    var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                                    var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                                    var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                                    var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                                    var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                                    md_Config configModel = new md_Config();
                                    long f = Int64.Parse(valueLoading);
                                    configModel.ContingentOrderNumber = valueContingent;
                                    configModel.Id = 1;
                                    configModel.LoadingOrderNumber = (f + 1).ToString();
                                    configModel.BasicCalculation = valueBasicCalc;
                                    configModel.MixerSize = valueMixerSize;
                                    configModel.CustomerNumber = valueCustomerNumber;
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.SaveChanges();

                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    ViewBag.ErrorLocationId = "Fehler beim anlegen des Auftrages! Fehler: Can not find [LocationId]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View(model);
                                }
                            }
                            else
                            {
                                ViewBag.ErrorRecipeId = "Fehler beim anlegen des Auftrages! Fehler: Can not find [RecipeId]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }
                        }
                        else
                        {
                            ViewBag.ErrorMaterialId = "Fehler beim anlegen des Auftrages! Fehler: Can not find [MaterialId]!";
                            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                            ViewBag.Location = Location();
                            ViewBag.OrderedDefault = 0;
                            ViewBag.DeliveredDefault = 0;
                            ViewBag.MaxLotDefault = 0;
                            ViewBag.MinLotDefault = 0;
                            return View(model);
                        }
                    }
                }
                else
                {
                    if (model.OrderedQuantity <= Convert.ToDecimal(0.00))
                    {
                        ErrorMessages("Lademenge muss größer 0 sein!", "Create");
                    }
                    else
                    {
                        if (model.Sort == null)
                        {
                            model.Sort = db.Md_order_LoadingOrder.Where(a => a.Sort != null).Count() + 1;
                        }
                        else
                        {
                            var values = db.Md_order_LoadingOrder.Where(a => a.Sort != null).OrderBy(a => a.Sort).ToList();

                            int r = 0;
                            for (int i = 1; i < values.Count; i++)
                            {
                                if (i == model.Sort)
                                    r = 1;
                                values[i].Sort = r + i;
                            }
                        }
                        model.State = 2;
                        model.DeliveryNoteType = null;
                        if (model.RegistrationDate == null)
                        {
                            model.RegistrationDate = DateTime.Now;
                        }
                        var SortDetailId = db.Md_order_Material.Where(a => a.Id == model.MaterialId).Select(a => a.SortId).FirstOrDefault();
                        var consistencyId = db.Md_order_Sort.Where(a => a.Id == SortDetailId).Select(a => a.ConsistencyId).FirstOrDefault();
                        if (consistencyId != null)
                        {
                            var consistency = db.Md_order_Consistency.Find(consistencyId);
                            if (consistency.ValueMin != null)
                            {
                                model.ConsistencyMin = consistency.ValueMin;
                            }
                            else
                            {
                                model.ConsistencyMin = 0;
                            }
                            if (consistency.ValueMax != null)
                            {
                                model.ConsistencyMax = consistency.ValueMax;
                            }
                            else
                            {
                                model.ConsistencyMax = 0;
                            }
                        }


                        //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                        decimal RemainingLot;
                        decimal MaxLotSizeProcent;
                        decimal RemainingLotProcent;
                        int RemainingLotAmount;

                        var MixerCapacity = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                        if (model.BackAmount == null)
                        {
                            model.BackAmount = 0;
                        }

                        var LoadingAmount = Convert.ToDecimal(model.OrderedQuantity - model.BackAmount);
                        //var LoadingAmount = Convert.ToDecimal(model.OrderedQuantity);

                        if (LoadingAmount < MixerCapacity)
                        {
                            var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                            var MaxLotSize = LoadingAmount / 100 * MaxLotSizeIP;
                            MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                            //var MaxLotSizeIP = LoadingAmount / MixerCapacity * 100;

                            model.RemainingLotAmount = 0;
                            model.RemainingLot = 0;
                            model.LotSizeInProcent = MaxLotSizeProcent;
                            model.LotAmount = 1;
                        }
                        else
                        {
                            var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                            var MaxLotSize = MixerCapacity / 100 * MaxLotSizeIP;
                            var LotAmount = Math.Floor(LoadingAmount / Convert.ToDecimal(MaxLotSize));
                            var RemainingAmount = LoadingAmount - LotAmount * MaxLotSize;
                            //var RemainingAmount = (LoadingAmount - LotAmount) * MaxLotSize;

                            if (RemainingAmount > Convert.ToDecimal(0.00))
                            {
                                RemainingLot = Convert.ToDecimal((MaxLotSize + RemainingAmount)) / 2;
                                LotAmount = LotAmount - 1;
                                MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                                //RemainingLotProcent = (100 / Convert.ToDecimal(MaxLotSize)) * RemainingLot;
                                RemainingLotProcent = 100 * RemainingLot;
                                RemainingLotAmount = 2;

                                model.RemainingLotAmount = RemainingLotAmount;
                                model.RemainingLot = RemainingLotProcent;
                                model.LotSizeInProcent = MaxLotSizeProcent;
                                model.LotAmount = Convert.ToInt32(LotAmount);
                            }
                            else
                            {
                                RemainingLot = 0;
                                MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                                RemainingLotProcent = 0;
                                RemainingLotAmount = 0;

                                model.RemainingLotAmount = RemainingLotAmount;
                                model.RemainingLot = RemainingLotProcent;
                                model.LotSizeInProcent = MaxLotSizeProcent;
                                model.LotAmount = Convert.ToInt32(LotAmount);
                            }
                        }
                        //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                        var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                        var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                        var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                        var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                        var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                        var valueSeason = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
                        md_Config configModel = new md_Config();
                        long f = Int64.Parse(valueLoading);
                        configModel.ContingentOrderNumber = valueContingent;
                        configModel.Id = 1;
                        configModel.LoadingOrderNumber = (f + 1).ToString();
                        configModel.BasicCalculation = valueBasicCalc;
                        configModel.MixerSize = valueMixerSize;
                        configModel.CustomerNumber = valueCustomerNumber;
                        configModel.Season = valueSeason;
                        db.Entry(configModel).State = EntityState.Modified;

                        db.Md_order_LoadingOrder.Add(model);
                        db.SaveChanges();

                        try
                        {
                            md_order_OrderComponents CompModel = new md_order_OrderComponents();
                            CompModel.Md_material_Material = null;
                            CompModel.Md_order_ContingentOrder = null;
                            CompModel.Md_order_LoadingOrder = null;
                            CompModel.Md_recipe_Recipe = null;
                            CompModel.LoadingOrderId = model.Id;
                            if (Request["RecipeId"] == null || Request["RecipeId"] == "")
                            {
                                ErrorMessages("Rezept konnte nicht geladen werden!", "Create");
                            }
                            else
                            {
                                CompModel.RecipeId = Convert.ToInt64(Request["RecipeId"]);
                            }

                            int BinderCount = 0;
                            int AdmixCount = 0;
                            int AggregatesCount = 0;
                            int WaterCount = 0;
                            int AdditivesCount = 0;

                            if (Request["BinderCount"] == null || Request["BinderCount"] == "")
                            {
                                ErrorMessages("Fehler bei Anzahl Rohstoffe im Rezept (Bindemittel)!", "Create");
                            }
                            else
                            {
                                BinderCount = Convert.ToInt32(Request["BinderCount"]);
                            }
                            if (Request["AdmixCount"] == null || Request["AdmixCount"] == "")
                            {
                                ErrorMessages("Fehler bei Anzahl Rohstoffe im Rezept (Zusatzmittel)!", "Create");
                            }
                            else
                            {
                                AdmixCount = Convert.ToInt32(Request["AdmixCount"]);
                            }
                            if (Request["AggregatesCount"] == null || Request["AggregatesCount"] == "")
                            {
                                ErrorMessages("Fehler bei Anzahl Rohstoffe im Rezept (Zuschlagstoffe)!", "Create");
                            }
                            else
                            {
                                AggregatesCount = Convert.ToInt32(Request["AggregatesCount"]);
                            }
                            if (Request["WaterCount"] == null || Request["WaterCount"] == "")
                            {
                                ErrorMessages("Fehler bei Anzahl Rohstoffe im Rezept (Wasser)!", "Create");
                            }
                            else
                            {
                                WaterCount = Convert.ToInt32(Request["WaterCount"]);
                            }
                            if (Request["AdditivesCount"] == null || Request["AdditivesCount"] == "")
                            {
                                ErrorMessages("Fehler bei Anzahl Rohstoffe im Rezept (Additive)!", "Create");
                            }
                            else
                            {
                                AdditivesCount = Convert.ToInt32(Request["AdditivesCount"]);
                            }

                            int totalcount = BinderCount + AdmixCount + AggregatesCount + WaterCount + AdditivesCount;

                            long MaterialSort = totalcount + 1;

                            for (int i = 0; i < BinderCount; i++)
                            {

                                if (Request["BinderSort" + i] != null && Request["BinderSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["BinderSort" + i]);
                                }

                                int BinderIdent = 0;
                                decimal BinderValue = 0;
                                decimal BinderAdmit = 0;

                                if (Request["BinderId" + i] != null && Request["BinderId" + i] != "")
                                {
                                    BinderIdent = Convert.ToInt32(Request["BinderId" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden eines Bindemittels", "Create");
                                }

                                if (Request["BinderValue" + i] != null && Request["BinderValue" + i] != "")
                                {
                                    BinderValue = Convert.ToDecimal(Request["BinderValue" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden des Wertes eines Bindemittels", "Create");
                                }

                                if (Request["BinderAdmit" + i] != null && Request["BinderAdmit" + i] != "")
                                {
                                    BinderAdmit = Convert.ToDecimal(Request["BinderAdmit" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim addieren der Werte eines Bindemittels", "Create");
                                }
                                CompModel.MaterialId = BinderIdent;
                                CompModel.Weight = BinderValue;
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = BinderValue + BinderAdmit;
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_LoadingOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == BinderIdent).Select(a => a.FunctionType).FirstOrDefault();
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < AdmixCount; i++)
                            {
                                if (Request["AdmixSort" + i] != null && Request["AdmixSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AdmixSort" + i]);
                                }

                                int AdmixIdent = 0;
                                decimal AdmixValue = 0;
                                decimal AdmixAdmit = 0;

                                if (Request["AdmixId" + i] != null && Request["AdmixId" + i] != "")
                                {
                                    AdmixIdent = Convert.ToInt32(Request["AdmixId" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden eines Zusatzmittels", "Create");
                                }

                                if (Request["AdmixValue" + i] != null && Request["AdmixValue" + i] != "")
                                {
                                    AdmixValue = Convert.ToDecimal(Request["AdmixValue" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden des Wertes eines Zusatzmittels", "Create");
                                }

                                if (Request["AdmixAdmit" + i] != null && Request["AdmixAdmit" + i] != "")
                                {
                                    AdmixAdmit = Convert.ToDecimal(Request["AdmixAdmit" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim addieren der Werte eines Zusatzmittels", "Create");
                                }

                                CompModel.MaterialId = AdmixIdent;
                                CompModel.Weight = AdmixValue;
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = AdmixValue + AdmixAdmit;
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_LoadingOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AdmixIdent).Select(a => a.FunctionType).FirstOrDefault();
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < WaterCount; i++)
                            {
                                if (Request["WaterSort" + i] != null && Request["WaterSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["WaterSort" + i]);
                                }

                                int WaterIdent = 0;
                                decimal WaterValue = 0;
                                decimal WaterAdmit = 0;

                                if (Request["WaterId" + i] != null && Request["WaterId" + i] != "")
                                {
                                    WaterIdent = Convert.ToInt32(Request["WaterId" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden eines Wassers", "Create");
                                }

                                if (Request["WaterValue" + i] != null && Request["WaterValue" + i] != "")
                                {
                                    WaterValue = Convert.ToDecimal(Request["WaterValue" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden des Wertes eines Wassers", "Create");
                                }

                                if (Request["WaterAdmit" + i] != null && Request["WaterAdmit" + i] != "")
                                {
                                    WaterAdmit = Convert.ToDecimal(Request["WaterAdmit" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim addieren der Werte eines Wassers", "Create");
                                }

                                CompModel.MaterialId = WaterIdent;
                                CompModel.Weight = WaterValue;
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = WaterValue + WaterAdmit;
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_LoadingOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == WaterIdent).Select(a => a.FunctionType).FirstOrDefault();
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < AdditivesCount; i++)
                            {
                                if (Request["AdditivesSort" + i] != null && Request["AdditivesSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AdditivesSort" + i]);
                                }

                                int AdditivesIdent = 0;
                                decimal AdditivesValue = 0;

                                if (Request["AdditivesId" + i] != null && Request["AdditivesId" + i] != "")
                                {
                                    AdditivesIdent = Convert.ToInt32(Request["AdditivesId" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden eines Additive", "Create");
                                }

                                if (Request["AdditivesValue" + i] != null && Request["AdditivesValue" + i] != "")
                                {
                                    AdditivesValue = Convert.ToDecimal(Request["AdditivesValue" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden des Wertes eines Additive", "Create");
                                }

                                CompModel.MaterialId = AdditivesIdent;
                                CompModel.Weight = AdditivesValue;
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = AdditivesValue;
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_LoadingOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AdditivesIdent).Select(a => a.FunctionType).FirstOrDefault();
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < AggregatesCount; i++)
                            {
                                if (Request["AggregatesSort" + i] != null && Request["AggregatesSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AggregatesSort" + i]);
                                }

                                int AggregatesIdent = 0;
                                decimal AggregatesValue = 0;

                                if (Request["AggregatesId" + i] != null && Request["AggregatesId" + i] != "")
                                {
                                    AggregatesIdent = Convert.ToInt32(Request["AggregatesId" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden eines Zuschlagstoffes", "Create");
                                }

                                if (Request["AggregatesValue" + i] != null && Request["AggregatesValue" + i] != "")
                                {
                                    AggregatesValue = Convert.ToDecimal(Request["AggregatesValue" + i]);
                                }
                                else
                                {
                                    ErrorMessages("Fehler beim laden des Wertes eines Zuschlagstoffes", "Create");
                                }

                                CompModel.MaterialId = AggregatesIdent;
                                CompModel.Weight = AggregatesValue;
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = AggregatesValue;
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_LoadingOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AggregatesIdent).Select(a => a.FunctionType).FirstOrDefault();
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            var Equip = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).ToList();

                            for (int i = 0; i < Equip.Count(); i++)
                            {
                                PCMS.Areas.RecipeM.Models.md_equipment EquipModel = new PCMS.Areas.RecipeM.Models.md_equipment();
                                var recId = Equip[i].MaterialId;
                                var recipematerialM = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId).Select(a => a.MaterialId).ToList();
                                for (int e = 0; e < recipematerialM.Count(); e++)
                                {
                                    EquipModel = db.Md_Equipment.Find(recipematerialM[e]);

                                    CompModel.MaterialId = EquipModel.Id;
                                    CompModel.Weight = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                                    CompModel.LotSetPoint = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                                    CompModel.Sort = Equip[i].Sort;
                                    CompModel.LocationNumber = EquipModel.Number.ToString();
                                    CompModel.FunctionType = 1;
                                    CompModel.Type = 2;
                                    db.Md_Components.Add(CompModel);
                                    db.SaveChanges();
                                }
                            }

                            return RedirectToAction("Index");
                        }
                        catch (Exception e)
                        {
                            configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                            db.Entry(configModel).State = EntityState.Modified;
                            db.Md_Components.RemoveRange(db.Md_Components.Where(a => a.LoadingOrderId == model.Id));
                            db.Md_order_LoadingOrder.Attach(model);
                            db.SaveChanges();

                            ViewBag.Location = Location();
                            ViewBag.OrderedDefault = 0;
                            ViewBag.DeliveredDefault = 0;
                            ViewBag.MaxLotDefault = 0;
                            ViewBag.MinLotDefault = 0;
                            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                            ErrorMessages("Lager gesperrt oder Fehler beim übergeben der Werte in die Übergabe Tabelle!", "Create");
                            return View(model);
                        }
                    }
                }
            }
            ViewBag.Location = Location();
            ViewBag.OrderedDefault = 0;
            ViewBag.DeliveredDefault = 0;
            ViewBag.MaxLotDefault = 0;
            ViewBag.MinLotDefault = 0;
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View(model);

        }

        [HttpGet]
        public ActionResult Edit(md_order_LoadingOrder model, long id)
        {
            model = db.Md_order_LoadingOrder.Find(id);

            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                var modelList = db.Md_Components.Where(a => a.LoadingOrderId == id).ToList();

                List<long?> MId = new List<long?>();
                List<string> Mnumber = new List<string>();
                List<string> Mdescription = new List<string>();

                if (modelList != null)
                {
                    foreach (var item in modelList)
                    {
                        if (item.Type == 1)
                        {
                            var tmp = db.Md_order_Material.Where(a => a.Id == item.MaterialId).FirstOrDefault();
                            MId.Add(tmp.Id);
                            Mnumber.Add(tmp.ArticleNumber);
                            Mdescription.Add(tmp.Name);
                        }
                        else if (item.Type == 2)
                        {
                            var tmp = db.Md_Equipment.Where(a => a.Id == item.MaterialId).FirstOrDefault();
                            MId.Add(tmp.Id);
                            Mnumber.Add(tmp.Number.ToString());
                            Mdescription.Add(tmp.Description);
                        }
                        else if (item.Type == 3)
                        {
                            var tmp = db.Md_order_Recipe.Where(a => a.Id == item.MaterialId).FirstOrDefault();
                            MId.Add(tmp.Id);
                            Mnumber.Add(tmp.Number.ToString());
                            Mdescription.Add(tmp.Description);
                        }
                    }
                }

                ViewBag.MaterialCount = db.Md_Components.Where(a => a.LoadingOrderId == id).Count();
                var Mvalue = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.Weight).ToList();
                var Mgroup = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.Type).ToList();
                var Mtype = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.FunctionType).ToList();
                var newSP = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.LotSetPoint).ToList();
                var OrderSP = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.OrderSetPoint).ToList();
                var LocactionNo = db.Md_Components.Where(a => a.LoadingOrderId == id).Select(a => a.LocationNumber).ToList();
                for (int i = 0; i < LocactionNo.Count(); i++)
                {
                    var locNumber = LocactionNo[i];
                    if (Convert.ToDecimal(locNumber) < 800)
                    {
                        LocUnlock.Add(db.Md_Location.Where(a => a.Number == locNumber && a.IsActive == true && a.IsDeleted == false).Select(a => a.Active1).FirstOrDefault());
                    }
                    else
                    {
                        LocUnlock.Add(true);
                    }
                }
                ViewBag.LocationUnl = new List<bool>(LocUnlock);
                ViewBag.materialGroup = new List<int>(Mgroup);
                ViewBag.materialId = new List<long?>(MId);
                ViewBag.materialNumber = new List<string>(Mnumber);
                ViewBag.materialDescription = new List<string>(Mdescription);
                ViewBag.materialValue = new List<decimal>(Mvalue);
                ViewBag.materialType = new List<int?>(Mtype);
                ViewBag.LotSetPoint = new List<decimal?>(newSP);
                ViewBag.OrderSetPoint = new List<decimal?>(OrderSP);
                ViewBag.LocationNo = new List<string>(LocactionNo);
            }
            else
            {
                ViewBag.BinderCount = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Count();
                var bId = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.Md_material_Material.Id).ToList();
                var bnumber = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var bdescription = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.Md_material_Material.Name).ToList();
                var bvalue = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.Weight).ToList();
                var bsetpoint = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.LotSetPoint).ToList();
                var bSort = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Select(a => a.Sort).ToList();
                List<decimal?> badmit = new List<decimal?>();
                for (int i = 0; i < bvalue.Count; i++)
                {
                    badmit.Add(bsetpoint[i] - bvalue[i]);
                }

                ViewBag.binderId = new List<long?>(bId);
                ViewBag.binderNumber = new List<string>(bnumber);
                ViewBag.binderDescription = new List<string>(bdescription);
                ViewBag.binderValue = new List<decimal>(bvalue);
                ViewBag.binderSort = new List<long>(bSort);
                ViewBag.binderAdmit = badmit;

                ViewBag.AdmixCount = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Count();
                var admId = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.Md_material_Material.Id).ToList();
                var admnumber = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var admdescription = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.Md_material_Material.Name).ToList();
                var admvalue = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.Weight).ToList();
                var admsetpoint = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.LotSetPoint).ToList();
                var admSort = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Select(a => a.Sort).ToList();
                List<decimal?> admadmit = new List<decimal?>();
                for (int i = 0; i < admvalue.Count; i++)
                {
                    admadmit.Add(admsetpoint[i] - admvalue[i]);
                }
                ViewBag.admixId = new List<long?>(admId);
                ViewBag.admixNumber = new List<string>(admnumber);
                ViewBag.admixDescription = new List<string>(admdescription);
                ViewBag.admixValue = new List<decimal>(admvalue);
                ViewBag.admixSort = new List<long>(admSort);
                ViewBag.admixAdmit = admadmit;

                List<string> retard = new List<string>();
                for(int i = 0; i<admId.Count;i++)
                {
                    if(GetRetardation(admId[i])== true)
                    {
                        retard.Add("vz");
                    }
                    else
                    {
                        retard.Add("");
                    }
                }
                ViewBag.retard = new List<string>(retard);

                ViewBag.WaterCount = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Count();
                var watId = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.Md_material_Material.Id).ToList();
                var watnumber = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var watdescription = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.Md_material_Material.Name).ToList();
                var watvalue = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.Weight).ToList();
                var watsetpoint = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.LotSetPoint).ToList();
                var watSort = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Select(a => a.Sort).ToList();
                List<decimal?> watadmit = new List<decimal?>();
                for (int i = 0; i < watvalue.Count; i++)
                {
                    watadmit.Add(watsetpoint[i] - watvalue[i]);
                }
                ViewBag.waterId = new List<long?>(watId);
                ViewBag.waterNumber = new List<string>(watnumber);
                ViewBag.waterDescription = new List<string>(watdescription);
                ViewBag.waterValue = new List<decimal>(watvalue);
                ViewBag.waterSort = new List<long>(watSort);
                ViewBag.waterAdmit = watadmit;

                ViewBag.AdditivesCount = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Count();
                var addiId = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Select(a => a.Md_material_Material.Id).ToList();
                var addinumber = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var addidescription = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Select(a => a.Md_material_Material.Name).ToList();
                var addivalue = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Select(a => a.Weight).ToList();
                var addiSort = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Select(a => a.Sort).ToList();
                ViewBag.addiId = new List<long?>(addiId);
                ViewBag.addiNumber = new List<string>(addinumber);
                ViewBag.addiDescription = new List<string>(addidescription);
                ViewBag.addiValue = new List<decimal>(addivalue);
                ViewBag.addiSort = new List<long>(addiSort);

                ViewBag.AggregatesCount = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Count();
                var aggrId = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Select(a => a.Md_material_Material.Id).ToList();
                var aggrnumber = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var aggrdescription = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Select(a => a.Md_material_Material.Name).ToList();
                var aggrvalue = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Select(a => a.Weight).ToList();
                var aggrSort = db.Md_Components.Where(a => a.LoadingOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Select(a => a.Sort).ToList();
                ViewBag.aggreId = new List<long?>(aggrId);
                ViewBag.aggreNumber = new List<string>(aggrnumber);
                ViewBag.aggreDescription = new List<string>(aggrdescription);
                ViewBag.aggreValue = new List<decimal>(aggrvalue);
                ViewBag.aggreSort = new List<long>(aggrSort);
            }

            ViewBag.CustomerId = db.Md_order_Customer.Where(a => a.CustomerId == model.CustomerNumber).Select(a => a.Id).FirstOrDefault();
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.Location = Location();
            ViewBag.Date = model.RegistrationDate;
            ViewBag.OrderedDefault = model.OrderedQuantity.ToString().Replace(".", ",");
            ViewBag.DeliveredDefault = model.DeliveredQuantity.ToString().Replace(".", ",");
            ViewBag.MaxLotDefault = model.MaxLot.ToString().Replace(".", ",");
            ViewBag.MinLotDefault = model.MinLot.ToString().Replace(".", ",");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_order_LoadingOrder model)
        {
            if (ModelState.IsValid)
            {
                // Hier Eintrag in die Tabelle OrderComponents --Start--
                //änderung Manu 2016_05_13 start
                model.UserName = "";
                try
                {
                    Areas.Admin.Models.AdminContext adminContex = new Admin.Models.AdminContext();
                    model.UserName = adminContex.AspNetUsers.Where(a => a.UserName == User.Identity.Name).Select(a => a.Email).FirstOrDefault();
                }
                catch (Exception e)
                {

                }
                //änderung Manu 2016_05_13 end
                if (Helper.Definitions.getFactoryGroup() == 1)
                {
                    if (model.State != 2)
                    {
                        ViewBag.ErrorOrderNotValid = "Nicht freigegebene Aufträge können nicht bearbeitet werden!";
                        ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                        ViewBag.Location = Location();
                        ViewBag.OrderedDefault = 0;
                        ViewBag.DeliveredDefault = 0;
                        ViewBag.MaxLotDefault = 0;
                        ViewBag.MinLotDefault = 0;
                        return View(model);
                    }
                    else
                    {
                        int counter = 0;

                        long? MaterialId;
                        string MaterialNumber;
                        string MaterialDescription;
                        decimal MaterialValue;
                        int MaterialType;
                        int MGroup;
                        decimal LotSetPoint;
                        decimal OrderSetPoint;
                        string LocNumber;

                        int tcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id).Count();

                        for (int b = 0; b < tcount; b++)
                        {
                            if (Request["MaterialId" + counter] != null && Request["MaterialId" + counter] != "")
                            {
                                MaterialId = Convert.ToInt32(Request["MaterialId" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatId = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatId]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialNumber" + counter] != null && Request["MaterialNumber" + counter] != "")
                            {
                                MaterialNumber = Request["MaterialNumber" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorMatNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatNumber]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialDescription" + counter] != null && Request["MaterialDescription" + counter] != "")
                            {
                                MaterialDescription = Request["MaterialDescription" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorMatDescription = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatDescription]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialValue" + counter] != null && Request["MaterialValue" + counter] != "")
                            {
                                MaterialValue = Convert.ToDecimal(Request["MaterialValue" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorSetPoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatValue]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialType" + counter] != null && Request["MaterialType" + counter] != "")
                            {
                                MaterialType = Convert.ToInt32(Request["MaterialType" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatType = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatType]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["MaterialGroup" + counter] != null && Request["MaterialGroup" + counter] != "")
                            {
                                MGroup = Convert.ToInt32(Request["MaterialGroup" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorMatGroup = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatGroup]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["LocationNumber" + counter] != null && Request["LocationNumber" + counter] != "")
                            {
                                LocNumber = Request["LocationNumber" + counter];
                            }
                            else
                            {
                                ViewBag.ErrorLocNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[LocationNumber]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["ChargeSetpoint" + counter] != null && Request["ChargeSetpoint" + counter] != "")
                            {
                                LotSetPoint = Convert.ToDecimal(Request["ChargeSetpoint" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorChargeSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }

                            if (Request["OrderSetpoint" + counter] != null && Request["OrderSetpoint" + counter] != "")
                            {
                                OrderSetPoint = Convert.ToDecimal(Request["OrderSetpoint" + counter]);
                            }
                            else
                            {
                                ViewBag.ErrorOrderSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View(model);
                            }
                            var recId = db.Md_order_Recipe.Where(z => z.Number == model.RecipeNumber).Select(y => y.Id).FirstOrDefault();

                            md_order_OrderComponents CompModel = new md_order_OrderComponents();
                            var compid = db.Md_Components.Where(a => a.LoadingOrderId == model.Id).Select(a => a.Id).ToList();
                            CompModel.Id = compid[b];
                            CompModel.RecipeId = db.Md_order_Recipe.Where(a => a.Number == model.RecipeNumber).Select(a => a.Id).FirstOrDefault();
                            CompModel.MaterialId = MaterialId;
                            CompModel.LoadingOrderId = model.Id;
                            CompModel.Weight = MaterialValue;
                            CompModel.FunctionType = MaterialType;
                            CompModel.Type = MGroup;
                            CompModel.LotSetPoint = Convert.ToDecimal(LotSetPoint.ToString().Replace(".", ","));
                            CompModel.OrderSetPoint = Convert.ToDecimal(OrderSetPoint.ToString().Replace(".", ","));
                            CompModel.LocationNumber = LocNumber;
                            db.Entry(CompModel).State = EntityState.Modified;
                            db.SaveChanges();

                            counter++;
                        }
                    }
                }
                else
                {
                    int cb = 0;
                    int ca = 0;
                    int cw = 0;
                    int cad = 0;
                    int cag = 0;

                    long? BinderId;
                    string BinderNumber;
                    string BinderDescription;
                    decimal BinderValue;
                    decimal BinderAdmit;

                    long? AdmixId;
                    string AdmixNumber;
                    string AdmixDescription;
                    decimal AdmixValue;
                    decimal AdmixAdmit;

                    long? WaterId;
                    string WaterNumber;
                    string WaterDescription;
                    decimal WaterValue;
                    decimal WaterAdmit;

                    long? AddiId;
                    string AddiNumber;
                    string AddiDescription;
                    decimal AddiValue;

                    long? AggreId;
                    string AggreNumber;
                    string AggreDescription;
                    decimal AggreValue;

                    //int twcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1).Count();
                    //int tacount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1).Count();
                    //int tbcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1).Count();
                    //int tadcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1).Count();
                    //int tagcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1).Count();

                    int twcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tacount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tbcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tadcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tagcount = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();

                    int totalCount = twcount + tacount + tbcount + tadcount + tagcount;

                    for (int b = 0; b < tbcount; b++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        BinderId = Convert.ToInt32(Request["BinderId" + cb]);
                        BinderNumber = Request["BinderNumber" + cb];
                        BinderDescription = Request["BinderDescription" + cb];
                        BinderValue = Convert.ToDecimal(Request["BinderValue" + cb]);
                        BinderAdmit = Convert.ToDecimal(Request["BinderAdmit" + cb]);

                        //var recId = db.Md_order_Recipe.Where(z => z.Id == model.RecipeId).Select(y => y.Id).FirstOrDefault();
                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == BinderId && a.RecipeId == model.RecipeId && a.LoadingOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = BinderValue + Convert.ToDecimal(Request["BinderAdmit"]);
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = BinderId;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.Weight = BinderValue;
                        CompModel.LotSetPoint = BinderValue + BinderAdmit;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == BinderId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == BinderId && a.LoadingOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cb++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int c = 0; c < tacount; c++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AdmixId = Convert.ToInt32(Request["AdmixId" + ca]);
                        AdmixNumber = Request["AdmixNumber" + ca];
                        AdmixDescription = Request["AdmixDescription" + ca];
                        AdmixValue = Convert.ToDecimal(Request["AdmixValue" + ca]);
                        AdmixAdmit = Convert.ToDecimal(Request["AdmixAdmit" + ca]);

                        //var recId = db.Md_order_Recipe.Where(z => z.Number == model.RecipeNumber).Select(y => y.Id).FirstOrDefault();
                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AdmixId && a.RecipeId == model.RecipeId && a.LoadingOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AdmixValue + Convert.ToDecimal(Request["AdmixAdmit"]);
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AdmixId;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.Weight = AdmixValue;
                        CompModel.LotSetPoint = AdmixValue + AdmixAdmit;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AdmixId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AdmixId && a.LoadingOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        ca++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < twcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        WaterId = Convert.ToInt32(Request["WaterId" + cw]);
                        WaterNumber = Request["WaterNumber" + cw];
                        WaterDescription = Request["WaterDescription" + cw];
                        WaterValue = Convert.ToDecimal(Request["WaterValue" + cw]);
                        WaterAdmit = Convert.ToDecimal(Request["WaterAdmit" + cw]);

                        //var recId = db.Md_order_Recipe.Where(z => z.Number == model.RecipeNumber).Select(y => y.Id).FirstOrDefault();
                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == WaterId && a.RecipeId == model.RecipeId && a.LoadingOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.LotSetPoint = WaterValue + Convert.ToDecimal(Request["WaterAdmit"]);
                        CompModel.MaterialId = WaterId;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.Weight = WaterValue;
                        CompModel.LotSetPoint = WaterAdmit + WaterValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == WaterId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == WaterId && a.LoadingOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cw++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < tadcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AddiId = Convert.ToInt32(Request["AdditivesId" + cad]);
                        AddiNumber = Request["AdditivesNumber" + cad];
                        AddiDescription = Request["AdditivesDescription" + cad];
                        AddiValue = Convert.ToDecimal(Request["AdditivesValue" + cad]);

                        //var recId = db.Md_order_Recipe.Where(z => z.Number == model.RecipeNumber).Select(y => y.Id).FirstOrDefault();
                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AddiId && a.RecipeId == model.RecipeId && a.LoadingOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AddiValue;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AddiId;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.Weight = AddiValue;
                        CompModel.LotSetPoint = AddiValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AddiId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AddiId && a.LoadingOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cad++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < tagcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AggreId = Convert.ToInt32(Request["AggregatesId" + cag]);
                        AggreNumber = Request["AggregatesNumber" + cag];
                        AggreDescription = Request["AggregatesDescription" + cag];
                        AggreValue = Convert.ToDecimal(Request["AggregatesValue" + cag]);

                        //var recId = db.Md_order_Recipe.Where(z => z.Number == model.RecipeNumber).Select(y => y.Id).FirstOrDefault();
                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AggreId && a.RecipeId == model.RecipeId && a.LoadingOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AggreValue;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AggreId;
                        CompModel.LoadingOrderId = model.Id;
                        CompModel.Weight = AggreValue;
                        CompModel.LotSetPoint = AggreValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AggreId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");
                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AggreId && a.LoadingOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cag++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var RecipeM = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).Select(a => a.MaterialId).FirstOrDefault();

                    var query = "SELECT * FROM md_recipe_RecipeMaterialM WHERE RecipeId = " + RecipeM + " ORDER BY sort";
                    var data = db.Md_recipe_RecipeMaterialM.SqlQuery(query).ToList();

                    for (int i = 0; i < data.Count(); i++)
                    {
                        var equipId = data[i].MaterialId;
                        //md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        var Sort = data[i].Sort + totalCount;
                        var Equipment = db.Md_Equipment.Find(equipId);
                        var EquipmentNumber = Equipment.Number;
                        var CompModel = db.Md_Components.Where(a => a.LoadingOrderId == model.Id && a.MaterialId == equipId && a.Type == 2).FirstOrDefault();
                        var Id = CompModel.Id;
                        var UpdateQuery = "UPDATE md_order_OrderComponents SET MaterialId = " + data[i].MaterialId + ", RecipeId = " + model.RecipeId + ", Weight = " + data[i].Weight.ToString().Replace(",", ".") + ", Value = " + data[i].Value.ToString().Replace(",", ".") + ", LoadingOrderId = " + model.Id + ", FunctionType = " + data[i].FunctionType + ", LocationNumber = '" + EquipmentNumber + "', LotSetPoint = " + data[i].Value.ToString().Replace(",", ".") + ", Type = 2, Sort = " + Sort + " Where Id = " + Id;
                        db.Database.ExecuteSqlCommand(UpdateQuery);
                        //db.SaveChanges();
                    }

                }
                // Hier Eintrag in die Tabelle OrderComponents --Ende --

                model.DeliveryNoteType = null;
                if (model.RegistrationDate == null)
                {
                    model.RegistrationDate = DateTime.Now;
                }

                //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                decimal RemainingLot;
                decimal MaxLotSizeProcent;
                decimal RemainingLotProcent;
                int RemainingLotAmount;

                var MixerCapacity = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                if (model.BackAmount == null)
                {
                    model.BackAmount = 0;
                }

                var LoadingAmount = Convert.ToDecimal(model.OrderedQuantity - model.BackAmount);
                //var LoadingAmount = Convert.ToDecimal(model.OrderedQuantity);

                if (LoadingAmount < MixerCapacity)
                {
                    var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                    var MaxLotSize = LoadingAmount / 100 * MaxLotSizeIP;
                    MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                    //var MaxLotSizeIP = LoadingAmount / MixerCapacity * 100;

                    model.RemainingLotAmount = 0;
                    model.RemainingLot = 0;
                    model.LotSizeInProcent = MaxLotSizeProcent;
                    model.LotAmount = 1;
                }
                else
                {
                    var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                    var MaxLotSize = MixerCapacity / 100 * MaxLotSizeIP;
                    var LotAmount = Math.Floor(LoadingAmount / Convert.ToDecimal(MaxLotSize));
                    var RemainingAmount = LoadingAmount - LotAmount * MaxLotSize;
                    //var RemainingAmount = (LoadingAmount - LotAmount) * MaxLotSize;

                    if (RemainingAmount > Convert.ToDecimal(0.00))
                    {
                        RemainingLot = Convert.ToDecimal((MaxLotSize + RemainingAmount)) / 2;
                        LotAmount = LotAmount - 1;
                        MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                        //RemainingLotProcent = (100 / Convert.ToDecimal(MaxLotSize)) * RemainingLot;
                        RemainingLotProcent = 100 * RemainingLot;
                        RemainingLotAmount = 2;

                        model.RemainingLotAmount = RemainingLotAmount;
                        model.RemainingLot = RemainingLotProcent;
                        model.LotSizeInProcent = MaxLotSizeProcent;
                        model.LotAmount = Convert.ToInt32(LotAmount);
                    }
                    else
                    {
                        RemainingLot = 0;
                        MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                        RemainingLotProcent = 0;
                        RemainingLotAmount = 0;

                        model.RemainingLotAmount = RemainingLotAmount;
                        model.RemainingLot = RemainingLotProcent;
                        model.LotSizeInProcent = MaxLotSizeProcent;
                        model.LotAmount = Convert.ToInt32(LotAmount);
                    }
                }
                //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                var SortDetailId = db.Md_order_Material.Where(a => a.Id == model.MaterialId).Select(a => a.SortId).FirstOrDefault();
                var consistencyId = db.Md_order_Sort.Where(a => a.Id == SortDetailId).Select(a => a.ConsistencyId).FirstOrDefault();
                if (consistencyId != null)
                {
                    var consistency = db.Md_order_Consistency.Find(consistencyId);
                    if (consistency.ValueMin != null)
                    {
                        model.ConsistencyMin = consistency.ValueMin;
                    }
                    else
                    {
                        model.ConsistencyMin = 0;
                    }
                    if (consistency.ValueMax != null)
                    {
                        model.ConsistencyMax = consistency.ValueMax;
                    }
                    else
                    {
                        model.ConsistencyMax = 0;
                    }
                }

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.Location = Location();
            ViewBag.OrderedDefault = model.OrderedQuantity.ToString().Replace(".", ",");
            ViewBag.DeliveredDefault = model.DeliveredQuantity.ToString().Replace(".", ",");
            ViewBag.MaxLotDefault = model.MaxLot;
            ViewBag.MinLotDefault = model.MinLot;
            return View(model);
        }

        public ActionResult Done(md_order_LoadingOrder model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_order_LoadingOrder.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("Done")]
        [ValidateAntiForgeryToken]
        public ActionResult DoneConfirmed(md_order_LoadingOrder model, long? id)
        {
            model = db.Md_order_LoadingOrder.Find(id);
            model.State = 4;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            Helper.DeliveryNotePrint.printLot(model.OrderNumber);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(md_order_LoadingOrder model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model = db.Md_order_LoadingOrder.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(md_order_LoadingOrder model, long? id)
        {
            model = db.Md_order_LoadingOrder.Find(id);
            model.State = 5;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            Order.Models.md_order_ContingentOrder contingent = new md_order_ContingentOrder();
            contingent = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == model.ContingentOrderNumber).FirstOrDefault();
            
            if (contingent != null)
            {
                var CycleTime = contingent.DeliveryCycleTimeTick;
                var DelQuan = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == contingent.ContingentNumber && a.State != 5).Sum(a => a.OrderedQuantity);
                var LOrderCount = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == contingent.ContingentNumber && a.State != 5).Count();
                var ActualConstrSiteTime = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == contingent.ContingentNumber).Select(a => a.ConstructionSiteTime).FirstOrDefault();
                var DistanceTimeTick = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == contingent.ContingentNumber).Select(a => a.DistanceTimeTick).FirstOrDefault();
                //var ActualDeliveryCycleTimeTick = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == contingent.ContingentNumber).Select(a => a.DeliveryCycleTimeTick).FirstOrDefault();

                var ProductionTime = TimeSpan.FromMinutes(Convert.ToDouble(model.DeliveryCycleAmount));
                var CycleTimeAmount = CycleTime * LOrderCount - DistanceTimeTick - ProductionTime.Ticks;

                contingent.LoadingTime = model.ConstructionSiteTime + TimeSpan.FromTicks(CycleTimeAmount);
                contingent.DeliveredQuantity = DelQuan;
                db.Entry(contingent).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        public ActionResult CreateContingent()
        {
            md_order_ContingentOrder model = new md_order_ContingentOrder();
            var value = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();

            model.ContingentNumber = value;
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.DeliveryType = db.Md_order_DeliveryNoteType.Where(a => a.Id == 1).Select(a => a.Description).FirstOrDefault();
            ViewBag.DeliveryTypeId = 1;
            ViewBag.Location = Location();
            ViewBag.OrderedDefault = 0;
            ViewBag.DeliveredDefault = 0;
            ViewBag.MaxLotDefault = 0;
            ViewBag.MinLotDefault = 0;
            if (DateTime.Now.DayOfWeek.ToString() == "Saturday" || DateTime.Now.DayOfWeek.ToString() == "Sunday")
            {
                ViewBag.SpNo = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.EDVCode).FirstOrDefault();
                ViewBag.SpDescription = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Description).FirstOrDefault();
                ViewBag.SpValue = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Amount).FirstOrDefault();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateContingent(md_order_ContingentOrder model)
        {
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                md_Config configModel = new md_Config();
                long f = Int64.Parse(valueContingent);
                configModel.ContingentOrderNumber = (f + 1).ToString();
                configModel.Id = 1;
                configModel.LoadingOrderNumber = valueLoading;
                configModel.BasicCalculation = valueBasicCalc;
                configModel.MixerSize = valueMixerSize;
                db.Entry(configModel).State = EntityState.Modified;
                db.SaveChanges();

                model.State = 2;
                model.DeliveryNoteType = null;
                db.Md_order_ContingentOrder.Add(model);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    if (model.OrderedQuantity > 0)
                    {
                        var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                        var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                        var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                        var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                        var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                        var valueSeason = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
                        md_Config configModel = new md_Config();
                        long f = Int64.Parse(valueContingent);
                        configModel.ContingentOrderNumber = (f + 1).ToString();
                        configModel.Id = 1;
                        configModel.LoadingOrderNumber = valueLoading;
                        configModel.BasicCalculation = valueBasicCalc;
                        configModel.MixerSize = valueMixerSize;
                        configModel.CustomerNumber = valueCustomerNumber;
                        configModel.Season = valueSeason;
                        db.Entry(configModel).State = EntityState.Modified;
                        db.SaveChanges();

                        model.State = 2;
                        model.DeliveryNoteType = null;
                        db.Md_order_ContingentOrder.Add(model);
                        db.SaveChanges();

                        try
                        {
                            // Einstellungen
                            md_order_OrderComponents CompModel = new md_order_OrderComponents();
                            CompModel.Md_material_Material = null;
                            CompModel.Md_order_ContingentOrder = null;
                            CompModel.Md_order_LoadingOrder = null;
                            CompModel.Md_recipe_Recipe = null;
                            CompModel.LoadingOrderId = null;
                            CompModel.ContingentOrderId = model.Id;
                            CompModel.RecipeId = Convert.ToInt64(Request["RecipeId"]);

                            int totalcount = Convert.ToInt32(Request["BinderCount"]) + Convert.ToInt32(Request["AdmixCount"]) + Convert.ToInt32(Request["AggregatesCount"]) + Convert.ToInt32(Request["WaterCount"]) + Convert.ToInt32(Request["AdditivesCount"]);
                            long MaterialSort = totalcount + 2;

                            for (int i = 0; i < Convert.ToInt32(Request["BinderCount"]); i++)
                            {
                                if (Request["BinderSort" + i] != null && Request["BinderSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["BinderSort" + i]);
                                }
                                else
                                {
                                    MaterialSort = totalcount;
                                    totalcount = totalcount++;
                                }

                                CompModel.MaterialId = Convert.ToInt32(Request["BinderId" + i]);
                                CompModel.Weight = Convert.ToDecimal(Request["BinderValue" + i].Replace(".", ","));
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = Convert.ToDecimal(Request["BinderValue" + i].Replace(".", ",")) + Convert.ToDecimal(Request["BinderAdmit" + i].Replace(".", ","));
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_ContingentOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                if (db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault() == null)
                                {
                                    CompModel.FunctionType = 1;
                                }
                                else
                                {
                                    CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault();
                                }
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < Convert.ToInt32(Request["AdmixCount"]); i++)
                            {
                                if (Request["AdmixSort" + i] != null && Request["AdmixSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AdmixSort" + i]);
                                }
                                else
                                {
                                    MaterialSort = totalcount;
                                    totalcount = totalcount++;
                                }

                                CompModel.MaterialId = Convert.ToInt32(Request["AdmixId" + i]);
                                CompModel.Weight = Convert.ToDecimal(Request["AdmixValue" + i].Replace(".", ","));
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = Convert.ToDecimal(Request["AdmixValue" + i].Replace(".", ",")) + Convert.ToDecimal(Request["AdmixAdmit" + i].Replace(".", ","));
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_ContingentOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                if (db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault() == null)
                                {
                                    CompModel.FunctionType = 1;
                                }
                                else
                                {
                                    CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault();
                                }
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < Convert.ToInt32(Request["WaterCount"]); i++)
                            {
                                if (Request["WaterSort" + i] != null && Request["WaterSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["WaterSort" + i]);
                                }
                                else
                                {
                                    MaterialSort = totalcount;
                                    totalcount = totalcount++;
                                }
                                CompModel.MaterialId = Convert.ToInt32(Request["WaterId" + i]);
                                CompModel.Weight = Convert.ToDecimal(Request["WaterValue" + i].Replace(".", ","));
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = Convert.ToDecimal(Request["WaterValue" + i].Replace(".", ",")) + Convert.ToDecimal(Request["WaterAdmit" + i].Replace(".", ","));
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_ContingentOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                if (db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault() == null)
                                {
                                    CompModel.FunctionType = 1;
                                }
                                else
                                {
                                    CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault();
                                }
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < Convert.ToInt32(Request["AdditivesCount"]); i++)
                            {
                                if (Request["AdditivesSort" + i] != null && Request["AdditivesSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AdditivesSort" + i]);
                                }
                                else
                                {
                                    MaterialSort = totalcount;
                                    totalcount = totalcount++;
                                }
                                CompModel.MaterialId = Convert.ToInt32(Request["AdditivesId" + i]);
                                CompModel.Weight = Convert.ToDecimal(Request["AdditivesValue" + i].Replace(".", ","));
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = Convert.ToDecimal(Request["AdditivesValue" + i].Replace(".", ","));
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_ContingentOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                if (db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault() == null)
                                {
                                    CompModel.FunctionType = 1;
                                }
                                else
                                {
                                    CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault();
                                }
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            for (int i = 0; i < Convert.ToInt32(Request["AggregatesCount"]); i++)
                            {
                                if (Request["AggregatesSort" + i] != null && Request["AggregatesSort" + i] != "")
                                {
                                    MaterialSort = Convert.ToInt64(Request["AggregatesSort" + i]);
                                }
                                else
                                {
                                    MaterialSort = totalcount;
                                    totalcount = totalcount++;
                                }
                                CompModel.MaterialId = Convert.ToInt32(Request["AggregatesId" + i]);
                                CompModel.Weight = Convert.ToDecimal(Request["AggregatesValue" + i].Replace(".", ","));
                                CompModel.Sort = MaterialSort;
                                CompModel.LotSetPoint = Convert.ToDecimal(Request["AggregatesValue" + i].Replace(".", ","));
                                if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                                {
                                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.LoadingOrderNumber) - 1).ToString();
                                    db.Entry(configModel).State = EntityState.Modified;
                                    db.Md_order_ContingentOrder.Remove(model);
                                    db.SaveChanges();
                                    ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                                }
                                else
                                {
                                    CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                                }
                                CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                                if (db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault() == null)
                                {
                                    CompModel.FunctionType = 1;
                                }
                                else
                                {
                                    CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == CompModel.MaterialId).Select(a => a.FunctionType).FirstOrDefault();
                                }
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();
                            }

                            var Equip = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).ToList();

                            for (int i = 0; i < Equip.Count(); i++)
                            {
                                PCMS.Areas.RecipeM.Models.md_equipment EquipModel = new PCMS.Areas.RecipeM.Models.md_equipment();
                                var recId = Equip[i].MaterialId;
                                var recipematerialM = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId).Select(a => a.MaterialId).ToList();
                                for (int e = 0; e < recipematerialM.Count(); e++)
                                {
                                    EquipModel = db.Md_Equipment.Find(recipematerialM[e]);

                                    CompModel.MaterialId = EquipModel.Id;
                                    CompModel.Weight = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                                    CompModel.LotSetPoint = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                                    CompModel.Sort = Equip[i].Sort;
                                    CompModel.LocationNumber = EquipModel.Number.ToString();
                                    CompModel.Type = 2;
                                    db.Md_Components.Add(CompModel);
                                    db.SaveChanges();
                                }
                            }

                            //var Equip = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).ToList();

                            //for (int i = 0; i < Equip.Count(); i++)
                            //{
                            //    PCMS.Areas.RecipeM.Models.md_equipment EquipModel = new PCMS.Areas.RecipeM.Models.md_equipment();
                            //    var recId = Equip[i].MaterialId;
                            //    var recipematerialM = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId).Select(a => a.MaterialId).ToList();
                            //    for (int e = 0; e < recipematerialM.Count(); e++)
                            //    {
                            //        EquipModel = db.Md_Equipment.Find(recipematerialM[e]);

                            //        CompModel.MaterialId = EquipModel.Id;
                            //        CompModel.Weight = Convert.ToDecimal(Equip[i].Weight);
                            //        CompModel.LotSetPoint = Convert.ToDecimal(Equip[i].Weight);
                            //        CompModel.Sort = Equip[i].Sort;
                            //        CompModel.LocationNumber = EquipModel.Number.ToString();
                            //        CompModel.Type = 2;
                            //        db.Md_Components.Add(CompModel);
                            //        db.SaveChanges();
                            //    }
                            //}
                            return RedirectToAction("Index");
                        }
                        catch
                        {
                            configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.ContingentOrderNumber) - 1).ToString();
                            db.Entry(configModel).State = EntityState.Modified;
                            db.Md_order_ContingentOrder.Remove(model);
                            db.SaveChanges();

                            ErrorMessages("Fehler beim Anlegen des Kontingentauftrages! Komponente konnten nicht in die Übergabe Tabelle geschrieben werden!", "Create");
                        }

                    }
                    else
                    {
                        ErrorMessages("Bestellte Menge kleiner oder gleich 0!", "CreateContingent");
                    }
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                ViewBag.Location = Location();
                return View(model);
            }
        }

        public ActionResult EditContingent(long id, md_order_ContingentOrder model)
        {
            model = db.Md_order_ContingentOrder.Find(id);
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                ViewBag.Location = Location();
                return View(model);
            }
            else
            {
                ViewBag.BinderCount = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                var bId = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Id).ToList();
                var bnumber = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var bdescription = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Name).ToList();
                var bvalue = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Weight).ToList();
                var bsetpoint = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.LotSetPoint).ToList();
                var bSort = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Sort).ToList();
                List<decimal?> badmit = new List<decimal?>();
                for (int i = 0; i < bvalue.Count; i++)
                {
                    badmit.Add(bsetpoint[i] - bvalue[i]);
                }

                ViewBag.binderId = new List<long?>(bId);
                ViewBag.binderNumber = new List<string>(bnumber);
                ViewBag.binderDescription = new List<string>(bdescription);
                ViewBag.binderValue = new List<decimal>(bvalue);
                ViewBag.binderSort = new List<long>(bSort);
                ViewBag.binderAdmit = badmit;

                ViewBag.AdmixCount = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                var admId = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Id).ToList();
                var admnumber = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var admdescription = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Name).ToList();
                var admvalue = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Weight).ToList();
                var admsetpoint = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.LotSetPoint).ToList();
                var admSort = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Sort).ToList();
                List<decimal?> admadmit = new List<decimal?>();
                for (int i = 0; i < admvalue.Count; i++)
                {
                    admadmit.Add(admsetpoint[i] - admvalue[i]);
                }
                ViewBag.admixId = new List<long?>(admId);
                ViewBag.admixNumber = new List<string>(admnumber);
                ViewBag.admixDescription = new List<string>(admdescription);
                ViewBag.admixValue = new List<decimal>(admvalue);
                ViewBag.admixSort = new List<long>(admSort);
                ViewBag.admixAdmit = admadmit;

                List<string> retard = new List<string>();
                for (int i = 0; i < admId.Count; i++)
                {
                    if (GetRetardation(admId[i]) == true)
                    {
                        retard.Add("vz");
                    }
                    else
                    {
                        retard.Add("");
                    }
                }
                ViewBag.retard = new List<string>(retard);

                ViewBag.WaterCount = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                var watId = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Id).ToList();
                var watnumber = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var watdescription = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Name).ToList();
                var watvalue = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Weight).ToList();
                var watsetpoint = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.LotSetPoint).ToList();
                var watSort = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Sort).ToList();
                List<decimal?> watadmit = new List<decimal?>();
                for (int i = 0; i < watvalue.Count; i++)
                {
                    watadmit.Add(watsetpoint[i] - watvalue[i]);
                }
                ViewBag.waterId = new List<long?>(watId);
                ViewBag.waterNumber = new List<string>(watnumber);
                ViewBag.waterDescription = new List<string>(watdescription);
                ViewBag.waterValue = new List<decimal>(watvalue);
                ViewBag.waterSort = new List<long>(watSort);
                ViewBag.waterAdmit = watadmit;

                ViewBag.AdditivesCount = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                var addiId = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Id).ToList();
                var addinumber = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var addidescription = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Name).ToList();
                var addivalue = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Weight).ToList();
                var addiSort = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Sort).ToList();
                ViewBag.addiId = new List<long?>(addiId);
                ViewBag.addiNumber = new List<string>(addinumber);
                ViewBag.addiDescription = new List<string>(addidescription);
                ViewBag.addiValue = new List<decimal>(addivalue);
                ViewBag.addiSort = new List<long>(addiSort);

                ViewBag.AggregatesCount = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                var aggrId = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Id).ToList();
                var aggrnumber = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.ArticleNumber).ToList();
                var aggrdescription = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Md_material_Material.Name).ToList();
                var aggrvalue = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Weight).ToList();
                var aggrSort = db.Md_Components.Where(a => a.ContingentOrderId == id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Select(a => a.Sort).ToList();
                ViewBag.aggreId = new List<long?>(aggrId);
                ViewBag.aggreNumber = new List<string>(aggrnumber);
                ViewBag.aggreDescription = new List<string>(aggrdescription);
                ViewBag.aggreValue = new List<decimal>(aggrvalue);
                ViewBag.aggreSort = new List<long>(aggrSort);
            }

            ViewBag.CustomerId = db.Md_order_Customer.Where(a => a.CustomerId == model.CustomerNumber).Select(a => a.Id).FirstOrDefault();
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.Location = Location();
            ViewBag.Date = model.RegistrationDate;
            ViewBag.OrderedDefault = model.OrderedQuantity.ToString().Replace(".", ",");
            ViewBag.DeliveredDefault = model.DeliveredQuantity.ToString().Replace(".", ",");
            ViewBag.MaxLotDefault = model.MaxLot.ToString().Replace(".", ",");
            ViewBag.MinLotDefault = model.MinLot.ToString().Replace(".", ",");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditContingent(md_order_ContingentOrder model)
        {
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                if (ModelState.IsValid)
                {
                    model.DeliveryNoteType = null;
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                ViewBag.Location = Location();
                return View(model);
            }
            else
            {
                if (model.State != 2)
                {
                    ViewBag.Error = "Nicht Freigegebene Aufträge können nicht geändert werden!";
                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                    if (Helper.Definitions.getFactoryGroup() == 1)
                    {
                        ViewBag.Location = new SelectList(db.Md_Location.Where(a => a.GroupId == 8 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
                    }
                    else
                    {
                        ViewBag.Location = new SelectList(db.Md_Location.Where(a => a.GroupId == 9 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
                    }
                    ViewBag.HotWater = false;
                    ViewBag.OrderedDefault = 0;
                    ViewBag.DeliveredDefault = 0;
                    ViewBag.MaxLotDefault = 0;
                    ViewBag.MinLotDefault = 0;

                    return View("EditContingent");
                }
                else
                {
                    model.DeliveryNoteType = null;
                    if (model.RegistrationDate == null)
                    {
                        model.RegistrationDate = DateTime.Now;
                    }
                    if (model.OrderedQuantity <= model.DeliveredQuantity)
                    {
                        model.State = 4;
                    }

                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();

                    int cb = 0;
                    int ca = 0;
                    int cw = 0;
                    int cad = 0;
                    int cag = 0;

                    long? BinderId;
                    string BinderNumber;
                    string BinderDescription;
                    decimal BinderValue;
                    decimal BinderAdmit;

                    long? AdmixId;
                    string AdmixNumber;
                    string AdmixDescription;
                    decimal AdmixValue;
                    decimal AdmixAdmit;

                    long? WaterId;
                    string WaterNumber;
                    string WaterDescription;
                    decimal WaterValue;
                    decimal WaterAdmit;

                    long? AddiId;
                    string AddiNumber;
                    string AddiDescription;
                    decimal AddiValue;

                    long? AggreId;
                    string AggreNumber;
                    string AggreDescription;
                    decimal AggreValue;

                    int twcount = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 4 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tacount = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 3 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tbcount = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 2 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tadcount = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 6 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();
                    int tagcount = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.Md_material_Material.MaterialGroupId == 1 && a.Type == 1 && a.LoadingOrderId == null && a.Md_material_Material.IsDeleted == false && a.Md_material_Material.IsActive == true).Count();

                    int totalCount = twcount + tacount + tbcount + tadcount + tagcount;

                    for (int b = 0; b < tbcount; b++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        BinderId = Convert.ToInt32(Request["BinderId" + cb]);
                        BinderNumber = Request["BinderNumber" + cb];
                        BinderDescription = Request["BinderDescription" + cb];
                        BinderValue = Convert.ToDecimal(Request["BinderValue" + cb].Replace(".", ","));
                        BinderAdmit = Convert.ToDecimal(Request["BinderAdmit" + cb].Replace(".", ","));

                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == BinderId && a.RecipeId == model.RecipeId && a.ContingentOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = BinderValue + BinderAdmit;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = BinderId;
                        CompModel.ContingentOrderId = model.Id;
                        CompModel.Weight = BinderValue;
                        CompModel.LotSetPoint = BinderValue + BinderAdmit;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == BinderId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == BinderId && a.ContingentOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cb++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int c = 0; c < tacount; c++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AdmixId = Convert.ToInt32(Request["AdmixId" + ca]);
                        AdmixNumber = Request["AdmixNumber" + ca];
                        AdmixDescription = Request["AdmixDescription" + ca];
                        AdmixValue = Convert.ToDecimal(Request["AdmixValue" + ca].Replace(".", ","));
                        AdmixAdmit = Convert.ToDecimal(Request["AdmixAdmit" + ca].Replace(".", ","));

                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AdmixId && a.RecipeId == model.RecipeId && a.ContingentOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AdmixValue + AdmixAdmit;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AdmixId;
                        CompModel.ContingentOrderId = model.Id;
                        CompModel.Weight = AdmixValue;
                        CompModel.LotSetPoint = AdmixValue + AdmixAdmit;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AdmixId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AdmixId && a.ContingentOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        ca++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < twcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        WaterId = Convert.ToInt32(Request["WaterId" + cw]);
                        WaterNumber = Request["WaterNumber" + cw];
                        WaterDescription = Request["WaterDescription" + cw];
                        WaterValue = Convert.ToDecimal(Request["WaterValue" + cw].Replace(".", ","));
                        WaterAdmit = Convert.ToDecimal(Request["WaterAdmit" + cw].Replace(".", ","));

                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == WaterId && a.RecipeId == model.RecipeId && a.ContingentOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.LotSetPoint = WaterValue + WaterAdmit;
                        CompModel.MaterialId = WaterId;
                        CompModel.ContingentOrderId = model.Id;
                        CompModel.Weight = WaterValue;
                        CompModel.LotSetPoint = WaterAdmit + WaterValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == WaterId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == WaterId && a.ContingentOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cw++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < tadcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AddiId = Convert.ToInt32(Request["AdditivesId" + cad]);
                        AddiNumber = Request["AdditivesNumber" + cad];
                        AddiDescription = Request["AdditivesDescription" + cad];
                        AddiValue = Convert.ToDecimal(Request["AdditivesValue" + cad].Replace(".", ","));

                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AddiId && a.RecipeId == model.RecipeId && a.ContingentOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AddiValue;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AddiId;
                        CompModel.ContingentOrderId = model.Id;
                        CompModel.Weight = AddiValue;
                        CompModel.LotSetPoint = AddiValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AddiId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AddiId && a.ContingentOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cad++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    for (int d = 0; d < tagcount; d++)
                    {
                        md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        AggreId = Convert.ToInt32(Request["AggregatesId" + cag]);
                        AggreNumber = Request["AggregatesNumber" + cag];
                        AggreDescription = Request["AggregatesDescription" + cag];
                        AggreValue = Convert.ToDecimal(Request["AggregatesValue" + cag].Replace(".", ","));

                        CompModel.Id = db.Md_Components.Where(a => a.MaterialId == AggreId && a.RecipeId == model.RecipeId && a.ContingentOrderId == model.Id).Select(a => a.Id).FirstOrDefault();
                        CompModel.LotSetPoint = AggreValue;
                        CompModel.RecipeId = model.RecipeId;
                        CompModel.MaterialId = AggreId;
                        CompModel.ContingentOrderId = model.Id;
                        CompModel.Weight = AggreValue;
                        CompModel.LotSetPoint = AggreValue;
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == model.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        CompModel.FunctionType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.MaterialId == AggreId).Select(a => a.FunctionType).FirstOrDefault();
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Sort = db.Md_Components.Where(a => a.MaterialId == AggreId && a.ContingentOrderId == model.Id && a.Type == 1).Select(a => a.Sort).FirstOrDefault();
                        cag++;

                        db.Entry(CompModel).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var RecipeM = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).Select(a => a.MaterialId).FirstOrDefault();

                    var query = "SELECT * FROM md_recipe_RecipeMaterialM WHERE RecipeId = " + RecipeM + " ORDER BY sort";
                    var data = db.Md_recipe_RecipeMaterialM.SqlQuery(query).ToList();

                    for (int i = 0; i < data.Count(); i++)
                    {
                        var equipId = data[i].MaterialId;
                        //md_order_OrderComponents CompModel = new md_order_OrderComponents();
                        var Sort = data[i].Sort + totalCount;
                        var Equipment = db.Md_Equipment.Find(equipId);
                        var EquipmentNumber = Equipment.Number;
                        var CompModel = db.Md_Components.Where(a => a.ContingentOrderId == model.Id && a.MaterialId == equipId && a.Type == 2).FirstOrDefault();
                        var Id = CompModel.Id;
                        var UpdateQuery = "UPDATE md_order_OrderComponents SET MaterialId = " + data[i].MaterialId + ", RecipeId = " + model.RecipeId + ", Weight = " + data[i].Weight.ToString().Replace(",", ".") + ", Value = " + data[i].Value.ToString().Replace(",", ".") + ", ContingentOrderId = " + model.Id + ", FunctionType = " + data[i].FunctionType + ", LocationNumber = '" + EquipmentNumber + "', LotSetPoint = " + data[i].Value.ToString().Replace(",", ".") + ", Type = 2, Sort = " + Sort + " Where Id = " + Id;
                        db.Database.ExecuteSqlCommand(UpdateQuery);
                        //db.SaveChanges();
                    }
                    // Hier Eintrag in die Tabelle OrderComponents --Ende --
                    return RedirectToAction("Index");
                }
            }
        }

        public ActionResult DeleteContingent(md_order_ContingentOrder model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_order_ContingentOrder md_order_ContingentOrder = db.Md_order_ContingentOrder.Find(id);
            if (md_order_ContingentOrder == null)
            {
                return HttpNotFound();
            }
            return PartialView(md_order_ContingentOrder);
        }

        [HttpPost, ActionName("DeleteContingent")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(md_order_ContingentOrder model, long? id)
        {
            model = db.Md_order_ContingentOrder.Find(id);
            model.State = 5;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();

            string contingentNumber = db.Md_order_ContingentOrder.Find(id).ContingentNumber;

            var Lmodel = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == contingentNumber).ToList();

            for (int i = 0; i < Lmodel.Count(); i++)
            {
                if (Lmodel[i].State == 2)
                {
                    Lmodel[i].State = 5;
                }
                db.Entry(Lmodel[i]).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        public ActionResult GenerateLoadingOrder(string Cid)
        {
            md_order_LoadingOrder Lmodel = new md_order_LoadingOrder();
            md_order_ContingentOrder Cmodel = new md_order_ContingentOrder();

            var LoadingNumberValue = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();

            var DelQuan = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == Cid && a.State != 5).Sum(a => a.OrderedQuantity);

            Cmodel = db.Md_order_ContingentOrder.Find(db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == Cid).Select(b => b.Id).FirstOrDefault());


            // Änderung vom 29.03.2016 Matzir start

            Lmodel.LoadingTime = Cmodel.LoadingTime;

            //var value = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == Cid && a.State != 5).ToList();

            //if (value.Count() == 0)
            //{
            //    var BatchRetardation = TimeSpan.FromMinutes(Convert.ToInt64(Cmodel.DeliveryCycleAmount));
            //    Lmodel.LoadingTime = Cmodel.ConstructionSiteTime - Cmodel.DistanceTime - BatchRetardation;
            //}
            //else
            //{
            //    var BatchRetardation = TimeSpan.FromMinutes(Convert.ToInt64(Cmodel.DeliveryCycleAmount));
            //    var loadingtime = Cmodel.ConstructionSiteTime + TimeSpan.FromTicks(Cmodel.DeliveryCycleTimeTick * value.Count());
            //    Lmodel.LoadingTime = loadingtime - BatchRetardation - Cmodel.DistanceTime;
            //}

            // Änderung vom 29.03.2016 Matzir ende

            Lmodel.OrderNumber = LoadingNumberValue;
            Lmodel.AdmixturesAdditionValue1 = Cmodel.AdmixturesAdditionValue1;
            Lmodel.AdmixturesAdditionValue2 = Cmodel.AdmixturesAdditionValue2;
            Lmodel.AdmixturesAdditionValue3 = Cmodel.AdmixturesAdditionValue3;
            Lmodel.AdmixturesDescription1 = Cmodel.AdmixturesDescription1;
            Lmodel.AdmixturesDescription2 = Cmodel.AdmixturesDescription2;
            Lmodel.AdmixturesDescription3 = Cmodel.AdmixturesDescription3;
            Lmodel.AdmixturesNumber1 = Cmodel.AdmixturesNumber1;
            Lmodel.AdmixturesNumber2 = Cmodel.AdmixturesNumber2;
            Lmodel.AdmixturesNumber3 = Cmodel.AdmixturesNumber3;
            Lmodel.BackAmount = Cmodel.BackAmount;
            Lmodel.BackAmountDN = Cmodel.BackAmountDN;
            Lmodel.BatchSize = Cmodel.BatchSize;
            Lmodel.CementAdditionValue1 = Cmodel.CementAdditionValue1;
            Lmodel.CementAdditionValue2 = Cmodel.CementAdditionValue2;
            Lmodel.CementAdditionValue3 = Cmodel.CementAdditionValue3;
            Lmodel.CementDescription1 = Cmodel.CementDescription1;
            Lmodel.CementDescription2 = Cmodel.CementDescription2;
            Lmodel.CementDescription3 = Cmodel.CementDescription3;
            Lmodel.CementNumber1 = Cmodel.CementNumber1;
            Lmodel.CementNumber2 = Cmodel.CementNumber2;
            Lmodel.CementNumber3 = Cmodel.CementNumber3;
            Lmodel.Comment1 = Cmodel.Comment1;
            Lmodel.Comment2 = Cmodel.Comment2;
            Lmodel.Comment3 = Cmodel.Comment3;
            Lmodel.ConstructionSiteDescription = Cmodel.ConstructionSiteDescription;
            Lmodel.ConstructionSiteNumber = Cmodel.ConstructionSiteNumber;
            Lmodel.ConstructionSiteTime = Cmodel.ConstructionSiteTime;
            Lmodel.ContingentOrderNumber = Cmodel.ContingentNumber;
            Lmodel.CustomerName = Cmodel.CustomerName;
            Lmodel.CustomerNumber = Cmodel.CustomerNumber;
            Lmodel.DeliveredQuantity = DelQuan;
            //Lmodel.LoadingTime = Cmodel.LoadingTime;
            Lmodel.OrderedQuantity = Cmodel.DeliveryCycleAmount;
            Lmodel.DeliveryCycleTime = Cmodel.DeliveryCycleTime;
            Lmodel.DistanceTime = Cmodel.DistanceTime;
            Lmodel.DNComment = Cmodel.DNComment;
            Lmodel.TraderId = Cmodel.TraderId;
            Lmodel.TraderName = Cmodel.TraderName;
            Lmodel.TraderNumber = Cmodel.TraderNumber;
            Lmodel.MaterialDescription = Cmodel.MaterialDescription;
            Lmodel.MaterialNumber = Cmodel.MaterialNumber;
            Lmodel.MixTime = Cmodel.MixTime;
            //Lmodel.OrderedQuantity = Cmodel.OrderedQuantity;
            Lmodel.RecipeDescription = Cmodel.RecipeDescription;
            Lmodel.RecipeNumber = Cmodel.RecipeNumber;
            Lmodel.SpecialDescription1 = Cmodel.SpecialDescription1;
            Lmodel.SpecialDescription2 = Cmodel.SpecialDescription2;
            Lmodel.SpecialDescription3 = Cmodel.SpecialDescription3;
            Lmodel.SpecialDescription4 = Cmodel.SpecialDescription4;
            Lmodel.SpecialDescription5 = Cmodel.SpecialDescription5;
            Lmodel.SpecialValue1 = Cmodel.SpecialValue1;
            Lmodel.SpecialValue2 = Cmodel.SpecialValue2;
            Lmodel.SpecialValue3 = Cmodel.SpecialValue3;
            Lmodel.SpecialValue4 = Cmodel.SpecialValue4;
            Lmodel.SpecialValue5 = Cmodel.SpecialValue5;
            Lmodel.WaterAdditionValue1 = Cmodel.WaterAdditionValue1;
            Lmodel.WaterAdditionValue2 = Cmodel.WaterAdditionValue2;
            Lmodel.WaterDescription1 = Cmodel.WaterDescription1;
            Lmodel.WaterLCDescription1 = Cmodel.WaterLCDescription1;
            Lmodel.WaterLCDescription2 = Cmodel.WaterLCDescription2;
            Lmodel.WaterLCNumber1 = Cmodel.WaterLCNumber1;
            Lmodel.WaterLCNumber2 = Cmodel.WaterLCNumber2;
            Lmodel.WaterLCValue1 = Cmodel.WaterLCValue1;
            Lmodel.WaterLCValue2 = Cmodel.WaterLCValue2;
            Lmodel.WaterNumber1 = Cmodel.WaterNumber1;
            Lmodel.WaterNumber2 = Cmodel.WaterNumber2;
            Lmodel.DeliveryNoteTypeId = Cmodel.DeliveryNoteTypeId;
            Lmodel.MinLot = Cmodel.MinLot;
            Lmodel.MaxLot = Cmodel.MaxLot;
            Lmodel.RecipeId = Cmodel.RecipeId;
            Lmodel.LocationId = Cmodel.LocationId;
            Lmodel.MaterialId = Cmodel.MaterialId;
            Lmodel.DeliveryCycleAmount = Cmodel.DeliveryCycleAmount;
            Lmodel.LocationId = Cmodel.LocationId;
            Lmodel.DeliveryNoteType = Cmodel.DeliveryNoteType;
            Lmodel.Workability = Cmodel.Workability;
            Lmodel.ConsistencySetPoint = Cmodel.ConsistencyValue;
            Lmodel.SpecialNumber1 = Cmodel.SpecialNumber1;
            Lmodel.SpecialNumber2 = Cmodel.SpecialNumber2;
            Lmodel.SpecialNumber3 = Cmodel.SpecialNumber3;
            Lmodel.SpecialNumber4 = Cmodel.SpecialNumber4;
            Lmodel.SpecialNumber5 = Cmodel.SpecialNumber5;


            var SortDetailId = db.Md_order_Material.Where(a => a.Id == Lmodel.MaterialId).Select(a => a.SortId).FirstOrDefault();
            var consistencyId = db.Md_order_Sort.Where(a => a.Id == SortDetailId).Select(a => a.ConsistencyId).FirstOrDefault();
            if (consistencyId != null)
            {
                var consistency = db.Md_order_Consistency.Find(consistencyId);
                if (consistency.ValueMin != null)
                {
                    Lmodel.ConsistencyMin = consistency.ValueMin;
                }
                else
                {
                    Lmodel.ConsistencyMin = 0;
                }
                if (consistency.ValueMax != null)
                {
                    Lmodel.ConsistencyMax = consistency.ValueMax;
                }
                else
                {
                    Lmodel.ConsistencyMax = 0;
                }
            }

            if (Cmodel.OrderedQuantity > Cmodel.DeliveredQuantity)
            {
                var RemainingOrdered = Cmodel.OrderedQuantity - Cmodel.DeliveredQuantity;

                if (RemainingOrdered < Cmodel.DeliveryCycleAmount)
                {
                    Lmodel.OrderedQuantity = RemainingOrdered;
                }
            }

            ViewBag.CFC = true;
            ViewBag.CFCJquery = 1;
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.DeliveryType = Cmodel.DeliveryNoteType.Description;
            ViewBag.DeliveryTypeId = Cmodel.DeliveryNoteTypeId;
            ViewBag.Location = Location();
            ViewBag.OrderedDefault = 0;
            ViewBag.DeliveredDefault = 0;
            ViewBag.MaxLotDefault = Lmodel.MaxLot.ToString().Replace(".", ",");
            ViewBag.MinLotDefault = Lmodel.MinLot.ToString().Replace(".", ",");
            if (DateTime.Now.DayOfWeek.ToString() == "Saturday" || DateTime.Now.DayOfWeek.ToString() == "Sunday")
            {
                ViewBag.SpNo = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.EDVCode).FirstOrDefault();
                ViewBag.SpDescription = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Description).FirstOrDefault();
                ViewBag.SpValue = db.Md_order_PriceMarkUp.Where(a => a.Saturday == true || a.Sunday == true).Select(a => a.Amount).FirstOrDefault();
            }
            else
            {
                ViewBag.SpDescription = "";
            }
            return View("Create", Lmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GenerateLoadingOrder(md_order_LoadingOrder model)
        {

            //änderung Manu 2016_05_13 start
            model.UserName = "";
            try
            {
                Areas.Admin.Models.AdminContext adminContex = new Admin.Models.AdminContext();
                model.UserName = adminContex.AspNetUsers.Where(a => a.UserName == User.Identity.Name).Select(a => a.Email).FirstOrDefault();
            }
            catch (Exception e)
            {

            }
            //änderung Manu 2016_05_13 end
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                if (model.Sort == null)
                {
                    model.Sort = db.Md_order_LoadingOrder.Where(a => a.Sort != null).Count() + 1;
                }
                else
                {
                    var values = db.Md_order_LoadingOrder.Where(a => a.Sort != null).OrderBy(a => a.Sort).ToList();

                    int r = 0;
                    for (int i = 1; i < values.Count; i++)
                    {
                        if (i == model.Sort)
                            r = 1;
                        values[i].Sort = r + i;
                    }
                }
                model.DeliveryNoteType = null;
                model.State = 2;
                if (model.RegistrationDate == null)
                {
                    model.RegistrationDate = DateTime.Now;
                }
                db.Md_order_LoadingOrder.Add(model);

                db.SaveChanges();

                var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                var valueSeason = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
                md_Config configModel = new md_Config();
                long f = Int64.Parse(valueLoading);
                configModel.ContingentOrderNumber = valueContingent;
                configModel.Id = 1;
                configModel.BasicCalculation = valueBasicCalc;
                configModel.LoadingOrderNumber = (f + 1).ToString();
                configModel.MixerSize = valueMixerSize;
                configModel.CustomerNumber = valueCustomerNumber;
                configModel.Season = valueSeason;
                db.Entry(configModel).State = EntityState.Modified;
                db.SaveChanges();

                // Hier Eintrag in die Tabelle OrderComponents --Start--

                var Cid = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == model.ContingentOrderNumber).Select(a => a.Id).FirstOrDefault();

                try
                {
                    if (model.MinLot > (model.OrderedQuantity - model.DeliveredQuantity))
                    {
                        ViewBag.ErrorMinCharge = "Min Charge darf nicht kleiner sein als Produktionsmenge - Restmenge";
                        ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                        ViewBag.Location = Location();
                        ViewBag.OrderedDefault = 0;
                        ViewBag.DeliveredDefault = 0;
                        ViewBag.MaxLotDefault = 0;
                        ViewBag.MinLotDefault = 0;
                        return View("Create", model);
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            int counter = 0;

                            long? MaterialId;
                            string MaterialNumber;
                            string MaterialDescription;
                            decimal MaterialValue;
                            int? MaterialType;
                            int MaterialGroup;
                            decimal NewSetpoint;
                            decimal OrderSetpoint;
                            string LocationNumber;
                            int twcount;

                            if (Request["MaterialCount"] != null && Request["MaterialCount"] != "")
                            {
                                twcount = Convert.ToInt32(Request["MaterialCount"]);
                            }
                            else
                            {
                                ViewBag.ErrorMatCount = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatCount]!";
                                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                ViewBag.Location = Location();
                                ViewBag.OrderedDefault = 0;
                                ViewBag.DeliveredDefault = 0;
                                ViewBag.MaxLotDefault = 0;
                                ViewBag.MinLotDefault = 0;
                                return View("Create", model);
                            }

                            md_order_OrderComponents CompModel = new md_order_OrderComponents();
                            CompModel.Md_material_Material = null;
                            CompModel.Md_order_ContingentOrder = null;
                            CompModel.Md_order_LoadingOrder = null;
                            CompModel.Md_recipe_Recipe = null;
                            CompModel.LoadingOrderId = model.Id;
                            CompModel.ContingentOrderId = null;

                            for (int b = 0; b <= twcount; b++)
                            {
                                CompModel.LocationNumber = null;
                                CompModel.LotSetPoint = 0;
                                CompModel.OrderSetPoint = 0;

                                if (Request["MaterialId" + counter] != null && Request["MaterialId" + counter] != "")
                                {
                                    MaterialId = Convert.ToInt32(Request["MaterialId" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorMatId = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatId]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["MaterialNumber" + counter] != null && Request["MaterialNumber" + counter] != "")
                                {
                                    MaterialNumber = Request["MaterialNumber" + counter];
                                }
                                else
                                {
                                    ViewBag.ErrorMatNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatNumber]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["MaterialDescription" + counter] != null && Request["MaterialDescription" + counter] != "")
                                {
                                    MaterialDescription = Request["MaterialDescription" + counter];
                                }
                                else
                                {
                                    ViewBag.ErrorMatDescription = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatDescription]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["MaterialValue" + counter] != null && Request["MaterialValue" + counter] != "")
                                {
                                    MaterialValue = Convert.ToDecimal(Request["MaterialValue" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorSetPoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatValue]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["MaterialType" + counter] != null && Request["MaterialType" + counter] != "")
                                {
                                    MaterialType = Convert.ToInt32(Request["MaterialType" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorMatType = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatType]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["MaterialGroup" + counter] != null && Request["MaterialGroup" + counter] != "")
                                {
                                    MaterialGroup = Convert.ToInt32(Request["MaterialGroup" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorMatGroup = "Fehler in den Rezeptdetails! Fehler: Can not find Request[MatGroup]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["LocationNumber" + counter] != null && Request["LocationNumber" + counter] != "")
                                {
                                    LocationNumber = Request["LocationNumber" + counter];
                                }
                                else
                                {
                                    ViewBag.ErrorLocNumber = "Fehler in den Rezeptdetails! Fehler: Can not find Request[LocationNumber]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["ChargeSetpoint" + counter] != null && Request["ChargeSetpoint" + counter] != "")
                                {
                                    NewSetpoint = Convert.ToDecimal(Request["ChargeSetpoint" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorChargeSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                if (Request["OrderSetpoint" + counter] != null && Request["OrderSetpoint" + counter] != "")
                                {
                                    OrderSetpoint = Convert.ToDecimal(Request["OrderSetpoint" + counter]);
                                }
                                else
                                {
                                    ViewBag.ErrorOrderSetpoint = "Fehler in den Rezeptdetails! Fehler: Can not find Request[ChargeSetpoint]!";
                                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                                    ViewBag.Location = Location();
                                    ViewBag.OrderedDefault = 0;
                                    ViewBag.DeliveredDefault = 0;
                                    ViewBag.MaxLotDefault = 0;
                                    ViewBag.MinLotDefault = 0;
                                    return View("Create", model);
                                }

                                CompModel.LocationNumber = LocationNumber;
                                CompModel.LotSetPoint = NewSetpoint;
                                CompModel.OrderSetPoint = OrderSetpoint;
                                CompModel.RecipeId = db.Md_order_Recipe.Where(a => a.Number == model.RecipeNumber).Select(a => a.Id).FirstOrDefault();
                                CompModel.MaterialId = MaterialId;
                                CompModel.Weight = MaterialValue;
                                CompModel.FunctionType = MaterialType;
                                CompModel.Type = MaterialGroup;
                                counter++;
                                db.Md_Components.Add(CompModel);
                                db.SaveChanges();

                                return RedirectToAction("Index");
                            }
                        }
                    }
                }
                catch
                {
                    configModel.ContingentOrderNumber = (Convert.ToInt64(configModel.ContingentOrderNumber) - 1).ToString();
                    db.Entry(configModel).State = EntityState.Modified;
                    db.Md_order_LoadingOrder.Remove(model);
                    db.SaveChanges();

                    ErrorMessages("Fehler beim generieren eines Ladeauftrages! Komponente konnten nicht in die Übergabe Tabelle geschrieben werden!", "Create");
                }



            }
            else
            {
                if (model.OrderedQuantity <= 0)
                {
                    ErrorMessages("Lademenge darf nicht kleiner oder gleich 0 sein!", "Create");
                }
                else
                {
                    if (model.Sort == null)
                    {
                        model.Sort = db.Md_order_LoadingOrder.Where(a => a.Sort != null).Count() + 1;
                    }
                    else
                    {
                        var values = db.Md_order_LoadingOrder.Where(a => a.Sort != null).OrderBy(a => a.Sort).ToList();

                        int r = 0;
                        for (int i = 1; i < values.Count; i++)
                        {
                            if (i == model.Sort)
                                r = 1;
                            values[i].Sort = r + i;
                        }
                    }
                    model.DeliveryNoteType = null;
                    model.State = 2;
                    if (model.RegistrationDate == null)
                    {
                        model.RegistrationDate = DateTime.Now;
                    }

                    //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                    decimal RemainingLot;
                    decimal MaxLotSizeProcent;
                    decimal RemainingLotProcent;
                    int RemainingLotAmount;

                    var MixerCapacity = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                    if (model.BackAmount == null)
                    {
                        model.BackAmount = 0;
                    }

                    var LoadingAmount = Convert.ToDecimal(model.OrderedQuantity - model.BackAmount);

                    if (LoadingAmount < MixerCapacity)
                    {
                        var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                        var MaxLotSize = LoadingAmount / 100 * MaxLotSizeIP;
                        MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                        //var MaxLotSizeIP = LoadingAmount / MixerCapacity * 100;

                        model.RemainingLotAmount = 0;
                        model.RemainingLot = 0;
                        model.LotSizeInProcent = MaxLotSizeProcent;
                        model.LotAmount = 1;
                    }
                    else
                    {
                        var MaxLotSizeIP = Convert.ToDecimal(model.BatchSize);
                        var MaxLotSize = MixerCapacity / 100 * MaxLotSizeIP;
                        var LotAmount = Math.Floor(LoadingAmount / Convert.ToDecimal(MaxLotSize));
                        var RemainingAmount = LoadingAmount - LotAmount * MaxLotSize;
                        //var RemainingAmount = LoadingAmount - LotAmount * MaxLotSize;


                        if (RemainingAmount > Convert.ToDecimal(0.00))
                        {
                            RemainingLot = Convert.ToDecimal((MaxLotSize + RemainingAmount)) / 2;
                            LotAmount = LotAmount - 1;
                            MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                            //RemainingLotProcent = (100 / Convert.ToDecimal(MaxLotSize)) * RemainingLot;
                            RemainingLotProcent = 100 * RemainingLot;
                            RemainingLotAmount = 2;

                            model.RemainingLotAmount = RemainingLotAmount;
                            model.RemainingLot = RemainingLotProcent;
                            model.LotSizeInProcent = MaxLotSizeProcent;
                            model.LotAmount = Convert.ToInt32(LotAmount);
                        }
                        else
                        {
                            RemainingLot = 0;
                            MaxLotSizeProcent = Convert.ToDecimal(MaxLotSize) * 100;
                            RemainingLotProcent = 0;
                            RemainingLotAmount = 0;

                            model.RemainingLotAmount = RemainingLotAmount;
                            model.RemainingLot = RemainingLotProcent;
                            model.LotSizeInProcent = MaxLotSizeProcent;
                            model.LotAmount = Convert.ToInt32(LotAmount);
                        }
                    }
                    //++++++++++++++++++++++++++++++ Kalkulation ++++++++++++++++++++++++++++++

                    db.Md_order_LoadingOrder.Add(model);
                    db.SaveChanges();

                    Order.Models.md_order_ContingentOrder contingent = new md_order_ContingentOrder();
                    contingent = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == model.ContingentOrderNumber).FirstOrDefault();
                    var DelQuan = db.Md_order_LoadingOrder.Where(a => a.ContingentOrderNumber == contingent.ContingentNumber && a.State != 5).Sum(a => a.OrderedQuantity);
                    if (DelQuan >= contingent.OrderedQuantity && contingent.State == 2)
                    {
                        contingent.State = 4;
                    }
                    contingent.LoadingTime = model.LoadingTime;
                    if (contingent.DeliveredQuantity == null)
                    {
                        contingent.DeliveredQuantity = 0;
                    }
                    if (contingent.OrderedQuantity > contingent.DeliveredQuantity)
                    {
                        contingent.LoadingTime = model.LoadingTime + model.DeliveryCycleTime; // Änderungen von mir 24.02.2016
                    }
                    contingent.DeliveredQuantity = DelQuan;
                    db.Entry(contingent).State = EntityState.Modified;
                    db.SaveChanges();

                    var valueLoading = db.Md_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                    var valueContingent = db.Md_Config.Where(a => a.Id == 1).Select(a => a.ContingentOrderNumber).FirstOrDefault();
                    var valueBasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
                    var valueMixerSize = db.Md_Config.Where(a => a.Id == 1).Select(a => a.MixerSize).FirstOrDefault();
                    var valueCustomerNumber = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
                    var valueSeason = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
                    md_Config configModel = new md_Config();
                    long f = Int64.Parse(valueLoading);
                    configModel.ContingentOrderNumber = valueContingent;
                    configModel.Id = 1;
                    configModel.BasicCalculation = valueBasicCalc;
                    configModel.LoadingOrderNumber = (f + 1).ToString();
                    configModel.MixerSize = valueMixerSize;
                    configModel.CustomerNumber = valueCustomerNumber;
                    configModel.Season = valueSeason;
                    db.Entry(configModel).State = EntityState.Modified;
                    db.SaveChanges();

                    // Hier Eintrag in die Tabelle OrderComponents
                    md_order_OrderComponents CompModel = new md_order_OrderComponents();
                    CompModel.Md_material_Material = null;
                    CompModel.Md_order_ContingentOrder = null;
                    CompModel.Md_order_LoadingOrder = null;
                    CompModel.Md_recipe_Recipe = null;
                    CompModel.LoadingOrderId = model.Id;
                    CompModel.ContingentOrderId = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == model.ContingentOrderNumber).Select(a => a.Id).FirstOrDefault();
                    CompModel.RecipeId = model.RecipeId;

                    int totalcount = Convert.ToInt32(Request["BinderCount"]) + Convert.ToInt32(Request["AdmixCount"]) + Convert.ToInt32(Request["AggregatesCount"]) + Convert.ToInt32(Request["WaterCount"]) + Convert.ToInt32(Request["AdditivesCount"]);
                    long MaterialSort = totalcount + 1;

                    for (int i = 0; i < Convert.ToInt32(Request["BinderCount"]); i++)
                    {
                        if (Request["BinderSort" + i] != null && Request["BinderSort" + i] != "")
                        {
                            MaterialSort = Convert.ToInt64(Request["BinderSort" + i]);
                        }

                        CompModel.MaterialId = Convert.ToInt32(Request["BinderId" + i]);
                        CompModel.Weight = Convert.ToDecimal(Request["BinderValue" + i]);
                        CompModel.Sort = MaterialSort;
                        CompModel.LotSetPoint = Convert.ToDecimal(Request["BinderValue" + i]) + Convert.ToDecimal(Request["BinderAdmit" + i]);
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        db.Md_Components.Add(CompModel);
                        db.SaveChanges();
                    }

                    for (int i = 0; i < Convert.ToInt32(Request["AdmixCount"]); i++)
                    {
                        if (Request["AdmixSort" + i] != null && Request["AdmixSort" + i] != "")
                        {
                            MaterialSort = Convert.ToInt64(Request["AdmixSort" + i]);
                        }

                        CompModel.MaterialId = Convert.ToInt32(Request["AdmixId" + i]);
                        CompModel.Weight = Convert.ToDecimal(Request["AdmixValue" + i]);
                        CompModel.Sort = MaterialSort;
                        CompModel.LotSetPoint = Convert.ToDecimal(Request["AdmixValue" + i]) + Convert.ToDecimal(Request["AdmixAdmit" + i]);
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        db.Md_Components.Add(CompModel);
                        db.SaveChanges();
                    }

                    for (int i = 0; i < Convert.ToInt32(Request["WaterCount"]); i++)
                    {
                        if (Request["WaterSort" + i] != null && Request["WaterSort" + i] != "")
                        {
                            MaterialSort = Convert.ToInt64(Request["WaterSort" + i]);
                        }
                        CompModel.MaterialId = Convert.ToInt32(Request["WaterId" + i]);
                        CompModel.Weight = Convert.ToDecimal(Request["WaterValue" + i]);
                        CompModel.Sort = MaterialSort;
                        CompModel.LotSetPoint = Convert.ToDecimal(Request["WaterValue" + i]) + Convert.ToDecimal(Request["WaterAdmit" + i]);
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        db.Md_Components.Add(CompModel);
                        db.SaveChanges();
                    }

                    for (int i = 0; i < Convert.ToInt32(Request["AdditivesCount"]); i++)
                    {
                        if (Request["AdditivesSort" + i] != null && Request["AdditivesSort" + i] != "")
                        {
                            MaterialSort = Convert.ToInt64(Request["AdditivesSort" + i]);
                        }
                        CompModel.MaterialId = Convert.ToInt32(Request["AdditivesId" + i]);
                        CompModel.Weight = Convert.ToDecimal(Request["AdditivesValue" + i]);
                        CompModel.Sort = MaterialSort;
                        CompModel.LotSetPoint = Convert.ToDecimal(Request["AdditivesValue" + i]);
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        db.Md_Components.Add(CompModel);
                        db.SaveChanges();
                    }

                    for (int i = 0; i < Convert.ToInt32(Request["AggregatesCount"]); i++)
                    {
                        if (Request["AggregatesSort" + i] != null && Request["AggregatesSort" + i] != "")
                        {
                            MaterialSort = Convert.ToInt64(Request["AggregatesSort" + i]);
                        }
                        CompModel.MaterialId = Convert.ToInt32(Request["AggregatesId" + i]);
                        CompModel.Weight = Convert.ToDecimal(Request["AggregatesValue" + i]);
                        CompModel.Sort = MaterialSort;
                        CompModel.LotSetPoint = Convert.ToDecimal(Request["AggregatesValue" + i]);
                        if (db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault() == null)
                        {
                            ErrorMessages("Lager für Material: " + CompModel.Md_material_Material.Name + " nicht freigegeben!", "Create");

                        }
                        else
                        {
                            CompModel.LocationNumber = db.Md_Location.Where(a => a.MaterialId == CompModel.MaterialId && a.Active1 == true).Select(a => a.Number).FirstOrDefault();
                        }
                        CompModel.Type = Convert.ToInt32(db.Md_order_Recipe.Where(a => a.Id == CompModel.RecipeId).Select(a => a.RecipeTypeId).FirstOrDefault());
                        db.Md_Components.Add(CompModel);
                        db.SaveChanges();
                    }

                    var Equip = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == model.RecipeId && a.Type == 3).ToList();

                    for (int i = 0; i < Equip.Count(); i++)
                    {
                        PCMS.Areas.RecipeM.Models.md_equipment EquipModel = new PCMS.Areas.RecipeM.Models.md_equipment();
                        var recId = Equip[i].MaterialId;
                        var recipematerialM = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId).Select(a => a.MaterialId).ToList();
                        for (int e = 0; e < recipematerialM.Count(); e++)
                        {
                            EquipModel = db.Md_Equipment.Find(recipematerialM[e]);

                            CompModel.RecipeId = model.RecipeId;
                            CompModel.MaterialId = EquipModel.Id;
                            CompModel.Weight = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                            CompModel.LotSetPoint = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recId && a.MaterialId == EquipModel.Id).Select(a => a.Value).FirstOrDefault();
                            //CompModel.Weight = Convert.ToDecimal(Equip[i].Weight);
                            //CompModel.LotSetPoint = Convert.ToDecimal(Equip[i].Weight);
                            CompModel.Sort = Equip[i].Sort;
                            CompModel.LocationNumber = EquipModel.Number.ToString();
                            CompModel.FunctionType = 1;
                            CompModel.Type = 2;
                            db.Md_Components.Add(CompModel);
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }

            }
            return View("Create", model);
        }

        public ActionResult PosUp(md_order_LoadingOrder model, md_order_LoadingOrder model2, long Id)
        {
            model = db.Md_order_LoadingOrder.Find(Id);
            DateTime dtime = Convert.ToDateTime(model.RegistrationDate);
            model2 = null;
            if (model.Sort < 1)
            {
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

                return RedirectToAction("Index");
            }
            else
            {
                if (model.State == 3)
                {
                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                    return RedirectToAction("Index");
                }
                else
                {
                    model2 = db.Md_order_LoadingOrder.Find(db.Md_order_LoadingOrder.Where(a => a.Sort < model.Sort && a.State != 4 && a.State != 5 && a.RegistrationDate.Value.Year == dtime.Year && a.RegistrationDate.Value.Month == dtime.Month && a.RegistrationDate.Value.Day == dtime.Day).OrderByDescending(a => a.Sort).Select(a => a.Id).FirstOrDefault());
                    if (model2 == null)
                    {
                        ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        var tmpSort2 = db.Md_order_LoadingOrder.Where(a => a.Sort < model.Sort && a.State != 4 && a.State != 5 && a.RegistrationDate.Value.Year == dtime.Year && a.RegistrationDate.Value.Month == dtime.Month && a.RegistrationDate.Value.Day == dtime.Day).OrderByDescending(a => a.Sort).Select(a => a.Sort).FirstOrDefault();
                        model2.Sort = model.Sort;
                        model.Sort = tmpSort2;

                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(model2).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                return RedirectToAction("Index");
            }
        }

        public ActionResult PosDown(md_order_LoadingOrder model, md_order_LoadingOrder model2, long Id)
        {
            model = db.Md_order_LoadingOrder.Find(Id);
            DateTime dtime = Convert.ToDateTime(model.RegistrationDate);
            model2 = null;
            if (model.Sort < 1)
            {
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                return RedirectToAction("Index");
            }
            else
            {
                if (model.State == 3)
                {
                    ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                    return RedirectToAction("Index");
                }
                else
                {
                    model2 = db.Md_order_LoadingOrder.Find(db.Md_order_LoadingOrder.Where(a => a.Sort > model.Sort && a.State != 4 && a.State != 5 && a.RegistrationDate.Value.Year == dtime.Year && a.RegistrationDate.Value.Month == dtime.Month && a.RegistrationDate.Value.Day == dtime.Day).OrderBy(a => a.Sort).Select(a => a.Id).FirstOrDefault());
                    if (model2 == null)
                    {
                        ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        var tmpSort2 = db.Md_order_LoadingOrder.Where(a => a.Sort > model.Sort && a.State != 4 && a.State != 5 && a.RegistrationDate.Value.Year == dtime.Year && a.RegistrationDate.Value.Month == dtime.Month && a.RegistrationDate.Value.Day == dtime.Day).OrderBy(a => a.Sort).Select(a => a.Sort).FirstOrDefault();
                        model2.Sort = model.Sort;
                        model.Sort = tmpSort2;

                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Entry(model2).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                return RedirectToAction("Index");
            }
        }

        public ActionResult ChooseAggregate()
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 1).ToList();
            return View(model);
        }

        public ActionResult ChooseBinder()
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 2).ToList();
            return View(model);
        }

        public ActionResult ChooseAddivites(PaginationModel pg)
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 6).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? 10));
        }

        public ActionResult ChooseAdmix()
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 3).ToList();
            return View(model);
        }

        public ActionResult ChooseWater()
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 4).ToList();
            return View(model);
        }


        //public ActionResult ChooseRecipe(long id)
        //{
        //    var model2 = db.Md_order_Recipe.Include(m => m.Md_Material).Where(a => a.Md_Material.Id == id).ToList();
        //    if(model2.Count() == 0)
        //    {
        //        var model = db.Md_order_Recipe.ToList();
        //        return View(model);
        //    }
        //    else
        //    { 
        //        var model = db.Md_order_Recipe.Where(a => a.MaterialId == id).ToList();
        //        return View(model);
        //    }
        //}

        public ActionResult ChooseNewRecipe(PaginationModel pg)
        {
            var model = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.RecipeTypeId == 1 && a.Md_Recipe.IsActive == true && a.Md_Recipe.IsDeleted == false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "RecipeNumber":
                                model = model.Where(m => m.Md_Recipe.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.RecipeNumber = pgFF.colVal;
                                break;
                            case "RecipeName":
                                model = model.Where(m => m.Md_Recipe.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.RecipeName = pgFF.colVal;
                                break;
                            case "Brand":
                                model = model.Where(m => m.Md_Material.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Brand = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Recipe.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_Recipe.Id).ToList();
                    break;
                case "RecipeNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Recipe.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_Recipe.Number).ToList();
                    break;
                case "RecipeName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Recipe.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_Recipe.Description).ToList();
                    break;
                case "Brand":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_Material.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_Material.Name).ToList();
                    break;
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChooseMaterial()
        {
            var model = db.Md_order_Material.Where(a => a.MaterialGroupId == 5).ToList();
            return View(model);
        }

        public ActionResult SelectDNType(long? id)
        {
            var DNId = db.Md_order_Sort.Where(a => a.Id == id).Select(a => a.DeliveryNoteTypeId).FirstOrDefault();
            md_order_DeliveryNoteType dnmodel = new md_order_DeliveryNoteType();

            dnmodel = db.Md_order_DeliveryNoteType.Find(DNId);

            return Json(new { delivernoteId = dnmodel.Id, deliveryNoteType = dnmodel.Description });
        }

        public ActionResult ChooseMaterialOverRecipe(long? Id)
        {
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                var MatNo = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id).Select(a => a.Md_Material.MaterialNumber).FirstOrDefault();
                var MatDes = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id).Select(a => a.Md_Material.Name).FirstOrDefault();
                var MatId = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id).Select(a => a.Md_Material.Id).FirstOrDefault();
                return Json(new { materialId = MatId, materialNo = MatNo, description = MatDes });
            }
            else
            {
                var MatNo = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id && a.Active == true).Select(a => a.Md_Material.MaterialNumber).FirstOrDefault();
                var MatDes = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id && a.Active == true).Select(a => a.Md_Material.Name).FirstOrDefault();
                var MatId = db.Md_order_RecipeMaterial.Where(a => a.Md_Recipe.Id == Id && a.Active == true).Select(a => a.Md_Material.Id).FirstOrDefault();
                return Json(new { materialId = MatId, materialNo = MatNo, description = MatDes });
            }
        }

        //public ActionResult CheckRecipe(long id)
        //{
        //    if(db.Md_order_Recipe.Include(m => m.Md_Material).Where(a => a.Md_Material.Id == id).ToList().Count()==1)
        //    {
        //        return Json(false);
        //    }
        //    else
        //    {
        //        return Json(true);
        //    }
        //}

        public ActionResult SelectNorm(long id)
        {
            var DetailsId = db.Md_order_Material.Where(a => a.Id == id).Select(a => a.SortId).FirstOrDefault();
            var NormId = db.Md_order_Sort.Where(a => a.Id == DetailsId).Select(a => a.NormId).FirstOrDefault();
            var NormDescription = db.Md_order_Norm.Where(a => a.Id == NormId).Select(a => a.Name).FirstOrDefault();

            return Json(new { Norm = NormDescription });
        }

        public ActionResult SelectConsistency(long id)
        {
            var DetailsId = db.Md_order_Material.Where(a => a.Id == id).Select(a => a.SortId).FirstOrDefault();
            var ConsistencyId = db.Md_order_Sort.Where(a => a.Id == DetailsId).Select(a => a.ConsistencyId).FirstOrDefault();
            var Class = db.Md_order_Consistency.Where(a => a.Id == ConsistencyId).Select(a => a.Class).FirstOrDefault();
            var ValueMin = db.Md_order_Consistency.Where(a => a.Id == ConsistencyId).Select(a => a.ValueMin).FirstOrDefault();
            var ValueMax = db.Md_order_Consistency.Where(a => a.Id == ConsistencyId).Select(a => a.ValueMax).FirstOrDefault();
            if (ValueMax == null)
            {
                return Json(new { ConsClass = Class, ConsValue = ValueMin });
            }
            else
            {
                return Json(new { ConsClass = Class, ConsValue = ValueMax });
            }
        }

        public ActionResult SelectRecipe(long id, PCMS.Areas.MasterData.Models.md_recipe_Recipe model)
        {
            model = db.Md_order_Recipe.Find(db.Md_order_RecipeMaterial.Where(m => m.MaterialId == id).Select(a => a.RecipeId).FirstOrDefault());
            return Json(new { RecNumber = model.Number, RecDescription = model.Description });
        }

        public ActionResult ChooseCustomer(PaginationModel pg)
        {
            var model = db.Md_order_Customer.Include(m => m.Md_masterData_Contact).Include(m => m.Md_masterData_Address).Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "CustomerId":
                                model = model.Where(m => m.CustomerId != null && m.CustomerId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "CustomerId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CustomerId).ToList();
                    else
                        model = model.OrderByDescending(m => m.CustomerId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChooseTrader(PaginationModel pg)
        {
            var model = db.Md_Trader.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_masterData_Address).Include(m => m.Md_masterData_Contact).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "Street":
                                model = model.Where(m => m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "City":
                                model = model.Where(m => m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "TraderId":
                                model = model.Where(m => m.TraderId.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "TraderId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.TraderId).ToList();
                    else
                        model = model.OrderByDescending(m => m.TraderId).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "Street":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                    break;
                case "ZipCode":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                    break;
                case "City":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult CheckConstructionSite(long id)
        {
            if (db.Md_order_ConstructionSite.Where(a => a.CustomerId == id).Count() > 1)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
        }

        public ActionResult ChooseConstructionSite(PaginationModel pg, long? id, string number)
        {
            if (id != null)
            {
                var model = db.Md_order_ConstructionSite.Where(m => m.IsActive == true && m.IsDeleted == false && m.CustomerId == id).Include(m => m.Md_masterData_Address).OrderBy(a => a.ConstructionSiteId).ToList();

                if (pg != null)
                {
                    foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                    {
                        if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                        {
                            pgFF.colVal = pgFF.colVal.ToLower();
                            switch (pgFF.colName)
                            {
                                case "ConstructionSiteId":
                                    model = model.Where(m => m.ConstructionSiteId != null && m.CustomerId == id && m.ConstructionSiteId.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.FilterCity = pgFF.colVal;
                                    break;
                                case "Name":
                                    model = model.Where(m => m.Name != null && m.CustomerId == id && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.FilterCity = pgFF.colVal;
                                    break;
                                case "Street":
                                    model = model.Where(m => m.Md_masterData_Address.Street != null && m.CustomerId == id && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.FilterCity = pgFF.colVal;
                                    break;
                                case "ZipCode":
                                    model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.CustomerId == id && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.FilterCity = pgFF.colVal;
                                    break;
                                case "City":
                                    model = model.Where(m => m.Md_masterData_Address.City != null && m.CustomerId == id && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.FilterCity = pgFF.colVal;
                                    break;
                            }
                        }
                    }
                }
                switch (pg.orderCol)
                {
                    case "ConstructionSiteId":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.ConstructionSiteId).ToList();
                        else
                            model = model.OrderByDescending(m => m.ConstructionSiteId).ToList();
                        break;
                    case "Name":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Name).ToList();
                        else
                            model = model.OrderByDescending(m => m.Name).ToList();
                        break;
                    case "Street":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                        break;
                    case "ZipCode":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                        break;
                    case "City":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                        break;
                }

                ViewBag.CustomerId = id;
                return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));

                //var model = db.Md_order_ConstructionSite.Where(a => a.CustomerId == id).ToList();
                //return View(model);
            }
            else
            {
                var model = db.Md_order_ConstructionSite.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_masterData_Address).OrderBy(a => a.ConstructionSiteId).ToList();

                if (pg != null)
                {
                    foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                    {
                        if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                        {
                            pgFF.colVal = pgFF.colVal.ToLower();
                            switch (pgFF.colName)
                            {
                                case "ConstructionSiteId":
                                    model = model.Where(m => m.ConstructionSiteId != null && m.ConstructionSiteId.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.ConstructionSiteId = pgFF.colVal;
                                    break;
                                case "Name":
                                    model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.Name = pgFF.colVal;
                                    break;
                                case "Street":
                                    model = model.Where(m => m.Md_masterData_Address.Street != null && m.Md_masterData_Address.Street.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.Street = pgFF.colVal;
                                    break;
                                case "ZipCode":
                                    model = model.Where(m => m.Md_masterData_Address.ZipCode != null && m.Md_masterData_Address.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.ZipCode = pgFF.colVal;
                                    break;
                                case "City":
                                    model = model.Where(m => m.Md_masterData_Address.City != null && m.Md_masterData_Address.City.ToLower().Contains(pgFF.colVal)).ToList();
                                    ViewBag.City = pgFF.colVal;
                                    break;
                            }
                        }
                    }
                }
                switch (pg.orderCol)
                {
                    case "ConstructionSiteId":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.ConstructionSiteId).ToList();
                        else
                            model = model.OrderByDescending(m => m.ConstructionSiteId).ToList();
                        break;
                    case "Name":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Name).ToList();
                        else
                            model = model.OrderByDescending(m => m.Name).ToList();
                        break;
                    case "Street":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.Street).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.Street).ToList();
                        break;
                    case "ZipCode":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.ZipCode).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.ZipCode).ToList();
                        break;
                    case "City":
                        if (pg.orderDir.Equals("desc"))
                            model = model.OrderBy(m => m.Md_masterData_Address.City).ToList();
                        else
                            model = model.OrderByDescending(m => m.Md_masterData_Address.City).ToList();
                        break;
                }
                ViewBag.CustomerId = id;
                return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));


                //var model = db.Md_order_ConstructionSite.ToList();
                //return View(model);
            }

        }

        public ActionResult SelectConstructionSite(long id, PCMS.Areas.MasterData.Models.md_masterData_ConstructionSite model)
        {
            model = db.Md_order_ConstructionSite.Find(db.Md_order_ConstructionSite.Where(a => a.CustomerId == id).Select(a => a.Id).FirstOrDefault());

            if (model == null)
            {
                return Json(new { ConId = "", Name = "", Distance = "" });
            }
            else
            {
                return Json(new { ConId = model.ConstructionSiteId, Name = model.Name, Distance = model.DistanceTime.Value.ToString() });
            }
        }

        public ActionResult ChooseDriver(PaginationModel pg)
        {
            var model = db.Md_order_Driver.Where(m => m.IsActive == true && m.IsDeleted == false).OrderBy(m => m.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrEmpty(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "DriverNumber":
                                model = model.Where(m => m.DriverNumber != null && m.DriverNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.DriverNumber = pgFF.colVal;
                                break;
                            case "LastName":
                                model = model.Where(m => m.Md_masterData_Contact.LastName != null && m.Md_masterData_Contact.LastName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LastName = pgFF.colVal;
                                break;
                            case "FirstName":
                                model = model.Where(m => m.Md_masterData_Contact.FirstName != null && m.Md_masterData_Contact.FirstName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FirstName = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "DriverNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.DriverNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.DriverNumber).ToList();
                    break;
                case "LastName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.LastName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.LastName).ToList();
                    break;
                case "FirstName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Md_masterData_Contact.FirstName).ToList();
                    else
                        model = model.OrderByDescending(m => m.Md_masterData_Contact.FirstName).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChooseVehicle(PaginationModel pg)
        {
            var model = db.md_order_Vehicle.Where(m => m.IsActive == true && m.IsDeleted == false).Include(m => m.Md_Customer).Include(m => m.md_Facilities).Include(m => m.md_masterData_Driver).Include(m => m.Md_masterData_VehicleType).OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "VehicleNumber":
                                model = model.Where(m => m.VehicleNumber != null && m.VehicleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.VehicleNumber = pgFF.colVal;
                                break;
                            case "PlateNumber":
                                model = model.Where(m => m.PlateNumber != null && m.PlateNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.NumberPlate = pgFF.colVal;
                                break;
                            case "Capacity":
                                model = model.Where(m => m.Capacity != null && m.Capacity.ToString().ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Capacity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "VehicleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.VehicleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.VehicleNumber).ToList();
                    break;
                case "PlateNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.PlateNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.PlateNumber).ToList();
                    break;
                case "Capacity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Capacity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Capacity).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult ChooseSpecials(long id)
        {
            var model = db.Md_order_PriceMarkUp.ToList();
            ViewBag.Id = id;
            return View(model);
        }

        public ActionResult RemoveItems(md_order_LoadingOrder model)
        {
            db.Md_Components.RemoveRange(db.Md_Components.Where(a => a.Md_order_LoadingOrder.State == 5 || a.Md_order_LoadingOrder.State == 4));
            db.Md_order_LoadingOrder.RemoveRange(db.Md_order_LoadingOrder.Where(a => a.State == 5 || a.State == 4));
            db.SaveChanges();
            var values = db.Md_order_LoadingOrder.OrderBy(a => a.Sort).ToList();

            for (int i = 0; i < values.Count; i++)
            {
                values[i].Sort = i + 1;
            }

            db.SaveChanges();

            if (values.Count > 1)
            {
                model = db.Md_order_LoadingOrder.Find(db.Md_order_LoadingOrder.Where(a => a.Sort == 1).Select(a => a.Id).FirstOrDefault());
                model.State = 2;
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult RemoveContingentItems(md_order_ContingentOrder model)
        {
            db.Md_order_ContingentOrder.RemoveRange(db.Md_order_ContingentOrder.Where(a => a.State == 4));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SelectComponentsAll(long id)
        {
            PCMS.Areas.MasterData.Models.md_recipe_Recipe modelRec = new MasterData.Models.md_recipe_Recipe();
            PCMS.Areas.RecipeM.Models.md_equipment modelEqu = new RecipeM.Models.md_equipment();
            PCMS.Areas.MasterData.Models.md_masterData_Material model = new MasterData.Models.md_masterData_Material();
            var matrec = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == id).Select(b => b.MaterialId).ToList();
            var matrecValue = db.Md_recipe_RecipeMaterialM.Where(b => b.RecipeId == id).Select(m => m.Value).ToList();
            var rec = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == id).Select(b => b.Type).ToList();
            var FuType = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == id).Select(b => b.FunctionType).ToList();


            for (int i = 0; i < matrec.Count(); i++)
            {

                if (rec[i] == 1)
                {
                    model = db.Md_order_Material.Find(matrec[i]);

                    MatId.Add(model.Id);
                    MatNo.Add(model.ArticleNumber);
                    MatDesc.Add(model.Name);
                    MatType.Add(FuType[i]);
                    MatGroup.Add(rec[i]);
                    MatValue.Add(matrecValue[i].ToString());
                }
                if (rec[i] == 2)
                {
                    modelEqu = db.Md_Equipment.Find(matrec[i]);

                    MatId.Add(modelEqu.Id);
                    MatNo.Add(modelEqu.Number.ToString());
                    MatDesc.Add(modelEqu.Description);
                    MatGroup.Add(rec[i]);
                    MatType.Add(FuType[i]);
                    MatValue.Add(matrecValue[i].ToString());
                }
                if (rec[i] == 3)
                {
                    modelRec = db.Md_order_Recipe.Find(matrec[i]);

                    var recmodel = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == modelRec.Id).Select(a => a.MaterialId).ToList();
                    var recmodel2 = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == modelRec.Id).Select(a => a.Value).ToList();
                    for (int r = 0; r < recmodel.Count(); r++)
                    {
                        if (modelRec.RecipeTypeId == 1)
                        {
                            SelectComponentsAll(modelRec.Id);
                        }
                        else
                        {
                            PCMS.Areas.RecipeM.Models.md_equipment equipModel = new RecipeM.Models.md_equipment();
                            var equipId = recmodel[r];
                            equipModel = db.Md_Equipment.Find(equipId);
                            MatId.Add(equipModel.Id);
                            MatNo.Add(equipModel.Number.ToString());
                            MatDesc.Add(equipModel.Description);
                            MatType.Add(99);
                            MatGroup.Add(2);

                            if (equipModel.Number == 900)
                            {
                                MatValue.Add(db.Md_order_Recipe.Where(a => a.Id == id).Select(a => a.MixingTime).FirstOrDefault().ToString() + ",00");
                            }
                            else
                            {
                                MatValue.Add(recmodel2[r].ToString());
                            }
                        }
                    }
                }
            }
            return Json(new
            {
                id = MatId,
                number = MatNo,
                description = MatDesc,
                value = MatValue,
                type = MatType,
                group = MatGroup
            });
        }

        public ActionResult SelectComponentsStart(long id, string ContingentId, PCMS.Areas.MasterData.Models.md_masterData_Material model)
        {
            List<string> retard = new List<string>();
            var orderId = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == ContingentId).Select(a => a.Id).FirstOrDefault();

            var CompModel = db.Md_Components.Where(a => a.ContingentOrderId == orderId && a.Type == 1 && a.LoadingOrderId == null).ToList();
            for (int c = 0; c < CompModel.Count(); c++)
            {
                if (CompModel[c].Md_material_Material.MaterialGroupId == 2)
                {
                    BinderId.Add(CompModel[c].Md_material_Material.Id);
                    BinderNumber.Add(CompModel[c].Md_material_Material.ArticleNumber);
                    BinderDescription.Add(CompModel[c].Md_material_Material.Name);
                    BinderValue.Add(CompModel[c].Weight.ToString().Replace(".", ","));
                    BinderSort.Add(CompModel[c].Sort);
                    BinderAdmit.Add((CompModel[c].LotSetPoint - CompModel[c].Weight).ToString().Replace(".", ","));
                }

                if (CompModel[c].Md_material_Material.MaterialGroupId == 3)
                {
                    AdmixId.Add(CompModel[c].Md_material_Material.Id);
                    AdmixNumber.Add(CompModel[c].Md_material_Material.ArticleNumber);
                    AdmixDescription.Add(CompModel[c].Md_material_Material.Name);
                    AdmixValue.Add(CompModel[c].Weight.ToString().Replace(".", ","));
                    AdmixSort.Add(CompModel[c].Sort);
                    AdmixAdmit.Add((CompModel[c].LotSetPoint - CompModel[c].Weight).ToString().Replace(".", ","));


                    
                    for (int i = 1; i < AdmixId.Count; i++)
                    {
                        if (GetRetardation(AdmixId[i]) == true)
                        {
                            retard.Add("vz");
                        }
                        else
                        {
                            retard.Add("");
                        }
                    }
                    //ViewBag.retard = new List<string>(retard);
                }

                if (CompModel[c].Md_material_Material.MaterialGroupId == 4)
                {
                    WaterId.Add(CompModel[c].Md_material_Material.Id);
                    WaterNumber.Add(CompModel[c].Md_material_Material.ArticleNumber);
                    WaterDescription.Add(CompModel[c].Md_material_Material.Name);
                    WaterValue.Add(CompModel[c].Weight.ToString().Replace(".", ","));
                    WaterSort.Add(CompModel[c].Sort);
                    WaterAdmit.Add((CompModel[c].LotSetPoint - CompModel[c].Weight).ToString().Replace(".", ","));
                }

                if (CompModel[c].Md_material_Material.MaterialGroupId == 1)
                {
                    AggregateId.Add(CompModel[c].Md_material_Material.Id);
                    AggregateNumber.Add(CompModel[c].Md_material_Material.ArticleNumber);
                    AggregateDescription.Add(CompModel[c].Md_material_Material.Name);
                    AggregateValue.Add(CompModel[c].Weight.ToString().Replace(".", ","));
                    AggregateSort.Add(CompModel[c].Sort);
                }

                if (CompModel[c].Md_material_Material.MaterialGroupId == 6)
                {
                    AdditiveId.Add(CompModel[c].Md_material_Material.Id);
                    AdditiveNumber.Add(CompModel[c].Md_material_Material.ArticleNumber);
                    AdditiveDescription.Add(CompModel[c].Md_material_Material.Name);
                    AdditiveValue.Add(CompModel[c].Weight.ToString().Replace(".", ","));
                    AdditiveSort.Add(CompModel[c].Sort);
                }
            }
            return Json(new
            {
                binderId = BinderId,
                binderNo = BinderNumber,
                binderDes = BinderDescription,
                binderValue = BinderValue,
                binderSort = BinderSort,
                binderAdmit = BinderAdmit,
                admixId = AdmixId,
                admixNo = AdmixNumber,
                admixDes = AdmixDescription,
                admixValue = AdmixValue,
                admixAdmit = AdmixAdmit,
                admixSort = AdmixSort,
                waterId = WaterId,
                waterNo = WaterNumber,
                waterDes = WaterDescription,
                waterValue = WaterValue,
                waterSort = WaterSort,
                waterAdmit = WaterAdmit,
                addiId = AdditiveId,
                addiNo = AdditiveNumber,
                addiDes = AdditiveDescription,
                addiVal = AdditiveValue,
                addiSort = AdditiveSort,
                aggreId = AggregateId,
                aggreNo = AggregateNumber,
                aggreDes = AggregateDescription,
                aggreValue = AggregateValue,
                aggreSort = AggregateSort,
                retardation = retard
            });
        }

        public ActionResult SelectComponents(long id, PCMS.Areas.MasterData.Models.md_masterData_Material model)
        {
            var matrec = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == id).Select(b => b.MaterialId).ToList();
            var matrecValue = db.Md_orderRecipe_RecipeMaterial.Where(b => b.RecipeId == id).Select(m => m.Weight).ToList();
            var matrecSort = db.Md_orderRecipe_RecipeMaterial.Where(c => c.RecipeId == id).Select(m => m.Sort).ToList();
            var matrecType = db.Md_orderRecipe_RecipeMaterial.Where(a => a.RecipeId == id).Select(m => m.Type).ToList();

            for (int i = 0; i < matrec.Count(); i++)
            {
                if (matrecType[i] == 1)
                {
                    model = db.Md_order_Material.Find(matrec[i]);

                    if (model.MaterialGroupId == 2)
                    {
                        BinderId.Add(model.Id);
                        BinderNumber.Add(model.ArticleNumber);
                        BinderDescription.Add(model.Name);
                        BinderValue.Add(matrecValue[i].Value.ToString().Replace(".", ","));
                        BinderSort.Add(matrecSort[i]);
                    }
                    if (model.MaterialGroupId == 3)
                    {
                        AdmixId.Add(model.Id);
                        AdmixNumber.Add(model.ArticleNumber);
                        AdmixDescription.Add(model.Name);
                        AdmixValue.Add(matrecValue[i].Value.ToString().Replace(".", ","));
                        AdmixSort.Add(matrecSort[i]);
                    }
                    if (model.MaterialGroupId == 4)
                    {
                        WaterId.Add(model.Id);
                        WaterNumber.Add(model.ArticleNumber);
                        WaterDescription.Add(model.Name);
                        WaterValue.Add(matrecValue[i].Value.ToString().Replace(".", ","));
                        WaterSort.Add(matrecSort[i]);
                    }
                    if (model.MaterialGroupId == 1)
                    {
                        AggregateId.Add(model.Id);
                        AggregateNumber.Add(model.ArticleNumber);
                        AggregateDescription.Add(model.Name);
                        AggregateValue.Add(matrecValue[i].Value.ToString().Replace(".", ","));
                        AggregateSort.Add(matrecSort[i]);
                    }
                    if (model.MaterialGroupId == 6)
                    {
                        AdditiveId.Add(model.Id);
                        AdditiveNumber.Add(model.ArticleNumber);
                        AdditiveDescription.Add(model.Name);
                        AdditiveValue.Add(matrecValue[i].Value.ToString().Replace(".", ","));
                        AdditiveSort.Add(matrecSort[i]);
                    }
                }
            }
            return Json(new
            {
                binderId = BinderId,
                binderNo = BinderNumber,
                binderDes = BinderDescription,
                binderValue = BinderValue,
                binderSort = BinderSort,
                admixId = AdmixId,
                admixNo = AdmixNumber,
                admixDes = AdmixDescription,
                admixValue = AdmixValue,
                admixSort = AdmixSort,
                waterId = WaterId,
                waterNo = WaterNumber,
                waterDes = WaterDescription,
                waterValue = WaterValue,
                waterSort = WaterSort,
                addiId = AdditiveId,
                addiNo = AdditiveNumber,
                addiDes = AdditiveDescription,
                addiVal = AdditiveValue,
                addiSort = AdditiveSort,
                aggreId = AggregateId,
                aggreNo = AggregateNumber,
                aggreDes = AggregateDescription,
                aggreValue = AggregateValue,
                aggreSort = AggregateSort
            });
        }

        public bool GetRetardation(long? Id)
        {
            bool checkMatId = false;
            var season = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
            var MatId = db.Md_order_RetardationTime.Where(a => a.Season == season).Select(a => a.MaterialId).ToList();
            for(int i = 0; i < MatId.Count; i++)
            {
                if(MatId[i] == Id)
                {
                    checkMatId = true;
                    break;
                }
            }
            return checkMatId;
        }

        public ActionResult ChooseDeliveryNoteType()
        {
            var model = db.Md_order_DeliveryNoteType.ToList();
            return View(model);
        }

        public ActionResult ChooseRetardation()
        {
            var season = db.Md_Config.Where(a => a.Id == 1).Select(a => a.Season).FirstOrDefault();
            var model = db.Md_order_RetardationTime.Where(a => a.Season == season).ToList();
            return View(model);
        }

        public ActionResult SelectRetardationMaterial(long id)
        {
            var model = db.Md_order_Material.Find(id);

            return Json(new { MatNo = model.MaterialNumber, MatName = model.Name });
        }

        public ActionResult SelectDate(DateTime regDate, int state)
        {

            if (state == 0)
            {
                var modelLoading = db.Md_order_LoadingOrder.Where(m => m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day).OrderBy(a => a.Sort).ToList();
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                var OrderId = new List<long?>();
                var IsValue = new List<decimal?>();

                for (int i = 0; i < modelLoading.Count(); i++)
                {
                    var tmpArticleNo = modelLoading[i].MaterialNumber;
                    OrderId.Add(modelLoading[i].Id);
                    var tmpOrderId = OrderId[i];
                    IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                ViewBag.MaterialMoving = new List<decimal?>(IsValue);
                return PartialView(modelLoading);
            }
            else
            {
                var modelLoading = db.Md_order_LoadingOrder.Where(m => m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day && m.State == state).OrderBy(a => a.Sort).ToList();
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                var OrderId = new List<long?>();
                var IsValue = new List<decimal?>();

                for (int i = 0; i < modelLoading.Count(); i++)
                {
                    var tmpArticleNo = modelLoading[i].MaterialNumber;
                    OrderId.Add(modelLoading[i].Id);
                    var tmpOrderId = OrderId[i];
                    IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
                }
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
                ViewBag.MaterialMoving = new List<decimal?>(IsValue);
                return PartialView(modelLoading);
            }
        }

        public ActionResult SelectDateContingent(DateTime regDate, int state)
        {
            var modelLoading = db.Md_order_ContingentOrder.Where(m => m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day).OrderBy(a => a.LoadingTime).ToList();
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            var OrderId = new List<long?>();
            var IsValue = new List<decimal?>();

            for (int i = 0; i < modelLoading.Count(); i++)
            {
                var tmpArticleNo = modelLoading[i].MaterialNumber;
                OrderId.Add(modelLoading[i].Id);
                var tmpOrderId = OrderId[i];
                IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
            }
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            ViewBag.MaterialMoving = new List<decimal?>(IsValue);
            return PartialView(modelLoading);
        }

        public ActionResult SelectState(int state, DateTime regDate)
        {
            if (state == 0)
            {
                var modelLoading = db.Md_order_LoadingOrder.Where(m => m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day).OrderBy(a => a.Sort).ToList();
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

                var OrderId = new List<long?>();
                var IsValue = new List<decimal?>();

                for (int i = 0; i < modelLoading.Count(); i++)
                {
                    var tmpArticleNo = modelLoading[i].MaterialNumber;
                    OrderId.Add(modelLoading[i].Id);
                    var tmpOrderId = OrderId[i];
                    IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
                }

                ViewBag.MaterialMoving = new List<decimal?>(IsValue);
                return PartialView("SelectDate", modelLoading);
            }
            else
            {
                var modelLoading = db.Md_order_LoadingOrder.Where(m => m.State == state && m.RegistrationDate.Value.Year == regDate.Year && m.RegistrationDate.Value.Month == regDate.Month && m.RegistrationDate.Value.Day == regDate.Day).OrderBy(a => a.Sort).ToList();
                ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();

                var OrderId = new List<long?>();
                var IsValue = new List<decimal?>();

                for (int i = 0; i < modelLoading.Count(); i++)
                {
                    var tmpArticleNo = modelLoading[i].MaterialNumber;
                    OrderId.Add(modelLoading[i].Id);
                    var tmpOrderId = OrderId[i];
                    IsValue.Add(db.Md_MaterialMoving.Where(a => a.OrderId == tmpOrderId && a.ArticleNo == tmpArticleNo).Select(a => a.QuantityIs).FirstOrDefault());
                }

                ViewBag.MaterialMoving = new List<decimal?>(IsValue);

                return PartialView("SelectDate", modelLoading);
            }

        }

        public ActionResult SetpointCalculation(decimal LotWeight, decimal LotCount, long? id)
        {
            var matrec = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == id).Select(b => b.MaterialId).ToList();
            var matrecValue = db.Md_recipe_RecipeMaterialM.Where(b => b.RecipeId == id).Select(m => m.Value).ToList();
            var rec = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == id).Select(b => b.Type).ToList();
            //int l = 0;
            for (int i = 0; i < matrec.Count(); i++)
            {
                if (rec[i] == 1)
                {
                    var BasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();

                    decimal newSP = Math.Round((LotWeight / BasicCalc) * matrecValue[i], 2);
                    decimal newOSP = Math.Round(newSP * LotCount, 2);

                    var model = db.Md_order_Material.Find(matrec[i]);

                    var LocNumber = db.Md_Location.Where(a => a.MaterialId == model.Id && a.IsActive == true && a.IsDeleted == false).Select(a => a.Number).FirstOrDefault();
                    var LocationUnlock = true;
                    if (db.Md_Location.Where(a => a.MaterialId == model.Id && a.IsActive == true && a.IsDeleted == false).Select(a => a.Active1).FirstOrDefault() == false)
                    {
                        LocationUnlock = false;
                    }
                    LocUnlock.Add(LocationUnlock);
                    LocNo.Add(LocNumber);

                    newSetpoint.Add(newSP.ToString().Replace(".", ","));
                    newOrderSetpoint.Add(newOSP.ToString().Replace(".", ","));
                }
                else
                {
                    Int64 recIdent = Convert.ToInt64(matrec[i]);
                    var recId = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recIdent).Select(a => a.MaterialId).ToList();
                    var recValue = db.Md_recipe_RecipeMaterialM.Where(a => a.RecipeId == recIdent).Select(a => a.Value).ToList();

                    for (int l = 0; l < recId.Count(); l++)
                    {
                        Int64 newRecId = Convert.ToInt64(recId[l]);
                        var LocNumber = db.Md_Equipment.Where(a => a.Id == newRecId && a.IsDeleted == false && a.IsActive == true).Select(a => a.Number).FirstOrDefault();
                        LocNo.Add(LocNumber.ToString());

                        if (LocNumber == 900 && rec[i] == 3)
                            newSetpoint.Add(String.IsNullOrWhiteSpace(db.Md_order_Recipe.Where(a => a.Id == id).Select(a => a.MixingTime).FirstOrDefault().ToString()) ? recValue[l].ToString() : db.Md_order_Recipe.Where(a => a.Id == id).Select(a => a.MixingTime).FirstOrDefault().ToString() + ",00");
                        else
                            newSetpoint.Add(recValue[l].ToString());
                        newOrderSetpoint.Add("0,00");
                    }
                }
            }
            return Json(new { newSetpoint = newSetpoint, newOrderSP = newOrderSetpoint, locNo = LocNo, locUnlock = LocUnlock });
        }

        public ActionResult Calculations(decimal OrdQuan, decimal DelQuan, decimal MaxLot)
        {
            var BasicCalc = db.Md_Config.Where(a => a.Id == 1).Select(a => a.BasicCalculation).FirstOrDefault();
            if (OrdQuan != 0 && MaxLot != 0)
            {
                if (Helper.Definitions.getFactoryGroup() == 1)
                {
                    decimal tempOrdQuan = OrdQuan - DelQuan;
                    decimal new_chargenCount = 0;
                    new_chargenCount = tempOrdQuan / MaxLot;
                    decimal ChargenCount = Math.Ceiling(new_chargenCount); // Immer aufrunden

                    decimal new_OrderedQuantity = ChargenCount * MaxLot;
                    decimal dif_OrderedQuantity = new_OrderedQuantity - tempOrdQuan;
                    decimal one_proz_rez_weight = MaxLot / BasicCalc;
                    decimal new_MaxCharge = MaxLot - (dif_OrderedQuantity / ChargenCount);
                    decimal new_chargen_weight_proz = new_MaxCharge / one_proz_rez_weight;

                    // ChargenCount
                    decimal Charge_weight = new_MaxCharge;
                    decimal Charge_proz = new_chargen_weight_proz;
                    decimal newOrderedQuantity = Charge_weight * ChargenCount;

                    if (BasicCalc == 1000)
                    {
                        Charge_proz = Math.Round(Charge_proz / 10, 2);
                    }

                    return Json(new { LotCount = ChargenCount, LotWeight = Charge_weight.ToString().Replace(".", ","), LotProc = Charge_proz.ToString().Replace(".", ","), NewOrdQuan = new_OrderedQuantity });
                }
                else
                {
                    return Json(true);
                }
            }
            return Json(false);
        }

        public ActionResult SelectCustomerOverConstructionSite(int id)
        {
            var model = db.Md_order_Customer.Find(id);
            return Json(new { number = model.CustomerId, name = model.Name });
        }

        public ActionResult ChangeContingentState(int id)
        {
            md_order_ContingentOrder model = new md_order_ContingentOrder();
            model = db.Md_order_ContingentOrder.Find(id);
            model.State = 4;
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            return Json(true);
        }

        //++++++++++++++++++++ Baustelle anlegen ++++++++++++++++++++

        public ActionResult CreateConstructionSite(MasterData.Models.MasterDataContactView model, long? id)
        {
            if (id != null)
            {
                //model.constructionSite.CustomerId = id;
                ViewBag.CustomerId = id;
                model.customer = db.Md_order_Customer.Find(id);
                return View(model);
            }
            else
            {
                return View();
            }
        }

        public ActionResult SaveConstructionSite(string name, string number, string street, string zip, string city, TimeSpan? distance, long? customer)
        {
            PCMS.Areas.MasterData.Models.md_masterData_ConstructionSite constructionsite = new PCMS.Areas.MasterData.Models.md_masterData_ConstructionSite();
            PCMS.Areas.MasterData.Models.md_masterData_Contact contactModel = new PCMS.Areas.MasterData.Models.md_masterData_Contact();
            PCMS.Areas.MasterData.Models.md_masterData_Address addressModel = new PCMS.Areas.MasterData.Models.md_masterData_Address();
            if (ModelState.IsValid)
            {
                while (number.Length < 3)
                {
                    number = "0" + number;
                }

                constructionsite.Name = name;
                constructionsite.ConstructionSiteId = number;
                constructionsite.CustomerId = customer;
                constructionsite.DistanceTime = distance;
                constructionsite.IsActive = true;
                constructionsite.IsDeleted = false;

                addressModel.Street = street;
                addressModel.ZipCode = zip;
                addressModel.City = city;

                db.Md_order_Address.Add(addressModel);
                db.SaveChanges();

                constructionsite.AddressId = addressModel.Id;
                db.Md_order_ConstructionSite.Add(constructionsite);

                db.SaveChanges();

                int hours = distance.Value.Hours;
                int minutes = distance.Value.Minutes;
                string Hour = hours.ToString();
                string Minute = minutes.ToString();
                if (hours < 10)
                {
                    Hour = "0" + hours.ToString();
                }
                if (minutes < 10)
                {
                    Minute = "0" + minutes.ToString();
                }
                string time = Hour + ":" + Minute;

                return Json(new { Id = constructionsite.Id, number = constructionsite.ConstructionSiteId, name = constructionsite.Name, distanceTime = time });
            }
            return Json(false);
        }

        //++++++++++++++++++++ Kunden anlegen ++++++++++++++++++++

        public ActionResult CreateCustomer()
        {
            ViewBag.Number = db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault();
            return View();
        }

        public ActionResult SaveCustomer(string name, string number, string numberEdv, string street, string zip, string city, string firstname, string lastname, string tel, string mobile, string trader, long? rating)
        {
            PCMS.Areas.MasterData.Models.md_masterData_Customer customerModel = new PCMS.Areas.MasterData.Models.md_masterData_Customer();
            PCMS.Areas.MasterData.Models.md_masterData_Contact contactModel = new PCMS.Areas.MasterData.Models.md_masterData_Contact();
            PCMS.Areas.MasterData.Models.md_masterData_Address addressModel = new PCMS.Areas.MasterData.Models.md_masterData_Address();
            if (ModelState.IsValid)
            {
                customerModel.Name = name;
                customerModel.CustomerId = number;
                customerModel.CustomerIdEDV = numberEdv;
                customerModel.TraderId = trader;
                customerModel.CustomerRatingId = rating;
                customerModel.IsActive = true;
                customerModel.IsDeleted = false;

                addressModel.Street = street;
                addressModel.ZipCode = zip;
                addressModel.City = city;

                contactModel.FirstName = firstname;
                contactModel.LastName = lastname;
                contactModel.Mobile = mobile;
                contactModel.Tel = tel;

                db.Md_order_Address.Add(addressModel);
                db.Md_order_Contact.Add(contactModel);
                db.SaveChanges();

                customerModel.AddressId = addressModel.Id;
                customerModel.ContactId = contactModel.Id;
                db.Md_order_Customer.Add(customerModel);

                db.SaveChanges();

                try
                {
                    var CustomerNumber = Convert.ToInt64(db.Md_Config.Where(a => a.Id == 1).Select(a => a.CustomerNumber).FirstOrDefault());
                    CustomerNumber = CustomerNumber + 1;
                    string query = "UPDATE md_Config SET CustomerNumber = " + CustomerNumber.ToString() + " WHERE Id = 1";
                    db.Database.ExecuteSqlCommand(query);
                }
                catch
                {

                }

                return Json(new { Id = customerModel.Id, Number = customerModel.CustomerId, Name = customerModel.Name });
            }
            return Json(false);
        }

        //++++++++++++++++++++ Händler anlegen ++++++++++++++++++++

        public ActionResult CreateTrader()
        {
            return View();
        }

        public ActionResult SaveTrader(string name, string number, string numberEdv, string street, string zip, string city, string firstname, string lastname, string tel, string mobile)
        {
            PCMS.Areas.MasterData.Models.md_masterData_Trader traderModel = new PCMS.Areas.MasterData.Models.md_masterData_Trader();
            PCMS.Areas.MasterData.Models.md_masterData_Contact contactModel = new PCMS.Areas.MasterData.Models.md_masterData_Contact();
            PCMS.Areas.MasterData.Models.md_masterData_Address addressModel = new PCMS.Areas.MasterData.Models.md_masterData_Address();
            if (ModelState.IsValid)
            {
                traderModel.Name = name;
                traderModel.TraderId = number;
                traderModel.TraderIdEDV = numberEdv;
                traderModel.IsActive = true;
                traderModel.IsDeleted = false;

                addressModel.Street = street;
                addressModel.ZipCode = zip;
                addressModel.City = city;

                contactModel.FirstName = firstname;
                contactModel.LastName = lastname;
                contactModel.Mobile = mobile;
                contactModel.Tel = tel;

                db.Md_order_Address.Add(addressModel);
                db.Md_order_Contact.Add(contactModel);
                db.SaveChanges();

                traderModel.AddressId = addressModel.Id;
                traderModel.ContactId = contactModel.Id;
                db.Md_Trader.Add(traderModel);

                db.SaveChanges();
                return Json(new { TraderId = traderModel.Id, TraderNumber = traderModel.TraderId, TraderName = traderModel.Name });
            }
            return Json(false);
        }

        public ActionResult ErrorMessages(string message, string destination)
        {
            ViewBag.Error = message;
            ViewBag.CompanyType = Helper.Definitions.getFactoryGroup();
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                ViewBag.Location = new SelectList(db.Md_Location.Where(a => a.GroupId == 8 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
            }
            else
            {
                ViewBag.Location = new SelectList(db.Md_Location.Where(a => a.GroupId == 9 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
            }
            ViewBag.HotWater = false;
            ViewBag.OrderedDefault = 0;
            ViewBag.DeliveredDefault = 0;
            ViewBag.MaxLotDefault = 0;
            ViewBag.MinLotDefault = 0;

            return View(destination);
        }

        public ActionResult Print(int id)
        {
            var loading = db.Md_order_LoadingOrder.Where(a => a.Id == id).FirstOrDefault();
            Helper.DeliveryNotePrint.printLot(loading.OrderNumber);
            return Json(true);
        }

        public SelectList Location()
        {
            if (Helper.Definitions.getFactoryGroup() == 1)
            {
                return new SelectList(db.Md_Location.Where(a => a.GroupId == 8 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
            }
            else
            {
                return new SelectList(db.Md_Location.Where(a => a.GroupId == 9 && a.IsActive == true && a.IsDeleted == false && a.Active2 == true).ToList(), "Id", "Description");
            }
        }

        public ActionResult CheckAmountTestRequired(long id)
        {
            var FamId = db.Md_order_Sort.Where(a => a.Id == id).Select(a => a.ConcreteFamilyId).FirstOrDefault();
            if (FamId != null)
            {
                if (db.ConcreteFamily.Where(a => a.Id == FamId).Select(a => a.AmountTestRequired).FirstOrDefault() == true)
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }
            return Json(false);
        }

        public ActionResult CheckTimeTestRequired(long id)
        {
            var FamId = db.Md_order_Sort.Where(a => a.Id == id).Select(a => a.ConcreteFamilyId).FirstOrDefault();

            if (FamId != null)
            {
                if (db.ConcreteFamily.Where(a => a.Id == FamId).Select(a => a.TimeTestRequired).FirstOrDefault() == true)
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }
            return Json(false);
        }

        public ActionResult EditLoadingTime(string OrderNumber)
        {
            var ContingentNumber = db.Md_order_LoadingOrder.Where(a => a.OrderNumber == OrderNumber).Select(a => a.ContingentOrderNumber).FirstOrDefault();
            if (ContingentNumber != null && ContingentNumber != "")
            {
                TimeSpan timeNow = DateTime.Now.TimeOfDay;

                Models.md_order_ContingentOrder model = new md_order_ContingentOrder();
                model = db.Md_order_ContingentOrder.Where(a => a.ContingentNumber == ContingentNumber).FirstOrDefault();
                model.LoadingTime = new TimeSpan(timeNow.Hours, timeNow.Minutes, 00) + model.DeliveryCycleTime;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return Json(true);
            }
            return Json(false);
        }



        //***************************************************************************Reporting
        // Ladeaufträge
        public ActionResult Report1(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            TopFilter top = pg.topFilter[0];
            DateTime on = new DateTime(Convert.ToInt32(top.from.Split('-')[0]), Convert.ToInt32(top.from.Split('-')[1]), Convert.ToInt32(top.from.Split('-')[2])).Date;
            var model = db.Md_order_LoadingOrder.Where(a => a.State == 4 || a.State == 5 || a.State == 3 || a.State == 2).OrderBy(a => a.Sort).ToList();
            model = model.Where(a => a.RegistrationDate.Value.Date == on.Date).ToList();
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            decimal sum = 0;
            decimal kies = 0;
            foreach (var dataset in model)
            {
                string status = "";
                if (dataset.State == 3)
                    status = "freigegeben";
                else if (dataset.State == 3)
                    status = "in bearbeitung";
                else if (dataset.State == 4)
                    status = "beendet";
                else if (dataset.State == 5)
                    status = "storniert";

                //ds.List.AddListRow(dataset.Id.ToString(), dataset.OrderNumber, dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.VehiclePlateNo, dataset.MaterialDescription, dataset.RecipeDescription, dataset.LoadingTime.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.State.ToString(), "", "", "", "", "", "", "", "", "", "");
                //ds.List.AddListRow(dataset.OrderNumber, dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.VehiclePlateNo, dataset.MaterialDescription, dataset.RecipeDescription, dataset.LoadingTime.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.State.ToString(), "", "", "", "", "", "", "", "", "", "", "");
                ds.List.AddListRow(dataset.OrderNumber, dataset.CustomerNumber + ", " + dataset.CustomerName, dataset.ConstructionSiteNumber + ", " + dataset.ConstructionSiteDescription, dataset.VehiclePlateNo, dataset.MaterialNumber, dataset.RecipeNumber, dataset.LoadingTime.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.State.ToString(), (dataset.OrderedQuantity ?? 0).ToString() + "m³", status, "", "", "", "", "", "", "", "", "");
                if (dataset.MaterialNumber == "80" || dataset.MaterialNumber == "81" || dataset.MaterialNumber == "82" || dataset.MaterialNumber == "83" || dataset.MaterialNumber == "84" || dataset.MaterialNumber == "85" || dataset.MaterialNumber == "86" || dataset.MaterialNumber == "87" || dataset.MaterialNumber == "88" || dataset.MaterialNumber == "89")
                    kies += dataset.OrderedQuantity ?? 0;
                sum += dataset.OrderedQuantity ?? 0;
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "Auftagsnr."));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Kunde"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Baustelle"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Kennzeichen"));
            repParams.Add(new ReportParameter("Hide_03", "true"));
            repParams.Add(new ReportParameter("Header_04", "Artikel"));
            repParams.Add(new ReportParameter("Hide_04", "true"));
            repParams.Add(new ReportParameter("Header_05", "Rezept"));
            repParams.Add(new ReportParameter("Hide_05", "false"));
            repParams.Add(new ReportParameter("Header_06", "Ladezeit"));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", "Entfernungszeit"));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", "Status"));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", "Menge"));
            repParams.Add(new ReportParameter("Hide_09", "false"));
            repParams.Add(new ReportParameter("Header_10", "Status"));
            repParams.Add(new ReportParameter("Hide_10", "false"));


            repParams.Add(new ReportParameter("Title", "Ladeaufträge"));
            repParams.Add(new ReportParameter("Date", on.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Betonmenge:     " + (sum - kies) + "m³ \nKiesMenge:        " + (kies) + "m³  \nGesamtmenge: " + sum + "m³"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/LOrder/LoadingOrder.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;


            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);

            //ViewBag.ReportViewer = reportViewer;
            //
            //return PartialView("Report");
        }
        // Kontingent Aufträge
        public ActionResult Report2(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_order_ContingentOrder.ToList();
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                //ds.List.AddListRow(dataset.Id.ToString(), dataset.ContingentNumber, dataset.LoadingTime.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.OrderedQuantity.ToString(), dataset.DeliveredQuantity.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.RecipeDescription, dataset.State.ToString(), "", "", "", "", "", "", "", "", "");
                ds.List.AddListRow(dataset.ContingentNumber, dataset.LoadingTime.ToString(), dataset.ConstructionSiteTime.ToString(), dataset.CustomerName, dataset.ConstructionSiteDescription, dataset.OrderedQuantity.ToString(), dataset.DeliveredQuantity.ToString(), dataset.RecipeDescription, dataset.State.ToString(), "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "Kontingentennummer"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Ladezeit"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Baustellenzeit"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Kunde"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Baustelle"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Bestellte Menge"));
            repParams.Add(new ReportParameter("Hide_05", "false"));
            repParams.Add(new ReportParameter("Header_06", "Gelieferte Menge"));
            repParams.Add(new ReportParameter("Hide_06", "false"));
            repParams.Add(new ReportParameter("Header_07", "Rezept"));
            repParams.Add(new ReportParameter("Hide_07", "false"));
            repParams.Add(new ReportParameter("Header_08", "Status"));
            repParams.Add(new ReportParameter("Hide_08", "false"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));


            repParams.Add(new ReportParameter("Title", "Kontingentaufträge"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Kontingentaufträge"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            //reportViewer.Width = System.Web.UI.WebControls.Unit.Percentage(100);
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView("Report");
        }
    }
}