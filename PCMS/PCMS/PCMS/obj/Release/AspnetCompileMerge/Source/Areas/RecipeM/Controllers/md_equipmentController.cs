﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Recipe.Models;
using PCMS.Areas.RecipeM.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;

namespace PCMS.Areas.RecipeM.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_equipmentController : Controller
    {
        private RecipeContext db = new RecipeContext();
        private RecipeMContext db2 = new RecipeMContext();

        // GET: RecipeM/md_equipment
        public ActionResult Index(PaginationModel pg)
        {
            var model = db2.Md_equipment.OrderBy(a => a.Number).Where(a=>a.IsDeleted==false).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Number":
                                model = model.Where(m => m.Number.ToString().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: RecipeM/md_equipment/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_equipment md_equipment = db2.Md_equipment.Find(id);
            if (md_equipment == null)
            {
                return HttpNotFound();
            }
            return View(md_equipment);
        }

        // GET: RecipeM/md_equipment/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RecipeM/md_equipment/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Number,Description")] md_equipment md_equipment)
        {
            if (ModelState.IsValid)
            {
                md_equipment.IsActive = true;
                md_equipment.IsDeleted = false;
                db2.Md_equipment.Add(md_equipment);
                db2.SaveChanges(User.Identity.Name);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_equipment);
        }

        // GET: RecipeM/md_equipment/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_equipment md_equipment = db.md_equipment.Find(id);
            if (md_equipment == null)
            {
                return HttpNotFound();
            }
            return View(md_equipment);
        }

        // POST: RecipeM/md_equipment/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Number,Description")] md_equipment md_equipment)
        {
            if (ModelState.IsValid)
            {
                md_equipment.IsActive = true;
                md_equipment.IsDeleted = false;
                db2.Entry(md_equipment).State = EntityState.Modified;
                db2.SaveChanges(User.Identity.Name);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_equipment);
        }

        // GET: RecipeM/md_equipment/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_equipment md_equipment = db2.Md_equipment.Find(id);
            if (md_equipment == null)
            {
                return HttpNotFound();
            }
            List<md_recipe_RecipeM> rM = db2.Md_recipe_Recipe.Where(a => a.IsDeleted == false && a.RecipeMaterials.Any(s => s.Type == 2 && s.MaterialId == id)).ToList();
            ViewBag.md_recipe_RecipeM = rM;
            return View(md_equipment);
        }

        // POST: RecipeM/md_equipment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_equipment md_equipment = db2.Md_equipment.Find(id);
            md_equipment.IsActive = true;
            md_equipment.IsDeleted = true;
            db2.Entry(md_equipment).State = EntityState.Modified;
            //db.md_equipment.Remove(md_equipment);
            db2.SaveChanges(User.Identity.Name);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db2.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
