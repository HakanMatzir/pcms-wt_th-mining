﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Material.Models;
using PCMS.Areas.Order.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

namespace PCMS.Areas.Material.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_material_LocationController : Controller
    {
        private MaterialContext db = new MaterialContext();

        // GET: Material/md_material_Location
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_material_Location.Include(m => m.Md_material_LocationGroup).Include(m => m.Md_material_Material).OrderBy(a=>a.Number).Where(m => m.IsDeleted == false && m.IsActive == true).AsQueryable();
            ///var model = db.Md_material_Location.Include(m => m.Md_material_LocationGroup).Include(m => m.Md_material_Material).Where(m=> m.IsDeleted == false && m.IsActive == true  ).ToList();
            
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "GroupId":
                                model = model.Where(m => m.Md_material_LocationGroup.Description != null && m.Md_material_LocationGroup.Description.ToLower().Contains(pgFF.colVal));
                                ViewBag.GroupId = pgFF.colVal;
                                break;
                            case "MaterialId":
                                model = model.Where( m => m.MaterialId != null && m.Md_material_Material.Name != null && m.Md_material_Material.Name.ToLower().Contains(pgFF.colVal));
                                ViewBag.MaterialId = pgFF.colVal;
                                break;
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal));
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal));
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id);
                    else
                        model = model.OrderByDescending(m => m.Id);
                    break;
                case "GroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GroupId);
                    else
                        model = model.OrderByDescending(m => m.GroupId);
                    break;
                case "MaterialId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialId);
                    else
                        model = model.OrderByDescending(m => m.MaterialId);
                    break;
                case "FacilityId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilityId);
                    else
                        model = model.OrderByDescending(m => m.FacilityId);
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number);
                    else
                        model = model.OrderByDescending(m => m.Number);
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description);
                    else
                        model = model.OrderByDescending(m => m.Description);
                    break;
                case "Max":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Max);
                    else
                        model = model.OrderByDescending(m => m.Max);
                    break;
                case "Min":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Min);
                    else
                        model = model.OrderByDescending(m => m.Min);
                    break;
                case "Value":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Value);
                    else
                        model = model.OrderByDescending(m => m.Value);
                    break;
                case "Capacity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Capacity);
                    else
                        model = model.OrderByDescending(m => m.Capacity);
                    break;
                case "Active1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active1);
                    else
                        model = model.OrderByDescending(m => m.Active1);
                    break;
                case "Active2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active2);
                    else
                        model = model.OrderByDescending(m => m.Active2);
                    break;
                case "Active3":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active3);
                    else
                        model = model.OrderByDescending(m => m.Active3);
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Material/md_material_Location/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_Location md_material_Location = db.Md_material_Location.Find(id);
            if (md_material_Location == null)
            {
                return HttpNotFound();
            }
            return View(md_material_Location);
        }

        // GET: Material/md_material_Location/Create
        public ActionResult Create()
        {
            ViewBag.GroupId = new SelectList(db.Md_material_LocationGroup.Where(m => m.IsDeleted==false), "Id", "Description");
            return View();
        }

        // POST: Material/md_material_Location/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GroupId,MaterialId,Number,Description,Max,Min,Capacity,Value,Active1,Active2,Active3")] md_material_Location md_material_Location)
        {
            md_material_Location.FacilityId = 1;
            if (ModelState.IsValid)
            {
                md_material_Location.IsActive = true;
                md_material_Location.IsDeleted = false;
                db.Md_material_Location.Add(md_material_Location);
                //db.SaveChanges();
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }

            ViewBag.GroupId = new SelectList(db.Md_material_LocationGroup.Where(m => m.IsDeleted == false), "Id", "Description", md_material_Location.MaterialId);
            return View(md_material_Location);
        }

        // GET: Material/md_material_Location/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_Location md_material_Location = db.Md_material_Location.Find(id);
            if (md_material_Location == null)
            {
                return HttpNotFound();
            }
            ViewBag.GroupId = new SelectList(db.Md_material_LocationGroup.Where(m => m.IsDeleted == false), "Id", "Description", md_material_Location.GroupId);
            ViewBag.MaterialMoving = db.Md_material_MaterialMoving.OrderByDescending(a => a.RegistrationDate).Where(a => a.LocationId == id).ToList();
            ViewBag.Sum = db.Md_material_MaterialMoving.Where(a => a.LocationId == id).Select(a => a.QuantityIs).Sum();

            return View(md_material_Location);
        }

        // POST: Material/md_material_Location/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GroupId,MaterialId,Number,Description,Max,Min,Capacity,Value,Active1,Active2,Active3")] md_material_Location md_material_Location)
        {
            md_material_Location.FacilityId = 1;
            if (ModelState.IsValid)
            {
                md_material_Location.IsActive = true;
                md_material_Location.IsDeleted = false;
                db.Entry(md_material_Location).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GroupId = new SelectList(db.Md_material_LocationGroup.Where(m => m.IsDeleted == false), "Id", "Description", md_material_Location.GroupId);
            ViewBag.MaterialMoving = db.Md_material_MaterialMoving.OrderByDescending(a => a.RegistrationDate).Where(a => a.LocationId == md_material_Location.Id).ToList();
            ViewBag.Sum = db.Md_material_MaterialMoving.Where(a => a.LocationId == md_material_Location.Id).Select(a => a.QuantityIs).Sum();
            return View(md_material_Location);
        }

        // GET: Material/md_material_Location/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_material_Location md_material_Location = db.Md_material_Location.Find(id);
            if (md_material_Location == null)
            {
                return HttpNotFound();
            }
            return View(md_material_Location);
        }

        // POST: Material/md_material_Location/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_material_Location md_material_Location = db.Md_material_Location.Find(id);
            md_material_Location.IsActive = true;
            md_material_Location.IsDeleted = true;
            db.Entry(md_material_Location).State = EntityState.Modified;
            //db.Md_material_Location.Remove(md_material_Location);
            db.SaveChanges(User.Identity.Name);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult choosMaterial(PCMS.Models.PaginationModel pg)
        {
            Areas.Recipe.Models.RecipeContext db2 = new Recipe.Models.RecipeContext();

            var model = db2.Md_masterData_Material.ToList();
            if (Helper.Definitions.getFactoryGroup() == 0)
            {
                 model = model.Where(m => m.IsActive == true && m.IsDeleted == false && (m.MaterialGroupId == 1 || m.MaterialGroupId == 2 || m.MaterialGroupId == 3 || m.MaterialGroupId == 4  || m.MaterialGroupId == 6)).OrderBy(a => a.Id).ToList();
                ViewBag.MaterialGroup = new SelectList(db2.Md_material_MaterialGroup.Where(a => (a.Id == 1 || a.Id == 2 || a.Id == 3 || a.Id == 4  || a.Id == 6)), "Id", "Description", ViewBag.MaterialGroupId);
            }
            else if (Helper.Definitions.getFactoryGroup() == 1)
            {
                 model = model.Where(m => m.IsActive == true && m.IsDeleted == false && (m.MaterialGroupId == 1 || m.MaterialGroupId == 2 || m.MaterialGroupId == 7  || m.MaterialGroupId == 8 || m.MaterialGroupId == 6)).OrderBy(a => a.Id).ToList();
                ViewBag.MaterialGroup = new SelectList(db2.Md_material_MaterialGroup.Where(a => (a.Id == 1 || a.Id == 2 || a.Id == 7 || a.Id == 8 || a.Id == 6)), "Id", "Description", ViewBag.MaterialGroupId);
            }
            

            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Name = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => (m.ShortName ?? "").ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ShortName = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.ArticleNumber = pgFF.colVal;
                                break;
                            case "MaterialGroupId":
                                model = model.Where(m => m.MaterialGroupId == Convert.ToInt64(pgFF.colVal)).ToList();
                                ViewBag.MaterialGroupId = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }
            
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }
        public ActionResult checkOutMaterial(long id, string val, string LotNoExtern)
        {
            decimal u = Convert.ToDecimal(val);
            int movingType = u >= 0 ? 0 : 1;

            md_material_Location l = db.Md_material_Location.Find(id);

            md_material_MaterialMoving checkOut = new md_material_MaterialMoving()
            {
                UserId = User.Identity.Name,
                MovingType = movingType,
                LineNo = 0,
                LocationNo = l.Number,
                LocationDescription = l.Description,
                LocationId = l.Id,
                ArticleDescription = l.Md_material_Material.Name,
                ArticleNo = l.Md_material_Material.ArticleNumber,
                RecipeDescription = null,
                RecipeId = 0,
                ScaleNo = null,
                QuantitySetpoint = null,
                QuantityIs = u,
                Unit = "kg",
                OrderId = null,
                OrderNo = null,
                OrderLineNo = null,
                LotNo = null,
                LotNoExtern = LotNoExtern,
                WarrantyDate = DateTime.Now,
                ExpirationDate = DateTime.Now,
                RegistrationDate = DateTime.Now,
                ApprovedTolPlus = null,
                ApprovedTolMinus = null,
                Trailing = null,
                ShiftFine = null,
                StartDate = DateTime.Now,
                StopDate = DateTime.Now,
                FacilitieId = 0,
                FacilitieDescription = null,
                Exported = null
            };
            db.Md_material_MaterialMoving.Add(checkOut);
            db.SaveChanges();
            string sum = db.Md_material_MaterialMoving.Where(a => a.LocationId == id).Select(a => a.QuantityIs).Sum().ToString();
            return Json(new { dateTime = DateTime.Now.ToString("dd:MM:yyyy  HH:mm:ss"), type = movingType, val = u, sum = sum , LotNoExtern = LotNoExtern , UserId = User.Identity.Name });
     
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = db.Md_material_Location.Include(m => m.Md_material_LocationGroup).Include(m => m.Md_material_Material).Where(m => m.IsDeleted == false && m.IsActive == true).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "GroupId":
                                model = model.Where(m => m.Md_material_LocationGroup.Description != null && m.Md_material_LocationGroup.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.GroupId = pgFF.colVal;
                                break;
                            case "MaterialId":
                                model = model.Where(m => m.MaterialId != null && m.Md_material_Material.Name != null && m.Md_material_Material.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.MaterialId = pgFF.colVal;
                                break;
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "GroupId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GroupId).ToList();
                    else
                        model = model.OrderByDescending(m => m.GroupId).ToList();
                    break;
                case "MaterialId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialId).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialId).ToList();
                    break;
                case "FacilityId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FacilityId).ToList();
                    else
                        model = model.OrderByDescending(m => m.FacilityId).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Max":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Max).ToList();
                    else
                        model = model.OrderByDescending(m => m.Max).ToList();
                    break;
                case "Min":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Min).ToList();
                    else
                        model = model.OrderByDescending(m => m.Min).ToList();
                    break;
                case "Value":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Value).ToList();
                    else
                        model = model.OrderByDescending(m => m.Value).ToList();
                    break;
                case "Capacity":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Capacity).ToList();
                    else
                        model = model.OrderByDescending(m => m.Capacity).ToList();
                    break;
                case "Active1":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active1).ToList();
                    else
                        model = model.OrderByDescending(m => m.Active1).ToList();
                    break;
                case "Active2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active2).ToList();
                    else
                        model = model.OrderByDescending(m => m.Active2).ToList();
                    break;
                case "Active3":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Active3).ToList();
                    else
                        model = model.OrderByDescending(m => m.Active3).ToList();
                    break;
            }

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Number != null ? dataset.Number.ToString() : "", dataset.Description, dataset.Md_material_LocationGroup.Description.ToString(), dataset.Md_material_Material !=null ? dataset.Md_material_Material.Name.ToString() : "", dataset.Max.ToString(), dataset.Min.ToString(), dataset.getValue().ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "Lagernummer"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Beschreibung"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Lagergruppe"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Material"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Max"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Min"));
            repParams.Add(new ReportParameter("Hide_05", "false"));
            repParams.Add(new ReportParameter("Header_06", "Bestand"));
            repParams.Add(new ReportParameter("Hide_06", "false"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Lager"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Lagerorte"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();


        }
    }
}
