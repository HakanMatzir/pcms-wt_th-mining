﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PCMS.Models
{
    public class LogContext : DbContext
    {
        public LogContext()
            : base("DefaultConnection")
        {
        }

        //public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<LogWrite> LogWrite { get; set; }

    }
    [Table("Log")]
    public partial class Log
    {
        public long Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        [DisplayName("Browser")]
        public string UserAgent { get; set; }
        [DisplayName("Benutzer")]
        public string UserName { get; set; }
        
        public string IP { get; set; }
        [DisplayName("Zeitpunkt")]
        [Editable(false)]
        public DateTime? Timestamp { get; set; }
      
    }
    [Table("Log")]
    public partial class LogWrite
    {
        public long Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        [DisplayName("Browser")]
        public string UserAgent { get; set; }
        [DisplayName("Benutzer")]
        public string UserName { get; set; }

        public string IP { get; set; }
        //[DisplayName("Zeitpunkt")]
        //[Editable(false)]
        public DateTime? Timestamp { get; set; }

    }

    public class LogModels
    {
    }
}