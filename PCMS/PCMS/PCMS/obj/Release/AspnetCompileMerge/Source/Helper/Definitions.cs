﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    //------!! Finger weg !!
    public static class Definitions
    {
       public static string[] allSieves = { "0,063", "0,125", "0,25", "0,5", "1", "1,4", "2", "2,8", "4", "5,6", "8", "11,2", "16", "22,4", "31,5", "45", "63" };
        public static int pageSize = 100;

        public static int getFactoryGroup()
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            try
            {
                var companytype = db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.CompanyTypeId).FirstOrDefault();
                return companytype;
            }
            catch(Exception e)
            {
                return -1;
            }
        }
        public static long getLoadinOrderNumber()
        {
           
            try
            {
                using (Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext())
                {
                    var companytype = db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.LoadingOrderNumber).FirstOrDefault();
                    return long.Parse(companytype);
                }

            }
            catch (Exception e)
            {
                return -1;
            }
        }
        public static void setLoadinOrderNumber(long number)
        {
            var config = new Areas.LIMS.Models.md_masterData_Config();
            
            try
            {
                config.Id = 1;
                config.LoadingOrderNumber = number.ToString();
                using (Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext())
                {
                    db.Md_masterData_Config.Attach(config);
                    db.Entry(config).Property(x => x.LoadingOrderNumber).IsModified = true;
                    db.SaveChanges();
                }
                
               
            }
            catch (Exception e)
            {
                
            }
        }
        public static string getPriner()
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            return db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.PrintName).FirstOrDefault();
        }
        public static int getCopyCount()
        {
            try
            {
                Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
                return db.Md_masterData_Config.Where(a => a.Id == 1).Select(a => a.Copys).FirstOrDefault();
            }
            catch
            {
                return 1;
            }
            
        }
        public static string setPriner(string printerName)
        {
            Areas.LIMS.Models.LimsContext db = new Areas.LIMS.Models.LimsContext();
            var conf = db.Md_masterData_Config.Where(a => a.Id == 1).FirstOrDefault();
            db.Entry(conf).State = System.Data.Entity.EntityState.Modified;
            conf.PrintName = printerName;
            db.SaveChanges();

            return printerName;

        }
    }
    
}