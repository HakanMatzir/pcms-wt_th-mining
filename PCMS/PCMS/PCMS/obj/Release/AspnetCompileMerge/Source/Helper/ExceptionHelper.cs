﻿using PCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class ExceptionHelper
    {
        public static void LogException(Exception e ,string username)
        {
            //e.InnerException.StackTrace
            ExceptionModel eM = new ExceptionModel();
            eM.UserName = username;
            eM.Message = e.Message;
            if (e.InnerException != null)
            {
                eM.InnerExeptionMessage = e.InnerException.Message ?? "";
                eM.InnerExecptionTargetSiteName = e.InnerException.TargetSite.Name ?? "";
                eM.InnerExeptionStackTrace = e.InnerException.StackTrace ?? "";
            }
            
            eM.Source = e.Source ?? "";
            eM.StackTrace = e.StackTrace ?? "";
           
        
            ExceptionContext db = new ExceptionContext();
            db.ExceptionModel.Add(eM);
            db.SaveChangesAsync();
            
        }
        public static void LogExceptionModel(ExceptionModel eM)
        {
            ExceptionContext db = new ExceptionContext();
            db.ExceptionModel.Add(eM);
            db.SaveChangesAsync();

        }
        public static class ExceptionView
        {
            public static string RenderExceptionView(Exception ex)
            {
                string InnerException = "";
                if (ex != null)
                {
                    InnerException += "<table class=\"table table-condensed table-hover\">";
                    InnerException += "<tbody>";
                    InnerException += "<tr><td colspan=\"2\"><h1 class=\"warning\"><p class=\"text-danger\"><small>Ausnahme vom Typ: " + ex.GetType().ToString() + "</small></p></h1></td></tr>";

                    InnerException += "<tr><td colspan=\"2\"><h4 class=\"warning\"><p class=\"text-danger\">Errormessage</p></h4></td></tr>";
                    InnerException += "<tr><td width=\"100\"></td><td><p class=\"text-primary\">" + ex.Message + "</p></td></tr>";


                    if (ex.GetType() == typeof(System.Data.SqlClient.SqlException))
                    {
                        System.Data.SqlClient.SqlException exTyped = (System.Data.SqlClient.SqlException)ex;

                        InnerException += "<tr><td width=\"100\" colspan=\"2\"><h5 class=\"warning\"><p class=\"text-danger\">Specific information for this Exception-Type</p></h5></td><tr>";
                        InnerException += "<tr><td width=\"100\"></td><td>";
                        InnerException += "<table class=\"table table-condensed table-hover\">";
                        InnerException += "<thead><th>Property</th><th>Value</th></thead>";
                        InnerException += "<tbody>";
                        InnerException += "<tr><td>Exception-Class</td><td>" + exTyped.Class.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Connection-ID</td><td>" + exTyped.ClientConnectionId.ToString() + "</td></tr>";
                        InnerException += "<tr><td>HRESULT</td><td>" + exTyped.ErrorCode.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Helplink</td><td>" + ((exTyped.HelpLink == null) ? "" : exTyped.HelpLink.ToString()) + "</td></tr>";
                        InnerException += "<tr><td>Linenumber.</td><td>" + exTyped.LineNumber.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Errortype</td><td>" + exTyped.Number.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Procedure</td><td>" + ((exTyped.Procedure == null) ? "" : exTyped.Procedure.ToString()) + "</td></tr>";
                        InnerException += "<tr><td>Server</td><td>" + exTyped.Server.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Source</td><td>" + exTyped.Source.ToString() + "</td></tr>";
                        InnerException += "<tr><td>State</td><td>" + exTyped.State.ToString() + "</td></tr>";
                        InnerException += "<tr><td>Target-Site</td><td>" + exTyped.TargetSite.ToString() + "</td></tr>";

                        //if(exTyped.Data != null && exTyped.Data.Count > 0)
                        //{                     
                        //    InnerException += "<tr><td width=\"100\" colspan=\"2\"><h5 class=\"warning\"><p class=\"text-danger\">Data</p></h5></td><tr>";
                        //    InnerException += "<tr><td width=\"100\"></td><td>";
                        //    InnerException += "<table class=\"table table-condensed table-hover\">";
                        //    InnerException += "<thead><th>Property</th><th>Value</th></thead>";
                        //    InnerException += "<tbody>";
                        //
                        //    
                        //    foreach (KeyValuePair<object, object> val in exTyped.Data)
                        //        {
                        //            InnerException += "<tr><td>" + val.Key.ToString() + "</td><td>" + val.Key.ToString() + "</td></tr>";
                        //        }                               
                        //    InnerException += "</tbody>";
                        //    InnerException += "</table>";
                        //    InnerException += "</td></tr>";
                        //}

                        if (exTyped.Errors != null && exTyped.Errors.Count > 1)
                        {
                            InnerException += "<tr><td width=\"100\" colspan=\"2\"><h5 class=\"warning\"><p class=\"text-danger\">Errors</p></h5></td><tr>";
                            InnerException += "<tr><td width=\"100\"></td><td>";
                            InnerException += "<table class=\"table table-condensed table-hover\">";
                            InnerException += "<thead><th>Property</th><th>Value</th></thead>";
                            InnerException += "<tbody>";
                            int iErrorCounter = 2;
                            foreach (System.Data.SqlClient.SqlError err in exTyped.Errors)
                            {
                                InnerException += "<tr><td colspan=\"2\"><b>Error: " + iErrorCounter++.ToString() + "</b></td></tr>";
                                InnerException += "<tr><td>Message</td><td>" + err.Message.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Exception-Class</td><td>" + err.Class.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Linenumber.</td><td>" + err.LineNumber.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Errortype</td><td>" + err.Number.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Procedure</td><td>" + err.Procedure.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Server</td><td>" + err.Server.ToString() + "</td></tr>";
                                InnerException += "<tr><td>Source</td><td>" + err.Source.ToString() + "</td></tr>";
                                InnerException += "<tr><td>State</td><td>" + err.State.ToString() + "</td></tr>";
                            }
                            InnerException += "</tbody>";
                            InnerException += "</table>";
                            InnerException += "</td></tr>";
                        }
                        InnerException += "</tbody>";
                        InnerException += "</table>";
                        InnerException += "</td></tr>";
                    }




                    List<String> lstStackTrace = new List<string>();

                    if (ex.StackTrace != null)
                    {
                        lstStackTrace = ex.StackTrace.Split(new string[] { "bei " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    }

                    if (lstStackTrace.Count > 0)
                    {
                        InnerException += "<tr><td colspan=\"2\"><h4 class=\"warning\"><p class=\"text-danger\">StackTrace</p></h4></td></tr>";
                        InnerException += "<tr><td colspan=\"2\">";
                        InnerException += "<table class=\"table table-condensed table-striped\">";
                        InnerException += "<tbody>";
                        foreach (string stack in lstStackTrace)
                        {
                            if (stack.Contains(":Zeile"))
                            {
                                InnerException += "<tr><td width=\"100\"><p class=\"text-primary\">Zeile " + stack.Split(new string[] { ":Zeile" }, StringSplitOptions.RemoveEmptyEntries)[1] + "</p></td><td><p class=\"text-primary\">" + stack.Split(new string[] { ":Zeile" }, StringSplitOptions.RemoveEmptyEntries)[0].Replace(" in ", "<br>") + "</p></td></tr>";
                            }
                            else
                            {
                                InnerException += "<tr><td width=\"100\"></td><td><p class=\"text-primary\">" + stack + "</p></td></tr>";
                            }
                        }
                        InnerException += "</tbody>";
                        InnerException += "</table>";
                        InnerException += "</td></tr>";
                    }

                    if (ex.InnerException != null)
                    {
                        InnerException += "<tr><td width=\"100\"><h5 class=\"warning\"><p class=\"text-danger\">InnerException</p></h5></td><td>";
                        InnerException += RenderExceptionView(ex.InnerException);
                        InnerException += "</td></tr>";
                    }
                    InnerException += "</tbody>";
                    InnerException += "</table>";
                }
                return InnerException;
            }
        }
    }
}